package com.saperion.sdb.client;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.AttributeSet;
import android.util.Xml;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ShareActionProvider;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.qoppa.android.pdfProcess.PDFDocument;
import com.qoppa.samples.viewer.PDFViewer;
import com.qoppa.samples.viewer.PageContents;
import com.radaee.pdf.Global;
import com.radaee.pdf.Page.Annotation;
import com.radaee.reader.PDFReader;
import com.radaee.reader.PDFReader.PDFReaderListener;
import com.radaee.util.PDFThumbView;
import com.radaee.view.PDFVPage;
import com.radaee.view.PDFViewThumb.PDFThumbListener;
import com.saperion.sdb.client.DocumentInfoFragment.DocumentInfoActivityInterface;
import com.saperion.sdb.client.R.id;
import com.saperion.sdb.client.utils.Log;
import com.saperion.sdb.client.utils.Utils;
import com.saperion.sdb.sdk.SdbApplication;
import com.saperion.sdb.sdk.models.Comment;
import com.saperion.sdb.sdk.models.Document;
import com.saperion.sdb.sdk.models.SupportedRenderFormat;
import com.saperion.sdb.sdk.models.SystemInfo;
import com.saperion.sdb.sdk.models.Version;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.RequestConstants;
import com.saperion.sdb.sdk.requestmanager.RequestManager.RequestListener;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;

import org.xmlpull.v1.XmlPullParser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import cx.hell.android.lib.pagesview.PagesView;
import cx.hell.android.lib.pdf.PDF;
import cx.hell.android.pdfview.Actions;
import cx.hell.android.pdfview.Options;
import cx.hell.android.pdfview.PDFPagesProvider;

/**
 * Created by nnn on 31.07.13.
 */
public class DocumentViewerActivity extends Activity implements RequestListener, DocumentInfoListViewInterface, SensorEventListener, OnClickListener, OnGestureListener {

    private static final String TAG = DocumentViewerActivity.class.getSimpleName();
    private Context mContext;

    private RequestListener requestListener = null;
    private Document document = null;
    private int currViewedVersion = 1;
    private ProgressDialog progressDialog = null;
    private RelativeLayout viewerContainer = null;
    private RelativeLayout thumbContainer = null;
    private LinearLayout viewerToolBar = null;
    
    
    private ShareActionProvider mShareActionProvider;
    private boolean hasToShare = false;


    public enum PDF_VIEWER_TYPE {
        RADEE,
        QOPPA,
        APV,
        WEBVIEW;
    }

    private String currPDFViewerType = PDF_VIEWER_TYPE.APV.toString();
    
    // Viewer Controller

    int currPageNumber = 1;
    int currZoomFactor = 1;

    
    private ImageButton buttonShowOrHideThumbnail;
    private ImageButton buttonviewfull;
    private ImageButton buttonSetCurrentVersion;
    private ImageButton buttonDownLoadFileVersion;
    private ImageButton buttonPdfDownLoadVersion;
    private ImageButton buttonChooseViewer;
    
    private ImageButton buttonZoomIn;
    private ImageButton buttonZoomOut;

    private ImageButton buttonPageUp;
    private ImageButton buttonPageDown;
    protected EditText editTextcurrPage ;
    private TextView pageNumber ;

    private boolean hasRenditionAsPdf = false;

    
    // APV Reader  
    private PDF pdf = null;
    private PagesView apvPagesView = null;
    private PDFPagesProvider pdfPagesProvider = null;
    private int box = 2;
    private int colorMode = Options.COLOR_MODE_NORMAL;
    private Actions actions = null;
    private SensorManager sensorManager = null;
    private float[] gravity = {0f, -9.81f, 0f};
    private long gravityAge = 0;
    private int prevOrientation;
    private boolean history = true;
    
    // Radee Pdf Reader variables
    private PDFReader radeePdfReader = null;
    private PDFThumbView m_thumb = null;
    private com.radaee.pdf.Document pdfDoc = new com.radaee.pdf.Document();
    private PDFReaderListener radeeReaderlistener = null;
    private PDFThumbListener radeeThumblistener = null;
    
    // QPdf Reader variables
    private PDFViewer qPDFViewer = null;

    // Info Viewer
    private Button buttonComments;
    private Button buttonVersion;
    private Button buttonActivity;
    private ArrayList<Comment> docCommentList = new ArrayList<Comment>();
    private ArrayList<Version> docVersionList = new ArrayList<Version>();
    private ArrayList<com.saperion.sdb.sdk.models.Activity> docActivitiesList = new ArrayList<com.saperion.sdb.sdk.models.Activity>();
    private DocumentVersionListAdapter versionListAdapter;
    private DocumentActivityListAdapter activityListAdapter;
    private DocumentCommentListAdapter commentListAdapter;
    private ListView mListView;
    private int curredVersion;
    private String currentListType = LIST_TYPE.VERSION.toString();
    public enum LIST_TYPE {
        COMMENT,
        VERSION,
        ACTIVITY;
    }
    private Drawable btn_not_selected;
    private Drawable btn_selected;
    private DocumentInfoActivityInterface activityCallbacks = sDummyCallbacks;
    private RequestListener mListener;
    
    
    // ViewFlipper
    protected GestureDetector gestureScanner;
    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;

    private Button next,previous;

    private ViewFlipper vf;
    private Animation animFlipInNext,animFlipOutNext, animFlipInPrevious, animFlipOutPrevious;
    
    // NavigationDrawer
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    
    public DocumentViewerActivity() {
    }

    public String getCurrPDFViewerType() {
        return currPDFViewerType;
    }

    public void setCurrPDFViewerType(String currPDFViewerType) {
        this.currPDFViewerType = currPDFViewerType;
    }
    


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.v(TAG, Utils.getMethodName() + "orientation ch  = " + newConfig.orientation);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

        }

        if (currPDFViewerType.equals(PDF_VIEWER_TYPE.APV.toString())) {
            qPDFViewer.onConfigurationChanged(newConfig);
        }

        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v(TAG, Utils.getMethodName() + "entry");

        setContentView(R.layout.document_viewer_activity);
        Global.Init(this);
        requestListener = this;
        mContext = this;
        mListener = this;
        Bundle bundle = getIntent().getExtras();

        if (bundle == null) {
            Log.e(TAG, Utils.getMethodName() + "bundle = null ");
            return;
        }
        this.document = bundle.getParcelable(DocumentFragmentActivity.PARAM_DOCUMENT);
        curredVersion = document.getVersionNumber();


        // init viewFlipper
        gestureScanner = new GestureDetector(this);

        //vf for viewflipper
        vf=(ViewFlipper)findViewById(R.id.ViewFlipper01);
        animFlipInNext = AnimationUtils.loadAnimation(this, R.anim.flipinnext);
        animFlipOutNext = AnimationUtils.loadAnimation(this, R.anim.flipoutnext);
        animFlipInPrevious = AnimationUtils.loadAnimation(this, R.anim.flipinprevious);
        animFlipOutPrevious = AnimationUtils.loadAnimation(this, R.anim.flipoutprevious);


        next = (Button) findViewById(R.id.b_slide_out);
        next.setText(">>");
        
        previous = (Button) findViewById(id.b_slide_in);
        previous.setText("<<");
        next.setOnClickListener(this);
        previous.setOnClickListener(this);        
        mListView = (ListView) findViewById(id.document_info_list_view);
        

        if (mListView == null)
            return;

        btn_selected = getResources().getDrawable(R.drawable.fns_orange_shape_selected);
        btn_not_selected = getResources().getDrawable(R.drawable.fns_btn_not_selected);

        activityListAdapter = new DocumentActivityListAdapter(mContext, this, docActivitiesList);
        versionListAdapter = new DocumentVersionListAdapter(mContext, this, docVersionList);
        commentListAdapter = new DocumentCommentListAdapter(mContext, this, docCommentList);

        mListView.setAdapter(versionListAdapter);

        mListView.setAnimationCacheEnabled(false);
        mListView.setScrollingCacheEnabled(false);
        mListView.setClickable(false);
        mListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {

                if ( currentListType.equals(LIST_TYPE.VERSION.toString())){
                    Version selectedVersion = versionListAdapter.getItem(position);

                    activityCallbacks.onVersionClicked(selectedVersion);

                }

            }
        });

        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> av, View v, int position, long id) {
                if ( currentListType.equals(LIST_TYPE.COMMENT.toString())){
                    Comment comment = commentListAdapter.getItem(position);
                    showContextMenu(v,comment);
                }

                return true;
            }
        });
        
        
        // build NavigationDrawer
        mTitle = mDrawerTitle = getTitle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        mDrawerList.setAdapter(versionListAdapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // enable ActionBar app icon to behave as action to toggle nav drawer
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setTitle(document.getName());
        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        buttonVersion = (Button) findViewById(id.b_doc_version);
        buttonVersion.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.v(TAG, Utils.getMethodName() + "buttonVersion clicked ");
                setButtonsBackGroundColorToDefault();
                setButtonSelected(buttonVersion);
                mListView.setClickable(true);
                // set new adapterEmailList
                mListView.setAdapter(versionListAdapter);
                currentListType = LIST_TYPE.VERSION.toString();
                if (document != null) {
                    Log.v(TAG, Utils.getMethodName() + "request get version list of " + document.getName());

                    com.saperion.sdb.client.utils.Utils.showLoadingDialog(progressDialog,v.getContext());
                    document.getVersions(mListener);
                }
            }
        });
         
        buttonComments = (Button) findViewById(id.b_doc_comment);

        buttonComments.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.v(TAG, Utils.getMethodName() + "buttonComments clicked ");
                setButtonsBackGroundColorToDefault();
                setButtonSelected(buttonComments);
                mListView.setClickable(true);
                // set new adapterEmailList
                mListView.setAdapter(commentListAdapter);
                currentListType = LIST_TYPE.COMMENT.toString();

                if (document != null) {
                    Log.v(TAG, Utils.getMethodName() + "request get comments list of " + document.getName());

                    com.saperion.sdb.client.utils.Utils.showLoadingDialog(progressDialog,v.getContext());
                    document.getComments(mListener);
                }
            }
        });

        buttonActivity = (Button) findViewById(id.b_doc_activity);
        buttonActivity.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.v(TAG, Utils.getMethodName() + "buttonActivity clicked ");
                setButtonsBackGroundColorToDefault();
                setButtonSelected(buttonActivity);
                mListView.setClickable(false);
                // set new adapterEmailList
                mListView.setAdapter(activityListAdapter);
                currentListType = LIST_TYPE.ACTIVITY.toString();
                if (document != null) {
                    Log.v(TAG, Utils.getMethodName() + "request get activitiy list of " + document.getName());

                    com.saperion.sdb.client.utils.Utils.showLoadingDialog(progressDialog,v.getContext());
                    document.getActivities(mListener);
                }
            }
        });

        viewerToolBar = (LinearLayout) findViewById(id.viewerToolBar);
        
        viewerContainer = (RelativeLayout) findViewById(id.viewerContainer);
        thumbContainer  = (RelativeLayout) findViewById(id.thumbContainer);
        thumbContainer.setVisibility(View.GONE);
        
        pageNumber = (TextView) findViewById(id.tv_pages_nr);

        buttonShowOrHideThumbnail = (ImageButton) findViewById(id.b_show_thumbnail);
        buttonShowOrHideThumbnail.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "buttonShowOrHideThumbnail clicked ");
                if (document != null) {
                    showhideThumbnail();
                }
            }
        });
        buttonviewfull = (ImageButton) findViewById(id.b_doc_view_full);
        buttonviewfull.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "buttonviewfull clicked ");
 
                if (document != null) {
                    Uri fileUri = document.getFileContentUri(mContext);
                    Log.v(TAG, Utils.getMethodName() + "opening " + fileUri.toString());

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(fileUri, document.getMimeType());
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    try {
                        startActivity(intent);
                    } catch (Exception e) {
                        com.saperion.sdb.client.utils.Utils.showErrorDialog(mContext, "Could not show file");
                        Log.e("error", "" + e);
                    }

                }
                
            }
        });

        buttonChooseViewer = (ImageButton) findViewById(id.b_choose_viewer);
        buttonChooseViewer.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "buttonChooseViewer clicked ");

              
                showChoosePdfContextMenu(v);
            }
        });
        
        buttonSetCurrentVersion = (ImageButton) findViewById(id.b_set_as_current_version);
        buttonSetCurrentVersion.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "buttonSetCurrentVersion clicked ");
                if (document != null) {
                    com.saperion.sdb.client.utils.Utils.showLoadingDialog(progressDialog,v.getContext());
                    document.restoreVersion(requestListener, currViewedVersion);
                }

            }
        });

        buttonDownLoadFileVersion= (ImageButton) findViewById(id.b_invite_user);
        buttonDownLoadFileVersion.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "buttonDownLoadFileVersion clicked ");
                if (document != null) {
                    com.saperion.sdb.client.utils.Utils.showLoadingDialog(progressDialog,v.getContext());
                    document.downloadFileByVersion(requestListener, currViewedVersion);
                }

            }
        });

        buttonPdfDownLoadVersion = (ImageButton) findViewById(id.b_pdf_download);
        buttonPdfDownLoadVersion.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "buttonPdfDownLoadVersion clicked ");
                if (document != null) {
                    File rendition = new File(document.getRenditionAbsPath());
                    
                    String destFile  = Environment.getDownloadCacheDirectory().getPath() + "version_"+curredVersion+"_"+document.getName();
                    File destPDf = new File(destFile);
                    try {
                        Utils.fileCopy(rendition, destPDf);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(mContext, destFile, Toast.LENGTH_SHORT).show();
                    
                    //com.saperion.sdb.client.utils.Utils.showLoadingDialog(progressDialog,v.getContext());
                    //document.downloadPdfbyVersion(requestListener, currViewedVersion);
                }

            }
        });
        editTextcurrPage = (EditText) findViewById(id.et_curr_page_nr);


        buttonPageUp = (ImageButton) findViewById(id.b_page_up);
        buttonPageUp.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "buttonPageUp clicked ");

                if (document != null && pdfDoc !=null) {
                        pageUp();
                }

            }
        });

        buttonPageDown = (ImageButton) findViewById(id.b_page_down);
        buttonPageDown.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "buttonPageDown clicked ");
                if (document != null && pdfDoc !=null) {
                    pageDown();
                }

            }
        });

        buttonZoomIn = (ImageButton) findViewById(id.b_zoom_in);
        buttonZoomIn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "buttonZoomIn clicked ");
                if (document != null) {
                    zoomIn();
                }

            }
        });

        buttonZoomOut = (ImageButton) findViewById(id.b_zoom_out);
        buttonZoomOut.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "buttonZoomOut clicked ");
                if (document != null) {
                    zoomOut();
                }

            }
        });

        if (currPDFViewerType.equals(PDF_VIEWER_TYPE.QOPPA.toString())){
            buildQPdfViewer();
        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.APV.toString())){
            buildAPVViewer();
        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.WEBVIEW.toString())){

        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.RADEE.toString())){
            buildRadeePdfViewer();
        }
        currentListType = LIST_TYPE.VERSION.toString();
        document.getVersions(this);
        getPdfOrFiletoView(document);

        ActionBar ab = getActionBar();
        ab.setDisplayShowHomeEnabled(true);
        ab.setHomeButtonEnabled(true);
        ab.setDisplayUseLogoEnabled(false);

    }

    
    // NavigationDrawer's methods

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.doc_viewer_actionbar, menu);

        // Locate MenuItem with ShareActionProvider
        MenuItem item = menu.findItem(R.id.menu_item_share);
        
        // Fetch and store ShareActionProvider
        mShareActionProvider = (ShareActionProvider) item.getActionProvider();
/*
        if (mShareActionProvider != null) {
            mShareActionProvider.setShareHistoryFileName(ShareActionProvider.DEFAULT_SHARE_HISTORY_FILE_NAME);
            mShareActionProvider.onPerformDefaultAction();
            mShareActionProvider.setShareIntent(createShareIntent());
        }
       */ 
        return super.onCreateOptionsMenu(menu);
    }

    /** Returns a share intent */
    private Intent getDefaultShareIntent(){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "SUBJECT");
        intent.putExtra(Intent.EXTRA_TEXT,"Extra Text");
        return intent;
    }

    /**
     * Creates a sharing {@link Intent}.
     *
     * @return The sharing intent.
     */
    private Intent createShareIntent() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
       
        Log.v(TAG, Utils.getMethodName() + "getRenditionAbsPath/getMimeType " +document.getRenditionAbsPath() + "  "+  document.getMimeType());
        shareIntent.setType("*/*");
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, document.getName());
        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(document.getRenditionAbsPath()));
        return shareIntent;
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        menu.findItem(id.menu_item_print).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }



        String filePath = "";
        
        // Handle action buttons
        switch(item.getItemId()) {
            case id.menu_item_print:


                if (   document.getMimeType().equals("application/pdf") )
                    filePath =   document.getRenditionAbsPath();
                else if  (   document.getMimeType().contains("image") )
                    filePath =   document.getFileAbsPath() ;
                
                Log.v(TAG, Utils.getMethodName() + "action_print " );

                
                if (filePath != null && filePath.length() > 0 )
                {
                    Intent printIntent = new Intent(this, PrintDialogActivity.class);

                    Log.v(TAG, Utils.getMethodName() + "get rendition " +document.getRenditionAbsPath() + " mine " + document.getMimeType());

                    // File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/test_file.pdf");
                    // printIntent.setDataAndType(Uri.fromFile(file), "application/pdf");
                    printIntent.setDataAndType(Uri.fromFile(new File(filePath)), document.getMimeType());
                    printIntent.putExtra("title", document.getName());
                    startActivity(printIntent);
                }
                
                return true;
            /*
            case id.menu_item_share:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "Check it out";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,"Subject");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                return true;
            */
            case id.menu_item_share:
                hasToShare = true;
                com.saperion.sdb.client.utils.Utils.showLoadingDialog(progressDialog,this);
                document.downloadFileByVersion(requestListener, currViewedVersion);
                
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    
    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectVersionItem(position);
        }
    }

    private void selectVersionItem(int position) {

        currViewedVersion = docVersionList.get(position).getNumber();
        
        Log.v(TAG, Utils.getMethodName() + "get file version " + currViewedVersion);
        
        document.getRenditionVersioned(this,currViewedVersion);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    
    
    
    private void setButtonsBackGroundColorToDefault(){

        //Button buttonComments = (Button) this.findViewById(id.b_doc_comment);
        //Button buttonVersion = (Button) this.findViewById(id.b_doc_version);
        // Button buttonActivity = (Button) this.findViewById(id.b_doc_activity);

        if (buttonVersion != null){
            buttonVersion.setBackgroundDrawable(btn_not_selected);
            buttonVersion.setTextColor(Color.BLACK);
        }
        if (buttonActivity != null) {
            buttonActivity.setBackgroundDrawable(btn_not_selected);
            buttonActivity.setTextColor(Color.BLACK);
        }
        if (buttonComments != null){
            buttonComments.setBackgroundDrawable(btn_not_selected);
            buttonComments.setTextColor(Color.BLACK);
        }
    }
    
    private void setButtonSelected(Button button){
        button.setBackgroundDrawable(btn_selected);
        button.setTextColor(Color.WHITE);
    }

    
    private void setLayoutVisible(LinearLayout layout) {
        //set nameExpandableLayout Visible
        layout.setVisibility(View.VISIBLE);

        final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        layout.measure(widthSpec, heightSpec);

        ValueAnimator mAnimator = slideAnimator(layout, 0, layout.getMeasuredHeight());
        mAnimator.start();

    }
    private void setLayoutGone(final LinearLayout layout) {
        int finalHeight = layout.getHeight();

        ValueAnimator mAnimator = slideAnimator(layout,finalHeight, 0);

        mAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationEnd(Animator animator) {
                //Height=0, but it set visibility to GONE
                layout.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });
        mAnimator.start();
    }
    
    private ValueAnimator slideAnimator(final LinearLayout layout, int start, int end) {

        ValueAnimator animator = ValueAnimator.ofInt(start, end);


        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                //Update Height
                int value = (Integer) valueAnimator.getAnimatedValue();

                ViewGroup.LayoutParams layoutParams = layout.getLayoutParams();
                layoutParams.height = value;
                layout.setLayoutParams(layoutParams);
            }
        });
        return animator;
    }
    
    private void showhideThumbnail(){

        if (currPDFViewerType.equals(PDF_VIEWER_TYPE.QOPPA.toString())){

        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.APV.toString())){

        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.WEBVIEW.toString())){

        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.RADEE.toString())){

            if (m_thumb != null ){
                m_thumb.thumbGotoPage(currPageNumber);
                m_thumb.thumbUpdatePage(currPageNumber);

                if (thumbContainer.getVisibility() == View.VISIBLE)
                {
                    thumbContainer.setVisibility( View.GONE);
                }
                else
                    thumbContainer.setVisibility( View.VISIBLE);
            }

        }

    }
    



    private void zoomIn(){
        currZoomFactor = currZoomFactor + 1;
        if (currPDFViewerType.equals(PDF_VIEWER_TYPE.QOPPA.toString())){
            qPDFViewer.setScale(currZoomFactor,null);
        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.APV.toString())){

            apvPagesView.doAction(actions
                    .getAction(Actions.ZOOM_OUT));
        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.WEBVIEW.toString())){

        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.RADEE.toString())){
          
            radeePdfReader.PDFSetScale(currZoomFactor);
            
        }

    }

    private void zoomOut(){

        if (currZoomFactor > 1)
        currZoomFactor = currZoomFactor - 1;
        if (currPDFViewerType.equals(PDF_VIEWER_TYPE.QOPPA.toString())){
  
            qPDFViewer.setScale(currZoomFactor,null);
        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.APV.toString())){
            apvPagesView.doAction(actions
                    .getAction(Actions.ZOOM_IN));

        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.WEBVIEW.toString())){

        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.RADEE.toString())){
  
            radeePdfReader.PDFSetScale(currZoomFactor);

        }

    }

    private void pageUp(){

        if (currPDFViewerType.equals(PDF_VIEWER_TYPE.QOPPA.toString())){

            PDFDocument qPDFDocument =  qPDFViewer.getDocument();
            if (qPDFDocument != null)
                pageNumber.setText(String.valueOf(qPDFViewer.getDocument().getPageCount()));

            if (currPageNumber < qPDFViewer.getDocument().getPageCount()) {
                currPageNumber = currPageNumber + 1;

                PageContents pc  = qPDFViewer.getCachcedPage(currPageNumber);
                qPDFViewer.cachePage(currPageNumber, pc);
        
            }
        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.APV.toString())){
            if (apvPagesView != null) {

                if (currPageNumber < apvPagesView.getPageCount() ) {
                    currPageNumber = currPageNumber + 1;
                    apvPagesView.scrollToPage(currPageNumber);
                }
            }
        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.WEBVIEW.toString())){

        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.RADEE.toString())){
            if (currPageNumber < pdfDoc.GetPageCount()) {
                currPageNumber = currPageNumber + 1;

                radeePdfReader.PDFGotoPage(currPageNumber);
                if (m_thumb != null){
                    m_thumb.thumbGotoPage(currPageNumber - 1);
                    // m_thumb.thumbUpdatePage(currPageNumber);
                }
            }
        }

        editTextcurrPage.setText(String.valueOf(currPageNumber));
    }
    private void pageDown(){

        if (currPDFViewerType.equals(PDF_VIEWER_TYPE.QOPPA.toString())){

            PDFDocument qPDFDocument =  qPDFViewer.getDocument();
            if (qPDFDocument != null)
                pageNumber.setText(String.valueOf(qPDFViewer.getDocument().getPageCount()));
            
            if (currPageNumber > 1) {
                currPageNumber = currPageNumber - 1;

                PageContents pc  = qPDFViewer.getCachcedPage(currPageNumber);
                qPDFViewer.cachePage(currPageNumber, pc);

            }
        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.APV.toString())){
            if (apvPagesView != null) {
                if (currPageNumber > 1) {
                    currPageNumber = currPageNumber - 1;
                    apvPagesView.scrollToPage(currPageNumber);
                }
                   
            }
        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.WEBVIEW.toString())){

        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.RADEE.toString())){
            if (currPageNumber > 1) {
                currPageNumber = currPageNumber - 1;

                radeePdfReader.PDFGotoPage(currPageNumber);
                if (m_thumb != null){
                    m_thumb.thumbGotoPage(currPageNumber - 1);
                   //  m_thumb.thumbUpdatePage(currPageNumber);
                }
            }
        }

        editTextcurrPage.setText(String.valueOf(currPageNumber));

    }
    
    private void getPdfOrFiletoView(Document document) {

        if (document != null) {

            currPageNumber = 1;
            editTextcurrPage.setText(String.valueOf(currPageNumber));
            
            String mine_type = document.getMimeType();
            Log.v(TAG, Utils.getMethodName() + "currViewedVersion " + currViewedVersion
                    + " mine type= " + mine_type );

            com.saperion.sdb.client.utils.Utils.showLoadingDialog(progressDialog,this);

            if (mine_type.contains("image")) {
                Log.v(TAG, Utils.getMethodName() + " getting file ver." + currViewedVersion);
                document.getFileByVersion(this, currViewedVersion);
            } else {
                SystemInfo systemInfo = SdbApplication.getInstance().getSystemInfos();
                SupportedRenderFormat[] renderFormats = systemInfo.getSupportedRenderFormats();
                for (int i =0; i < renderFormats.length;i++) {
                    if (renderFormats[i].getMimetypes().equals(mine_type)) {
                        hasRenditionAsPdf = true;
                        break;
                    }
                }
                if (mine_type.contains("pdf"))
                    hasRenditionAsPdf = true;
                
                if (hasRenditionAsPdf) {
                    Log.v(TAG, Utils.getMethodName() + " getting rendition of version = " + currViewedVersion);
                    document.getRenditionVersioned(this, currViewedVersion);
                } else {
                    showDefaultFileNameAndIcon();
                }
            }
   

        }
    }
    
    // build radee pdf viewer
    private void buildRadeePdfViewer(){

        radeeThumblistener = new PDFThumbListener() {
            @Override
            public void OnPageClicked(int i) {
                Log.v(TAG, Utils.getMethodName() + "page clicked " + i );
                currPageNumber = i + 1;
                radeePdfReader.PDFGotoPage(currPageNumber);
                editTextcurrPage.setText(String.valueOf(currPageNumber));
            }
        };
                
        radeeReaderlistener = new PDFReaderListener() {

            @Override
            public void OnPageModified(int pageno) {
                Log.v(TAG, Utils.getMethodName() + "pageno " + pageno);
                currPageNumber = pageno;
                radeePdfReader.PDFGotoPage(currPageNumber);
            }

            @Override
            public void OnPageChanged(int pageno) {

            }

            @Override
            public void OnAnnotClicked(PDFVPage vpage, Annotation annot) {

            }

            @Override
            public void OnSelectEnd(String text) {

            }

            @Override
            public void OnOpenURI(String uri) {

            }

            @Override
            public void OnOpenMovie(String path) {

            }

            @Override
            public void OnOpenSound(int[] paras, String path) {

            }

            @Override
            public void OnOpenAttachment(String path) {

            }

            @Override
            public void OnOpen3D(String path) {

            }
        };
        
        thumbContainer.removeAllViews();
        viewerContainer.removeAllViews();

        XmlPullParser parser = getResources().getXml(R.xml.radee_pdf_reader_attrs);
        AttributeSet attributes = Xml.asAttributeSet(parser);
        radeePdfReader = new PDFReader(mContext,attributes);
        viewerContainer.addView(radeePdfReader);

        m_thumb =  new PDFThumbView(mContext,attributes);
        thumbContainer.setVisibility(View.VISIBLE);
        thumbContainer.addView(m_thumb);

    }

        // build qPdf  viewer
    private void buildQPdfViewer(){
        thumbContainer.setVisibility(View.GONE);
        thumbContainer.removeAllViews();
        viewerContainer.removeAllViews();
        qPDFViewer = new PDFViewer(this,viewerContainer);

    }

    // build APV Pdf  viewer
    private void buildAPVViewer(){

        thumbContainer.setVisibility(View.GONE);
        thumbContainer.removeAllViews();
        viewerContainer.removeAllViews();

        sensorManager = null;

        SharedPreferences options = PreferenceManager.getDefaultSharedPreferences(this);

        if (Options.setOrientation(this)) {
            sensorManager = (SensorManager) this.getSystemService(this.SENSOR_SERVICE);
            if (sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER).size() > 0) {
                gravity[0] = 0f;
                gravity[1] = -9.81f;
                gravity[2] = 0f;
                gravityAge = 0;
                sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                        SensorManager.SENSOR_DELAY_NORMAL);
                this.prevOrientation = options.getInt(Options.PREF_PREV_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                this.setRequestedOrientation(this.prevOrientation);
            }
            else {
                this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
        }

        history  = options.getBoolean(Options.PREF_HISTORY, true);
        boolean eink = options.getBoolean(Options.PREF_EINK, false);

        this.apvPagesView = new PagesView(this);
        
        this.apvPagesView.setEink(eink);
        if (eink)
            this.setTheme(android.R.style.Theme_Light);
        this.apvPagesView.setNook2(options.getBoolean(Options.PREF_NOOK2, false));

        if (options.getBoolean(Options.PREF_KEEP_ON, false))
            this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        else
            this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        actions = new Actions(options);
        this.apvPagesView.setActions(actions);

        // setZoomLayout(options);
        
        this.apvPagesView = new PagesView(this);
        viewerContainer.addView(apvPagesView);

    }


    private void showPdfFileInWebView(File file) {
        Log.v(TAG, Utils.getMethodName() + "entry");

            // Log.d("SwA", "Current URL  1["+currentURL+"]");

            WebView webView = (WebView) this.findViewById(id.webPage);

            // Aktiviere Javaskript
            webView.getSettings().setJavaScriptEnabled(true);

            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setUseWideViewPort(true);

            //Erlaube das heranzoomen
            webView.getSettings().setBuiltInZoomControls(true);
            webView.getSettings().setSupportZoom(true);

            //Schalte das Scrollbarbalken ab
            webView.setVerticalScrollBarEnabled(false);

            webView.getSettings().setLoadWithOverviewMode(true);
            // this call require API 16
            //  webView.getSettings().setAllowFileAccessFromFileURLs(true);
            webView.getSettings().setAllowFileAccess(true);
            webView.getSettings().setGeolocationEnabled(true);
            webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

            // Setze Webviewclient
            webView.setWebViewClient(new WebViewClient());
            webView.freeMemory();
            //webView.setWebChromeClient(new WebChromeClient());
            // webView.setDownloadListener(this);


            // SwAWebClient webClient = new SwAWebClient();
            /*
            wv.setWebViewClient(new WebViewClient() {
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    Toast.makeText(this, description + " (" + errorCode + ") " + failingUrl, Toast.LENGTH_SHORT).show();
                    Log.e("SwA", description + " (" + errorCode + ") " + failingUrl);
                }
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    Toast.makeText(this, url, Toast.LENGTH_SHORT).show();
                    return super.shouldOverrideUrlLoading(view, url);
                }
            });
            */

            // wv.setWebViewClient(new SwAWebClient());


            //File lFile = new File(Environment.getExternalStorageDirectory() + "/web/viewer.html");


            //File lFile = new File(Environment.getExternalStorageDirectory() + "/pdfjs/web/pdfviewer.html");
            //File lFile = new File(Environment.getExternalStorageDirectory() + "/pdfjs/examples/helloworld/index.html");
            File lFile = new File(Environment.getExternalStorageDirectory() + "/pdfjs/examples/acroforms/index.html");
            android.util.Log.e("SwA", "Current URL [" + lFile.getAbsolutePath() + "]");
            webView.loadUrl("file://" + lFile.getAbsolutePath());

            // webView.loadUrl("https://docs.google.com/gview?embedded=true&url="+"file://"+lFile.getAbsolutePath());
        

    }
    

    @Override
    public void onRequestFinished(Request request, Bundle resultData) {

        if (this == null)
            return;

        int result_code = resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);
        int requestType = request.getRequestType();
        String errorMessage = resultData.getString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE);

        com.saperion.sdb.client.utils.Utils.cancelLoadingDialog(progressDialog);

        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " resultCode = "
                + result_code);

        switch (result_code) {

            case ResponseConstants.RESPONSE_CODE_OK:
            case ResponseConstants.RESPONSE_CODE_CREATED:
                break;
            case ResponseConstants.RESPONSE_CODE_UNKOWN_ERROR:
            case ResponseConstants.RESPONSE_CODE_BAD_REQUEST:
            case ResponseConstants.RESPONSE_CODE_ERROR:
            case ResponseConstants.RESPONSE_CODE_FORBIDDEN:
            case ResponseConstants.RESPONSE_CODE_INTERNAL_SERVER_ERROR:
            case ResponseConstants.RESPONSE_CODE_INTERNAL_SERVICE_UNAVAILABLE:
            case ResponseConstants.RESPONSE_CODE_METHOD_NOT_ALLOWED:
            case ResponseConstants.RESPONSE_CODE_NOT_FOUND:
            case ResponseConstants.RESPONSE_CODE_UNAUTHORIZED:
            case ResponseConstants.RESPONSE_CODE_PRECONDITION_FAILED:
                if (errorMessage != null )
                    com.saperion.sdb.client.utils.Utils.showErrorDialog(mContext, errorMessage);
                return;
            default: {
                com.saperion.sdb.client.utils.Utils.showErrorDialog(mContext, "unbekannter Fehler");
                return;
            }
        }

        switch (requestType) {
            case RequestConstants.REQUEST_TYPE_COMMENT_ADD:
            case RequestConstants.REQUEST_TYPE_COMMENT_DELETE:
            case RequestConstants.REQUEST_TYPE_COMMENT_UPDATE:
            case RequestConstants.REQUEST_TYPE_DOCUMENT_ADD_COMMENT:
                Log.d(TAG, com.saperion.sdb.sdk.utils.Utils.getMethodName() + " REQUEST_TYPE_DOCUMENT_ADD_COMMENT = ");

                commentListAdapter.setCurrEditedComment(null);
                if (currentListType.equals(LIST_TYPE.COMMENT.toString())){
                    Utils.showLoadingDialog(this);
                    // send new request to refresh comment list
                    document.getComments(this);
                }

                break;

            case RequestConstants.REQUEST_TYPE_DOCUMENT_GET_RENDITION_BY_VERSION:

                String renditionPath = resultData.getString(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                if (renditionPath == null) return;
                File renditionFile = new File(renditionPath);
                document.setRenditionAbsPath(renditionPath);

                if (currPDFViewerType.equals(PDF_VIEWER_TYPE.QOPPA.toString())){
                    Log.d(TAG, Utils.getMethodName() + "loading " + renditionFile.toURI().getPath());
                    //intent refers to a local file
                    qPDFViewer.loadDocument(renditionFile.toURI().getPath());
                    PDFDocument qPDFDocument =  qPDFViewer.getDocument();
                    if (qPDFDocument != null)
                        pageNumber.setText(String.valueOf(qPDFViewer.getDocument().getPageCount()));


                } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.APV.toString())){

                    Options.setOrientation(this);
                    SharedPreferences options = PreferenceManager.getDefaultSharedPreferences(mContext);

                    this.pdf = new PDF(new File(renditionPath), this.box);
                    this.pdf.setApplicationContext(this.getApplicationContext());

                    if (!this.pdf.isValid()) {
                        Log.v(TAG, Utils.getMethodName() + "Invalid PDF");
                        if (this.pdf.isInvalidPassword()) {
                            Toast.makeText(mContext, "This file needs a password", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(mContext, "Invalid PDF file", Toast.LENGTH_SHORT).show();
                        }
                        return;
                    }
                    this.colorMode = Options.getColorMode(options);
                    this.pdfPagesProvider =
                            new PDFPagesProvider(this, pdf,
                                    options.getBoolean(Options.PREF_OMIT_IMAGES, false), options.getBoolean(
                                    Options.PREF_RENDER_AHEAD, true));
                    apvPagesView.setFocusable(true);
                    apvPagesView.setFocusableInTouchMode(true);
                    apvPagesView.setPagesProvider(pdfPagesProvider);
                    LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
                    lp.setMargins(5,5,5,5);
                    apvPagesView.setLayoutParams(lp);

                    //Bookmark b = new Bookmark(this.getApplicationContext()).open();
                    //apvPagesView.setStartBookmark(b, filePath);
                    //b.close();

                    pageNumber.setText( "/" + this.pdfPagesProvider.getPageCount());

              
                } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.RADEE.toString())){
                    // RadeePdfReader
                    pdfDoc = new com.radaee.pdf.Document();

                    int res =  pdfDoc.Open( renditionFile.getAbsolutePath(), null );
                    Log.d(TAG, Utils.getMethodName() + "doc " + document.getName()
                            + " renditionFile = " + renditionFile.getAbsolutePath() + " result " +res );
                    if (res < 0)
                    {
                        com.saperion.sdb.client.utils.Utils.showErrorDialog(mContext, "Couldn't read rendition " + document.getName());
                        break;
                    }
                    pdfDoc.SetCache( Global.tmp_path + "/temp.dat" );//set temporary cache for editing.

                    radeePdfReader.PDFOpen(pdfDoc, false, radeeReaderlistener);
                    pageNumber.setText(String.valueOf("/" + String.valueOf(pdfDoc.GetPageCount())));
                    if (m_thumb != null) {
                        m_thumb.thumbOpen(pdfDoc, radeeThumblistener);
                        m_thumb.thumbGotoPage(currPageNumber - 1);
                    }
                    
      

                }         
                else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.WEBVIEW.toString())){
                    File scrFile = new File(renditionPath);
                    File dstFile = new File(Environment.getExternalStorageDirectory() + "/pdfjs/examples/acroforms/rendition.pdf");

                    // delete it before copy
                    dstFile.delete();

                    try {
                        Utils.fileCopy(scrFile, dstFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    showPdfFileInWebView(dstFile);
                }
            break;

            case RequestConstants.REQUEST_TYPE_DOCUMENT_DOWNLOAD_RENDITION_BY_VERSION:
                Options.setOrientation(this);

    
                break;
            
            case RequestConstants.REQUEST_TYPE_DOCUMENT_DOWNLOAD_FILE_BY_VERSION:
                Options.setOrientation(this);

                String filePath = resultData.getString(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                
                if (filePath != null)
                {

                    Log.v(TAG, Utils.getMethodName() + "getFileAbsPath " + filePath);

                    if (hasToShare)
                    {  // share downloaded file version
                        hasToShare =false;
                        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                        sharingIntent.setType(document.getMimeType());
                        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,document.getName());
                        sharingIntent.putExtra(android.content.Intent.EXTRA_STREAM, Uri.fromFile(new File(filePath)));
                        startActivity(Intent.createChooser(sharingIntent, "Share via"));

                    } else {
                        // or download onlyhasToShare
                        String msg =" Document " + filePath +  " ver. " + currViewedVersion + " is saved ";
                        Log.d(TAG, Utils.getMethodName() + msg);
                        Toast.makeText(this.getApplicationContext(),msg,Toast.LENGTH_LONG).show();
                    }
                    break;
                }
                break;

            case RequestConstants.REQUEST_TYPE_DOCUMENT_GET_FILE_BY_VERSION:
                // file is not PDF
                Options.setOrientation(this);

                document = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);


                if (!hasRenditionAsPdf) {


                    String mineType = document.getMimeType();
                    String fileAbsPath = document.getFileAbsPath();
                    XmlPullParser parser = getResources().getXml(R.xml.radee_pdf_reader_attrs);
                    AttributeSet attributes = Xml.asAttributeSet(parser);
                    if (fileAbsPath != null && mineType.contains("image"))
                    {                      
                        Bitmap bitmap = BitmapFactory.decodeFile(fileAbsPath);

                        ZoomableImageView zImgView  =  new ZoomableImageView(mContext,attributes);
                        zImgView.setImageBitmap(bitmap);
                        viewerContainer.addView(zImgView);

                        thumbContainer.setVisibility(View.GONE);
                        viewerToolBar.setVisibility(View.GONE);
                        
                    } else {
                        // if this document doesn't have rendition or is an image file, we show only file symbol

                        showDefaultFileNameAndIcon();
                         
                    }
                }


            case RequestConstants.REQUEST_TYPE_DOCUMENT_GET_COMMENTS:

                if (currentListType.equals(LIST_TYPE.COMMENT.toString())){

                    commentListAdapter.clear();
                    docCommentList.clear();
                    docCommentList =
                            resultData.getParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);

                    Log.d(TAG, Utils.getMethodName() + " docCommentList = " + docCommentList.toString());

                    // set empty comment at position 0
                    Comment comment = new Comment();
                    comment.setComment("");
                    comment.setOwnerId(document.getOwnerId());
                    comment.setId("");
                    comment.setCreationDate("");
                    commentListAdapter.insert(comment, 0);

                    commentListAdapter.addAll(docCommentList);
                    mListView.scrollTo(0, 0);
                    commentListAdapter.notifyDataSetChanged();
                }

                break;

            case RequestConstants.REQUEST_TYPE_DOCUMENT_GET_ACTIVITIES:

                if (currentListType.equals(LIST_TYPE.ACTIVITY.toString())){

                    docActivitiesList.clear();
                    this.docActivitiesList =
                            resultData.getParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);

                    Log.d(TAG, Utils.getMethodName() + " docActivitiesList.toString() = " + docActivitiesList.toString());
                    activityListAdapter.clear();
                    activityListAdapter.addAll(docActivitiesList);
                    activityListAdapter.notifyDataSetChanged();
                }

                break;
            case RequestConstants.REQUEST_TYPE_DOCUMENT_GET_VERSIONS:
                
                if (currentListType.equals(LIST_TYPE.VERSION.toString())){

                    docVersionList.clear();
                   
                    this.docVersionList =
                            resultData.getParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                    versionListAdapter.clear();
                    Log.d(TAG, Utils.getMethodName() + " docVersionList = " + docVersionList.toString());
                    versionListAdapter.addAll(docVersionList);
                    versionListAdapter.notifyDataSetChanged();
                }


                break;
            default:
                break;

        }
    }

    private void showDefaultFileNameAndIcon() {

        XmlPullParser parser = getResources().getXml(R.xml.radee_pdf_reader_attrs);
        AttributeSet attributes = Xml.asAttributeSet(parser);
        
        //ImageView iv = new ImageView(mContext,attributes);
        //Drawable da = getResources().getDrawable(R.drawable.default_doc_a);
        //iv.setImageDrawable(da);

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
                R.drawable.default_doc_a);
        ZoomableImageView zImgView  =  new ZoomableImageView(mContext,attributes);
        zImgView.setImageBitmap(bitmap);
        viewerContainer.setGravity(Gravity.CENTER_VERTICAL);
        viewerContainer.setGravity(Gravity.CENTER_HORIZONTAL);
        viewerContainer.addView(zImgView);
        


        thumbContainer.setVisibility(View.GONE);
        viewerToolBar.setVisibility(View.GONE);
        
       // TextView docName = (TextView) this.findViewById(R.id.tv_document_name);
       // docName.setText(document.getName());

    }


    private void showChoosePdfContextMenu(final View v) {

        PopupMenu popupMenu = new PopupMenu(this, v);
        popupMenu.getMenuInflater().inflate(R.menu.choose_pdf_viewer_menu, popupMenu.getMenu());

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case id.APV_menu_item:
                                setCurrPDFViewerType(DocumentViewerActivity.PDF_VIEWER_TYPE.APV.toString());
                                buildAPVViewer();
                                break;
                            case id.QOPPA_menu_item:
                                setCurrPDFViewerType(DocumentViewerActivity.PDF_VIEWER_TYPE.QOPPA.toString());
                                buildQPdfViewer();
                                break;
                            case id.RADEE_menu_item:
                                setCurrPDFViewerType(DocumentViewerActivity.PDF_VIEWER_TYPE.RADEE.toString());
                                buildRadeePdfViewer();
                                break;
                            case id.WEBVIEW_menu_item:
                                setCurrPDFViewerType(DocumentViewerActivity.PDF_VIEWER_TYPE.WEBVIEW.toString());
                                break;
                            default:
                                return true;
                        }
                        getPdfOrFiletoView(document);

                        return true;
                    }
                });

        popupMenu.show();
    }
                
                
    
                /*
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.choose_pdf_viewer_menu, menu);

    }

    @Override
    public void onOptionsItemSelected(MenuItem item) {
        super.onPrepareOptionsMenu(item);

        switch (item.getItemId()) {
            case id.APV_menu_item:
                this.setCurrPDFViewerType(DocumentViewerFragment.PDF_VIEWER_TYPE.APV.toString());
                break;
            case id.QOPPA_menu_item:
                this.setCurrPDFViewerType(DocumentViewerFragment.PDF_VIEWER_TYPE.QOPPA.toString());
                break;
            case id.RADEE_menu_item:
                this.setCurrPDFViewerType(DocumentViewerFragment.PDF_VIEWER_TYPE.WEBVIEW.toString());
                break;
            case id.WEBVIEW_menu_item:
                this.setCurrPDFViewerType(DocumentViewerFragment.PDF_VIEWER_TYPE.RADEE.toString());
                break;
            default:

        }

        if (mTwoPane) {

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.document_viewer_fragment, viewerFragment).commit();

        }
        else {

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.document_fragment_container, viewerFragment).commit();
        }

    }


    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        // Set a different FRAGMENT_GROUPID on each fragment.
        // A simple check, only continues on the correct fragment.
        if(item.getGroupId() == FRAGMENT_GROUPID)
        {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
            selectedCategory = (BudgetCategory) cListView.getItemAtPosition(info.position);
            switch (item.getItemId())
            {
                case MENU_EDIT:
                    // Do something!
                    return true;
                case MENU_REMOVE:
                    // Do something!
                    return true;
            }
        }
        // Be sure to return false or super's so it will proceed to the next fragment!
        return super.onContextItemSelected(item);
    }
*/
    
    @Override
    public void onRequestConnectionError(Request request, int statusCode) {

    }

    @Override
    public void onRequestDataError(Request request) {

    }

    @Override
    public void onRequestServiceError(Request request, Bundle resultData) {

    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    /**
     * A dummy implementation of the {@link com.saperion.sdb.client.DocumentInfoFragment.DocumentInfoActivityInterface} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private static DocumentInfoActivityInterface sDummyCallbacks = new DocumentInfoActivityInterface() {
        @Override
        public Document getCurrentDocument() {
            return null;
        }

        @Override
        public void onVersionClicked(Version version) {

        }


    };


    @Override
    public void onCommentAdded(String comment) {

        Utils.showLoadingDialog(this);
        document.addComments(comment,this);
    }

    @Override
    public void onVersionClicked(Document document) {

    }

    @Override
    public void onCommentUpdate(Comment currEditedComment) {
        Utils.showLoadingDialog(this);
        currEditedComment.update(this);
    }



    private void showContextMenu(final View v,final Comment comment) {

        PopupMenu popupMenu = new PopupMenu(this, v);
        popupMenu.getMenuInflater().inflate(R.menu.comment_edit_contextmenu, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {

                if (item.getItemId() == R.id.edit_menu_item) {
                    commentListAdapter.edit(v,comment);
                } else if (item.getItemId() == R.id.remove_menu_item) {
                    Utils.showLoadingDialog(getParent());
                    comment.delete(mListener);
                }


                return true;
            }
        });

        popupMenu.show();
    }



    @Override
    public void onClick(View v) {
        Log.v(TAG, Utils.getMethodName() + " entry " );
        if (v == next) {
            vf.setInAnimation(animFlipInNext);
            vf.setOutAnimation(animFlipOutNext);
            vf.showNext();
        }
        if (v == previous) {
            vf.setInAnimation(animFlipInPrevious);
            vf.setOutAnimation(animFlipOutPrevious);
            vf.showPrevious();
        }
    }
    //this is the part to handle Gesture Listener
    @Override
    public boolean onTouchEvent(MotionEvent me){
        Log.v(TAG, Utils.getMethodName() + " entry " );
        return gestureScanner.onTouchEvent(me);
    }
    
    @Override
    public boolean onDown(MotionEvent e){
        Log.v(TAG, Utils.getMethodName() + " entry " );
        return true;
    }
    
    //FLING gesture listener
    @Override
    public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY){

        Log.v(TAG, Utils.getMethodName() + " entry ");
        try {
            if(e1.getX() > e2.getX() && Math.abs(e1.getX() - e2.getX()) > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                Toast.makeText(this.getApplicationContext(), "Left", Toast.LENGTH_LONG).show();
                vf.setInAnimation(animFlipInPrevious);
                vf.setOutAnimation(animFlipOutPrevious);
                vf.showPrevious();

            }else if (e1.getX() < e2.getX() && e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                Toast.makeText(this.getApplicationContext(), "Right", Toast.LENGTH_LONG).show();


                vf.setInAnimation(animFlipInNext);
                vf.setOutAnimation(animFlipOutNext);
                vf.showNext();
            }
        } catch (Exception e) {
            // nothing
        }
        return true;

    }
    @Override
    public boolean onScroll(MotionEvent e1,MotionEvent e2,float distanceX,float distanceY){
        return true;
    }
    @Override
    public void onLongPress(MotionEvent e){}
    @Override
    public void onShowPress(MotionEvent e){}
    @Override
    public boolean onSingleTapUp(MotionEvent e){ return true;}
}
