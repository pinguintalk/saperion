package com.saperion.sdb.client;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.saperion.sdb.client.ShareListAdapter.ShareListViewInterface;
import com.saperion.sdb.client.dialogs.ErrorDialogFragment.ErrorDialogFragmentBuilder;
import com.saperion.sdb.client.utils.Log;
import com.saperion.sdb.sdk.SdbApplication;
import com.saperion.sdb.sdk.models.Setting;
import com.saperion.sdb.sdk.models.Settings;
import com.saperion.sdb.sdk.models.Share;
import com.saperion.sdb.sdk.models.Space;
import com.saperion.sdb.sdk.models.User;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.RequestConstants;
import com.saperion.sdb.sdk.requestmanager.RequestManager.RequestListener;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.rights.ShareRight;
import com.saperion.sdb.sdk.utils.Utils;

import java.util.ArrayList;


/**
 * Created by nnn on 22.11.13.
 */
public class ShareDialogFragment extends DialogFragment implements RequestListener, ShareListViewInterface {

    private static final String TAG = ShareDialogFragment.class.getSimpleName();


    TextView tvShareTitle;
    TextView tvShareUserCount;
    AutoCompleteTextView etShareEmail;
    ImageButton btnOk;
    ImageButton btnCancel;

    ImageView btnCloseFragment;
    
    ProgressDialog progressDialog = null;
    Space space = null;
    RequestListener requestListener;
    ListView shareListView = null;
    ShareListAdapter shareListAdapter;
    static ArrayList<Share> shareList = new ArrayList<Share>();
    static Setting[] settingList = null;
    Share currShare = null;
    Activity activity;
    protected boolean isInputValuesValid = false;
    User currentUser= null;
    ArrayAdapter<String> adapterEmailList = null;
    private static String[] USERS_EMAIL = new String[]{""};

    private ImageButton mButtonEditableFlag;
    private ImageButton mButtonShareableFlag;
    Drawable btn_selected_drawable;
    Drawable btn_not_selected_drawwable;
    
    public ShareDialogFragment(Activity activity, Space space) {

        this.space = space;
        this.activity = activity;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Light_Dialog);
        // setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogTheme);

    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.share_dialog_fragment, container);

        getDialog().setTitle("Share Space with other user");
        
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        btnCloseFragment  = (ImageView) view.findViewById(R.id.share_dialog_close_button);

        btnCloseFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v(TAG, Utils.getMethodName() + " btnCloseFragment " );
                onDismiss(getDialog());
            }
        });
        
        btn_selected_drawable = view.getResources().getDrawable(R.drawable.fns_bg_orange_shape_no_stroke);
        btn_not_selected_drawwable  = view.getResources().getDrawable(R.drawable.fns_bg_grey_shape_no_stroke);
        
        requestListener = this;
        tvShareTitle = (TextView) view.findViewById(R.id.share_dialog_title);
        tvShareUserCount = (TextView) view.findViewById(R.id.share_user_count);
        
        shareListView = (ListView) view.findViewById(R.id.share_list_view);
        shareListAdapter = new ShareListAdapter(this.activity, this, shareList);
        shareListView.setAdapter(shareListAdapter);
        
        shareListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                Toast.makeText(getActivity().getApplicationContext(),
                        ((TextView) v).getText(), Toast.LENGTH_SHORT).show();

            }
        });


        ArrayList<User> userListTmp = SdbApplication.getInstance().getUserList(this);

        if (userListTmp != null) {
            USERS_EMAIL = new String[userListTmp.size()];

            for (int i = 0; i < userListTmp.size(); i++) {

                    USERS_EMAIL[i] = userListTmp.get(i).getEmail();
            }
        }
      
        
        adapterEmailList = new ArrayAdapter<String>(this.getActivity(),
                android.R.layout.select_dialog_item, USERS_EMAIL);
        etShareEmail = (AutoCompleteTextView) view.findViewById(R.id.autocomplete_tv_email);
        etShareEmail.setAdapter(adapterEmailList);
        
        
        btnOk = (ImageButton) view.findViewById(R.id.b_share_ok);
        btnOk.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.v(TAG, Utils.getMethodName() + " btnOk " );

            String userEmail = etShareEmail.getText().toString();
            if (userEmail == null ||userEmail.isEmpty()) return;
            Share share = new Share();
            share.setName(space.getName());
            share.setReceiverName(userEmail);
            share.setSpaceId(space.getId());

                if (mButtonShareableFlag.getBackground().equals(btn_selected_drawable)) {

                    share.addRight(ShareRight.SHARE);
                } else {
  
                    if (mButtonEditableFlag.getBackground().equals(btn_selected_drawable)) {

                        share.addRight(ShareRight.WRITE);
                    } 
                }

            showLoadingDialog();
            share.save(requestListener);
        }
        });

        btnCancel = (ImageButton) view.findViewById(R.id.b_share_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v(TAG, Utils.getMethodName() + " btnCancel " );
                etShareEmail.setText("");
            }
        });
        
        if (space !=null) {
            // showLoadingDialog();
            // refreshShareList();
            tvShareTitle.setText("Share " + space.getName() + " with other users");
            SdbApplication.getInstance().getShareList(this,space.getId());
        }

        mButtonShareableFlag = (ImageButton) view.findViewById(R.id.b_shareable);
        mButtonShareableFlag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v(TAG, Utils.getMethodName() + " mButtonShareableFlag " );
                if (mButtonShareableFlag.getBackground().equals(btn_selected_drawable)) {

                    Log.v(TAG, Utils.getMethodName() + "mButtonShareableFlag 1");

                    mButtonShareableFlag.setBackgroundResource(R.drawable.fns_bg_grey_shape_no_stroke);
                } else {

                    Log.v(TAG, Utils.getMethodName() + "mButtonShareableFlag 2");
                    mButtonShareableFlag.setBackgroundResource(R.drawable.fns_bg_orange_shape_no_stroke);
                }
            }
        });
        mButtonShareableFlag.setBackgroundResource(R.drawable.fns_bg_grey_shape_no_stroke);

        mButtonEditableFlag = (ImageButton) view.findViewById(R.id.b_editable);
        mButtonEditableFlag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mButtonEditableFlag.getBackground().equals(btn_selected_drawable)) {

                    Log.v(TAG, Utils.getMethodName() + "mButtonShareableFlag 1");

                    mButtonEditableFlag.setBackgroundResource(R.drawable.fns_bg_grey_shape_no_stroke);
                } else {
    
                    Log.v(TAG, Utils.getMethodName() + "mButtonShareableFlag 2");
                    mButtonEditableFlag.setBackgroundResource(R.drawable.fns_bg_orange_shape_no_stroke);
                }
            }
        });
        mButtonEditableFlag.setBackgroundResource(R.drawable.fns_bg_grey_shape_no_stroke);
        
        
        // SdbApplication.getInstance().getCurrentUser(this);
        // SdbApplication.getInstance().getCurrentSettings(this);

    }

    

    private void refreshShareList() {
        shareList.clear();
        shareList.addAll(space.getShares(getActivity().getApplicationContext(), this));
        shareListAdapter.notifyDataSetChanged();
    }

    private void showErrorDialog(String message) {
        new ErrorDialogFragmentBuilder(getActivity())
                .setTitle(R.string.dialog_data_error_title).setMessage(message).show();
        return;
    }

    @Override
    public void onRequestFinished(Request request, Bundle resultData) {
        if (getActivity() == null)
            return;

        int result_code = resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);
        int requestType = request.getRequestType();
        String errorMessage = resultData.getString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE);

        cancelLoadingDialog();

        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " resultCode = "
                + result_code);

        switch (result_code) {

            case ResponseConstants.RESPONSE_CODE_OK:
            case ResponseConstants.RESPONSE_CODE_CREATED:
            case ResponseConstants.RESPONSE_CODE_NO_CONTENT:
                break;
            case ResponseConstants.RESPONSE_CODE_UNKOWN_ERROR:
            case ResponseConstants.RESPONSE_CODE_BAD_REQUEST:
            case ResponseConstants.RESPONSE_CODE_ERROR:
            case ResponseConstants.RESPONSE_CODE_FORBIDDEN:
            case ResponseConstants.RESPONSE_CODE_INTERNAL_SERVER_ERROR:
            case ResponseConstants.RESPONSE_CODE_INTERNAL_SERVICE_UNAVAILABLE:
            case ResponseConstants.RESPONSE_CODE_METHOD_NOT_ALLOWED:
            case ResponseConstants.RESPONSE_CODE_NOT_FOUND:
                return;
            case ResponseConstants.RESPONSE_CODE_PRECONDITION_FAILED:
            case ResponseConstants.RESPONSE_CODE_TOO_MANY_REQUESTS:
                showErrorDialog(errorMessage);
                return;
            default: {
                showErrorDialog("unkown error !"); // should not come here !
                return;
            }
        }

        switch (requestType) {


            case RequestConstants.REQUEST_TYPE_USER_GET_LIST:

                ArrayList<User> tmpUserList =
                        resultData.getParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                USERS_EMAIL = new String[tmpUserList.size()];
                
                for (int i = 0; i < tmpUserList.size(); i++) {
                        USERS_EMAIL[i] = tmpUserList.get(i).getEmail();                  
                }

                adapterEmailList = new ArrayAdapter<String>(this.getActivity(),
                       android.R.layout.select_dialog_item, USERS_EMAIL);
                etShareEmail.setAdapter(adapterEmailList);
                
                break;
            case RequestConstants.REQUEST_TYPE_SHARE_CREATE:
            case RequestConstants.REQUEST_TYPE_SHARE_UPDATE:
            case RequestConstants.REQUEST_TYPE_SHARE_DELETE:

                SdbApplication.getInstance().getShareList(this,space.getId());
                break;

            case RequestConstants.REQUEST_TYPE_SHARE_GET_LIST:

                ArrayList<Share> shareListmp = resultData.getParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                if (shareListmp != null) {
                    tvShareUserCount.setText(shareListmp.size() + " Users");
                
                    Log.v(TAG, Utils.getMethodName() + " shareList = " + shareListmp.toString());
                    shareList.clear();
                    shareList.addAll(shareListmp);
                    shareListAdapter.notifyDataSetChanged();
                 }
                break;
            
            case RequestConstants.REQUEST_TYPE_USER_GET_CURRENT:
                currentUser = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);


                break;
            case RequestConstants.REQUEST_TYPE_DOCUMENT_DELETE_LINK:

                Log.v(TAG, Utils.getMethodName() + " link deleted");
                if (space !=null) {
                    refreshShareList();
                }
                break;
            case RequestConstants.REQUEST_TYPE_ACCOUNT_GET_SETTINGS:
                Settings settings = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                settingList = settings.getList().clone();
              
                for (int s=0; s < settingList.length ; s++) {
                    // Log.v(TAG, Utils.getMethodName() + "key " + settingList[s].getKey().toString() + " value " + settingList[s].getValue().toString());

                }
                break;
            
        }

    }

    @Override
    public void onRequestConnectionError(Request request, int statusCode) {

        cancelLoadingDialog();
        int requestType = request.getRequestType();
        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " statusCode = "
                + statusCode);
        showServiceErrorDialog(getResources().getString(R.string.dialog_connection_error_message));

        Toast.makeText(getActivity().getApplicationContext(),
                R.string.dialog_connection_error_message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestDataError(Request request) {

        cancelLoadingDialog();
        int requestType = request.getRequestType();
        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " statusCode = ");

        showServiceErrorDialog(getResources().getString(R.string.dialog_data_error_message));

        Toast.makeText(getActivity().getApplicationContext(),
                R.string.dialog_data_error_message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestServiceError(Request request, Bundle resultData) {
        cancelLoadingDialog();
        int requestType = request.getRequestType();

        int error_code = resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);

        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " error_type = "
                + error_code);

        Toast.makeText(getActivity().getApplicationContext(),
                R.string.dialog_service_error_title, Toast.LENGTH_SHORT).show();

    }

    public void showLoadingDialog() {

        
        if (progressDialog != null) {
            progressDialog.cancel();
            progressDialog = null;
        }

        progressDialog = new ProgressDialog(this.getActivity());

        progressDialog.setTitle(getResources().getString(R.string.progress_dialog_title));
        progressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        progressDialog.setCancelable(true);
        progressDialog.setIndeterminate(true);

        progressDialog.show();
  
    }
    
    private void showServiceErrorDialog(String message) {

        Context context = getActivity();
        if (context == null) return;

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(R.string.dialog_service_error_title);
        builder.setMessage(message);

        builder.setPositiveButton(R.string.dialog_error_button_close_txt, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do do my action here

                dialog.dismiss();
            }

        });
        /*
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // I do not need any action here you might
                dialog.dismiss();
            }
        });
        */
        AlertDialog alert = builder.create();
        alert.show();

    }    public void cancelLoadingDialog() {
    
        if (progressDialog != null) {
            progressDialog.cancel();
            progressDialog = null;
        }

    }

    @Override
    public void onDeleteShare(Parcelable linkParcelable) {
        showLoadingDialog();
        
        Share share = (Share) linkParcelable;
        share.delete(this);
    }

    @Override
    public void onUpdateShare(Parcelable shareParcelable) {
        Share share = (Share) shareParcelable;
        share.save(this);
    }


    public abstract class TextValidator implements TextWatcher {
        private EditText editText;

        public TextValidator(EditText textView) {
            this.editText = textView;
        }

        public abstract void validate(EditText textView, String text);

        @Override
        final public void afterTextChanged(Editable s) {
            if (editText == null) return;
            String text = editText.getText().toString();
            validate(editText, text);
        }

        @Override
        final public void beforeTextChanged(CharSequence s, int start, int count, int after) { /* Don't care */ }

        @Override
        final public void onTextChanged(CharSequence s, int start, int before, int count) { /* Don't care */ }
    }

}
