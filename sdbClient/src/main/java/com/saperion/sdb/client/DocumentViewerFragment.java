package com.saperion.sdb.client;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.qoppa.android.pdfProcess.PDFDocument;
import com.qoppa.samples.viewer.PDFViewer;
import com.qoppa.samples.viewer.PageContents;
import com.radaee.pdf.Global;
import com.radaee.pdf.Page.Annotation;
import com.radaee.reader.PDFReader;
import com.radaee.reader.PDFReader.PDFReaderListener;
import com.radaee.util.PDFThumbView;
import com.radaee.view.PDFVPage;
import com.radaee.view.PDFViewThumb.PDFThumbListener;
import com.saperion.sdb.client.DocumentInfoFragment.DocumentInfoActivityInterface;
import com.saperion.sdb.client.R.id;
import com.saperion.sdb.client.R.layout;
import com.saperion.sdb.client.utils.Log;
import com.saperion.sdb.sdk.models.Comment;
import com.saperion.sdb.sdk.models.Document;
import com.saperion.sdb.sdk.models.Version;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.RequestConstants;
import com.saperion.sdb.sdk.requestmanager.RequestManager.RequestListener;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.utils.Utils;

import org.xmlpull.v1.XmlPullParser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import cx.hell.android.lib.pagesview.PagesView;
import cx.hell.android.lib.pdf.PDF;
import cx.hell.android.pdfview.Actions;
import cx.hell.android.pdfview.Options;
import cx.hell.android.pdfview.PDFPagesProvider;

/**
 * Created by nnn on 31.07.13.
 */
public class DocumentViewerFragment extends SdbBasicFragment implements RequestListener, DocumentInfoListViewInterface, SensorEventListener {

    private static final String TAG = DocumentViewerFragment.class.getSimpleName();
    private Context mContext;

    private RequestListener requestListener = null;
    private Document document = null;
    private int currViewedVersion;

    private RelativeLayout viewerContainer = null;
    private RelativeLayout thumbContainer = null;

    @Override
    public void onCommentAdded(String comment) {
        
    }

    @Override
    public void onVersionClicked(Document document) {

    }

    @Override
    public void onCommentUpdate(Comment currEditedComment) {

    }


    public enum PDF_VIEWER_TYPE {
        RADEE,
        QOPPA,
        APV,
        WEBVIEW;
    }


    private String currPDFViewerType = PDF_VIEWER_TYPE.RADEE.toString();
    
    // Viewer Controller

    int currPageNumber = 1;
    int currZoomFactor = 1;

    LinearLayout expandableDocInfoLayout;    
    Button docInfoExpandSwitcher;
    ValueAnimator mAnimator;
    
    private ImageButton buttonShowOrHideThumbnail;
    private ImageButton buttonviewfull;
    private ImageButton buttonSetCurrentVersion;
    private ImageButton buttonDownLoadFileVersion;
    private ImageButton buttonPdfDownLoadVersion;
    private ImageButton buttonChooseViewer;
    
    private ImageButton buttonZoomIn;
    private ImageButton buttonZoomOut;

    private ImageButton buttonPageUp;
    private ImageButton buttonPageDown;
    protected EditText editTextcurrPage ;
    private TextView pageNumber ;

    private boolean isPdf = false;

    
    // APV Reader  
    private PDF pdf = null;
    private PagesView apvPagesView = null;
    private PDFPagesProvider pdfPagesProvider = null;
    private int box = 2;
    private int colorMode = Options.COLOR_MODE_NORMAL;
    private Actions actions = null;
    private SensorManager sensorManager = null;
    private float[] gravity = {0f, -9.81f, 0f};
    private long gravityAge = 0;
    private int prevOrientation;
    private boolean history = true;
    
    // Radee Pdf Reader variables
    private PDFReader radeePdfReader = null;
    private PDFThumbView m_thumb = null;
    private com.radaee.pdf.Document pdfDoc = new com.radaee.pdf.Document();
    private PDFReaderListener radeeReaderlistener = null;
    private PDFThumbListener radeeThumblistener = null;
    
    // QPdf Reader variables
    private PDFViewer qPDFViewer = null;

    // Info Viewer
    private Button buttonComments;
    private Button buttonVersion;
    private Button buttonActivity;
    private ArrayList<Comment> docCommentList = new ArrayList<Comment>();
    private ArrayList<Version> docVersionList = new ArrayList<Version>();
    private ArrayList<com.saperion.sdb.sdk.models.Activity> docActivitiesList = new ArrayList<com.saperion.sdb.sdk.models.Activity>();
    private DocumentVersionListAdapter versionListAdapter;
    private DocumentActivityListAdapter activityListAdapter;
    private DocumentCommentListAdapter commentListAdapter;
    private ListView mListView;
    private String currentListType = LIST_TYPE.VERSION.toString();
    public enum LIST_TYPE {
        COMMENT,
        VERSION,
        ACTIVITY;
    }
    private Drawable btn_not_selected;
    private Drawable btn_selected;
    private DocumentInfoActivityInterface activityCallbacks = sDummyCallbacks;
    private RequestListener mListener;
    
    public DocumentViewerFragment() {
    }

    public String getCurrPDFViewerType() {
        return currPDFViewerType;
    }

    public void setCurrPDFViewerType(String currPDFViewerType) {
        this.currPDFViewerType = currPDFViewerType;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        Log.v(TAG, Utils.getMethodName() + "entry");
        mContext = getActivity();
        mListener = this;

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.v(TAG, Utils.getMethodName() + "orientation ch  = " + newConfig.orientation);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

        }

        if (currPDFViewerType.equals(PDF_VIEWER_TYPE.APV.toString())) {
            qPDFViewer.onConfigurationChanged(newConfig);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Log.v(TAG, Utils.getMethodName() + "entry");
        View rootView = inflater.inflate(layout.document_viewer_fragment, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        Log.v(TAG, Utils.getMethodName() + "entry");
        Global.Init(getActivity());
        requestListener = this;
        Bundle bundle = getArguments();

        if (bundle == null) {
            Log.e(TAG, Utils.getMethodName() + "bundle = null ");
            return;
        }
        this.document = bundle.getParcelable(DocumentFragmentActivity.PARAM_DOCUMENT);
        currViewedVersion = bundle.getInt(DocumentFragmentActivity.PARAM_DOCUMENT_VERSION);

        if (view != null) {
            mListView = (ListView) view.findViewById(id.document_info_list_view);
        }

        if (mListView == null)
            return;

        btn_selected = getActivity().getResources().getDrawable(R.drawable.fns_orange_shape_selected);
        btn_not_selected = getActivity().getResources().getDrawable(R.drawable.fns_btn_not_selected);

        activityListAdapter = new DocumentActivityListAdapter(mContext, this, docActivitiesList);
        versionListAdapter = new DocumentVersionListAdapter(mContext, this, docVersionList);
        commentListAdapter = new DocumentCommentListAdapter(mContext, this, docCommentList);

        mListView.setAdapter(commentListAdapter);

        mListView.setAnimationCacheEnabled(false);
        mListView.setScrollingCacheEnabled(false);
        mListView.setClickable(false);
        mListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {

                if ( currentListType.equals(LIST_TYPE.VERSION.toString())){
                    Version selectedVersion = versionListAdapter.getItem(position);

                    activityCallbacks.onVersionClicked(selectedVersion);

                }

            }
        });

        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> av, View v, int position, long id) {
                if ( currentListType.equals(LIST_TYPE.COMMENT.toString())){
                    Comment comment = commentListAdapter.getItem(position);

                    // showContextMenu(v,comment);

                }

                return true;
            }
        });
        buttonVersion = (Button) view.findViewById(id.b_doc_version);
        buttonVersion.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.v(TAG, Utils.getMethodName() + "buttonVersion clicked ");
                setButtonsBackGroundColorToDefault();
                setButtonSelected(buttonVersion);
                mListView.setClickable(true);
                // set new adapterEmailList
                mListView.setAdapter(versionListAdapter);
                currentListType = LIST_TYPE.VERSION.toString();
                if (document != null) {
                    Log.v(TAG, Utils.getMethodName() + "request get version list of " + document.getName());

                    showLoadingDialog();
                    document.getVersions(mListener);
                }
            }
        });
        
        buttonComments = (Button) view.findViewById(id.b_doc_comment);

        buttonComments.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.v(TAG, Utils.getMethodName() + "buttonComments clicked ");
                setButtonsBackGroundColorToDefault();
                setButtonSelected(buttonComments);
                mListView.setClickable(true);
                // set new adapterEmailList
                mListView.setAdapter(commentListAdapter);
                currentListType = LIST_TYPE.COMMENT.toString();

                if (document != null) {
                    Log.v(TAG, Utils.getMethodName() + "request get comments list of " + document.getName());

                    showLoadingDialog();
                    document.getComments(mListener);
                }
            }
        });

        buttonActivity = (Button) view.findViewById(id.b_doc_activity);
        buttonActivity.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.v(TAG, Utils.getMethodName() + "buttonActivity clicked ");
                setButtonsBackGroundColorToDefault();
                setButtonSelected(buttonActivity);
                mListView.setClickable(false);
                // set new adapterEmailList
                mListView.setAdapter(activityListAdapter);
                currentListType = LIST_TYPE.ACTIVITY.toString();
                if (document != null) {
                    Log.v(TAG, Utils.getMethodName() + "request get activitiy list of " + document.getName());

                    showLoadingDialog();
                    document.getActivities(mListener);
                }
            }
        });
        expandableDocInfoLayout = (LinearLayout) view.findViewById(id.expandableDocInfoLayout);
        //Add onPreDrawListener
        expandableDocInfoLayout.getViewTreeObserver().addOnPreDrawListener(
                new ViewTreeObserver.OnPreDrawListener() {

                    @Override
                    public boolean onPreDraw() {
                        expandableDocInfoLayout.getViewTreeObserver().removeOnPreDrawListener(this);
                        expandableDocInfoLayout.setVisibility(View.GONE);

                        final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                        final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                        expandableDocInfoLayout.measure(widthSpec, heightSpec);

                        mAnimator = slideAnimator(expandableDocInfoLayout, 0, expandableDocInfoLayout.getMeasuredHeight());
                        return true;
                    }
                });

        docInfoExpandSwitcher = (Button) view.findViewById(id.b_slide_in_out);
        docInfoExpandSwitcher.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (expandableDocInfoLayout.getVisibility() == View.GONE) {
                    docInfoExpandSwitcher.setText(">>");
                    setLayoutVisible(expandableDocInfoLayout);

                } else {
                    docInfoExpandSwitcher.setText("<<");
                    setLayoutGone(expandableDocInfoLayout);
                }
            }
        });
        
        viewerContainer = (RelativeLayout) view.findViewById(id.viewerContainer);
        thumbContainer  = (RelativeLayout) view.findViewById(id.thumbContainer);
        thumbContainer.setVisibility(View.GONE);
        
        pageNumber = (TextView) view.findViewById(id.tv_pages_nr);

        buttonShowOrHideThumbnail = (ImageButton) view.findViewById(id.b_show_thumbnail);
        buttonShowOrHideThumbnail.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "buttonShowOrHideThumbnail clicked ");
                if (document != null) {
                    showhideThumbnail();
                }
            }
        });
        buttonviewfull = (ImageButton) view.findViewById(id.b_doc_view_full);
        buttonviewfull.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "buttonviewfull clicked ");
 
                if (document != null) {
                    Uri fileUri = document.getFileContentUri(mContext);
                    Log.v(TAG, Utils.getMethodName() + "opening " + fileUri.toString());

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(fileUri, document.getMimeType());
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    try {
                        startActivity(intent);
                    } catch (Exception e) {
                        showErrorDialog(mContext, "Could not show file");
                        Log.e("error", "" + e);
                    }

                }
                
            }
        });

        buttonChooseViewer = (ImageButton) view.findViewById(id.b_choose_viewer);
        buttonChooseViewer.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "buttonChooseViewer clicked ");

              
                showChoosePdfContextMenu(v);
            }
        });
        
        buttonSetCurrentVersion = (ImageButton) view.findViewById(id.b_set_as_current_version);
        buttonSetCurrentVersion.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "buttonSetCurrentVersion clicked ");
                if (document != null) {
                    showLoadingDialog();
                    document.restoreVersion(requestListener, currViewedVersion);
                }

            }
        });

        buttonDownLoadFileVersion= (ImageButton) view.findViewById(id.b_invite_user);
        buttonDownLoadFileVersion.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "buttonDownLoadFileVersion clicked ");
                if (document != null) {
                    showLoadingDialog();
                    document.downloadFileByVersion(requestListener, currViewedVersion);
                }

            }
        });

        buttonPdfDownLoadVersion = (ImageButton) view.findViewById(id.b_pdf_download);
        buttonPdfDownLoadVersion.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "buttonPdfDownLoadVersion clicked ");
                if (document != null) {
                    showLoadingDialog();
                    document.downloadPdfbyVersion(requestListener, currViewedVersion);
                }

            }
        });
        editTextcurrPage = (EditText) view.findViewById(id.et_curr_page_nr);


        buttonPageUp = (ImageButton) view.findViewById(id.b_page_up);
        buttonPageUp.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "buttonPageUp clicked ");

                if (document != null && pdfDoc !=null) {
                        pageUp();
                }

            }
        });

        buttonPageDown = (ImageButton) view.findViewById(id.b_page_down);
        buttonPageDown.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "buttonPageDown clicked ");
                if (document != null && pdfDoc !=null) {
                    pageDown();
                }

            }
        });

        buttonZoomIn = (ImageButton) view.findViewById(id.b_zoom_in);
        buttonZoomIn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "buttonZoomIn clicked ");
                if (document != null) {
                    zoomIn();
                }

            }
        });

        buttonZoomOut = (ImageButton) view.findViewById(id.b_zoom_out);
        buttonZoomOut.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "buttonZoomOut clicked ");
                if (document != null) {
                    zoomOut();
                }

            }
        });

        if (currPDFViewerType.equals(PDF_VIEWER_TYPE.QOPPA.toString())){
            buildQPdfViewer();
        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.APV.toString())){
            buildAPVViewer();
        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.WEBVIEW.toString())){

        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.RADEE.toString())){
            buildRadeePdfViewer();
        }

        getPdfOrFiletoView(document);
        document.getVersions(this);
    }

    private void setButtonsBackGroundColorToDefault(){

        //Button buttonComments = (Button) getActivity().findViewById(id.b_doc_comment);
        //Button buttonVersion = (Button) getActivity().findViewById(id.b_doc_version);
        // Button buttonActivity = (Button) getActivity().findViewById(id.b_doc_activity);

        if (buttonVersion != null){
            buttonVersion.setBackgroundDrawable(btn_not_selected);
            buttonVersion.setTextColor(Color.BLACK);
        }
        if (buttonActivity != null) {
            buttonActivity.setBackgroundDrawable(btn_not_selected);
            buttonActivity.setTextColor(Color.BLACK);
        }
        if (buttonComments != null){
            buttonComments.setBackgroundDrawable(btn_not_selected);
            buttonComments.setTextColor(Color.BLACK);
        }
    }
    
    private void setButtonSelected(Button button){
        button.setBackgroundDrawable(btn_selected);
        button.setTextColor(Color.WHITE);
    }

    
    private void setLayoutVisible(LinearLayout layout) {
        //set nameExpandableLayout Visible
        layout.setVisibility(View.VISIBLE);

        final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        layout.measure(widthSpec, heightSpec);

        ValueAnimator mAnimator = slideAnimator(layout, 0, layout.getMeasuredHeight());
        mAnimator.start();

    }
    private void setLayoutGone(final LinearLayout layout) {
        int finalHeight = layout.getHeight();

        ValueAnimator mAnimator = slideAnimator(layout,finalHeight, 0);

        mAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationEnd(Animator animator) {
                //Height=0, but it set visibility to GONE
                layout.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });
        mAnimator.start();
    }
    
    private ValueAnimator slideAnimator(final LinearLayout layout, int start, int end) {

        ValueAnimator animator = ValueAnimator.ofInt(start, end);


        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                //Update Height
                int value = (Integer) valueAnimator.getAnimatedValue();

                ViewGroup.LayoutParams layoutParams = layout.getLayoutParams();
                layoutParams.height = value;
                layout.setLayoutParams(layoutParams);
            }
        });
        return animator;
    }
    
    private void showhideThumbnail(){

        if (currPDFViewerType.equals(PDF_VIEWER_TYPE.QOPPA.toString())){

        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.APV.toString())){

        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.WEBVIEW.toString())){

        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.RADEE.toString())){

            if (m_thumb != null ){
                m_thumb.thumbGotoPage(currPageNumber);
                m_thumb.thumbUpdatePage(currPageNumber);

                if (thumbContainer.getVisibility() == View.VISIBLE)
                {
                    thumbContainer.setVisibility( View.GONE);
                }
                else
                    thumbContainer.setVisibility( View.VISIBLE);
            }

        }

    }
    



    private void zoomIn(){
        currZoomFactor = currZoomFactor + 1;
        if (currPDFViewerType.equals(PDF_VIEWER_TYPE.QOPPA.toString())){
            qPDFViewer.setScale(currZoomFactor,null);
        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.APV.toString())){

            apvPagesView.doAction(actions
                    .getAction(Actions.ZOOM_OUT));
        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.WEBVIEW.toString())){

        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.RADEE.toString())){
          
            radeePdfReader.PDFSetScale(currZoomFactor);
            
        }

    }

    private void zoomOut(){

        if (currZoomFactor > 1)
        currZoomFactor = currZoomFactor - 1;
        if (currPDFViewerType.equals(PDF_VIEWER_TYPE.QOPPA.toString())){
  
            qPDFViewer.setScale(currZoomFactor,null);
        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.APV.toString())){
            apvPagesView.doAction(actions
                    .getAction(Actions.ZOOM_IN));

        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.WEBVIEW.toString())){

        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.RADEE.toString())){
  
            radeePdfReader.PDFSetScale(currZoomFactor);

        }

    }

    private void pageUp(){

        if (currPDFViewerType.equals(PDF_VIEWER_TYPE.QOPPA.toString())){

            PDFDocument qPDFDocument =  qPDFViewer.getDocument();
            if (qPDFDocument != null)
                pageNumber.setText(String.valueOf(qPDFViewer.getDocument().getPageCount()));

            if (currPageNumber < qPDFViewer.getDocument().getPageCount()) {
                currPageNumber = currPageNumber + 1;

                PageContents pc  = qPDFViewer.getCachcedPage(currPageNumber);
                qPDFViewer.cachePage(currPageNumber, pc);
        
            }
        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.APV.toString())){
            if (apvPagesView != null) {

                if (currPageNumber < apvPagesView.getPageCount() ) {
                    currPageNumber = currPageNumber + 1;
                    apvPagesView.scrollToPage(currPageNumber);
                }
            }
        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.WEBVIEW.toString())){

        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.RADEE.toString())){
            if (currPageNumber < pdfDoc.GetPageCount()) {
                currPageNumber = currPageNumber + 1;

                radeePdfReader.PDFGotoPage(currPageNumber);
                if (m_thumb != null){
                    m_thumb.thumbGotoPage(currPageNumber - 1);
                    // m_thumb.thumbUpdatePage(currPageNumber);
                }
            }
        }

        editTextcurrPage.setText(String.valueOf(currPageNumber));
    }
    private void pageDown(){

        if (currPDFViewerType.equals(PDF_VIEWER_TYPE.QOPPA.toString())){

            PDFDocument qPDFDocument =  qPDFViewer.getDocument();
            if (qPDFDocument != null)
                pageNumber.setText(String.valueOf(qPDFViewer.getDocument().getPageCount()));
            
            if (currPageNumber > 1) {
                currPageNumber = currPageNumber - 1;

                PageContents pc  = qPDFViewer.getCachcedPage(currPageNumber);
                qPDFViewer.cachePage(currPageNumber, pc);

            }
        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.APV.toString())){
            if (apvPagesView != null) {
                if (currPageNumber > 1) {
                    currPageNumber = currPageNumber - 1;
                    apvPagesView.scrollToPage(currPageNumber);
                }
                   
            }
        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.WEBVIEW.toString())){

        } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.RADEE.toString())){
            if (currPageNumber > 1) {
                currPageNumber = currPageNumber - 1;

                radeePdfReader.PDFGotoPage(currPageNumber);
                if (m_thumb != null){
                    m_thumb.thumbGotoPage(currPageNumber - 1);
                   //  m_thumb.thumbUpdatePage(currPageNumber);
                }
            }
        }

        editTextcurrPage.setText(String.valueOf(currPageNumber));

    }
    
    private void getPdfOrFiletoView(Document document) {

        if (document != null) {

            currPageNumber = 1;
            editTextcurrPage.setText(String.valueOf(currPageNumber));
            
            String mine_type = document.getMimeType();
            Log.v(TAG, Utils.getMethodName() + "currViewedVersion " + currViewedVersion
                    + " mine type= " + mine_type);

            showLoadingDialog();

            if (mine_type.contains("png") || mine_type.contains("gif") ||
                    mine_type.contains("jpg") ||  mine_type.contains("jpeg") ||  mine_type.contains("bmp")) {
                isPdf = false;
                Log.v(TAG, Utils.getMethodName() + " getting picture ver." + currViewedVersion);
                document.getFileByVersion(this, currViewedVersion);
            }
            else {

                isPdf = true;
                Log.v(TAG, Utils.getMethodName() + " getting rendition doc version = " + currViewedVersion);
                document.getRenditionVersioned(this, currViewedVersion);
            }
        }
    }
    
    // build radee pdf viewer
    private void buildRadeePdfViewer(){

        radeeThumblistener = new PDFThumbListener() {
            @Override
            public void OnPageClicked(int i) {
                Log.v(TAG, Utils.getMethodName() + "page clicked " + i );
                currPageNumber = i + 1;
                radeePdfReader.PDFGotoPage(currPageNumber);
                editTextcurrPage.setText(String.valueOf(currPageNumber));
            }
        };
                
        radeeReaderlistener = new PDFReaderListener() {

            @Override
            public void OnPageModified(int pageno) {
                Log.v(TAG, Utils.getMethodName() + "pageno " + pageno);
                currPageNumber = pageno;
                radeePdfReader.PDFGotoPage(currPageNumber);
            }

            @Override
            public void OnPageChanged(int pageno) {

            }

            @Override
            public void OnAnnotClicked(PDFVPage vpage, Annotation annot) {

            }

            @Override
            public void OnSelectEnd(String text) {

            }

            @Override
            public void OnOpenURI(String uri) {

            }

            @Override
            public void OnOpenMovie(String path) {

            }

            @Override
            public void OnOpenSound(int[] paras, String path) {

            }

            @Override
            public void OnOpenAttachment(String path) {

            }

            @Override
            public void OnOpen3D(String path) {

            }
        };
        
        thumbContainer.removeAllViews();
        viewerContainer.removeAllViews();

        XmlPullParser parser = getResources().getXml(R.xml.radee_pdf_reader_attrs);
        AttributeSet attributes = Xml.asAttributeSet(parser);
        radeePdfReader = new PDFReader(mContext,attributes);
        viewerContainer.addView(radeePdfReader);

        m_thumb =  new PDFThumbView(mContext,attributes);
        thumbContainer.setVisibility(View.VISIBLE);
        thumbContainer.addView(m_thumb);

    }

        // build qPdf  viewer
    private void buildQPdfViewer(){
        thumbContainer.setVisibility(View.GONE);
        thumbContainer.removeAllViews();
        viewerContainer.removeAllViews();
        qPDFViewer = new PDFViewer(getActivity(),viewerContainer);

    }

    // build APV Pdf  viewer
    private void buildAPVViewer(){

        thumbContainer.setVisibility(View.GONE);
        thumbContainer.removeAllViews();
        viewerContainer.removeAllViews();

        sensorManager = null;

        SharedPreferences options = PreferenceManager.getDefaultSharedPreferences(getActivity());

        if (Options.setOrientation(getActivity())) {
            sensorManager = (SensorManager) getActivity().getSystemService(getActivity().SENSOR_SERVICE);
            if (sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER).size() > 0) {
                gravity[0] = 0f;
                gravity[1] = -9.81f;
                gravity[2] = 0f;
                gravityAge = 0;
                sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                        SensorManager.SENSOR_DELAY_NORMAL);
                this.prevOrientation = options.getInt(Options.PREF_PREV_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                getActivity().setRequestedOrientation(this.prevOrientation);
            }
            else {
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
        }

        history  = options.getBoolean(Options.PREF_HISTORY, true);
        boolean eink = options.getBoolean(Options.PREF_EINK, false);

        this.apvPagesView = new PagesView(getActivity());
        
        this.apvPagesView.setEink(eink);
        if (eink)
            getActivity().setTheme(android.R.style.Theme_Light);
        this.apvPagesView.setNook2(options.getBoolean(Options.PREF_NOOK2, false));

        if (options.getBoolean(Options.PREF_KEEP_ON, false))
            getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        else
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        actions = new Actions(options);
        this.apvPagesView.setActions(actions);

        // setZoomLayout(options);
        
        this.apvPagesView = new PagesView(getActivity());
        viewerContainer.addView(apvPagesView);

    }


    private void showPdfFileInWebView(File file) {
        Log.v(TAG, Utils.getMethodName() + "entry");

            // Log.d("SwA", "Current URL  1["+currentURL+"]");

            WebView webView = (WebView) getActivity().findViewById(R.id.webPage);

            // Aktiviere Javaskript
            webView.getSettings().setJavaScriptEnabled(true);

            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setUseWideViewPort(true);

            //Erlaube das heranzoomen
            webView.getSettings().setBuiltInZoomControls(true);
            webView.getSettings().setSupportZoom(true);

            //Schalte das Scrollbarbalken ab
            webView.setVerticalScrollBarEnabled(false);

            webView.getSettings().setLoadWithOverviewMode(true);
            // this call require API 16
            //  webView.getSettings().setAllowFileAccessFromFileURLs(true);
            webView.getSettings().setAllowFileAccess(true);
            webView.getSettings().setGeolocationEnabled(true);
            webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

            // Setze Webviewclient
            webView.setWebViewClient(new WebViewClient());
            webView.freeMemory();
            //webView.setWebChromeClient(new WebChromeClient());
            // webView.setDownloadListener(this);


            // SwAWebClient webClient = new SwAWebClient();
            /*
            wv.setWebViewClient(new WebViewClient() {
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    Toast.makeText(getActivity(), description + " (" + errorCode + ") " + failingUrl, Toast.LENGTH_SHORT).show();
                    Log.e("SwA", description + " (" + errorCode + ") " + failingUrl);
                }
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    Toast.makeText(getActivity(), url, Toast.LENGTH_SHORT).show();
                    return super.shouldOverrideUrlLoading(view, url);
                }
            });
            */

            // wv.setWebViewClient(new SwAWebClient());


            //File lFile = new File(Environment.getExternalStorageDirectory() + "/web/viewer.html");


            //File lFile = new File(Environment.getExternalStorageDirectory() + "/pdfjs/web/pdfviewer.html");
            //File lFile = new File(Environment.getExternalStorageDirectory() + "/pdfjs/examples/helloworld/index.html");
            File lFile = new File(Environment.getExternalStorageDirectory() + "/pdfjs/examples/acroforms/index.html");
            android.util.Log.e("SwA", "Current URL [" + lFile.getAbsolutePath() + "]");
            webView.loadUrl("file://" + lFile.getAbsolutePath());

            // webView.loadUrl("https://docs.google.com/gview?embedded=true&url="+"file://"+lFile.getAbsolutePath());
        

    }
    

    @Override
    public void onRequestFinished(Request request, Bundle resultData) {

        if (getActivity() == null)
            return;

        int result_code = resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);
        int requestType = request.getRequestType();
        String errorMessage = resultData.getString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE);

        cancelLoadingDialog();

        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " resultCode = "
                + result_code);

        switch (result_code) {

            case ResponseConstants.RESPONSE_CODE_OK:
            case ResponseConstants.RESPONSE_CODE_CREATED:
                break;
            case ResponseConstants.RESPONSE_CODE_UNKOWN_ERROR:
            case ResponseConstants.RESPONSE_CODE_BAD_REQUEST:
            case ResponseConstants.RESPONSE_CODE_ERROR:
            case ResponseConstants.RESPONSE_CODE_FORBIDDEN:
            case ResponseConstants.RESPONSE_CODE_INTERNAL_SERVER_ERROR:
            case ResponseConstants.RESPONSE_CODE_INTERNAL_SERVICE_UNAVAILABLE:
            case ResponseConstants.RESPONSE_CODE_METHOD_NOT_ALLOWED:
            case ResponseConstants.RESPONSE_CODE_NOT_FOUND:
            case ResponseConstants.RESPONSE_CODE_UNAUTHORIZED:
            case ResponseConstants.RESPONSE_CODE_PRECONDITION_FAILED:
            case ResponseConstants.RESPONSE_CODE_TOO_MANY_REQUESTS:
                if (errorMessage != null )
                    showErrorDialog(mContext, errorMessage);
                return;
            default: {
                showErrorDialog(mContext, "unbekannter Fehler");
                return;
            }
        }

        switch (requestType) {
            case RequestConstants.REQUEST_TYPE_DOCUMENT_GET_RENDITION_BY_VERSION:

                String renditionPath = resultData.getString(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                if (renditionPath == null) return;
                File renditionFile = new File(renditionPath);


                if (currPDFViewerType.equals(PDF_VIEWER_TYPE.QOPPA.toString())){
                    Log.d(TAG, Utils.getMethodName() + "loading " + renditionFile.toURI().getPath());
                    //intent refers to a local file
                    qPDFViewer.loadDocument(renditionFile.toURI().getPath());
                    PDFDocument qPDFDocument =  qPDFViewer.getDocument();
                    if (qPDFDocument != null)
                        pageNumber.setText(String.valueOf(qPDFViewer.getDocument().getPageCount()));


                } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.APV.toString())){

                    Options.setOrientation(getActivity());
                    SharedPreferences options = PreferenceManager.getDefaultSharedPreferences(mContext);

                    this.pdf = new PDF(new File(renditionPath), this.box);
                    this.pdf.setApplicationContext(getActivity().getApplicationContext());

                    if (!this.pdf.isValid()) {
                        Log.v(TAG, Utils.getMethodName() + "Invalid PDF");
                        if (this.pdf.isInvalidPassword()) {
                            Toast.makeText(mContext, "This file needs a password", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(mContext, "Invalid PDF file", Toast.LENGTH_SHORT).show();
                        }
                        return;
                    }
                    this.colorMode = Options.getColorMode(options);
                    this.pdfPagesProvider =
                            new PDFPagesProvider(getActivity(), pdf,
                                    options.getBoolean(Options.PREF_OMIT_IMAGES, false), options.getBoolean(
                                    Options.PREF_RENDER_AHEAD, true));
                    apvPagesView.setFocusable(true);
                    apvPagesView.setFocusableInTouchMode(true);
                    apvPagesView.setPagesProvider(pdfPagesProvider);
                    RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
                    lp.setMargins(5,5,5,5);
                    apvPagesView.setLayoutParams(lp);

                    //Bookmark b = new Bookmark(getActivity().getApplicationContext()).open();
                    //apvPagesView.setStartBookmark(b, filePath);
                    //b.close();

                    pageNumber.setText( "/" + this.pdfPagesProvider.getPageCount());

              
                } else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.RADEE.toString())){
                    // RadeePdfReader
                    pdfDoc = new com.radaee.pdf.Document();

                    int res =  pdfDoc.Open( renditionFile.getAbsolutePath(), null );
                    Log.d(TAG, Utils.getMethodName() + "doc " + document.getName()
                            + " renditionFile = " + renditionFile.getAbsolutePath() + " result " +res );
                    if (res < 0)
                    {
                        showErrorDialog(mContext, "Couldn't read rendition " + document.getName());
                        break;
                    }
                    pdfDoc.SetCache( Global.tmp_path + "/temp.dat" );//set temporary cache for editing.

                    radeePdfReader.PDFOpen(pdfDoc, false, radeeReaderlistener);
                    pageNumber.setText(String.valueOf("/" + String.valueOf(pdfDoc.GetPageCount())));
                    if (m_thumb != null) {
                        m_thumb.thumbOpen(pdfDoc, radeeThumblistener);
                        m_thumb.thumbGotoPage(currPageNumber - 1);
                    }
                    
      

                }         
                else if (currPDFViewerType.equals(PDF_VIEWER_TYPE.WEBVIEW.toString())){
                    File scrFile = new File(renditionPath);
                    File dstFile = new File(Environment.getExternalStorageDirectory() + "/pdfjs/examples/acroforms/rendition.pdf");

                    // delete it before copy
                    dstFile.delete();

                    try {
                        Utils.fileCopy(scrFile, dstFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    showPdfFileInWebView(dstFile);
                }
            break;

            case RequestConstants.REQUEST_TYPE_DOCUMENT_DOWNLOAD_FILE_BY_VERSION:
                Options.setOrientation(getActivity());

                String filePath = resultData.getString(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                
                if (filePath != null)
                {
                    String msg =" Document " + filePath +  " ver. " + currViewedVersion + " is saved in ";
                    Log.d(TAG, Utils.getMethodName() + msg);
                    Toast.makeText(getActivity().getApplicationContext(),msg,Toast.LENGTH_SHORT);
                    break;
                }
                break;

            case RequestConstants.REQUEST_TYPE_DOCUMENT_GET_FILE_BY_VERSION:
                // file is not PDF
                Options.setOrientation(getActivity());

                String file = resultData.getString(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);

                /*
                
                String image = "<html><body><center><img src=\""+file.substring(file.lastIndexOf("/")+1,file.length())+"\"/></center></body></html>";
                String filePathName = "file://"+ file.substring(0,file.lastIndexOf("/") + 1);
                Log.d(TAG, Utils.getMethodName() + file + " " + filePathName + " " +  image);
                
                // other to view image in webview container 
                WebView webView = new WebView(mContext);
                webView.loadDataWithBaseURL(filePathName,image, "text/html", "UTF-8","");
                //Erlaube das heranzoomen
                webView.getSettings().setBuiltInZoomControls(false);
                webView.getSettings().setSupportZoom(true);
                webView.getSettings().setDefaultZoom(ZoomDensity.FAR);
                webView.getSettings().setLightTouchEnabled(true);
                //Schalte das Scrollbarbalken ab
                webView.setVerticalScrollBarEnabled(false);
                webView.setHorizontalScrollBarEnabled(false);

                webView.getSettings().setLoadWithOverviewMode(true);
                webView.getSettings().setAllowFileAccess(true);
                webView.getSettings().setGeolocationEnabled(false);
                viewerContainer.addView(webView);
                 */



                XmlPullParser parser = getResources().getXml(R.xml.radee_pdf_reader_attrs);
                AttributeSet attributes = Xml.asAttributeSet(parser);
                Bitmap bitmap = BitmapFactory.decodeFile(file);

                ZoomableImageView touch =  new ZoomableImageView(mContext,attributes);
                touch.setImageBitmap(bitmap);

                thumbContainer.setVisibility(View.GONE);
                viewerContainer.addView(touch);

            case RequestConstants.REQUEST_TYPE_DOCUMENT_GET_COMMENTS:

                if (currentListType.equals(LIST_TYPE.COMMENT.toString())){

                    commentListAdapter.clear();
                    docCommentList.clear();
                    docCommentList =
                            resultData.getParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);

                    Log.d(TAG, Utils.getMethodName() + " docCommentList = " + docCommentList.toString());

                    // set empty comment at position 0
                    Comment comment = new Comment();
                    comment.setComment("");
                    comment.setOwnerId("");
                    comment.setId("");
                    comment.setCreationDate("");
                    commentListAdapter.insert(comment, 0);

                    commentListAdapter.addAll(docCommentList);
                    mListView.scrollTo(0, 0);
                    commentListAdapter.notifyDataSetChanged();

                }

                break;

            case RequestConstants.REQUEST_TYPE_DOCUMENT_GET_ACTIVITIES:

                if (currentListType.equals(LIST_TYPE.ACTIVITY.toString())){

                    docActivitiesList.clear();
                    this.docActivitiesList =
                            resultData.getParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);

                    Log.d(TAG, Utils.getMethodName() + " docActivitiesList.toString() = " + docActivitiesList.toString());
                    activityListAdapter.clear();
                    activityListAdapter.addAll(docActivitiesList);
                    activityListAdapter.notifyDataSetChanged();
                }

                break;
            case RequestConstants.REQUEST_TYPE_DOCUMENT_GET_VERSIONS:
                if (currentListType.equals(LIST_TYPE.VERSION.toString())){

                    docVersionList.clear();
                    this.docVersionList =
                            resultData.getParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);

                    versionListAdapter.clear();
                    versionListAdapter.addAll(docVersionList);
                    versionListAdapter.notifyDataSetChanged();
                }


                break;
            default:
                break;

        }

    }



    private void showChoosePdfContextMenu(final View v) {

        PopupMenu popupMenu = new PopupMenu(getActivity(), v);
        popupMenu.getMenuInflater().inflate(R.menu.choose_pdf_viewer_menu, popupMenu.getMenu());

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.APV_menu_item:
                                setCurrPDFViewerType(DocumentViewerFragment.PDF_VIEWER_TYPE.APV.toString());
                                buildAPVViewer();
                                break;
                            case id.QOPPA_menu_item:
                                setCurrPDFViewerType(DocumentViewerFragment.PDF_VIEWER_TYPE.QOPPA.toString());
                                buildQPdfViewer();
                                break;
                            case id.RADEE_menu_item:
                                setCurrPDFViewerType(DocumentViewerFragment.PDF_VIEWER_TYPE.RADEE.toString());
                                buildRadeePdfViewer();
                                break;
                            case id.WEBVIEW_menu_item:
                                setCurrPDFViewerType(DocumentViewerFragment.PDF_VIEWER_TYPE.WEBVIEW.toString());
                                break;
                            default:
                                return true;
                        }
                        getPdfOrFiletoView(document);

                        return true;
                    }
                });

        popupMenu.show();
    }
                
                
    
                /*
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.choose_pdf_viewer_menu, menu);

    }

    @Override
    public void onOptionsItemSelected(MenuItem item) {
        super.onPrepareOptionsMenu(item);

        switch (item.getItemId()) {
            case id.APV_menu_item:
                this.setCurrPDFViewerType(DocumentViewerFragment.PDF_VIEWER_TYPE.APV.toString());
                break;
            case id.QOPPA_menu_item:
                this.setCurrPDFViewerType(DocumentViewerFragment.PDF_VIEWER_TYPE.QOPPA.toString());
                break;
            case id.RADEE_menu_item:
                this.setCurrPDFViewerType(DocumentViewerFragment.PDF_VIEWER_TYPE.WEBVIEW.toString());
                break;
            case id.WEBVIEW_menu_item:
                this.setCurrPDFViewerType(DocumentViewerFragment.PDF_VIEWER_TYPE.RADEE.toString());
                break;
            default:

        }

        if (mTwoPane) {

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.document_viewer_fragment, viewerFragment).commit();

        }
        else {

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.document_fragment_container, viewerFragment).commit();
        }

    }


    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        // Set a different FRAGMENT_GROUPID on each fragment.
        // A simple check, only continues on the correct fragment.
        if(item.getGroupId() == FRAGMENT_GROUPID)
        {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
            selectedCategory = (BudgetCategory) cListView.getItemAtPosition(info.position);
            switch (item.getItemId())
            {
                case MENU_EDIT:
                    // Do something!
                    return true;
                case MENU_REMOVE:
                    // Do something!
                    return true;
            }
        }
        // Be sure to return false or super's so it will proceed to the next fragment!
        return super.onContextItemSelected(item);
    }
*/
    
    @Override
    public void onRequestConnectionError(Request request, int statusCode) {

    }

    @Override
    public void onRequestDataError(Request request) {

    }

    @Override
    public void onRequestServiceError(Request request, Bundle resultData) {

    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    /**
     * A dummy implementation of the {@link DocumentInfoActivityInterface} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private static DocumentInfoActivityInterface sDummyCallbacks = new DocumentInfoActivityInterface() {
        @Override
        public Document getCurrentDocument() {
            return null;
        }

        @Override
        public void onVersionClicked(Version version) {

        }


    };
  
}
