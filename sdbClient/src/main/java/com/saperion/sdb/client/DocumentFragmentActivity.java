package com.saperion.sdb.client;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.saperion.sdb.client.R.id;
import com.saperion.sdb.client.utils.Log;
import com.saperion.sdb.client.utils.Utils;
import com.saperion.sdb.sdk.models.Document;
import com.saperion.sdb.sdk.models.Version;

/**
 * An activity representing a list of Items. This activity has different
 * presentations for handset and tablet-size devices. On handsets, the activity
 * presents a list of items, which when touched, lead to a
 * {@link com.saperion.sdb.client.ItemListActivity} representing item details. On tablets, the activity
 * presents the list of items and item details side-by-side using two vertical
 * panes.
 */
public class DocumentFragmentActivity extends FragmentActivity implements DocumentInfoFragment.DocumentInfoActivityInterface {

    private static final String TAG = DocumentFragmentActivity.class.getSimpleName();

    public static final int PICK_FILE_REQUEST_CODE = 10;

    public static final String PARAM_DOCUMENT = "saperion.com.sdb.client.document";
    public static final String PARAM_DOCUMENT_VERSION = "saperion.com.sdb.client.param_version";

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;
    private FragmentHolder retainedFragments;
    private Document document; // current Document

    OnClickListener l = new OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };


    private class FragmentHolder {
        DocumentInfoFragment documentInfoFragment;
        DocumentViewerFragment documentViewerFragment;
        // more fragments...
    }
    
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // do nothing, just override
        Log.i(TAG, Utils.getMethodName() + "do nothing !");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        Log.v(TAG, Utils.getMethodName() + "entry");
        // Enable the option menu for the Fragment

        retainedFragments = (FragmentHolder)
                getLastCustomNonConfigurationInstance();
        if (retainedFragments == null) {
            retainedFragments = new FragmentHolder();
        }
        else
        {
            Log.v(TAG, Utils.getMethodName() + "retainedFragments != null");
        }
        
        Bundle bundle = getIntent().getExtras();
        this.document = bundle.getParcelable(PARAM_DOCUMENT);

        String mine_type = document.getMimeType();
        Log.v(TAG, com.saperion.sdb.sdk.utils.Utils.getMethodName() + "uri " + document.getFileAbsPath() + " mine type= " + mine_type);

        setContentView(R.layout.document_fragment_activity);

        // Create an instance of DocumentInfoFragment
        DocumentInfoFragment docInfoFragment = new DocumentInfoFragment();

        // In case this activity was started with special instructions createInstance an Intent,
        // pass the Intent's extras to the fragment as arguments
        docInfoFragment.setArguments(bundle);
        DocumentViewerFragment viewerFragment = new DocumentViewerFragment();
        viewerFragment.setArguments(bundle);


        
        if (findViewById(id.document_fragment_container) != null) {

            // However, if we're being restored createInstance a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                Log.v(TAG, Utils.getMethodName() + "savedInstanceState != null");
                return;
            }


            getSupportFragmentManager().beginTransaction()
                    .add(R.id.document_fragment_container, viewerFragment).commit();
            
            Log.v(TAG, Utils.getMethodName() + "create views");
          /*  // Add the doc info fragment to the 'document_fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.document_fragment_container, docInfoFragment).commit();
            */

            

        }
        
        else{
            // The detail container view will be present only in the
            // large-screen layouts (res/values-large and
            // res/values-sw600dp). If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
            
            /*
            
            // Add the doc info fragment to the 'document_info_fragment' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.document_info_fragment, docInfoFragment).commit();


            DocumentViewerFragment viewerFragment = new DocumentViewerFragment();

            viewerFragment.setArguments(bundle);

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.document_viewer_fragment, viewerFragment).commit(); 
                   

            viewerFragment =  (DocumentViewerFragment) getSupportFragmentManager().findFragmentById(R.id.document_viewer_fragment);

            if (viewerFragment != null) {

                viewerFragment = new DocumentViewerFragment();
                viewerFragment.setArguments(bundle);

                getSupportFragmentManager().beginTransaction().add
                  (R.id.document_viewer_fragment, viewerFragment).commit();
            }
             */

        }

        ActionBar actionBar = getActionBar();
        if (actionBar!=null){
            Drawable bgColor = getResources().getDrawable(R.color.grey);
            actionBar.setTitle( "" );
            actionBar.setBackgroundDrawable(bgColor);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        }
        
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {

        Log.v(TAG, com.saperion.sdb.sdk.utils.Utils.getMethodName() + "entry");
        // Save the current  selection in case we need to recreate the fragment
        outState.putParcelable(PARAM_DOCUMENT, this.document);

 

        super.onSaveInstanceState(outState);
    }
    
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.v(TAG, Utils.getMethodName() + "entry");
        if (savedInstanceState != null){

            this.document = savedInstanceState.getParcelable(PARAM_DOCUMENT);

        }
    }



    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        Log.v(TAG, Utils.getMethodName() + "entry");
        return retainedFragments;
    }
    
    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        Log.v(TAG, Utils.getMethodName() + "entry");

        // we need this here because when a configuration change happens,
        // onAttachFragment is called *before* onCreate
        retainedFragments = (FragmentHolder)
                getLastCustomNonConfigurationInstance();
        if (retainedFragments == null) {
            retainedFragments = new FragmentHolder();
        }

        // repeat this 'if' clause for all Fragments
        if (fragment instanceof DocumentInfoFragment) {
            retainedFragments.documentInfoFragment = (DocumentInfoFragment) fragment;
            Log.v(TAG, Utils.getMethodName() + " documentInfoFragment attached");
        }
        
        if (fragment instanceof DocumentViewerFragment) {
            retainedFragments.documentViewerFragment = (DocumentViewerFragment) fragment;
            Log.v(TAG, Utils.getMethodName() + " DocumentViewerFragment attached");

        }
        // set remaining fragments into holder object...
    }
    
    
    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.v(TAG, Utils.getMethodName() + "entry");
    }





    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.v(TAG, Utils.getMethodName() + "entry ");

        if (mTwoPane) {
            if (resultCode == Activity.RESULT_OK) {
                String filepath = data.getStringExtra("file_path");
                Toast.makeText(this, filepath, Toast.LENGTH_SHORT).show();


            }
        }
    }


    @Override
    public Document getCurrentDocument() {
        return this.document;
    }

    @Override
    public void onVersionClicked(Version version) {
        if (version == null)
            return;

        Log.v(TAG, Utils.getMethodName() + "selected doc  " + this.document.getName() + " version  " + version.getNumber());
       
       //  this.document = version.getDocument(this.getApplicationContext());
       
        Bundle bundle = new Bundle();
        bundle.putParcelable(PARAM_DOCUMENT,  this.document);
        bundle.putInt(PARAM_DOCUMENT_VERSION, version.getNumber());

        DocumentViewerFragment viewerFragment = new DocumentViewerFragment();

        viewerFragment.setArguments(bundle);
        
        if (mTwoPane) {
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.document_viewer_fragment, viewerFragment).commit(); 

        }
        else {
            
               getSupportFragmentManager().beginTransaction()
                .replace(R.id.document_fragment_container, viewerFragment).commit();
        }
        
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.v(TAG, Utils.getMethodName() + "entry ");

        Bundle bundle = getIntent().getExtras();
        this.document = bundle.getParcelable(PARAM_DOCUMENT);

        String mine_type = document.getMimeType();
        Log.v(TAG, com.saperion.sdb.sdk.utils.Utils.getMethodName() + "uri " + document.getFileAbsPath() + " mine type= " + mine_type);

        
        Bundle arguments = new Bundle();
        arguments.putParcelable(PARAM_DOCUMENT, document);
        arguments.putInt(PARAM_DOCUMENT_VERSION, document.getVersionNumber());

        /*
        DocumentInfoFragment docInfoFragment = null;
        if (retainedFragments.documentInfoFragment != null)
            docInfoFragment = retainedFragments.documentInfoFragment;
        else
        {
            docInfoFragment = new DocumentInfoFragment();

            docInfoFragment.setArguments(arguments);
                            // Add the doc info fragment to the 'document_info_fragment' FrameLayout
            getSupportFragmentManager().beginTransaction()
                .replace(R.id.document_info_fragment, docInfoFragment).commit();
        }
        

        DocumentViewerFragment viewerFragment = null;
        if (retainedFragments.documentViewerFragment != null)
            viewerFragment = retainedFragments.documentViewerFragment;
        else
            viewerFragment = new DocumentViewerFragment();

        viewerFragment.setArguments(bundle);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.document_viewer_fragment, viewerFragment).commit();
                */

    }
    
}
