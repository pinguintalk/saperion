package com.saperion.sdb.client;


import com.saperion.sdb.sdk.models.Comment;
import com.saperion.sdb.sdk.models.Document;

public interface DocumentInfoListViewInterface {
    /**
     * Callback for when an item has been selected.
     */

    public void onCommentAdded(String comment);

    public void onVersionClicked(Document document);

    public void onCommentUpdate(Comment currEditedComment);
}
