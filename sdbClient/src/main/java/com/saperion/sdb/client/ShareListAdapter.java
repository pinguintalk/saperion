package com.saperion.sdb.client;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saperion.sdb.client.utils.Log;
import com.saperion.sdb.sdk.models.Share;
import com.saperion.sdb.sdk.rights.ShareRight;
import com.saperion.sdb.sdk.utils.Utils;

import java.util.ArrayList;
import java.util.Collection;

public class ShareListAdapter extends ArrayAdapter<Share> {

    private static final String TAG = ShareListAdapter.class.getSimpleName();

    private static LayoutInflater mInflater = null;

    private Activity activity;

    private ShareListViewInterface mCallback;

    ArrayList<Share> subItems;
    ArrayList<Share> allItems;
    
    public ShareListAdapter(Activity context, ShareListViewInterface shareListViewCallbacks,
                            ArrayList<Share> shareList) {
        super(context, 0, shareList);
        this.activity = context;
        this.mCallback = shareListViewCallbacks;
        this.subItems = shareList;
        this.allItems = this.subItems;
        
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Log.v(TAG, Utils.getMethodName() + "entry ");
        View view = convertView;

        Share item = getItem(position);
      
        if (convertView == null) {

            view = mInflater.inflate(R.layout.share_cell, null);
            view.setTag(new ViewHolder(view,activity));
        }
        ((ViewHolder) view.getTag()).populateView(item);

       
        
        return view;

    }


    public void updateData(View mView, Share share) {
        if (mView != null) {
            TextView displayNameTV = (TextView) mView.findViewById(R.id.user_display_name);

            displayNameTV.setText(share.getReceiverName());

            ImageView mImageViewShare = (ImageView) mView.findViewById(R.id.iv_can_share);
            ImageView mImageViewEditShare = (ImageView) mView.findViewById(R.id.iv_can_edit);


            if(share.getRights().contains(ShareRight.SHARE))
                mImageViewShare.setAlpha(0.9f);
            else
                mImageViewShare.setAlpha(0.4f);

            if(share.getRights().contains(ShareRight.WRITE))
                mImageViewEditShare.setAlpha(0.9f);
            else
                mImageViewEditShare.setAlpha(0.4f);

        }
    }
    

    class ViewHolder {

        private TextView mTextViewUserDisplayName;


        Activity activity;
        private View mView = null;
        private LinearLayout settingButtonSet;
        private LinearLayout editButtonSet;
        private LinearLayout imageViewSetShareRights;
        
        private ImageButton mButtonDeleteShare;
        private ImageButton mButtonOk;
        private ImageButton mButtonEdit;
        private ImageButton mButtonCancel;

        private ImageButton mButtonEditableFlag;
        private ImageButton mButtonShareableFlag;

        private ImageView mImageViewShareable;
        private ImageView mImageViewEditable;

        Drawable btn_selected_drawable;
        Drawable btn_not_selected_drawwable;
        
        private Share mCurrShare = null;

        public ViewHolder(View view,Activity parent_activity) {
            this.mView = view;
            this.activity = parent_activity;
            mTextViewUserDisplayName = (TextView) mView.findViewById(R.id.user_display_name);

            btn_selected_drawable = mView.getResources().getDrawable(R.drawable.fns_bg_orange_shape_no_stroke);
            btn_not_selected_drawwable  = mView.getResources().getDrawable(R.drawable.fns_bg_grey_shape_no_stroke);
            
            mButtonDeleteShare = (ImageButton) mView.findViewById(R.id.b_delete_share);
            mButtonDeleteShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.v(TAG, Utils.getMethodName() + "mButtonDeleteShare " + mCurrShare.toString());
                    if (mCurrShare != null) {
                        mCallback.onDeleteShare(mCurrShare);
                    }

                }
            });

            mButtonEdit = (ImageButton) mView.findViewById(R.id.b_edit);
            mButtonEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.v(TAG, Utils.getMethodName() + "mButtonEdit ");
                    showEditMode(v);
                }
            });
            

            mButtonOk = (ImageButton) mView.findViewById(R.id.b_ok);
            mButtonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.v(TAG, Utils.getMethodName() + "mButtonOk ");


                    Collection<ShareRight> rights = mCurrShare.getRights();
                            
                    if (mCurrShare != null) {

                        if (mButtonShareableFlag.getBackground().equals(btn_selected_drawable)) {
      
                            rights.add(ShareRight.SHARE);
                        } else {                 
                            rights.remove(ShareRight.SHARE);
                            if (mButtonEditableFlag.getBackground().equals(btn_selected_drawable)) {

                                rights.add(ShareRight.WRITE);
                            } else {

                                rights.remove(ShareRight.WRITE);
                            }
                        }


                        Log.v(TAG, Utils.getMethodName() + "rights " +rights.toString());
                        
                        mCurrShare.setRights(rights);
                        mCallback.onUpdateShare(mCurrShare);
                    }

                }
            });

            mButtonCancel = (ImageButton) mView.findViewById(R.id.b_cancel);
            mButtonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.v(TAG, Utils.getMethodName() + "mButtonCancel ");
                    populateView(mCurrShare);
                }
            });

            settingButtonSet = (LinearLayout) mView.findViewById(R.id.share_setting_button_set);
            settingButtonSet.setVisibility(View.GONE );

            editButtonSet = (LinearLayout) mView.findViewById(R.id.share_edit_button_set);

            imageViewSetShareRights =(LinearLayout) mView.findViewById(R.id.share_setting_image_set);


            mButtonEditableFlag = (ImageButton) mView.findViewById(R.id.b_editable);
            mButtonEditableFlag.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.v(TAG, Utils.getMethodName() + "mButtonEditableFlag ");


                    if (!mButtonShareableFlag.getDrawable().equals(btn_selected_drawable)) {

                        if (mButtonEditableFlag.getDrawable().equals(btn_selected_drawable)) {

                            mButtonEditableFlag.setBackgroundResource(R.drawable.fns_bg_grey_shape_no_stroke);
                        } else {
                            mButtonEditableFlag.setBackgroundResource(R.drawable.fns_bg_orange_shape_no_stroke);
                        }
                    }
                    

                }
            });
            
            mButtonShareableFlag = (ImageButton) mView.findViewById(R.id.b_shareable);
            mButtonShareableFlag.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mButtonShareableFlag.getDrawable().equals(btn_selected_drawable)) {

                        Log.v(TAG, Utils.getMethodName() + "mButtonShareableFlag 1");
                        
                        mButtonShareableFlag.setBackgroundResource(R.drawable.fns_bg_grey_shape_no_stroke);
                    } else {

                        Log.v(TAG, Utils.getMethodName() + "mButtonShareableFlag 2");
                        mButtonShareableFlag.setBackgroundResource(R.drawable.fns_bg_orange_shape_no_stroke);
                    }
                }
            });

            mImageViewShareable = (ImageView) mView.findViewById(R.id.iv_can_share);
            mImageViewEditable  = (ImageView) mView.findViewById(R.id.iv_can_edit);


        }
        
 
        public void populateView(Share share) {
            Log.v(TAG, Utils.getMethodName() + "entry ");
            mCurrShare = share;
            mTextViewUserDisplayName.setText(share.getReceiverName());

            mButtonEdit.setVisibility(View.VISIBLE);
            editButtonSet.setVisibility(View.GONE);

            imageViewSetShareRights.setVisibility(View.VISIBLE);
            settingButtonSet.setVisibility(View.GONE);
            
            if(mCurrShare.getRights().contains(ShareRight.SHARE))
                mImageViewShareable.setAlpha(1f);
            else
                mImageViewShareable.setAlpha(0.4f);

            if(mCurrShare.getRights().contains(ShareRight.WRITE))
                mImageViewEditable.setAlpha(1f);
            else
                mImageViewEditable.setAlpha(0.4f);



        }

        public void showEditMode(View v) {

            Log.v(TAG, Utils.getMethodName() + "mButtonEdit ");

            mButtonEdit.setVisibility(View.GONE);
            editButtonSet.setVisibility(View.VISIBLE);

            imageViewSetShareRights.setVisibility(View.GONE);
            settingButtonSet.setVisibility(View.VISIBLE);


            if(mCurrShare.getRights().contains(ShareRight.SHARE))
                mButtonShareableFlag.setBackgroundResource(R.drawable.fns_bg_orange_shape_no_stroke);
            else
                mButtonShareableFlag.setBackgroundResource(R.drawable.fns_bg_grey_shape_no_stroke);

            if(mCurrShare.getRights().contains(ShareRight.WRITE))
                mButtonEditableFlag.setBackgroundResource(R.drawable.fns_bg_orange_shape_no_stroke);
            else
                mButtonEditableFlag.setBackgroundResource(R.drawable.fns_bg_grey_shape_no_stroke);


        }

    }


    

    @Override
    public int getCount() {
        return subItems.size();
    }

    
    public interface ShareListViewInterface {
        /**
         * Callback for when an item has been selected.
         */

        public void onDeleteShare(Parcelable shareParcelabl);


        public void onUpdateShare(Parcelable shareParcelabl);
    }


}
