package com.saperion.sdb.client;

/**
 * Created by nnn on 13.09.13.
 */

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;

import com.saperion.sdb.client.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;



public class PullDownListView extends ListView implements OnScrollListener {

    private static final String TAG = PullDownListView.class.getSimpleName();
    
    private ListViewTouchEventListener mTouchListener;
    private boolean pulledDown;
    public static int maxDiff = 20;

    public PullDownListView(Context context) {
        super(context);
        
        init();
    }

    public PullDownListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PullDownListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        Log.v(TAG, Utils.getMethodName() + "entry");
        setOnScrollListener(this);
    }

    private float lastY;
    private float diff;
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            Log.v(TAG, Utils.getMethodName() + "MotionEvent.ACTION_DOWN");
            lastY = ev.getRawY();
        } else if (ev.getAction() == MotionEvent.ACTION_MOVE) {
            
           // Log.v(TAG, Utils.getMethodName() + "MotionEvent.ACTION_MOVE");
     
            float newY = ev.getRawY();
            diff = (newY - lastY);

            if(diff > 0 ){
               
                setPulledDown(true);
                // show animation
                Log.v(TAG, Utils.getMethodName() + "newY " + newY  + " lastY " +lastY+ " diff = " +diff);
                postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isPulledDown()) {
                            if (mTouchListener != null) {
                                mTouchListener.onListViewPullingDown(diff);
                                setPulledDown(false);
                            }
                        }
                    }
                }, 0);

                /*
                postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isPulledDown()) {
                            if (mTouchListener != null) {
                                mTouchListener.onListViewPulledDown();
                                setPulledDown(false);
                            }
                        }
                    }
                }, 500);*/
                
                
            }

            lastY = newY;
        } else if (ev.getAction() == MotionEvent.ACTION_UP) {
            Log.v(TAG, Utils.getMethodName() + "MotionEvent.ACTION_UP");

            if (isPulledDown()){
                postDelayed(new Runnable() {
                    @Override
                    public void run() {
                      
                            if (mTouchListener != null) {
                                mTouchListener.onListViewTouchUp(diff);
                            }
                    }
                }, 200);
            }
            else {
                super.dispatchTouchEvent(ev);
                postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (mTouchListener != null) {
                            mTouchListener.onListViewResetPosition();
                        }
                    }
                }, 500);

            }

            lastY = 0;
           
        }

        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {
        //Log.v(TAG, Utils.getMethodName() + "entry");
        setPulledDown(false);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

        Log.v(TAG, Utils.getMethodName() + "  entry");
    }

    public interface ListViewTouchEventListener {
        public void onListViewPullingDown(float move_distance);
        public void onListViewPulledDown();
        public void onListViewTouchUp(float move_distance);
        public void onListViewResetPosition();

    }

    public void setListViewTouchListener(
            ListViewTouchEventListener touchListener) {
        this.mTouchListener = touchListener;
        
        
    }

    public ListViewTouchEventListener getListViewTouchListener() {
        return mTouchListener;
    }

    public boolean isPulledDown() {
        return pulledDown;
    }

    public void setPulledDown(boolean pulledDown) {
        this.pulledDown = pulledDown;
    }
}
