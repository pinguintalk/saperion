package com.saperion.sdb.client;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.saperion.sdb.client.utils.Log;
import com.saperion.sdb.sdk.SdbApplication;
import com.saperion.sdb.sdk.config.WSConfig;
import com.saperion.sdb.sdk.models.ErrorMessage;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.RequestManager.RequestListener;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.client.utils.Utils;

/**
 * Created by nnn on 14.11.13.
 */
public class LoginTabFragment extends Fragment implements RequestListener {

    private static final String TAG = LoginTabFragment.class.getSimpleName();

    public static final String INTENT_PARAM_CONFIG_LINK = "com.saperion.sdb.client.config_link";
    /** (non-Javadoc)
     * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
     */

    private Button btnLogin;
    private EditText txtServer;
    private EditText txtEmail;
    private EditText txtPassword;
    private CheckBox checkBoxStoreEmailAndPassword;
    protected ProgressDialog progressDialog = null;
    private AccountManager mAccountManager;

    private String mServerUrl;
    private String mUsername;
    private String mPassword;

    protected boolean mRequestNewAccount = true;
    private Account account = null;
    
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container == null) {
            // We have different layouts, and in one of them this
            // fragment's containing frame doesn't exist.  The fragment
            // may still be created createInstance its saved state, but there is
            // no reason to try to create its view hierarchy because it
            // won't be displayed.  Note this is not needed -- we could
            // just run the code below, where we would create and return
            // the view hierarchy; it would just never be used.
            return null;
        }
        return (RelativeLayout) inflater.inflate(R.layout.tab_login_layout, container, false);
    }

    @Override
    public void onPause() {
        super.onPause();

        if (progressDialog != null) {
            progressDialog.cancel();
            progressDialog = null;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (progressDialog != null) {
            progressDialog.cancel();
            progressDialog = null;
        }

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        txtServer = (EditText) getActivity().findViewById(R.id.txtDisplayName);
        txtEmail = (EditText) getActivity().findViewById(R.id.etEmail);
        txtPassword = (EditText) getActivity().findViewById(R.id.txtPassword);

        checkBoxStoreEmailAndPassword = (CheckBox) getActivity().findViewById(R.id.checkBoxStoreEmailAndPassword);
        checkBoxStoreEmailAndPassword.setChecked(true);

        Intent intent = getActivity().getIntent();
        // check if this intent is started via custom scheme link
        String url = "";
        String username = "";
        String link = intent.getStringExtra(INTENT_PARAM_CONFIG_LINK);
        
        if (link != null && !TextUtils.isEmpty(link)) {
            String[] linkStr = link.split("\\?");
            url = linkStr[2];
            username = linkStr[3];
        }


        if (url != null && !TextUtils.isEmpty(url)) {
            txtServer.setText(url);
            if (username != null && !TextUtils.isEmpty(username))
                txtEmail.setText(username);
        } else {
            txtServer.setText(SdbApplication.getInstance().getServerUrl());
            txtEmail.setText(SdbApplication.getInstance().getUserName());
            txtPassword.setText(SdbApplication.getInstance().getPassword());

        }

        btnLogin = (Button) getActivity().findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                loginButtonClicked(false);
            }
        });

    }

    /* Login Button onClick Handler */
    private void loginButtonClicked(boolean kickOtherSessions) {

        login();
    }

    private void login() {
        Log.v(TAG, Utils.getMethodName() + " entry");
        
        showLoadingDialog();

        mUsername = txtEmail.getText().toString();
        mPassword = txtPassword.getText().toString();
        mServerUrl = txtServer.getText().toString() +  WSConfig.WS_API_VERSION;

        SdbApplication.getInstance().login(mServerUrl, mUsername, mPassword, this);
    }

    @Override
    public void onRequestFinished(Request request, Bundle resultData) {
        cancelLoadingDialog();
        int result_code = resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);

        Log.v(TAG, "Login result_code: " + result_code);
        if (result_code == ResponseConstants.RESPONSE_CODE_OK)
            Utils.showMessageDialog(getActivity(), "Login Message", "You are logged in");

        else
            showServiceErrorDialog("Login failed");

    }

    @Override
    public void onRequestConnectionError(Request request, int statusCode) {
        cancelLoadingDialog();

    }

    @Override
    public void onRequestDataError(Request request) {
        cancelLoadingDialog();

    }

    @Override
    public void onRequestServiceError(Request request, Bundle resultData) {

        cancelLoadingDialog();
        int resp_code = resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);

        ErrorMessage msg = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE);

        int requestType = request.getRequestType();
        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " resultCode = "
                + resp_code + "errorMessage " + msg.getMessage());

        showServiceErrorDialog(msg.getMessage());
    }

    public void cancelLoadingDialog() {

        if (progressDialog != null) {
            progressDialog.cancel();
            progressDialog = null;
        }
    }

    public void showLoadingDialog() {

        if (progressDialog != null) {
            progressDialog.cancel();
            progressDialog = null;
        }

        progressDialog = new ProgressDialog(this.getActivity());

        progressDialog.setTitle(R.string.progress_dialog_title);
        progressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        progressDialog.setCancelable(true);
        progressDialog.setIndeterminate(true);

        progressDialog.show();
    }


    private void showServiceErrorDialog(String message) {
        if(this.getActivity() == null) return;
            AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());

        builder.setTitle(R.string.dialog_service_error_title);
        builder.setMessage(message);

        builder.setPositiveButton(R.string.dialog_error_button_close_txt, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do do my action here
                dialog.dismiss();
            }

        });

        AlertDialog alert = builder.create();
        alert.show();

    }


}
