package com.saperion.sdb.client;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.saperion.sdb.client.utils.Utils;
import com.saperion.sdb.client.utils.Log;
import com.saperion.sdb.sdk.SdbApplication;
import com.saperion.sdb.sdk.models.Space;

/**
 * An activity representing a single Item detail screen. This activity is only
 * used on handset devices. On tablet-size devices, item details are presented
 * side-by-side with a list of items in a {@link MainFragmentActivity}.
 * <p/>
 * This activity is mostly just a 'shell' activity containing nothing more than
 * a {@link ItemListFragment}.
 */
public class ItemListActivity extends FragmentActivity {

    private static final String TAG = ItemListActivity.class.getSimpleName();
    private String fileToUpload = "";
    public FragmentInterface fragmentInterface;

    public String getFileToUpload() {
        return fileToUpload;
    }

    public void setFileToUpload(String fileToUpload) {
        this.fileToUpload = fileToUpload;
    }

    public interface FragmentInterface {
        public void passDataToFragment(String someValue);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);

        ActionBar actionBar = getActionBar();
        if (actionBar!=null){
            Drawable bgColor = getResources().getDrawable(R.color.grey);
            actionBar.setTitle( "" );
            actionBar.setBackgroundDrawable(bgColor);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(R.layout.actionbar_view);

            View actionBarView = actionBar.getCustomView();

            Spinner dropdownMenu = (Spinner) actionBarView.findViewById(R.id.dropdown_list);

            dropdownMenu.setVisibility(View.GONE);

            ActionBar.LayoutParams lp = new ActionBar.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
            lp.gravity = Gravity.RIGHT;
            actionBarView.setLayoutParams(lp);

            ImageButton buttonSettings = (ImageButton) actionBarView.findViewById(R.id.b_settings);
            buttonSettings.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.v(TAG, com.saperion.sdb.client.utils.Utils.getMethodName() + "menu_settings");

                    Intent newIntent = new Intent(v.getContext(), LoginActivity.class);
                    startActivity(newIntent);

                }
            });

            ImageButton buttonWatchRecycled = (ImageButton) actionBarView.findViewById(R.id.b_watch_recycle);
            buttonWatchRecycled.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (SdbApplication.getInstance().isShowRecycledItems()) {
                        SdbApplication.getInstance().setShowRecycle(false);

                    } else {
                        SdbApplication.getInstance().setShowRecycle(true);
                    }

                    SpacesFragment spaceFragment =
                            (SpacesFragment) getSupportFragmentManager().findFragmentByTag(SpacesFragment.class.getSimpleName());
                    if (spaceFragment != null)
                        spaceFragment.getSpaces();


                    ItemListFragment itemFragment = (ItemListFragment) getSupportFragmentManager()
                            .findFragmentByTag(ItemListFragment.class.getSimpleName());

                    if (itemFragment != null) {
                        itemFragment.getChildren();
                    }
                }
            });

        }

        // savedInstanceState is non-null when there is fragment state
        // saved createInstance previous configurations of this activity
        // (e.g. when rotating the screen createInstance portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.

            Bundle bundle = getIntent().getExtras();

            Space space = bundle.getParcelable(ItemListFragment.SPACE);
            if (space != null)
                Log.v(TAG, Utils.getMethodName() + "showing space  " + space.getName());

            // forward bundle to details fragment
            ItemListFragment fragment = new ItemListFragment();
            fragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.item_detail_container, fragment).commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This ID represents the Home or Up button. In the case of this
                // activity, the Up button is shown. Use NavUtils to allow users
                // to navigate up one level in the application structure. For
                // more details, see the Navigation pattern on Android Design:
                //
                // http://developer.android.com/design/patterns/navigation.html#up-vs-back
                //
                NavUtils.navigateUpTo(this, new Intent(this, MainFragmentActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onItemClicked(View view) {

        Log.v(TAG, Utils.getMethodName() + "view : ");

        TextView name = (TextView) findViewById(R.id.item_name);

        Log.d(TAG, Utils.getMethodName() + name);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.v(TAG, Utils.getMethodName() + "entry ");

        if (resultCode == Activity.RESULT_OK) {
            String filepath = data.getStringExtra("file_path");
            Toast.makeText(this, filepath, Toast.LENGTH_SHORT).show();


            if (fragmentInterface == null) return;
            if (filepath == null) return;
            // pass file to upload to fragment 
            fragmentInterface.passDataToFragment(filepath);


        }
    }


}
