package com.saperion.sdb.client;

public final class Constants {
    public static final String SPACE_TYPE = "space";
    public static final String FOLDER_TYPE = "folder";
    public static final String DOCUMENT_TYPE = "document";

    /**
     * Account type string.
     */
    public static final String ACCOUNT_TYPE = "com.saperion.sdb";

    /**
     * Authtoken type string.
     */
    public static final String AUTHTOKEN_TYPE = "com.saperion.sdb";

    public static final String ITENT_ACTION_LOGIN = "com.saperion.sdb.client.action_login";

    /* mine types */
    public static final String MINE_TYPE_PDF = "document";

    public static final float TRANSPARENT_GRADE = 0.3f;
}
