package com.saperion.sdb.client;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Parcelable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.saperion.sdb.client.utils.Log;
import com.saperion.sdb.sdk.models.Link;
import com.saperion.sdb.sdk.models.User;
import com.saperion.sdb.sdk.rights.UserRight;
import com.saperion.sdb.sdk.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

public class LinkListAdapter extends ArrayAdapter<Link> {

    private static final String TAG = LinkListAdapter.class.getSimpleName();

    private static LayoutInflater mInflater = null;
    private int mLayoutType = 1;
    private Activity activity;
    private int mCurrSelectedItemPosition = -1;
    private String mCurrentSelectedItemId = null;

    private View mCurrentSelectedView = null;
    private LinkListViewInterface mCallback;

    private boolean isInEditMode = false;
    private boolean showRecycledItems = false;
    ArrayList<Link> subItems;
    ArrayList<Link> allItems;
    
    public LinkListAdapter(Activity context, LinkListViewInterface linkListViewCallbacks,
                           ArrayList<Link> linkList) {
        super(context, 0, linkList);
        this.activity = context;
        this.mCallback = linkListViewCallbacks;
        this.subItems = linkList;
        this.allItems = this.subItems;
        
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    
    public boolean isInEditMode() {
        return isInEditMode;
    }

    public void setInEditMode(boolean isInEditMode) {
        this.isInEditMode = isInEditMode;
    }


    

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Log.v(TAG, Utils.getMethodName() + "entry ");
        View view = convertView;

        Link item = getItem(position);
      
        if (convertView == null) {

            view = mInflater.inflate(R.layout.link_cell, null);
            view.setTag(new ViewHolder(view,activity));

            ((ViewHolder) view.getTag()).populateView(item);

        } else {
            ((ViewHolder) view.getTag()).populateView(item);

        }

        
        return view;

    }


    public void updateData(View mView, User user) {
        if (mView != null) {
            TextView displayNameTV = (TextView) mView.findViewById(R.id.user_display_name);
            TextView firstName =
                    (TextView) mView.findViewById(R.id.user_first_name);
            TextView lastName = (TextView) mView.findViewById(R.id.user_last_name);

            displayNameTV.setText(user.getDisplayname());

            firstName.setText(user.getFirstname());
            lastName.setText(user.getLastname());


            ImageView mImageViewShare = (ImageView) mView.findViewById(R.id.iv_share_user);
            ImageView mImageViewInviteUser = (ImageView) mView.findViewById(R.id.iv_invite_user);
            ImageView mImageViewManageUser = (ImageView) mView.findViewById(R.id.iv_manage_user);


            if(user.getRights().contains(UserRight.SHARE))
                mImageViewShare.setAlpha(0.9f);
            else
                mImageViewShare.setAlpha(0.4f);

            if(user.getRights().contains(UserRight.INVITE))
                mImageViewInviteUser.setAlpha(0.9f);
            else
                mImageViewInviteUser.setAlpha(0.4f);

            if(user.getRights().contains(UserRight.MANAGE))
                mImageViewManageUser.setAlpha(0.9f);
            else
                mImageViewManageUser.setAlpha(0.4f);
        }
    }
    

    class ViewHolder {

        private TextView mTextViewName;
        private TextView mTextViewAccessCount;
        private TextView mTexViewValueDate;

        Activity activity;
        private View mView = null;

        Point p;
        private ImageButton mButtonDeleteLink;
        private ImageButton mButtonShowLinkUrl;


        private Link mCurrLink = null;
    

        
        public ViewHolder(View view,Activity parent_activity) {
            this.mView = view;
            this.activity = parent_activity;
            mTextViewName = (TextView) mView.findViewById(R.id.tv_link_name);
            mTextViewAccessCount = (TextView) mView.findViewById(R.id.tv_link_access_count);
            mTexViewValueDate = (TextView) mView.findViewById(R.id.tv_link_valid_until);

            
            mButtonDeleteLink = (ImageButton) mView.findViewById(R.id.b_link_delete);
            mButtonDeleteLink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.v(TAG, Utils.getMethodName() + "mButtonDeleteLink " + mCurrLink.toString());

                    showYesNoDialogWhileDeleteLink("Delete Link", "Do you want to delete this link ?" , mCurrLink);

                }
            });
            

            mButtonShowLinkUrl = (ImageButton) mView.findViewById(R.id.b_show_link_url);
            mButtonShowLinkUrl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.v(TAG, Utils.getMethodName() + "mButtonShowLinkUrl ");
                    // showLinkPopup(activity, p, mCurrLink.getUrl());
                    com.saperion.sdb.client.utils.Utils.copyTextToClipBoard(getContext(), mCurrLink.getUrl());

                    Toast.makeText(getContext(), "document link " + mCurrLink.getUrl() + "is copied to clipboard", Toast.LENGTH_LONG).show();
                }
            });

            int[] location = new int[2];

            // Get the x, y location and store it in the location[] array
            // location[0] = x, location[1] = y.
            mButtonShowLinkUrl.getLocationOnScreen(location);

            //Initialize the Point with x, and y positions
            p = new Point();
            p.x = location[0];
            p.y = location[1];
        }
        
 
        public void populateView(Link link) {
            Log.v(TAG, Utils.getMethodName() + "entry ");
            mCurrLink = link;
            mTextViewName.setText(link.getName());
           

            boolean tabletSize = this.activity.getResources().getBoolean(R.bool.isTablet);
            if (tabletSize) {
                mTextViewAccessCount.setText(link.getCurrentAccessCount() +" of " + link.getAccessLimit());
            } else {
                mTextViewAccessCount.setText(link.getCurrentAccessCount() +" of " + link.getAccessLimit() + " Hits");
            }
            
            TimeZone utc = TimeZone.getTimeZone("UTC");
            SimpleDateFormat input_date_format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
            input_date_format.setTimeZone(utc);

            SimpleDateFormat output_date_format = new SimpleDateFormat("dd/MM/yyyy");
            try {
                Date expiredDay = input_date_format.parse(link.getExpirationDate());
                String dateInString = output_date_format.format(expiredDay);

                if (tabletSize) {
                    mTexViewValueDate.setText(dateInString);
                } else {
                    mTexViewValueDate.setText("valid until " + dateInString);
                }
           
            } catch (ParseException e) {
                e.printStackTrace();
                Log.e(TAG, Utils.getMethodName() + "error  " + e.toString());
            }
        }

    }

    private void showYesNoDialogWhileDeleteLink(String title,String message,final Link link) {

        if (activity == null) return;
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setTitle(title);
        builder.setMessage(message);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                mCallback.onDeleteLink(link);
                dialog.dismiss();
            }

        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do do my action here
                dialog.dismiss();
            }

        });

        AlertDialog alert = builder.create();
        alert.show();

    }
    

    @Override
    public int getCount() {
        return subItems.size();
    }
/*
    // Get the x and y position after the button is draw on screen
// (It's important to note that we can't get the position in the onCreate(),
// because at that stage most probably the view isn't drawn yet, so it will return (0, 0))
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

        int[] location = new int[2];
        Button button = (Button) findViewById(R.id.show_popup);

        // Get the x, y location and store it in the location[] array
        // location[0] = x, location[1] = y.
        button.getLocationOnScreen(location);

        //Initialize the Point with x, and y positions
        p = new Point();
        p.x = location[0];
        p.y = location[1];
    }
    
    */

    // The method that displays the popup.
    private void showLinkPopup(final Activity activity, Point p, String linkText) {
        int popupWidth = 200;
        int popupHeight = 150;

        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout) activity.findViewById(R.id.link_popup);
        LayoutInflater layoutInflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.link_popup_layout, viewGroup);

        // Creating the PopupWindow
        final PopupWindow popup = new PopupWindow(activity);
        popup.setContentView(layout);
        popup.setWidth(popupWidth);
        popup.setHeight(popupHeight);
        popup.setFocusable(true);

        // Some offset to align the popup a bit to the right, and a bit down, relative to button's position.
        int OFFSET_X = 30;
        int OFFSET_Y = 30;

        // Clear the default translucent background
        popup.setBackgroundDrawable(new BitmapDrawable());

        // Displaying the popup at the specified location, + offsets.
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, p.x + OFFSET_X, p.y + OFFSET_Y);

        TextView linText = (TextView) layout.findViewById(R.id.textViewLink);
        linText.setText(linkText);
    }
    
    public interface LinkListViewInterface {
        /**
         * Callback for when an item has been selected.
         */

        public void onDeleteLink(Parcelable linkParcelabl);

        public void onShowLink(Link link);

    }


}
