package com.saperion.sdb.client;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import com.saperion.sdb.client.utils.Log;
import com.saperion.sdb.sdk.models.User;
import com.saperion.sdb.sdk.rights.UserRight;
import com.saperion.sdb.sdk.utils.Utils;

import java.util.Collection;

/**
 * Created by nnn on 22.11.13.
 */
public class InviteUserDialogFragment extends DialogFragment {

    private static final String TAG = InviteUserDialogFragment.class.getSimpleName();
    
    EditText etFirstName;
    EditText etLastName;
    EditText etEmail;
    ImageButton btnOk;
    ImageButton btnCancel;



    private ImageButton mButtonShare;
    private ImageButton mButtonInviteUser;
    private ImageButton mButtonManageUser;

    Drawable btn_selected_drawable;
    Drawable btn_not_selected_drawwable;
    private User mCurrOpenedUser = null;
    CreateUserDialogListener createUserDialogListener = null ;
    
    public InviteUserDialogFragment(CreateUserDialogListener parentFragment) {

        createUserDialogListener = parentFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        // Pick a style based on the num.
        int style = DialogFragment.STYLE_NORMAL, theme = 0;
        /*
        switch ((mNum-1)%6) {
            case 1: style = DialogFragment.STYLE_NO_TITLE; break;
            case 2: style = DialogFragment.STYLE_NO_FRAME; break;
            case 3: style = DialogFragment.STYLE_NO_INPUT; break;
            case 4: style = DialogFragment.STYLE_NORMAL; break;
            case 5: style = DialogFragment.STYLE_NORMAL; break;
            case 6: style = DialogFragment.STYLE_NO_TITLE; break;
            case 7: style = DialogFragment.STYLE_NO_FRAME; break;
            case 8: style = DialogFragment.STYLE_NORMAL; break;
        }
        switch ((mNum-1)%6) {
            case 4: theme = android.R.style.Theme_Holo; break;
            case 5: theme = android.R.style.Theme_Holo_Light_Dialog; break;
            case 6: theme = android.R.style.Theme_Holo_Light; break;
            case 7: theme = android.R.style.Theme_Holo_Light_Panel; break;
            case 8: theme = android.R.style.Theme_Holo_Light; break;
            
        } */
        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Light_Dialog);
        // setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogTheme);
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.create_user_dialog_fragment, container);
        
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCurrOpenedUser = new User();
        
        btn_selected_drawable = view.getResources().getDrawable(R.drawable.fns_bg_orange_shape_no_stroke);
        btn_not_selected_drawwable  = view.getResources().getDrawable(R.drawable.fns_bg_grey_shape_no_stroke);
        
        mButtonShare = (ImageButton) view.findViewById(R.id.b_share);
        mButtonShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Collection<UserRight> rights = mCurrOpenedUser.getRights();
                Log.v(TAG, Utils.getMethodName() + "mButtonShare ");
                if (mButtonShare.getBackground().equals(btn_selected_drawable)){
                    mButtonShare.setBackgroundDrawable(btn_not_selected_drawwable);
                    rights.remove(UserRight.SHARE);
                }
                else{
                    mButtonShare.setBackgroundDrawable(btn_selected_drawable);
                    rights.add(UserRight.SHARE);
                }

                mCurrOpenedUser.setRights(rights);

            }
        });
        mButtonInviteUser = (ImageButton) view.findViewById(R.id.b_invite_user);
        mButtonInviteUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Collection<UserRight> rights = mCurrOpenedUser.getRights();
                Log.v(TAG, Utils.getMethodName() + "mButtonInviteUser ");
                if (mButtonInviteUser.getBackground().equals(btn_selected_drawable)) {
                    mButtonInviteUser.setBackgroundDrawable(btn_not_selected_drawwable);
                    rights.remove(UserRight.INVITE);
                } else {
                    mButtonInviteUser.setBackgroundDrawable(btn_selected_drawable);
                    rights.add(UserRight.INVITE);
                }

                mCurrOpenedUser.setRights(rights);
            }
        });
        mButtonManageUser = (ImageButton) view.findViewById(R.id.b_manage_user);
        mButtonManageUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Collection<UserRight> rights = mCurrOpenedUser.getRights();

                Log.v(TAG, Utils.getMethodName() + "mButtonManageUser ");
                if (mButtonManageUser.getBackground().equals(btn_selected_drawable)) {
                    mButtonManageUser.setBackgroundDrawable(btn_not_selected_drawwable);
                    rights.remove(UserRight.MANAGE);

                }
                else {
                    mButtonManageUser.setBackgroundDrawable(btn_selected_drawable);
                    rights.add(UserRight.MANAGE);
                }
                mCurrOpenedUser.setRights(rights);
            }
        });
        etFirstName = (EditText) view.findViewById(R.id.etFirstName);
        etLastName =  (EditText) view.findViewById(R.id.etLastName);
        etEmail = (EditText) view.findViewById(R.id.etEmail);
        
        btnOk = (ImageButton) view.findViewById(R.id.invite_user_b_ok);
        btnOk.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.v(TAG, Utils.getMethodName() + " btnOk " );
            mCurrOpenedUser.setFirstname(etFirstName.getText().toString());
            mCurrOpenedUser.setLastname(etLastName.getText().toString());
            mCurrOpenedUser.setEmail(etEmail.getText().toString());

            if (createUserDialogListener != null) {
                createUserDialogListener.onFinishCreateUserDialog(mCurrOpenedUser);
            }
        }
        });

        btnCancel = (ImageButton) view.findViewById(R.id.invite_user_b_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v(TAG, Utils.getMethodName() + " btnCancel " );
            etFirstName.setText("");
            etLastName.setText("");
            etEmail.setText("");
            }
        });
    }



    public interface CreateUserDialogListener {
        void onFinishCreateUserDialog(User user);
    }

}
