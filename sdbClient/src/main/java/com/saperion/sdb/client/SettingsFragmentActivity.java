package com.saperion.sdb.client;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabHost.TabContentFactory;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import android.widget.Toast;

import com.saperion.sdb.client.utils.Log;

import java.util.HashMap;

/**
 * Created by nnn on 14.11.13.
 */
public class SettingsFragmentActivity extends FragmentActivity implements TabHost.OnTabChangeListener {

    private static final String TAG = SettingsFragmentActivity.class.getSimpleName();
    public static final String INTENT_PARAM_CONFIG_LINK = "com.saperion.sdb.client.config_link";
    
    private TabHost mTabHost;
    private HashMap mapTabInfo = new HashMap();
    private TabInfo mLastTab = null;

    private class TabInfo {
        private String tag;
        private Class clss;
        private Bundle args;
        private Fragment fragment;
        TabInfo(String tag, Class clazz, Bundle args) {
            this.tag = tag;
            this.clss = clazz;
            this.args = args;
        }

    }

    class TabFactory implements TabContentFactory {

        private final Context mContext;

        /**
         * @param context
         */
        public TabFactory(Context context) {
            mContext = context;
        }

        /** (non-Javadoc)
         * @see android.widget.TabHost.TabContentFactory#createTabContent(java.lang.String)
         */
        public View createTabContent(String tag) {
            View v = new View(mContext);
            v.setMinimumWidth(0);
            v.setMinimumHeight(0);
            return v;
        }

    }
    /** (non-Javadoc)
     * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Step 1: Inflate layout
        setContentView(R.layout.setting_fragment_activity);
        // Step 2: Setup TabHost
        
        initialiseTabHost(savedInstanceState);
        if (savedInstanceState != null) {
            mTabHost.setCurrentTabByTag(savedInstanceState.getString("tab")); //set the tab as per the saved state
        }

    }

    /** (non-Javadoc)
     * @see android.support.v4.app.FragmentActivity#onSaveInstanceState(android.os.Bundle)
     */
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("tab", mTabHost.getCurrentTabTag()); //save the tab selected
        super.onSaveInstanceState(outState);
    }

    /**
     * Step 2: Setup TabHost
     */
    private void initialiseTabHost(Bundle args) {
        mTabHost = (TabHost)findViewById(android.R.id.tabhost);
        mTabHost.setup();
        TabInfo tabInfo = null;
        String tabName = "My Account";
        SettingsFragmentActivity.addTab(this, this.mTabHost, this.mTabHost.newTabSpec(tabName).setIndicator(tabName), (tabInfo = new TabInfo(tabName, MyAccountTabFragment.class, args)));
        this.mapTabInfo.put(tabInfo.tag, tabInfo);


        
        tabName = "Usages";     
        SettingsFragmentActivity.addTab(this, this.mTabHost, this.mTabHost.newTabSpec(tabName).setIndicator(tabName), (tabInfo = new TabInfo(tabName, UserQuotaTabFragment.class, args)));
        this.mapTabInfo.put(tabInfo.tag, tabInfo);

        tabName = "Users";
        SettingsFragmentActivity.addTab(this, this.mTabHost, this.mTabHost.newTabSpec(tabName).setIndicator(tabName), (tabInfo = new TabInfo(tabName, UserTabFragment.class, args)));
        this.mapTabInfo.put(tabInfo.tag, tabInfo);

        tabName = "Login";
        
        TabSpec loginTabSpec = this.mTabHost.newTabSpec(tabName).setIndicator(tabName);
        // set intent to get config link from web client
        Intent intent4Login = getIntent();
        String link = intent4Login.getStringExtra(INTENT_PARAM_CONFIG_LINK);
        
        loginTabSpec.setContent(intent4Login);
        SettingsFragmentActivity.addTab(this, this.mTabHost, loginTabSpec, 
                (tabInfo = new TabInfo(tabName, LoginTabFragment.class, args)));
        this.mapTabInfo.put(tabInfo.tag, tabInfo);

        for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {

            final TextView tv = (TextView) mTabHost.getTabWidget().getChildAt(i)
                    .findViewById(android.R.id.title);
            if (tv == null)
                continue;
            else {
                tv.setTextColor(getResources().getColor(R.color.orange));
                tv.setGravity(Gravity.CENTER_VERTICAL);
            }
        }
        
        // Default to first tab
        if (link != null && !TextUtils.isEmpty(link)) {
            this.onTabChanged("Login");
            mTabHost.setCurrentTab(3);
        }
        else
            this.onTabChanged("My Account");
        //
        mTabHost.setOnTabChangedListener(this);
    }

    private static View prepareTabView(Context context, String text)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.tab_layout, null);
        ((TextView) view.findViewById(R.id.tabTitle)).setText(text);
        return view;

    }
    
    /**
     * @param activity
     * @param tabHost
     * @param tabSpec

     */
    private static void addTab(SettingsFragmentActivity activity, TabHost tabHost, TabHost.TabSpec tabSpec, TabInfo tabInfo) {
        // Attach a Tab view factory to the spec
        tabSpec.setContent(activity.new TabFactory(activity));
        String tag = tabSpec.getTag();
        
        // Check to see if we already have a fragment for this tab, probably
        // createInstance a previously saved state.  If so, deactivate it, because our
        // initial state is that a tab isn't shown.
        tabInfo.fragment = activity.getSupportFragmentManager().findFragmentByTag(tag);
        if (tabInfo.fragment != null && !tabInfo.fragment.isDetached()) {
            FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
            ft.detach(tabInfo.fragment);
            ft.commit();
            activity.getSupportFragmentManager().executePendingTransactions();
        }

        tabHost.addTab(tabSpec);
    }

    /** (non-Javadoc)
     * @see android.widget.TabHost.OnTabChangeListener#onTabChanged(java.lang.String)
     */
    public void onTabChanged(String tag) {  
        TabInfo newTab = (TabInfo) this.mapTabInfo.get(tag);
        if (mLastTab != newTab) {
            FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
            if (mLastTab != null) {
                if (mLastTab.fragment != null) {
                    ft.detach(mLastTab.fragment);
                }
            }
            if (newTab != null) {
                if (newTab.fragment == null) {
                    newTab.fragment = Fragment.instantiate(this,
                            newTab.clss.getName(), newTab.args);
                    ft.add(R.id.realtabcontent, newTab.fragment, newTab.tag);
                } else {
                    ft.attach(newTab.fragment);
                }
            }

            mLastTab = newTab;
            ft.commit();
            this.getSupportFragmentManager().executePendingTransactions();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

      

        if (resultCode == Activity.RESULT_OK) {
            String filepath = data.getStringExtra("file_path");

            Log.v(TAG, com.saperion.sdb.client.utils.Utils.getMethodName() + "filepath " +filepath);
            
            Toast.makeText(this.getApplicationContext(), filepath, Toast.LENGTH_SHORT).show();
           
           //  MyAccountTabFragment maf = (MyAccountTabFragment) this.getSupportFragmentManager().findFragmentById(R.layout.tab_my_account_layout);

            if (mLastTab.fragment instanceof MyAccountTabFragment){
                MyAccountTabFragment maf = (MyAccountTabFragment)   mLastTab.fragment;
                if (maf !=null)
                    maf.updateAvatar(filepath);
            }


        }

    }
}