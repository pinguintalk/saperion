package com.saperion.sdb.client;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.saperion.sdb.client.LinkListAdapter.LinkListViewInterface;
import com.saperion.sdb.client.dialogs.ErrorDialogFragment.ErrorDialogFragmentBuilder;
import com.saperion.sdb.client.utils.Log;
import com.saperion.sdb.sdk.SdbApplication;
import com.saperion.sdb.sdk.models.Document;
import com.saperion.sdb.sdk.models.Link;
import com.saperion.sdb.sdk.models.Setting;
import com.saperion.sdb.sdk.models.Settings;
import com.saperion.sdb.sdk.models.User;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.RequestConstants;
import com.saperion.sdb.sdk.requestmanager.RequestManager.RequestListener;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.rights.UserRight;
import com.saperion.sdb.sdk.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


/**
 * Created by nnn on 22.11.13.
 */
public class LinkDialogFragment extends DialogFragment implements RequestListener, LinkListViewInterface {

    private static final String TAG = LinkDialogFragment.class.getSimpleName();
    static final int DATE_DIALOG_ID = 100;
    
    TextView tvLinkFileName;
    
    EditText etLinkName;
    EditText etLinkPassword;
    EditText etLinkAccessLimit;
    EditText etLinkValidUntil;

    TextView tvLinkName;
    TextView tvLinkPassword;
    TextView tvLinkAccessLimit;
    TextView tvLinkValidUntil;

    ImageView closeButton;
    
    ImageButton btnOk;
    ImageButton btnCancel;

    ProgressDialog progressDialog = null;
    Document document = null;
    RequestListener requestListener;
    ListView linkListView = null;
    LinkListAdapter linkListAdapter;
    static ArrayList<Link> linkList = new ArrayList<Link>();
    static Setting[] settingList = null;
    Link currLink = null;
    Activity activity;
    protected boolean isInputValuesValid = false;
    User currentUser= null;
    Drawable edittext_bg_red = null;
    SimpleDateFormat time_zone_date_format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ"); 
    SimpleDateFormat simple_date_format = new SimpleDateFormat("dd-MM-yyyy");

    private int year;
    private int month;
    private int day;
    private TextView text_date;

    private DatePicker date_picker;

    Drawable orange_border;
    Drawable no_border;
    
    LinearLayout llLinkName;
    
    public LinkDialogFragment(Activity activity, Document document) {

        this.document = document;
        this.activity = activity;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
                
        //setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Holo_Light_Dialog);
        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Light_Dialog);
        // setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogTheme);
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.links_dialog_fragment, container);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestListener = this;
        closeButton = (ImageView) view.findViewById(R.id.link_dialog_close_button);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v(TAG, Utils.getMethodName() + " closeButton " );
                onDismiss(getDialog());
            }
        });
        
        llLinkName = (LinearLayout) view.findViewById(R.id.ll_link_name);
        orange_border = view.getResources().getDrawable(R.drawable.fns_edittext_orange_border);
        no_border  = view.getResources().getDrawable(R.drawable.fns_edittext_default_border);
        
        edittext_bg_red = getActivity().getResources().getDrawable(R.drawable.fns_edittext_red_border);

        tvLinkName = (TextView) view.findViewById(R.id.tv_link_name);
        tvLinkPassword = (TextView) view.findViewById(R.id.tv_link_password);
        tvLinkAccessLimit = (TextView) view.findViewById(R.id.tv_link_access_limit);
        tvLinkValidUntil = (TextView) view.findViewById(R.id.tv_expired_date);
        
        tvLinkFileName = (TextView) view.findViewById(R.id.link_file_name);
                
        linkListView = (ListView) view.findViewById(R.id.link_list_view);

        linkListAdapter = new LinkListAdapter(this.activity,this, linkList);

        linkListView.setAdapter(linkListAdapter);

        linkListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                Toast.makeText(getActivity().getApplicationContext(),
                        ((TextView) v).getText(), Toast.LENGTH_SHORT).show();

            }
        });
        
        etLinkName = (EditText) view.findViewById(R.id.et_link_name);
        etLinkName.addTextChangedListener(new TextValidator(etLinkName) {
            @Override
            public void validate(EditText editText, String text) {
                Log.v(TAG, Utils.getMethodName() + " validate etLinkName "+ text );

                if (text !=null && text.length() > 0) {
                    tvLinkName.setVisibility(View.GONE);
                }
                else {
                    tvLinkName.setVisibility(View.VISIBLE);
                }
                    
            }
        });
        
        etLinkPassword =  (EditText) view.findViewById(R.id.et_link_password);
        etLinkPassword.addTextChangedListener(new TextValidator(etLinkPassword) {
            @Override
            public void validate(EditText editText, String text) {
                Log.v(TAG, Utils.getMethodName() + " validate etLinkPassword " );
                if (etLinkPassword.getText().length() > 0)
                    tvLinkPassword.setVisibility(View.GONE);
                else
                    tvLinkPassword.setVisibility(View.VISIBLE);
  
            }
        });
        
        etLinkValidUntil = (EditText) view.findViewById(R.id.et_expired_date);
        etLinkValidUntil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
                String currentValue = etLinkValidUntil.getText().toString();
                Log.v(TAG, Utils.getMethodName() + " etLinkValidUntil " + currentValue);
                
                
                if (currentValue !=null && currentValue.length() >0) {
                    String[] values = currentValue.split("-");
                    year = Integer.valueOf(values[2]);
                    month = Integer.valueOf(values[1]) - 1;
                    day = Integer.valueOf(values[0]);
                }

                Log.v(TAG, Utils.getMethodName() + "date : " + day+month+year);
                    DatePickerDialog dpd =  new DatePickerDialog(getActivity(), datePickerListener,
                            year, month ,day);
                    dpd.show();
                

            }
        });
        etLinkValidUntil.addTextChangedListener(new TextValidator(etLinkValidUntil) {
            @Override
            public void validate(EditText editText, String text) {
                String inputText = etLinkValidUntil.getText().toString();


                if (isExpiredDayValid(inputText))
                    tvLinkValidUntil.setVisibility(View.GONE);
                else
                    tvLinkValidUntil.setVisibility(View.VISIBLE);

            }
        });
        etLinkAccessLimit = (EditText) view.findViewById(R.id.et_link_access_limit);
        etLinkAccessLimit.addTextChangedListener(new TextValidator(etLinkAccessLimit) {
            @Override
            public void validate(EditText editText, String text) {
                String inputText = etLinkAccessLimit.getText().toString();

                
                if (etLinkAccessLimit.getText().length() > 0 && isNumeric(inputText))
                    tvLinkAccessLimit.setVisibility(View.GONE);
                else
                    tvLinkAccessLimit.setVisibility(View.VISIBLE);

            }
        });


        
        btnOk = (ImageButton) view.findViewById(R.id.b_link_ok);
        btnOk.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (currentUser !=null) {
                 Log.v(TAG, Utils.getMethodName() + " currentUser " + currentUser.toString() );                     
                if (!currentUser.getRights().toString().contains(UserRight.SHARE.toString())){
                    com.saperion.sdb.client.utils.Utils.showMessageDialog(getActivity(), "Error", "sorry, you don't have permission to create document link"); 
                }
            }
            else return;

            if (!isInputFieldsInvalid() ){
                com.saperion.sdb.client.utils.Utils.showMessageDialog(getActivity(), "Error", "Please check the input fields");
                return;
            }
            
            Link link = new Link();
            link.setName(etLinkName.getText().toString());
            link.setPassword(etLinkPassword.getText().toString());
            link.setAccessLimit(Integer.valueOf(etLinkAccessLimit.getText().toString()));

            long todayInMilliSeconds = 0;
            try {
                todayInMilliSeconds = simple_date_format.parse(etLinkValidUntil.getText().toString()).getTime();
            } catch (ParseException e) {
                Log.e(TAG, Utils.getMethodName() + "ParseException : " + e.toString());
            }
            Date expiredDate = new Date(todayInMilliSeconds);

            String dateInString = time_zone_date_format.format(expiredDate);
            Log.v(TAG, Utils.getMethodName() + "dateInString : " + dateInString);
            
            link.setExpirationDate(dateInString);
     
            if (document !=null) {                
                showLoadingDialog();
                link.setDocumentId(document.getId());
                link.save(requestListener);
            }

        }
        });

        btnCancel = (ImageButton) view.findViewById(R.id.b_link_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v(TAG, Utils.getMethodName() + " btnCancel " );

                setDefaultValues();
            }
        });
        
        if (document !=null) {
            showLoadingDialog();
            refreshLinkList();
            tvLinkFileName.setText(document.getName());
        }

        setCurrentDate();
        SdbApplication.getInstance().getCurrentUser(this);
        SdbApplication.getInstance().getCurrentSettings(this);

    }


    public static boolean isNumeric(String str)
    {
        for (char c : str.toCharArray())
        {
            if (!Character.isDigit(c)) return false;
        }
        return true;
    }
    
    public void setCurrentDate() {


        final Calendar calendar = Calendar.getInstance();

        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

    }
    
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        
        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,int selectedMonth, int selectedDay) {
            day = selectedDay;
            year = selectedYear;
            month = selectedMonth + 1;
            String monthstr = String.valueOf(month);
            if (month < 10)
                monthstr = "0"+month;
            
            etLinkValidUntil.setText(new StringBuilder().append(day).append("-").append(monthstr)
            .append("-").append(year));

        }
    };



    public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        DatePickerDialog datePickerDialog = null;
        private int mYear;
        private int mMonth;
        private int mDay;

        @Override
        public void onCreate(Bundle savedInstanceState) {
           super.onCreate(savedInstanceState);
            Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            setRetainInstance(true);
            Calendar calendar = Calendar.getInstance();

            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            datePickerDialog =  new DatePickerDialog(getActivity(), this, mDay, mMonth, mYear);
  
            return datePickerDialog;
 
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            int month = mm +1 ;
            etLinkValidUntil.setText(dd+"-"+month+"-"+yy);
        }
    }
    
    private void setDefaultValues() {

        etLinkName.setText("");
        etLinkPassword.setText("");
        etLinkAccessLimit.setText("");
        etLinkValidUntil.setText("");
        
        etLinkName.setBackgroundResource(R.drawable.fns_edittext_orange_border);
        etLinkValidUntil.setBackgroundResource(R.drawable.fns_edittext_orange_border);
        etLinkAccessLimit.setBackgroundResource(R.drawable.fns_edittext_orange_border);
        etLinkPassword.setBackgroundResource(R.drawable.fns_edittext_orange_border);
    }

    private boolean isInputFieldsInvalid() {
        if ((tvLinkName.getVisibility() == View.VISIBLE)
                || (tvLinkValidUntil.getVisibility() == View.VISIBLE)
                || (tvLinkAccessLimit.getVisibility() == View.VISIBLE)
                || (tvLinkPassword.getVisibility() == View.VISIBLE))
            return false;
        
        return true;
    }
    
    private boolean isExpiredDayValid(String inputStr){
        boolean returnValue = true;
        if (settingList == null) return false;

        if (inputStr == null) return false;
        
        for (int s=0; s < settingList.length ; s++) {
            
            if (settingList[s].getKey().equals(Setting.MAX_EXPIRATION_DATE_KEY_NAME)){
   
                if (!inputStr.isEmpty()) {

                    Date expiredDay = null;
                    int valueInDay =  0;
                    try {
                        expiredDay = simple_date_format.parse(inputStr);
                        long time_diff =  expiredDay.getTime() - (new Date().getTime());
                        
                        if (time_diff <0 ) return false;
                        
                        valueInDay = (int ) time_diff / (24 * 60 * 60 * 1000);

                        Log.v(TAG, Utils.getMethodName() + " valueInDay = " + valueInDay);

                    } catch (ParseException e) {
                        Log.e(TAG, Utils.getMethodName() + "ParseException : " + e.toString());
                    }

                    if (valueInDay > Integer.valueOf(settingList[s].getValue())) {
                        return false;
                    }
                }
                
            }

        }
        
        return true;
    
    }
    private boolean isInputValuesValid() {
        boolean returnValue = true;
        if (settingList == null) return false;
        
        for (int s=0; s < settingList.length ; s++) {
            if (settingList[s].getKey().equals(Setting.MAX_EXPIRATION_DATE_KEY_NAME)){
                String  inputStr= etLinkValidUntil.getText().toString();
                if (!inputStr.isEmpty()) {

                    Date expiredDay = null;
                    int valueInDay =  0;
                    try {
                        expiredDay = simple_date_format.parse(inputStr);
                        long time_diff =   expiredDay.getTime() - (new Date().getTime());

                        valueInDay = (int ) time_diff / (24 * 60 * 60 * 1000);
 
                        Log.v(TAG, Utils.getMethodName() + " valueInDay = " + valueInDay);
                        
                    } catch (ParseException e) {
                        Log.e(TAG, Utils.getMethodName() + "ParseException : " + e.toString());
                    }
                   
                    if (valueInDay > Integer.valueOf(settingList[s].getValue())) {
                        returnValue = false;
                        etLinkValidUntil.setBackgroundResource(R.drawable.fns_edittext_red_border);
                    }
                }
                else {
                    etLinkValidUntil.setBackgroundResource(R.drawable.fns_edittext_red_border);
                    returnValue = false;
                }
            }
            else  if (settingList[s].getKey().equals(Setting.MAX_ACCESS_COUNT_KEY_NAME)){
                String  inputStr= etLinkAccessLimit.getText().toString();
                int server_value = Integer.valueOf(settingList[s].getValue());
                
                if (!inputStr.isEmpty()) {
                    if (server_value > 0  ) 
                    {
                        int accessLimit = Integer.valueOf(inputStr);
                        if (accessLimit > server_value) {
                            returnValue = false;
                            etLinkAccessLimit.setBackgroundResource(R.drawable.fns_edittext_red_border);
                        }
                    }
                }
                else {
                    // etLinkAccessLimit.setBackground(edittext_bg_red);
                    etLinkAccessLimit.setBackgroundResource(R.drawable.fns_edittext_red_border);
                    returnValue = false;
                }
            }
            else  if (settingList[s].getKey().equals(Setting.PASSWORD_REQUIRED_KEY_NAME)){

                if ((settingList[s].getValue().equals("true"))) {
                    if (etLinkPassword.getText().toString().isEmpty()){
                        returnValue = false;
                        etLinkPassword.setBackgroundResource(R.drawable.fns_edittext_red_border);
                       // etLinkPassword.setBackground(edittext_bg_red);
                    }
                    
                }
            }    
        }
 
        return returnValue;
    }
    

    private void refreshLinkList() {
        linkList.clear();
        linkList.addAll(document.getLinks(getActivity().getApplicationContext(), this));
        linkListAdapter.notifyDataSetChanged();
    }

    private void showErrorDialog(String message) {
        new ErrorDialogFragmentBuilder(getActivity())
                .setTitle(R.string.dialog_data_error_title).setMessage(message).show();
        return;
    }

    @Override
    public void onRequestFinished(Request request, Bundle resultData) {
        if (getActivity() == null)
            return;

        int result_code = resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);
        int requestType = request.getRequestType();
        String errorMessage = resultData.getString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE);

        cancelLoadingDialog();
        
        setDefaultValues();
        
        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " resultCode = "
                + result_code);

        switch (result_code) {

            case ResponseConstants.RESPONSE_CODE_OK:
            case ResponseConstants.RESPONSE_CODE_CREATED:
            case ResponseConstants.RESPONSE_CODE_NO_CONTENT:
                break;
            case ResponseConstants.RESPONSE_CODE_UNKOWN_ERROR:
            case ResponseConstants.RESPONSE_CODE_BAD_REQUEST:
            case ResponseConstants.RESPONSE_CODE_ERROR:
            case ResponseConstants.RESPONSE_CODE_FORBIDDEN:
            case ResponseConstants.RESPONSE_CODE_INTERNAL_SERVER_ERROR:
            case ResponseConstants.RESPONSE_CODE_INTERNAL_SERVICE_UNAVAILABLE:
            case ResponseConstants.RESPONSE_CODE_METHOD_NOT_ALLOWED:
            case ResponseConstants.RESPONSE_CODE_NOT_FOUND:
                return;
            case ResponseConstants.RESPONSE_CODE_PRECONDITION_FAILED:
            case ResponseConstants.RESPONSE_CODE_TOO_MANY_REQUESTS:
                showErrorDialog(errorMessage);
                return;
            default: {
                showErrorDialog("unkown error !"); // should not come here !
                return;
            }
        }

        switch (requestType) {
                                                                                                                              
            case RequestConstants.REQUEST_TYPE_DOCUMENT_GET_LINKS:

                ArrayList<Link> linkListmp = resultData.getParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                Log.v(TAG, Utils.getMethodName() + " shareList = " +linkListmp.toString());
                linkList.clear();
                linkList.addAll(linkListmp);
                linkListAdapter.notifyDataSetChanged();
                break;
            case RequestConstants.REQUEST_TYPE_DOCUMENT_CREATE_LINK:

                if (document !=null) {
                    refreshLinkList();
                }
                break;
            case RequestConstants.REQUEST_TYPE_USER_GET_CURRENT:
                currentUser = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);

                break;
            case RequestConstants.REQUEST_TYPE_DOCUMENT_DELETE_LINK:

                Log.v(TAG, Utils.getMethodName() + " link deleted");
                if (document !=null) {
                    refreshLinkList();
                }
                break;
            case RequestConstants.REQUEST_TYPE_ACCOUNT_GET_SETTINGS:
                Settings settings = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                settingList = settings.getList().clone();
              
                for (int s=0; s < settingList.length ; s++) {
                    // Log.v(TAG, Utils.getMethodName() + "key " + settingList[s].getKey().toString() + " value " + settingList[s].getValue().toString());

                    if (settingList[s].getKey().equals(Setting.MAX_EXPIRATION_DATE_KEY_NAME)){
                        int value = Integer.valueOf(settingList[s].getValue());
                        if(value == -1)
                            etLinkValidUntil.setHint("unlimit");
                        else {

                            Date currentDate = new Date();
                            long dateInMilliSec = currentDate.getTime() +  (value * 24 *60 * 60 * 1000);

                            Date untilDate = new Date(dateInMilliSec);
                            etLinkValidUntil.setHint(simple_date_format.format(untilDate));

                        }

                        if (etLinkValidUntil.getText().length() > 0)
                            tvLinkValidUntil.setVisibility(View.GONE);
                        else
                            tvLinkValidUntil.setVisibility(View.VISIBLE);
                        
                    } else if (settingList[s].getKey().equals(Setting.MAX_ACCESS_COUNT_KEY_NAME)){
                        int value = Integer.valueOf(settingList[s].getValue());
                        if(value == -1)
                            etLinkAccessLimit.setHint("unlimit");
                        else
                            etLinkAccessLimit.setHint("max " + settingList[s].getValue());

                        if (etLinkAccessLimit.getText().length() > 0)
                            tvLinkAccessLimit.setVisibility(View.GONE);
                        else
                            tvLinkAccessLimit.setVisibility(View.VISIBLE);
                        
                    }  else if (settingList[s].getKey().equals(Setting.PASSWORD_REQUIRED_KEY_NAME)){
                        if (settingList[s].getValue().equals("TRUE")) {
                            etLinkPassword.setHint("required");
                            if (etLinkPassword.getText().length() > 0)
                                tvLinkPassword.setVisibility(View.GONE);
                            else
                                tvLinkPassword.setVisibility(View.VISIBLE);
                        }
                        else {
                            etLinkPassword.setHint("optional");
                            tvLinkPassword.setVisibility(View.GONE);

                        }
                    }
                }
                break;            
        }

    }

    @Override
    public void onRequestConnectionError(Request request, int statusCode) {

        cancelLoadingDialog();
        int requestType = request.getRequestType();
        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " statusCode = "
                + statusCode);
        showServiceErrorDialog(getResources().getString(R.string.dialog_connection_error_message));


    }

    @Override
    public void onRequestDataError(Request request) {

        cancelLoadingDialog();
        int requestType = request.getRequestType();
        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " statusCode = ");

        showServiceErrorDialog(getResources().getString(R.string.dialog_data_error_message));
    }

    @Override
    public void onRequestServiceError(Request request, Bundle resultData) {
        cancelLoadingDialog();
        int requestType = request.getRequestType();

        int error_code = resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);

        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " error_type = "
                + error_code);

        switch (error_code){
            case ResponseConstants.RESPONSE_CODE_UNAUTHORIZED:
                String errMsg = resultData.getString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE);
                Toast.makeText(getActivity(), errMsg, Toast.LENGTH_LONG).show();
                return;
        }

    }

    public void showLoadingDialog() {

        Log.v(TAG, Utils.getMethodName() + " entry");
        /*
        if (progressDialog != null) {
            progressDialog.cancel();
            progressDialog = null;
        }

        progressDialog = new ProgressDialog(this.getActivity());

        progressDialog.setTitle(getResources().getString(R.string.progress_dialog_title));
        progressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        progressDialog.setCancelable(true);
        progressDialog.setIndeterminate(true);

        progressDialog.show();
        */


        View view = getActivity().getActionBar().getCustomView();
        if (view != null)
            view.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
    }
    
    private void showServiceErrorDialog(String message) {

        Context context = getActivity();
        if (context == null) return;

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(R.string.dialog_service_error_title);
        builder.setMessage(message);

        builder.setPositiveButton(R.string.dialog_error_button_close_txt, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do do my action here

                dialog.dismiss();
            }

        });
        /*
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // I do not need any action here you might
                dialog.dismiss();
            }
        });
        */
        AlertDialog alert = builder.create();
        alert.show();

    }    
    
    public void cancelLoadingDialog() {
    /*
        if (progressDialog != null) {
            progressDialog.cancel();
            progressDialog = null;
        }
    */
        View view = getActivity().getActionBar().getCustomView();
        if (view != null)
            view.findViewById(R.id.progressBar).setVisibility(View.GONE);

    }

    @Override
    public void onDeleteLink(Parcelable linkParcelable) {
        showLoadingDialog();
        
        Link link = (Link) linkParcelable;
        link.delete(this);
    }

    @Override
    public void onShowLink(Link link) {
        showLoadingDialog();
        currLink = link;
        link.delete(this);
    }

    public abstract class TextValidator implements TextWatcher {
        private EditText editText;

        public TextValidator(EditText textView) {
            this.editText = textView;
        }

        public abstract void validate(EditText textView, String text);

        @Override
        final public void afterTextChanged(Editable s) {
            if (editText == null) return;
            String text = editText.getText().toString();
            validate(editText, text);
        }

        @Override
        final public void beforeTextChanged(CharSequence s, int start, int count, int after) { /* Don't care */ }

        @Override
        final public void onTextChanged(CharSequence s, int start, int before, int count) { /* Don't care */ }
    }

}
