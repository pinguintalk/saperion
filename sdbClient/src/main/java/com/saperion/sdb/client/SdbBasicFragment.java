package com.saperion.sdb.client;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.saperion.sdb.client.R.string;
import com.saperion.sdb.client.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

public class SdbBasicFragment extends Fragment {

    private static final String TAG = SdbBasicFragment.class.getSimpleName();

    private ProgressDialog progressDialog = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        Log.v(TAG, Utils.getMethodName() + "entry");

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.v(TAG, Utils.getMethodName() + "orientation ch  = " + newConfig.orientation);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

        }
    }

    @Override
    public void setRetainInstance(boolean value) {
        super.setRetainInstance(value);

        Log.v(TAG, Utils.getMethodName() + "entry");

    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    public void showLoadingDialog() {

        if (progressDialog != null) {
            progressDialog.cancel();
            progressDialog = null;
        }

        progressDialog = new ProgressDialog(this.getActivity());

        progressDialog.setTitle("Processing...");
        progressDialog.setMessage("Please wait.");
        progressDialog.setCancelable(true);
        progressDialog.setIndeterminate(true);

        progressDialog.show();
    }

    public void cancelLoadingDialog() {

        if (progressDialog != null) {
            progressDialog.cancel();
            progressDialog = null;
        }
    }


    public void showErrorDialog(Context context, String message) {

        if (context == null) return;

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(string.dialog_service_error_title);
        builder.setMessage(message);

        builder.setPositiveButton(string.dialog_error_button_close_txt, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do do my action here

                dialog.dismiss();
            }

        });
        /*
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // I do not need any action here you might
                dialog.dismiss();
            }
        });
        */
        AlertDialog alert = builder.create();
        alert.show();

    }
}
