package com.saperion.sdb.client;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saperion.sdb.client.utils.Log;
import com.saperion.sdb.sdk.SdbApplication;
import com.saperion.sdb.sdk.models.Account;
import com.saperion.sdb.sdk.models.SystemInfo;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.RequestConstants;
import com.saperion.sdb.sdk.requestmanager.RequestManager.RequestListener;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.utils.Utils;


/**
 * Created by nnn on 14.11.13.
 */
public class UserQuotaTabFragment extends Fragment implements RequestListener{


    private static final String TAG = UserQuotaTabFragment.class.getSimpleName();
    private ProgressDialog progressDialog = null;



    ImageView quotaLimitImage;
    ImageView userLimitImage;
    
    ImageView usedQuotaImage;
    ImageView usedUsersImage;

    // values
    TextView tvQuotaLimit;
    TextView tvUsedQuota;
    TextView tvUserLimit;
    TextView tvUsedUser;

    /** (non-Javadoc)
     * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
     */
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container == null) {
            // We have different layouts, and in one of them this
            // fragment's containing frame doesn't exist.  The fragment
            // may still be created createInstance its saved state, but there is
            // no reason to try to create its view hierarchy because it
            // won't be displayed.  Note this is not needed -- we could
            // just run the code below, where we would create and return
            // the view hierarchy; it would just never be used.
            return null;
        }
        return (LinearLayout) inflater.inflate(R.layout.tab_user_quota, container, false);
    }



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        quotaLimitImage = (ImageView) getActivity().findViewById(R.id.quota_limit_image_view);
        userLimitImage  = (ImageView) getActivity().findViewById(R.id.user_limit_image_view);

        
        usedQuotaImage = (ImageView) getActivity().findViewById(R.id.used_quota_image_btn);
        usedUsersImage  = (ImageView) getActivity().findViewById(R.id.used_user_image_btn);

        tvUsedQuota = (TextView) getActivity().findViewById(R.id.used_quota_txt);
        tvQuotaLimit = (TextView) getActivity().findViewById(R.id.quota_limit_txt);
        tvUsedUser = (TextView) getActivity().findViewById(R.id.used_users_txt);
        tvUserLimit = (TextView) getActivity().findViewById(R.id.user_limit_txt);

        
        SdbApplication.getInstance().getAccounts(this);

        SdbApplication.getInstance().getSystemInformations(this);
        
    }


   

    @Override
    public void onRequestFinished(Request request, Bundle resultData) {

        cancelLoadingDialog();
        int resp_code = resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);

        String errorMessage = resultData.getString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE);

        int requestType = request.getRequestType();
        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " resultCode = "
                + resp_code);

        switch (resp_code) {
            case 0:
            case ResponseConstants.RESPONSE_CODE_OK:
            case ResponseConstants.RESPONSE_CODE_CREATED:
                break;

            case ResponseConstants.RESPONSE_CODE_BAD_REQUEST:
            case ResponseConstants.RESPONSE_CODE_ERROR:
            case ResponseConstants.RESPONSE_CODE_FORBIDDEN:
            case ResponseConstants.RESPONSE_CODE_INTERNAL_SERVER_ERROR:
            case ResponseConstants.RESPONSE_CODE_INTERNAL_SERVICE_UNAVAILABLE:
            case ResponseConstants.RESPONSE_CODE_METHOD_NOT_ALLOWED:
            case ResponseConstants.RESPONSE_CODE_NOT_FOUND:

            case ResponseConstants.RESPONSE_CODE_PRECONDITION_FAILED:
            case ResponseConstants.RESPONSE_CODE_TOO_MANY_REQUESTS:
                showServiceErrorDialog(errorMessage);
                return;
            case ResponseConstants.RESPONSE_CODE_NO_CONTENT:
                return;

            default: {
                showServiceErrorDialog("unkown error");
                return;
            }
        }

        switch (requestType) {

            case RequestConstants.REQUEST_TYPE_ACCOUNT_GET:
                Account account = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);

                if (account == null) return;
                  
                int  storageUsed = (int) account.getUsedStorage() / 1000;
                int  storageLimit  = (int) account.getStorageLimit() / 1000;
                
                int storageUsedInPercent = (storageUsed*100) / storageLimit;
                int user_limit= account.getUserLimit();
                int used_user = account.getUsedUsers();

                int userUsedInPercent = (used_user*100) / user_limit;

                tvUsedQuota.setText(String.valueOf(storageUsedInPercent) + "%/" + String.valueOf(storageUsed) + " GB");
                tvQuotaLimit.setText(String.valueOf(storageLimit) + " GB");

                tvUsedUser.setText(String.valueOf(userUsedInPercent) +"%/" + String.valueOf(used_user));
                tvUserLimit.setText(String.valueOf(user_limit));


                int imageWidth = (storageUsed * quotaLimitImage.getMeasuredWidth() ) / storageLimit;
                
                Log.d(TAG, Utils.getMethodName() + " imageWidth = " + imageWidth );
                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(imageWidth , usedQuotaImage.getMeasuredHeight());
                layoutParams.setMargins(1,1,0,0);
                usedQuotaImage.setLayoutParams(layoutParams);

                imageWidth = (used_user * userLimitImage.getMeasuredWidth()) / user_limit;

                Log.d(TAG, Utils.getMethodName() + " imageWidth = " + imageWidth );
                layoutParams = new FrameLayout.LayoutParams(imageWidth ,usedUsersImage.getMeasuredHeight());
                layoutParams.setMargins(1,1,0,0);
                usedUsersImage.setLayoutParams(layoutParams);

                break;

            case RequestConstants.REQUEST_TYPE_GET_SYSTEM_INFOS:
                SystemInfo system_Info_infos = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                SdbApplication.getInstance().setSystemInfos(system_Info_infos);
                
                TextView tvServerVersion = (TextView) getActivity().findViewById(R.id.tv_service_version);
                tvServerVersion.setText(system_Info_infos.getVersion());

                TextView tvBuildNr = (TextView) getActivity().findViewById(R.id.tv_build_number);
                tvBuildNr.setText(system_Info_infos.getBuildNumber());
                /*
                for (String lang:system_Info_infos.getSupportedLanguages()) {
                    Log.v(TAG, Utils.getMethodName() + lang);
                }

                for (SupportedRenderFormat format:system_Info_infos.getSupportedRenderFormats()) {
                    Log.v(TAG, Utils.getMethodName() + format.getMimetypes());
                }

                for (Feature feature:system_Info_infos.getFeatures()) {
                    Log.v(TAG, Utils.getMethodName() + feature.getName());
                }
                */

                break;

        }

    }

    @Override
    public void onRequestConnectionError(Request request, int statusCode) {
        cancelLoadingDialog();

        int requestType = request.getRequestType();
        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " statusCode = "
                + statusCode);
    }

    @Override
    public void onRequestDataError(Request request) {
        cancelLoadingDialog();

        int requestType = request.getRequestType();
        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType );
    }

    @Override
    public void onRequestServiceError(Request request, Bundle resultData) {
        cancelLoadingDialog();
        int resp_code = resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);

        String errorMessage = resultData.getString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE);

        int requestType = request.getRequestType();
        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " resultCode = "
                + resp_code);
    }

    private void showServiceErrorDialog(String message) {


        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());

        builder.setTitle("Service Error");
        builder.setMessage(message);

        builder.setPositiveButton(getResources().getString(R.string.close),
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        // Do do my action here

                        dialog.dismiss();
                    }

                });
        /*
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // I do not need any action here you might
                dialog.dismiss();
            }
        });
        */
        AlertDialog alert = builder.create();
        alert.show();

    }

    public void cancelLoadingDialog() {

        if (progressDialog != null) {
            progressDialog.cancel();
            progressDialog = null;
        }

    }

    public void showLoadingDialog() {


        if (progressDialog != null) {
            progressDialog.cancel();
            progressDialog = null;
        }

        progressDialog = new ProgressDialog(this.getActivity());

        progressDialog.setTitle(getResources().getString(R.string.progress_dialog_title));
        progressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        progressDialog.setCancelable(true);
        progressDialog.setIndeterminate(true);

        progressDialog.show();

    }
    
}
