package com.saperion.sdb.client;

import android.content.Context;

import com.saperion.sdb.sdk.models.Space;

public interface SpaceListViewInterface {
    /**
     * Callback for when an item has been selected.
     */

    public void itemIconOrNameClickedEvent(Space space);

    public void itemTrashClickedEvent(Space space);

    public void itemOkClickedEvent(Space space);

    public void itemFavoritClickEvent(Space space);

    public void itemSyncAppClickedEvent(Space space);

    public void itemSyncDesktopClickedEvent(Space space);

    void itemRestoreClickedEvent(Space space);

    void itemDeleteClickedEvent(Space space);

    void itemOpenShareDialogClickedEvent(Context mContext, Space mCurrOpenedSpace);

    
}
