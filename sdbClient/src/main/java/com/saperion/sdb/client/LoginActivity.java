package com.saperion.sdb.client;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.saperion.sdb.client.R.string;
import com.saperion.sdb.client.utils.Log;
import com.saperion.sdb.sdk.SdbApplication;
import com.saperion.sdb.sdk.config.WSConfig;
import com.saperion.sdb.sdk.models.ErrorMessage;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.RequestManager.RequestListener;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.utils.Utils;

/**
 * LoginActivity.java -  log in,
 * stores the user's data in the persistance preference
 *
 * @author nnn
 * @see
 */
public class LoginActivity extends AccountAuthenticatorActivity implements RequestListener {

    private static final String TAG = LoginActivity.class.getSimpleName();

    public static final String INTENT_PARAM_LINK = "com.saperion.sdb.client.link";


    public static final String PARAM_ACTION = "com.saperion.sdb.client.Login";
    public static final String PARAM_CONFIRMCREDENTIALS = "confirmCredentials";
    public static final String PARAM_PASSWORD = "password";
    public static final String PARAM_USERNAME = "username";
    public static final String PARAM_AUTHTOKEN_TYPE = "authtokenType";
    private static final String ACCOUNT_TYPE = "com.saperion.sdb";

    private Button btnLogin;
    private EditText txtServer;
    private EditText txtEmail;
    private EditText txtPassword;
    private CheckBox checkBoxStoreEmailAndPassword;
    protected ProgressDialog progressDialog = null;
    private AccountManager mAccountManager;

    private String mServerUrl;

    private String mUsername;
    private String mPassword;

    protected boolean mRequestNewAccount = true;
    private Account account = null;

    @Override
    public void onPause() {
        super.onPause();

        if (progressDialog != null) {
            progressDialog.cancel();
            progressDialog = null;
        }

    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (progressDialog != null) {
            progressDialog.cancel();
            progressDialog = null;
        }

    }

    @Override
    public void onBackPressed() {
        login();
        finish();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        this.setTitle("File'n'Share Login");

        txtServer = (EditText) findViewById(R.id.txtDisplayName);
        txtEmail = (EditText) findViewById(R.id.etEmail);
        txtPassword = (EditText) findViewById(R.id.txtPassword);

        checkBoxStoreEmailAndPassword = (CheckBox) findViewById(R.id.checkBoxStoreEmailAndPassword);
        checkBoxStoreEmailAndPassword.setChecked(true);

        Intent intent = getIntent();
        // check if this intent is started via custom scheme link
        String url = "";
        String username = "";
        String link = intent.getStringExtra(INTENT_PARAM_LINK);

        //together://config?service?https://213.61.60.88:8601/fns-service?your.email@saperion.com
        
        if (link != null && !TextUtils.isEmpty(link)) {
          // String schemeLink = link.substring(link.indexOf("?") + 1);
            String[] linkStr = link.split("\\?");
            url = linkStr[2];
            username = linkStr[3];
        }


        if (url != null && !TextUtils.isEmpty(url)) {
            txtServer.setText(url);
            if (username != null && !TextUtils.isEmpty(username))
                txtEmail.setText(username);
                // get default password if exists
                txtPassword.setText(SdbApplication.getInstance().getPassword());
        } else {
            txtServer.setText(SdbApplication.getInstance().getServerUrl());
            txtEmail.setText(SdbApplication.getInstance().getUserName());
            txtPassword.setText(SdbApplication.getInstance().getPassword());

        }

        btnLogin = (Button) findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                loginButtonClicked(false);
            }
        });

    }

    /* Login Button onClick Handler */
    private void loginButtonClicked(boolean kickOtherSessions) {

        login();
    }

    private void login() {
        showLoadingDialog();

        mUsername = txtEmail.getText().toString();
        mPassword = txtPassword.getText().toString();
        mServerUrl = txtServer.getText().toString();

        SdbApplication.getInstance().setServerUrl(mServerUrl + WSConfig.WS_API_VERSION);
        SdbApplication.getInstance().setUserName(mUsername);
        SdbApplication.getInstance().setUserName(mPassword);
        SdbApplication.getInstance().login(mServerUrl, mUsername, mPassword, this);
    }

    @Override
    public void onRequestFinished(Request request, Bundle resultData) {
        cancelLoadingDialog();
        int result_code = resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);

        Log.v(TAG, "Login result_code: " + result_code);

        finish();
    }

    @Override
    public void onRequestConnectionError(Request request, int statusCode) {
        cancelLoadingDialog();

    }

    @Override
    public void onRequestDataError(Request request) {
        cancelLoadingDialog();

    }

    @Override
    public void onRequestServiceError(Request request, Bundle resultData) {


        cancelLoadingDialog();
        int resp_code = resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);

        ErrorMessage msg = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE);

        int requestType = request.getRequestType();
        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " resultCode = "
                + resp_code + "errorMessage " + msg.getMessage());

        showServiceErrorDialog(msg.getMessage());
    }

    public void cancelLoadingDialog() {

        if (progressDialog != null) {
            progressDialog.cancel();
            progressDialog = null;
        }
    }

    public void showLoadingDialog() {

        if (progressDialog != null) {
            progressDialog.cancel();
            progressDialog = null;
        }

        progressDialog = new ProgressDialog(this);

        progressDialog.setTitle(R.string.progress_dialog_title);
        progressDialog.setMessage(getResources().getString(string.progress_dialog_message));
        progressDialog.setCancelable(true);
        progressDialog.setIndeterminate(true);

        progressDialog.show();
    }


    private void showServiceErrorDialog(String message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(string.dialog_service_error_title);
        builder.setMessage(message);

        builder.setPositiveButton(R.string.dialog_error_button_close_txt, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do do my action here

                dialog.dismiss();
            }

        });

        AlertDialog alert = builder.create();
        alert.show();

    }
}
