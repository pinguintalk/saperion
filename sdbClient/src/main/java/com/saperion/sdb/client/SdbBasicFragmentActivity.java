package com.saperion.sdb.client;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.saperion.sdb.sdk.requestmanager.Request;

import java.util.ArrayList;

public class SdbBasicFragmentActivity extends FragmentActivity {

    private static final String SAVED_STATE_REQUEST_LIST = "savedStateRequestList";

    protected ArrayList<Request> mRequestList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (savedInstanceState != null) {
            mRequestList = savedInstanceState.getParcelableArrayList(SAVED_STATE_REQUEST_LIST);
        } else {
            mRequestList = new ArrayList<Request>();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelableArrayList(SAVED_STATE_REQUEST_LIST, mRequestList);
    }


}
