package com.saperion.sdb.client;

import com.saperion.sdb.sdk.models.User;

public interface UserListViewInterface {
    /**
     * Callback for when an item has been selected.
     */

    public void onDeleteUser(User user);

    public void onUpdateUser(User user);

    public void onGetUserAvatar(User user);

    public void onCreateUser(User user);

}
