package com.saperion.sdb.client;

import android.app.ActionBar;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Spinner;
import android.widget.Toast;

import com.ipaulpro.afilechooser.utils.FileUtils;
import com.saperion.sdb.client.utils.Log;
import com.saperion.sdb.client.utils.Utils;
import com.saperion.sdb.sdk.SdbApplication;
import com.saperion.sdb.sdk.models.Document;
import com.saperion.sdb.sdk.models.Folder;
import com.saperion.sdb.sdk.models.Item;
import com.saperion.sdb.sdk.models.Space;
import com.saperion.sdb.sdk.models.SystemInfo;
import com.saperion.sdb.sdk.models.User;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.RequestConstants;
import com.saperion.sdb.sdk.requestmanager.RequestManager.RequestListener;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.security.KeyStoreDeprecated;
import com.saperion.sdb.sdk.security.KeyStoreHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;

/**
 * An activity representing a list of Items. This activity has different
 * presentations for handset and tablet-size devices. On handsets, the activity
 * presents a list of items, which when touched, lead to a
 * {@link ItemListActivity} representing item details. On tablets, the activity
 * presents the list of items and item details side-by-side using two vertical
 * panes.
 */
public class MainFragmentActivity extends FragmentActivity implements OnClickListener,
        SpacesFragment.Callbacks, RequestListener {

    private static final String TAG = MainFragmentActivity.class.getSimpleName();

    public static final int PICK_FILE_REQUEST_CODE = 10;

    
    private   ActionBar actionBar = null;
    ImageButton buttonWatchRecycled;

    ImageView avatarIcon ;
    
    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;
    private FragmentHolder retainedFragments;
    String access_token = "";
    int itemListFragmentID = 0;
    ItemListFragment mItemFragment = null;
    SpacesFragment spaceFragment;
    private Space selectedSpace = null;
    private int currNavPosition = -1;
    
    protected RequestListener listender;

    public static final String SELECTED_SPACE = "SELECTED_SPACE";
    public static final String CURR_NAV_POSITION = "CURR_NAV_POSITION";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Request Permission to display the Progress Bar...
        //requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        //setProgressBarIndeterminateVisibility(false);
        super.onCreate(savedInstanceState);

        Log.v(TAG, Utils.getMethodName() + "entry");

        setContentView(R.layout.main_fragment_activity);

        listender = this;
        // Check whether the activity is using the layout version with
        // the fragment_container FrameLayout. If so, we must add the first fragment
        if (findViewById(R.id.fragment_container) != null) {
                    
            // However, if we're being restored createInstance a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                Log.v(TAG, Utils.getMethodName() + "savedInstanceState != null");
                return;
            }

            retainedFragments = (FragmentHolder)
                    getLastCustomNonConfigurationInstance();
            if (retainedFragments == null) {
                retainedFragments = new FragmentHolder();
            }
            else
            {
                Log.v(TAG, Utils.getMethodName() + "retainedFragments != null");
            }

            // Create an instance of SpacesFragment
            spaceFragment = new SpacesFragment();

            // In case this activity was started with special instructions createInstance an Intent,
            // pass the Intent's extras to the fragment as arguments
            spaceFragment.setArguments(getIntent().getExtras());

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, spaceFragment).commit();

        }
        else
        {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-large and
            // res/values-sw600dp). If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;

        }

        unlockKeyStore();

        // saveKey();

        //If Application is opened by external file
        Intent fileIntent = getIntent();
        if (fileIntent.getData() != null) {
            Uri data = fileIntent.getData();
            String dataString = fileIntent.getDataString();
            //String scheme = fileIntent.getScheme();
            //Log.v(TAG, Utils.getMethodName() + "scheme " + scheme);
            Log.v(TAG, Utils.getMethodName() + "data.getPath() " + data.getPath());
            Log.v(TAG, Utils.getMethodName() + "dataString " + dataString);
            if (dataString.length() > 0) {
                Intent newIntent = new Intent(this.getApplicationContext(), SettingsFragmentActivity.class);
                newIntent.putExtra(SettingsFragmentActivity.INTENT_PARAM_CONFIG_LINK, dataString);
                startActivity(newIntent);
            } else {
                File f = new File(data.getPath());
                SdbApplication.getInstance().copyFileToTempFolder(f);
                Document document = new Document();
                document.setName(f.getAbsoluteFile().getName());
                SdbApplication.getInstance().setmClipboardItem(document);
            }
            
			/*
            Intent intent = new Intent(this, OpenFileActivity.class);
			intent.setDataAndType(Uri.fromFile(f), "application/pdf");
			intent.setAction("android.intent.action.VIEW");
			startActivity(intent);
			*/
        }


        initActionBar();
        SdbApplication.getInstance().getSystemInformations(this);
        SdbApplication.getInstance().startSyncTasks(this);
    }

    private ActionBar initActionBar() {
        Log.v(TAG, Utils.getMethodName() + "entry");
        actionBar = getActionBar();
       
        actionBar.setTitle( "" );
        Drawable bgColor = getResources().getDrawable(R.color.grey);
        actionBar.setBackgroundDrawable(bgColor);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(R.layout.actionbar_view);
        //init
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeButtonEnabled(true);
        if (!mTwoPane)
            actionBar.setDisplayUseLogoEnabled(false);

        View actionBarView = actionBar.getCustomView();

        Spinner dropdownMenu = (Spinner) actionBarView.findViewById(R.id.dropdown_list);

        dropdownMenu.setVisibility(View.GONE);

        ActionBar.LayoutParams lp = new ActionBar.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
        lp.gravity = Gravity.RIGHT;
        actionBarView.setLayoutParams(lp);

        ImageButton buttonSettings = (ImageButton) actionBarView.findViewById(R.id.b_settings);
        buttonSettings.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "menu_settings");
/*
                    Intent newIntent = new Intent(v.getContext(), LoginActivity.class);
                    startActivity(newIntent);
*/
                Intent newIntent = new Intent(v.getContext(),  SettingsFragmentActivity.class);
                startActivity(newIntent);
            }
        });

        buttonWatchRecycled = (ImageButton) actionBarView.findViewById(R.id.b_watch_recycle);
        buttonWatchRecycled.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "buttonWatchRecycled");

                if (SdbApplication.getInstance().isShowRecycledItems()) {
                    SdbApplication.getInstance().setShowRecycle(false);
                    Drawable bg_not_selected = getResources().getDrawable(R.drawable.fns_btn_actionbar_selector);
                    buttonWatchRecycled.setBackgroundDrawable(bg_not_selected);

                } else {
                    SdbApplication.getInstance().setShowRecycle(true);
                    Drawable bg_selected = getResources().getDrawable(R.drawable.fns_orange_shape_selected);
                    buttonWatchRecycled.setBackgroundDrawable(bg_selected);
                }
                
                if (mTwoPane) {
                    SpacesFragment spacesFragment = (SpacesFragment) getSupportFragmentManager().findFragmentByTag(SpacesFragment.class.getSimpleName());
                    if (spacesFragment != null)
                        spacesFragment.getSpaces();
                    ItemListFragment itemListFragment = (ItemListFragment) getSupportFragmentManager().findFragmentByTag(ItemListFragment.class.getSimpleName());
                    if (itemListFragment != null)
                        itemListFragment.getChildren();
                }
                else {
                 Fragment currFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                   if (currFragment != null) {
                     if (currFragment instanceof SpacesFragment) {
                        ((SpacesFragment) currFragment).getSpaces();
                     }
                     else if (currFragment instanceof ItemListFragment)
                        ((ItemListFragment) currFragment).getChildren();
                     }
                }
            }
        });

        avatarIcon = (ImageView) actionBarView.findViewById(R.id.avatar_icon);

        User user = SdbApplication.getInstance().getCurrentUser(this);
        if (user !=null) {
            user.getAvatar(this);
            setAvatar(user);
        }
        
        return actionBar;
        
    }
    
    private void setAvatar(User user) {
        Log.v(TAG, Utils.getMethodName() + "entry");

        if (user != null){
            String avatarPath = user.getAvatarAbsPath();
            if (avatarPath != null ) {
                Bitmap bMap = BitmapFactory.decodeFile(avatarPath);
                avatarIcon.setImageBitmap(bMap);
            }
        }
    }

    
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.v(TAG, Utils.getMethodName() + "entry");
        if (savedInstanceState != null){

            selectedSpace = savedInstanceState.getParcelable(SELECTED_SPACE);
            currNavPosition = savedInstanceState.getInt(CURR_NAV_POSITION);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.v(TAG, Utils.getMethodName() + "entry");
        if(newConfig.orientation==Configuration.ORIENTATION_LANDSCAPE){

            Log.v("On Config Change","LANDSCAPE");
        }else{

            Log.v("On Config Change","PORTRAIT");
        }
        setNewActionBarWidth();
    }
    
    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        Log.v(TAG, Utils.getMethodName() + "entry");
        return retainedFragments; 
    }

    @Override
    public void onAttachFragment(Fragment fragment) {      
        super.onAttachFragment(fragment);
        Log.v(TAG, Utils.getMethodName() + "entry");
        
        // we need this here because when a configuration change happens,
        // onAttachFragment is called *before* onCreate
        retainedFragments = (FragmentHolder)
                getLastCustomNonConfigurationInstance();
        if (retainedFragments == null) {
            Log.v(TAG, Utils.getMethodName() + "retainedFragments == null");
            retainedFragments = new FragmentHolder();
        } 

        // repeat this 'if' clause for all Fragments
        if (fragment instanceof SpacesFragment) {
            retainedFragments.spacesFragment = (SpacesFragment) fragment;
            Log.v(TAG, Utils.getMethodName() + " spacesFragment attached");

        }

        if (fragment instanceof ItemListFragment) {
            retainedFragments.itemListFragment = (ItemListFragment) fragment;
            Log.v(TAG, Utils.getMethodName() + " itemListFragment attached");

        }
        // set remaining fragments into holder object...
    }
    
    
    private class FragmentHolder {
        SpacesFragment spacesFragment;
        ItemListFragment itemListFragment;
        // more fragments...
    }


    private void setNewActionBarWidth() {

        Log.v(TAG, Utils.getMethodName() + " entry");
        actionBar = initActionBar();
        
        if(mTwoPane){
            
            View actionBarView = actionBar.getCustomView();

            DisplayMetrics displaymetrics = Resources.getSystem().getDisplayMetrics();

            int display_width = displaymetrics.widthPixels;
            
            SpacesFragment spaceFragment =
                    (SpacesFragment) getSupportFragmentManager().findFragmentById(R.id.spaces_fragment);

            int fm_width = (display_width /12)*5 + 3; //spaceFragment.getView().getWidth();
            RelativeLayout actionbarRightLayout = (RelativeLayout) actionBarView.findViewById(R.id.actionbar_right_layout);

            int width_in_px =  (display_width - fm_width);

            Log.v(TAG, com.saperion.sdb.client.utils.Utils.getMethodName()
                    + " display_width " + display_width + " fm_width " + fm_width + " width_in_px " + width_in_px );
           
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width_in_px , LayoutParams.MATCH_PARENT);
            //layoutParams.setMargins(width_in_dp, 0, 0, 0);
            actionbarRightLayout.setLayoutParams(layoutParams);

        }
    }

    

    @Override
    public void invalidateOptionsMenu() {
        Log.v(TAG, Utils.getMethodName() + "entry");

    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.v(TAG, Utils.getMethodName() + "entry");
        // menu.add("Settings");
        //menu.add("Synchronize");
/*
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_action_bar, menu);
*/
        return true;

    }

    
    
    @Override
    public void onResume() {
        super.onResume();
        Log.v(TAG, Utils.getMethodName() + "entry");
        SdbApplication.getInstance().updateAccessToken();        
        
        /*
        if(selectedSpace != null) {
            SpacesFragment spaceFragment =
                    (SpacesFragment) getSupportFragmentManager().findFragmentById(R.id.spaces_fragment);
            
            if(spaceFragment != null){
                spaceFragment.setmSelectedSpace(selectedSpace);
                setNewActionBarWidth();
            }
        }
        */

        Fragment fragment = null;
                
        if (mTwoPane)
            fragment =  getSupportFragmentManager()
                .findFragmentByTag(ItemListFragment.class.getSimpleName());
        else
            fragment =  getSupportFragmentManager()
                    .findFragmentById(R.id.fragment_container);

        if (fragment != null & fragment instanceof ItemListFragment) {
            ((ItemListFragment) fragment).setmCurrentParentPosition(currNavPosition);
        }

        setNewActionBarWidth();
    }


    /**
     * checking keystore is locked or not
     */
    private void unlockKeyStore() {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2
                && Build.VERSION.SDK_INT > VERSION_CODES.HONEYCOMB_MR2) {

            KeyStoreDeprecated ksd = KeyStoreDeprecated.getInstance();

            if (ksd.state() == KeyStoreDeprecated.State.UNLOCKED) {
                return;
            }
            try {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
                    startActivity(new Intent(KeyStoreHelper.OLD_UNLOCK_ACTION));
                } else if (Build.VERSION.SDK_INT < VERSION_CODES.JELLY_BEAN_MR2) {
                    startActivity(new Intent(KeyStoreHelper.UNLOCK_ACTION));
                }
            } catch (ActivityNotFoundException e) {
                com.saperion.sdb.sdk.utils.Log.e(TAG, "No UNLOCK activity: " + e.getMessage());
            }
        } else {
            // TODO check lock/unlock status of new KeyStore API 
            Log.i(TAG, Utils.getMethodName() + "check lock/unlock status of new KeyStore API ");
        }
    }


    private void hideKeyboard() {

        InputMethodManager inputManager =
                (InputMethodManager) MainFragmentActivity.this
                        .getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(MainFragmentActivity.this.getCurrentFocus()
                .getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        Log.v(TAG, com.saperion.sdb.sdk.utils.Utils.getMethodName() + "entry");
        // Save the current  selection in case we need to recreate the fragment
        outState.putParcelable(SELECTED_SPACE, selectedSpace);

        ItemListFragment itemFragment = (ItemListFragment) getSupportFragmentManager()
                .findFragmentById(R.id.items_fragment);

        if (itemFragment != null) {
            currNavPosition = itemFragment.getmCurrentParentPosition();
            outState.putInt(SELECTED_SPACE, currNavPosition);
        }

        super.onSaveInstanceState(outState);
    }
    
    @Override
    protected void onPause() {
        super.onPause();
        Log.v(TAG, Utils.getMethodName() + "entry");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.v(TAG, Utils.getMethodName() + "entry");
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.v(TAG, Utils.getMethodName() + "entry ");

        ItemListFragment itemFragment = null;
        if (mTwoPane) 
        {
             itemFragment =
                    (ItemListFragment) getSupportFragmentManager().findFragmentById(
                            R.id.items_fragment);
        }
        else
        {
             itemFragment =
                    (ItemListFragment) getSupportFragmentManager().findFragmentById(
                            R.id.fragment_container);
        }
        
            if (itemFragment !=null && 
                 resultCode == Activity.RESULT_OK) {
                /*
                if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                {
                Uri uri = null;
                if (data != null) {
                    uri = data.getData();

                    String filePath = com.saperion.sdb.client.utils.Utils.getPath(getApplicationContext(), uri);

                    Log.v(TAG, com.saperion.sdb.sdk.utils.Utils.getMethodName() + "filePath " + filePath);
                    
                    Log.v(TAG, "Uri: " + uri.toString() +" filepath " + filePath);
                    Toast.makeText(this, filePath, Toast.LENGTH_LONG).show();
                    itemFragment.uploadFile(filePath);
                }
                }
                else    
                {
                    String file_path = data.getExtras().getString("file_path");
                    if (file_path !=null)
                     itemFragment.uploadFile(file_path);
                }*/
             
                // using of aFileChooser lib
                if (data != null) {
                    // Get the URI of the selected file
                    final Uri uri = data.getData();
                    android.util.Log.i(TAG, "Uri = " + uri.toString());
                    try {
                        // Get the file path from the URI
                        final String file_path = FileUtils.getPath(this, uri);
                        Toast.makeText(this,
                                "File selected: " + file_path, Toast.LENGTH_LONG).show();

                        if (file_path !=null)
                            itemFragment.uploadFile(file_path);
                        
                    } catch (Exception e) {
                        android.util.Log.e("FileSelectorTestActivity", "File select error", e);
                    }
                } 
            }
        
    }

    @Override
    public void onClick(View arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void OnNameOrIconClickedEvent(Space space) {
       
        Log.v(TAG, Utils.getMethodName() + "entry  " );
        selectedSpace = space;
        // Capture the article fragment createInstance the activity layout
        ItemListFragment mItemFragment = (ItemListFragment)
                getSupportFragmentManager().findFragmentById(R.id.items_fragment);

        if (mItemFragment != null) {
            // If item frag is available, we're in two-pane layout...
            Log.v(TAG, Utils.getMethodName() + "calling onItemClickedEvent  " );
            // Call a method in the ArticleFragment to updateData its content
            mItemFragment.onSpaceSelected(space);

        } else {
            // If the frag is not available, we're in the one-pane layout and must swap frags...

            //actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayUseLogoEnabled(false);

            // Create fragment and give it an argument for the selected space
            mItemFragment = new ItemListFragment();
            // save fragment ID for later use
            itemListFragmentID = mItemFragment.getId();

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            // Replace whatever is in the fragment_container view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            Bundle bundle = new Bundle();
            bundle.putParcelable(ItemListFragment.SPACE, space);
            mItemFragment.setArguments(bundle);
            transaction.replace(R.id.fragment_container, mItemFragment);
            transaction.addToBackStack(null);
            // Commit the transaction
            transaction.commit();

        }

        Spinner dropdownMenu = (Spinner) actionBar.getCustomView().findViewById(R.id.dropdown_list);
        dropdownMenu.setVisibility(View.VISIBLE);
    }

    /**
     * of selected space
     */
    @Override
    public void onItemClickedEvent(Space space) {

        if (space == null)
            return;

        if (mTwoPane) {

            OnNameOrIconClickedEvent(space);
        }

    }

    @Override
    public void onSpaceUpdate(Space space) {

        // updateData only if we have 2 panes
        if (!mTwoPane) return;

        ItemListFragment itemFragment =
                (ItemListFragment) getSupportFragmentManager().findFragmentById(
                        R.id.items_fragment);
        if (itemFragment != null){
            //itemFragment.updateBreadcrumbSpace(space);
            itemFragment.updateNavList(space);
            itemFragment.getChildren();
        }
    }

    @Override
    public void onSpaceDelete(Space space) {

        // updateData only if we have 2 panes
        if (!mTwoPane) return;
        ItemListFragment itemFragment =
                (ItemListFragment) getSupportFragmentManager().findFragmentById(
                        R.id.items_fragment);
        if (itemFragment != null)
            itemFragment.clearList();
    }

    private void showSettingsPopup(final Activity context, View v) {
        // int popupWidth = 200;
        // int popupHeight = 150;
        //
        // // Inflate the popup_layout.xml
        // LinearLayout viewGroup = (LinearLayout)
        // context.findViewById(R.id.popup);
        // LayoutInflater layoutInflater = (LayoutInflater) context
        // .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // View layout = layoutInflater.inflate(R.layout.popup_settings,
        // viewGroup);
        //
        // // Creating the PopupWindow
        // final PopupWindow popup = new PopupWindow(context);
        // popup.setContentView(layout);
        // popup.setWidth(popupWidth);
        // popup.setHeight(popupHeight);
        // popup.setFocusable(true);
        //
        // // Some offset to align the popup a bit to the right, and a bit down,
        // relative to button's position.
        // int OFFSET_X = 30;
        // int OFFSET_Y = 0;
        //
        // popup.showAsDropDown(v, OFFSET_X, OFFSET_Y);
    }


    @Override
    public void onRequestConnectionError(Request request, int statusCode) {

        Log.v(TAG, Utils.getMethodName() + "entry ");
    }

    @Override
    public void onRequestServiceError(Request request, Bundle resultData) {

        Log.v(TAG, Utils.getMethodName() + "entry ");
    }

    @Override
    public void onRequestDataError(Request request) {

        Log.v(TAG, Utils.getMethodName() + "entry ");
    }

    @Override
    public void onRequestFinished(Request request, Bundle resultData) {
        Log.v(TAG, Utils.getMethodName() + "entry ");
        int requestType = request.getRequestType();
        int resp_code = resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);
        
        Log.d(TAG, com.saperion.sdb.sdk.utils.Utils.getMethodName() + " requestType = " + requestType + " resultCode = "
                + resp_code);

        switch (resp_code) {
            case 0:
            case ResponseConstants.RESPONSE_CODE_OK:
            case ResponseConstants.RESPONSE_CODE_CREATED:
                break;

            case ResponseConstants.RESPONSE_CODE_BAD_REQUEST:
            case ResponseConstants.RESPONSE_CODE_ERROR:
            case ResponseConstants.RESPONSE_CODE_FORBIDDEN:
            case ResponseConstants.RESPONSE_CODE_INTERNAL_SERVER_ERROR:
            case ResponseConstants.RESPONSE_CODE_INTERNAL_SERVICE_UNAVAILABLE:
            case ResponseConstants.RESPONSE_CODE_METHOD_NOT_ALLOWED:
            case ResponseConstants.RESPONSE_CODE_NOT_FOUND:

            case ResponseConstants.RESPONSE_CODE_PRECONDITION_FAILED:
            case ResponseConstants.RESPONSE_CODE_TOO_MANY_REQUESTS:

                return;
            case ResponseConstants.RESPONSE_CODE_NO_CONTENT:
                return;

            default: {
                return;
            }
        }

        switch (requestType) {

            case RequestConstants.REQUEST_TYPE_USER_GET_AVATAR:
                User user = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                setAvatar(user);
                break;
            case RequestConstants.REQUEST_TYPE_GET_SYSTEM_INFOS:
                SystemInfo system_Info_infos = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                SdbApplication.getInstance().setSystemInfos(system_Info_infos);

                break;
            case RequestConstants.REQUEST_TYPE_SYNC_SPACE_LIST:
                ArrayList<Space> synchronizedSpaceList = resultData.getParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                if (synchronizedSpaceList != null && synchronizedSpaceList.size() > 0) {

                    for (Space syncSpace:synchronizedSpaceList){

                            Log.v(TAG, com.saperion.sdb.sdk.utils.Utils.getMethodName() + "syncSpace " + syncSpace.getName());

                    }
                    
                    ItemListFragment itemListFragment = null;
                    SpacesFragment spaceFragment = null;
                    // update list automatically
                    if (mTwoPane) {
                        
                        spaceFragment = (SpacesFragment) getSupportFragmentManager().findFragmentByTag(SpacesFragment.class.getSimpleName());

                         itemListFragment = (ItemListFragment) getSupportFragmentManager().
                                findFragmentByTag(ItemListFragment.class.getSimpleName());                      

                    }  else {
                        Fragment fragment  = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                        if  (fragment !=null &&  fragment instanceof ItemListFragment) {
                            itemListFragment = ((ItemListFragment) fragment);
                        } else if  (fragment !=null &&  fragment instanceof SpacesFragment) {
                            spaceFragment = ((SpacesFragment) fragment);
                        }
                    }
                    
                    if (spaceFragment != null)
                        spaceFragment.refreshListView();
                    
                    // get current opened space/folder and check it is in modified space list or not
                    if (itemListFragment != null) {
                        Spinner navSpinner = (Spinner) actionBar.getCustomView().findViewById(R.id.dropdown_list);
                        if (navSpinner != null) {
                            
                            if (navSpinner.getCount() < 1)  return;
                            
                            Map<String, Object> spinnerItem = (Map<String, Object>) navSpinner.getAdapter().getItem(navSpinner.getSelectedItemPosition());
                            Item item = (Item) spinnerItem.get(ItemListFragment.ACTION_DROPDOWN_ITEM);
                            if (item.getType().equals(Constants.SPACE_TYPE)) {
                                Space space = (Space ) item;
                                for (Space syncSpace:synchronizedSpaceList){
                                    if (syncSpace.getId().equals(space.getId())) {
                                        Log.v(TAG, com.saperion.sdb.sdk.utils.Utils.getMethodName() + "updating synchronized space " + syncSpace.getName());
                                        itemListFragment.getChildren();
                                        break;
                                    }
                                }
                            } else if  (item.getType().equals(Constants.FOLDER_TYPE)) {
                                Folder folder = (Folder ) item;
                                for (Space syncSpace:synchronizedSpaceList){
                                    if (syncSpace.getId().equals(folder.getSpaceId())) {
                                        Log.v(TAG, com.saperion.sdb.sdk.utils.Utils.getMethodName() + "updating synchronized folder " + folder.getName());
                                        itemListFragment.getChildren();
                                        break;
                                    }
                                }
                            }
                                
                        }
                        
                        
                    }
                    
                }
                break;
        }
    }
    
      @Override
      public boolean onOptionsItemSelected(MenuItem item) {
          
          switch (item.getItemId()) {
              case android.R.id.home:

                  Log.v(TAG, Utils.getMethodName() + "home ");

                  Spinner dropdownMenu = (Spinner) actionBar.getCustomView().findViewById(R.id.dropdown_list);
                  int currPos = dropdownMenu.getSelectedItemPosition();
                  Log.v(TAG, Utils.getMethodName() + "curr pos " +  currPos);
                  Fragment fragment = null;
                  
                  if (mTwoPane) {
                      fragment = getSupportFragmentManager().findFragmentByTag(SpacesFragment.class.getSimpleName());
                      ItemListFragment itemListFragment = (ItemListFragment) getSupportFragmentManager().
                              findFragmentByTag(ItemListFragment.class.getSimpleName());
                      if  (itemListFragment !=null) {
                          itemListFragment.navigateToItem(currPos-1);

                         
                      }
                  }  else {
                      fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                      if  (fragment !=null &&  fragment instanceof ItemListFragment) {
                          ((ItemListFragment) fragment).navigateToItem(currPos-1);
                      }
                  }
                  
                  if (currPos == 0){
                      dropdownMenu.setVisibility(View.GONE);
                      actionBar.setDisplayHomeAsUpEnabled(false);
                      
                      if (fragment !=null && fragment instanceof SpacesFragment)     {
                          ((SpacesFragment) fragment).setmSelectedSpace(null);
                          ((SpacesFragment) fragment).refreshListView();

                      } else {
                             if  (fragment !=null &&  fragment instanceof ItemListFragment)     {

                                 // Create fragment and give it an argument for the selected space
                                 spaceFragment = new SpacesFragment();

                                 FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

                                 // Replace whatever is in the fragment_container view with this fragment,
                                 // and add the transaction to the back stack
                                 transaction.replace(R.id.fragment_container, spaceFragment);
                                 transaction.addToBackStack(null);
                                 transaction.commit();

                          }
                      }
                    
                  }
      
                  return true;
              case 2:
  
                  return true;
              default:
                  return super.onOptionsItemSelected(item);
              
  
          }
  
      }  
}



