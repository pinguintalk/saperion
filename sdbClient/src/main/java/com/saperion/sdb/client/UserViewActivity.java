package com.saperion.sdb.client;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.saperion.sdb.client.utils.Log;
import com.saperion.sdb.sdk.SdbApplication;
import com.saperion.sdb.sdk.models.User;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.RequestConstants;
import com.saperion.sdb.sdk.requestmanager.RequestManager.RequestListener;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.utils.Utils;

import java.util.ArrayList;

/**
 * Created by nnn on 12.11.13.
 */
public class UserViewActivity extends Activity implements UserListViewInterface, RequestListener {

    private static final String TAG = UserViewActivity.class.getSimpleName();
    GridView gridView;

    private static ArrayList<User> userList = new ArrayList<User>();
    private UserListAdapter userListAdapter;
    private ProgressDialog progressDialog = null;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.users_activity);

        SdbApplication.getInstance().getUserList(this);
        gridView = (GridView) findViewById(R.id.user_grid_view);

        userListAdapter = new UserListAdapter(getApplicationContext(),this, userList);

        gridView.setAdapter(userListAdapter);

        gridView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                Toast.makeText(getApplicationContext(),
                        ((TextView) v).getText(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public void onDeleteUser(User user) {
        
    }

    @Override
    public void onUpdateUser(User user) {
        showLoadingDialog();
        user.save(this);
    }

    @Override
    public void onGetUserAvatar(User user) {

        user.getAvatar(this);
    }

    @Override
    public void onCreateUser(User user) {

    }

    @Override
    public void onRequestFinished(Request request, Bundle resultData) {

        cancelLoadingDialog();
        int resp_code = resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);

        String errorMessage = resultData.getString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE);

        int requestType = request.getRequestType();
        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " resultCode = "
                + resp_code);

        switch (resp_code) {
            case 0:
            case ResponseConstants.RESPONSE_CODE_OK:
            case ResponseConstants.RESPONSE_CODE_CREATED:
                break;

            case ResponseConstants.RESPONSE_CODE_BAD_REQUEST:
            case ResponseConstants.RESPONSE_CODE_ERROR:
            case ResponseConstants.RESPONSE_CODE_FORBIDDEN:
            case ResponseConstants.RESPONSE_CODE_INTERNAL_SERVER_ERROR:
            case ResponseConstants.RESPONSE_CODE_INTERNAL_SERVICE_UNAVAILABLE:
            case ResponseConstants.RESPONSE_CODE_METHOD_NOT_ALLOWED:
            case ResponseConstants.RESPONSE_CODE_NOT_FOUND:

            case ResponseConstants.RESPONSE_CODE_PRECONDITION_FAILED:
            case ResponseConstants.RESPONSE_CODE_TOO_MANY_REQUESTS:
                showServiceErrorDialog(errorMessage);
                return;
            case ResponseConstants.RESPONSE_CODE_NO_CONTENT:
                return;
            
            default: {
                showServiceErrorDialog("unkown error");
                return;
            }
        }

        switch (requestType) {

            case RequestConstants.REQUEST_TYPE_USER_GET_LIST:

                ArrayList<User> tmpUserList =
                        resultData.getParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);

                userList.clear();
                userList.addAll(tmpUserList);
                userListAdapter.notifyDataSetChanged();
                break;

            case RequestConstants.REQUEST_TYPE_USER_DELETE:
                User deleted_user = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                userList.remove(deleted_user);
                userListAdapter.notifyDataSetChanged();
                break;

            case RequestConstants.REQUEST_TYPE_USER_CREATE:               
            case RequestConstants.REQUEST_TYPE_USER_GET_AVATAR:
                User user = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                int pos = updateUserInList(userList,user);

                View userCellView =
                        gridView.getChildAt(pos);
                if (userCellView != null && userListAdapter != null) {
                    if (requestType == RequestConstants.REQUEST_TYPE_USER_GET_AVATAR)
                        userListAdapter.updateAvatar(userCellView, user);
                    else
                        userListAdapter.updateData(userCellView, user);
                }
                break;

        }

        
    }

    @Override
    public void onRequestConnectionError(Request request, int statusCode) {
        cancelLoadingDialog();

        int requestType = request.getRequestType();
        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " statusCode = "
                + statusCode);
    }

    @Override
    public void onRequestDataError(Request request) {
        cancelLoadingDialog();

        int requestType = request.getRequestType();
        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType );
    }

    @Override
    public void onRequestServiceError(Request request, Bundle resultData) {
        cancelLoadingDialog();
        int resp_code = resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);

        String errorMessage = resultData.getString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE);

        int requestType = request.getRequestType();
        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " resultCode = "
                + resp_code);
    }
    
    /*
    *  updateData cached list
   */
    private int updateUserInList(ArrayList<User> userList, User user) {


        String id = user.getId();

        for (int i = 0; i < userList.size(); i++) {
            if (userList.get(i).getId().equals(id) ) {
                userList.set(i, user);
                return i;
            }
        }

        return -1;

    }
    


    private void showServiceErrorDialog(String message) {


        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Service Error");
        builder.setMessage(message);

        builder.setPositiveButton(getResources().getString(R.string.close),
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        // Do do my action here

                        dialog.dismiss();
                    }

                });
        /*
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // I do not need any action here you might
                dialog.dismiss();
            }
        });
        */
        AlertDialog alert = builder.create();
        alert.show();

    }

    public void cancelLoadingDialog() {

        if (progressDialog != null) {
            progressDialog.cancel();
            progressDialog = null;
        }

    }

    public void showLoadingDialog() {

        
        if (progressDialog != null) {
            progressDialog.cancel();
            progressDialog = null;
        }

        progressDialog = new ProgressDialog(this);

        progressDialog.setTitle(getResources().getString(R.string.progress_dialog_title));
        progressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        progressDialog.setCancelable(true);
        progressDialog.setIndeterminate(true);

        progressDialog.show();
        
    }
}