package com.saperion.sdb.client;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.saperion.sdb.client.R.color;
import com.saperion.sdb.client.R.id;
import com.saperion.sdb.client.R.layout;
import com.saperion.sdb.client.utils.Log;
import com.saperion.sdb.sdk.SdbApplication;
import com.saperion.sdb.sdk.models.Activity;
import com.saperion.sdb.sdk.models.Document;
import com.saperion.sdb.sdk.models.User;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.RequestConstants;
import com.saperion.sdb.sdk.requestmanager.RequestManager.RequestListener;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.utils.Utils;

import java.util.ArrayList;

public class DocumentActivityListAdapter extends ArrayAdapter<Activity> {

    private static final String TAG = DocumentActivityListAdapter.class.getSimpleName();

    private static LayoutInflater mInflater = null;
    private int mLayoutType = 1;
    private Context mContext;
    private int mCurrSelectedItemPosition = -1;
    private String mCurrentSelectedItemId = null;

    private View mCurrentSelectedView = null;
    private DocumentInfoListViewInterface mCallback;

    private boolean isInEditMode = false;

    public DocumentActivityListAdapter(Context context, DocumentInfoListViewInterface documentInfoListViewInterface,
                                       ArrayList<Activity> documentActivityList) {
        super(context, 0, documentActivityList);
        this.mContext = context;
        this.mCallback = documentInfoListViewInterface;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public boolean isInEditMode() {
        return isInEditMode;
    }

    public void setInEditMode(boolean isInEditMode) {
        this.isInEditMode = isInEditMode;
    }

    public View getmCurrentSelectedView() {
        return mCurrentSelectedView;
    }

    public void setmCurrentSelectedView(View mCurrentSelectedView) {
        this.mCurrentSelectedView = mCurrentSelectedView;
    }

    public String getmCurrentSelectedItemId() {
        return mCurrentSelectedItemId;
    }

    public int getCurrSelectedItemPosition() {
        return mCurrSelectedItemPosition;

    }

    public void setCurrSelectedItemPosition(int pos) {
        this.mCurrSelectedItemPosition = pos;

    }

    public void setmCurrentSelectedItemId(String mCurrentSelectedItemId) {
        this.mCurrentSelectedItemId = mCurrentSelectedItemId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.v(TAG, Utils.getMethodName() + "entry ");
        View view = convertView;

        Activity item = getItem(position);

        if (convertView == null) {

            view = mInflater.inflate(layout.document_activity_list_cell, null);
            view.setTag(new ViewHolder(view));
        }

        ((ViewHolder) view.getTag()).populateView(item);

        return view;

    }


    class ViewHolder {
        private RelativeLayout relativeLayout;
        View mView;
        private ImageView avatar;
        RequestListener listener;
        private TextView userNameTextView;      
        private TextView activityText;
        private TextView timeText;

        private LinearLayout buttonSetLayout;
        private Document mCurrOpenedDocument = null;
        private ImageView mImageViewFavorit;

        public ViewHolder(View view) {
            this.mView = view;

            avatar = (ImageView) mView.findViewById(id.avatar);
            listener = new RequestListener() {

                @Override
                public void onRequestFinished(Request request, Bundle resultData) {

                    int resp_code = resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);

                    int requestType = request.getRequestType();
                    Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " resultCode = "
                            + resp_code);
                    switch (requestType) {
                        case RequestConstants.REQUEST_TYPE_USER_GET_AVATAR:
                            User user = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                            String avatarPath = user.getAvatarAbsPath();
                            if (avatarPath !=null && avatarPath.length() > 0) {
                                Bitmap bitmap = BitmapFactory.decodeFile(user.getAvatarAbsPath());
                                avatar.setImageBitmap(bitmap);
                            }
                            break;
                    }
                }

                @Override
                public void onRequestConnectionError(Request request, int statusCode) {

                }

                @Override
                public void onRequestDataError(Request request) {

                }

                @Override
                public void onRequestServiceError(Request request, Bundle resultData) {

                }
            };
            relativeLayout = (RelativeLayout) mView.findViewById(id.document_activity_cell);
            userNameTextView = (TextView) mView.findViewById(id.username_txt);
            activityText = (TextView) mView.findViewById(id.activity_txt);
            timeText = (TextView) mView.findViewById(id.time_txt);
        }

        public void populateView(Activity item) {

            User user = SdbApplication.getInstance().getUser(item.getOwnerId());
            if (user != null)  {
                user.getAvatar(listener);
            }
            
            userNameTextView.setText(item.getOwnerName());
            userNameTextView.setTextColor(getContext().getResources().getColor(color.orange));
            activityText.setText(item.getActivity());
            
            String date_string= "";
            
            String last_modified =item.getCreationDate();
            if (last_modified != null && last_modified.length() > 0) {
                date_string = com.saperion.sdb.client.utils.Utils.getTimeExtraFormat(mContext,last_modified);
            }
            timeText.setText(date_string);
        }

    }
}
