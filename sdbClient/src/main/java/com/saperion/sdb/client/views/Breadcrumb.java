package com.saperion.sdb.client.views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saperion.sdb.sdk.models.Item;

import java.util.ArrayList;
import java.util.List;

public class Breadcrumb extends LinearLayout {

    private List<? super Item> mCurrentItemList = new ArrayList<Item>();
    private List<TextView> mTextViewList = new ArrayList<TextView>();
    private List<String> path = new ArrayList<String>();
    private TextView mTvCrumb;
    private OnClickListener navListener;
    private static int width;

    public Breadcrumb(Context context) {
        super(context);
    }

    public Breadcrumb(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public List<? super Item> getCurrentItemList() {
        return mCurrentItemList;
    }

    public void setCurrentItemList(List<? super Item> mItemList) {
        this.mCurrentItemList = mItemList;
    }

    public List<TextView> getTextViewList() {
        return mTextViewList;
    }

    public int getCurrentItemListSize() {
        return mCurrentItemList.size();
    }

    public void addElement(Item item, OnClickListener listener) {
        navListener = listener;
        fillmElements();
        mCurrentItemList.add(item);
        createElement(mCurrentItemList.size() - 1);
        // cutBreadCrumb();
        drawElements();
    }

    public void updateSpace(Item item, OnClickListener listener) {
        navListener = listener;
        mCurrentItemList.remove(0);
        fillmElements();
        mCurrentItemList.add(0, item);
        createElement(0);
        //cutBreadCrumb();
        drawElements();
    }

    public void removeLastElement() {
        mCurrentItemList.remove(mCurrentItemList.size() - 1);
        fillmElements();
        //cutBreadCrumb();
        drawElements();
    }

    public void navigateToItem(int position) {
        while (position != mCurrentItemList.size() - 1) {
            this.removeLastElement();
        }
    }

    public Item getCurrentItem() {
        return ((Item) mCurrentItemList.get(mCurrentItemList.size() - 1));
    }

    public String getCurrentLocationId() {
        return ((Item) mCurrentItemList.get(mCurrentItemList.size() - 1)).getId();
    }

    public String getCurrentLocationName() {
        return ((Item) mCurrentItemList.get(mCurrentItemList.size() - 1)).getName();
    }

    public String getCurrentLocationType() {
        return ((Item) mCurrentItemList.get(mCurrentItemList.size() - 1)).getType();
    }

    public Item getCurrentLocationItem() {
        return ((Item) mCurrentItemList.get(mCurrentItemList.size() - 1));
    }

    private void cutBreadCrumb() {

        int spaceMin = (int) (width * 0.3);
        Boolean removed = false;

        Paint paint = new Paint();
        paint.setTypeface(Typeface.DEFAULT);
        final float densityMultiplier = getContext().getResources().getDisplayMetrics().density;
        final float scaledPx = 18 * densityMultiplier;
        paint.setTextSize(scaledPx);

        int bcWidth = 0;
        for (TextView temp : mTextViewList) {
            bcWidth += paint.measureText(temp.getText().toString());
        }

        while (width < bcWidth) {
            bcWidth = 0;
            if (!(mTextViewList.size() > 1)) {
                CharSequence text = mTextViewList.get(0).getText();
                text = text.subSequence(0, text.length() - 4) + "...";

                //String text = mTextViewList.get(0).getText().toString();
                //text = text.substring(0, Math.min(text.length(), 4)) + "...";

                mTextViewList.get(0).setText(text);

            } else if (!(mTextViewList.size() > 2)) {
                int spaceWidth = (int) paint.measureText(mTextViewList.get(0).getText().toString());
                if (spaceWidth > spaceMin) {
                    CharSequence text = mTextViewList.get(0).getText();
                    text = text.subSequence(0, text.length() - 4) + "...";
                    mTextViewList.get(0).setText(text);
                } else {
                    CharSequence text = mTextViewList.get(1).getText();
                    text = text.subSequence(0, text.length() - 4) + "...";
                    mTextViewList.get(1).setText(text);
                }

            } else if (mTextViewList.size() > 2) {
                if (!removed) {
                    CharSequence text = mTextViewList.get(1).getText();

                    text = text.subSequence(0, text.length() - 4) + "...";
                    if (text.length() <= 6) {
                        mTextViewList.get(1).setText(" > ...");
                        removed = true;
                    } else
                        mTextViewList.get(1).setText(text);
                } else {
                    CharSequence text = mTextViewList.get(2).getText();
                    text = text.subSequence(0, text.length() - 4) + "...";
                    if (text.length() <= 6) {
                        mTextViewList.remove(2);
                    } else
                        mTextViewList.get(2).setText(text);
                }
            }
            for (TextView temp : mTextViewList) {
                bcWidth += paint.measureText(temp.getText().toString());
            }
        }
        if (removed) {
            mTextViewList.get(1).setId(mCurrentItemList.size() - mTextViewList.size() + 1);
        }

    }

    private void fillmElements() {
        mTextViewList.clear();
        for (int i = 0; i < mCurrentItemList.size(); i++) {
            createElement(i);
        }
    }

    private void drawElements() {
        //set color
        CharSequence colorText = mTextViewList.get(mTextViewList.size() - 1).getText();
        Spannable WordtoSpan = new SpannableString(colorText);
        WordtoSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#ffbb33")), 3,
                colorText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mTextViewList.get(mTextViewList.size() - 1).setText(WordtoSpan);
        //draw elements
        this.removeAllViews();
        int i = 0;
        for (TextView temp : mTextViewList) {
            this.addView(temp, i);
            i++;
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        //check if there is a new Element
        View parent = (View) this.getParent().getParent();
        //bc
        //ImageButton bSet = (ImageButton) parent.findViewById(R.id.b_settings);
        
        //width = parent.getWidth() - bSet.getWidth() - 15;
        super.onLayout(changed, l, t, r, b);
    }

    private void createElement(int index) {
        mTvCrumb = new TextView(getContext());
        mTvCrumb.setText(" > " + ((Item) mCurrentItemList.get(index)).getName());
        mTvCrumb.setTextSize(18);
        mTvCrumb.setTypeface(Typeface.DEFAULT);
        mTvCrumb.setId(index);
        mTvCrumb.setClickable(true);
        mTvCrumb.setOnClickListener(navListener);
        mTextViewList.add(index, mTvCrumb);
    }

}
