package com.saperion.sdb.client;

import android.app.Activity;
import android.content.Context;
import android.database.CharArrayBuffer;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saperion.sdb.client.R.id;
import com.saperion.sdb.client.utils.Log;
import com.saperion.sdb.sdk.models.Document;
import com.saperion.sdb.sdk.models.Folder;
import com.saperion.sdb.sdk.models.Item;
import com.saperion.sdb.sdk.models.SpacedItem;
import com.saperion.sdb.sdk.states.DocumentState;
import com.saperion.sdb.sdk.utils.Utils;

import java.io.File;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;

public class ItemListAdapter<ItemType> extends ArrayAdapter<ItemType> {

    protected static final String TAG = ItemListAdapter.class.getSimpleName();

    private static LayoutInflater inflater = null;
    private Activity context;

    private int mOpenItemPosition = -1;
    private ItemListViewInterface mCallback;

    private String mCurrentSelectedItemId = "";
    private boolean isInEditMode = false;

    public boolean isInEditMode() {
        return isInEditMode;
    }

    public void setInEditMode(boolean isInEditMode) {
        this.isInEditMode = isInEditMode;
    }

    public ItemListAdapter(Activity context, ItemListViewInterface iTemClickedCallback,
                           ArrayList<ItemType> items) {
        super(context, 0, items);
        this.context = context;
        this.mCallback = iTemClickedCallback;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SpacedItem item = (SpacedItem) getItem(position);

        // Log.v(TAG, Utils.getMethodName() + "id " +item.getId());
        // Log.v(TAG, Utils.getMethodName() + "name " +item.getName());

        if (convertView == null) {

            ItemViewHolder view_holder = null;
            convertView = inflater.inflate(R.layout.item_cell, parent, false);
            try {
                view_holder = new ItemViewHolder(convertView);
                convertView.setTag(view_holder);
            } catch (ClassNotFoundException e) {
                // TODO Auto-generated catch block
                Log.e(TAG, Utils.getMethodName() + "ClassNotFoundException " + e.toString());
            }
        }

        if (mCurrentSelectedItemId.equals(item.getId())) { // is cell
            // expanded
            ((ItemViewHolder) convertView.getTag()).showOpenView(item);
            if (isInEditMode()) {
                ((ItemViewHolder) convertView.getTag()).showEditMode();
            }
        } else
            ((ItemViewHolder) convertView.getTag()).populateView(item);

        return convertView;
    }

    public int getmOpenItemPosition() {
        return mOpenItemPosition;
    }

    public void setmOpenItemPosition(int mOpenItemPosition) {
        this.mOpenItemPosition = mOpenItemPosition;
    }

    public String getmCurrentSelectedItemId() {
        return mCurrentSelectedItemId;
    }

    public void setmCurrentSelectedItemId(String mCurrentSelectedItemId) {
        this.mCurrentSelectedItemId = mCurrentSelectedItemId;
    }

    public void update(View mView, Item new_item) {
        if (mView != null) {
            EditText mEditTextName = (EditText) mView.findViewById(R.id.item_edit_name);
            EditText mEditTextDescription =
                    (EditText) mView.findViewById(R.id.item_edit_description);
            EditText mEditTextTags = (EditText) mView.findViewById(R.id.item_edit_description);

            mEditTextName.setText(new_item.getName());

            mEditTextDescription.setText(new_item.getDescription());
            mEditTextTags.setText(new_item.getTags().toString());
        }

    }

    public void updatePreview(View mView, Document doc) {
        if (mView == null)
            return;
        if (doc == null)
            return;

        ImageView mItemIcon = (ImageView) mView.findViewById(R.id.item_icon);
        String preview_file = doc.getPreviewAbsPath();
        if (preview_file != null && preview_file.length() > 0 ) {
           
            File file = new java.io.File(preview_file);
            if (file.exists()) {
            // File file = context.getFileStreamPath(preview_file);
            Log.v(TAG, Utils.getMethodName() + "updating preview " + file.getAbsolutePath());

            Bitmap bMap = BitmapFactory.decodeFile(preview_file);
            mItemIcon.setImageBitmap(bMap);
            }
        }
        else {
            mItemIcon.setImageResource(R.drawable.default_doc);
        }
    }

    class ItemViewHolder {

        private boolean isIconAndNameClicked = false;
        private String spacedItemId = "";

        public String getSpacedItemId() {
            return spacedItemId;
        }

        public void setSpacedItemId(String spacedItemId) {
            this.spacedItemId = spacedItemId;
        }

        private ImageView mItemIcon;

        private TextView mTextViewName;
        private TextView mTextViewDescription;
        private TextView mTextViewTags;

        private LinearLayout buttonSetLayout;
        private LinearLayout editButtonSetLayout;

        private CharArrayBuffer mCharArrayBufferName;
        private CharArrayBuffer mCharArrayBufferDescription;
        private CharArrayBuffer mCharArrayBufferStates;
        private View mView = null;

        private SpacedItem mItem = null;

        private ImageButton mButtonEdit;
        private ImageButton mButtonOk;       
        private ImageButton mButtonCut;
        private ImageButton mButtonDownload;
        private ImageButton mButtoOpenLinkDialog;
        private ImageButton mButtonCancel;
        private ImageButton mButtonRecycle;
        private ImageButton mButtonDelete;
        private ImageButton mButtonRestore;
        
        private EditText mEditTextName;
        private EditText mEditTextDescription;
        private EditText mEditTextTags;

        private TextView mTextViewDate;
        private TextView mTextViewSize;

        private ImageView mLinkListIcon;
        private ImageView mCommentIcon;
        private ImageView mDeletedBanner;
        
        private String currentItemType = "";

        private NumberFormat formatter = new DecimalFormat("#0.00");
        private final String[] Q = new String[]{"B", "KB", "MB", "GB", "TB", "PB", "EB"};

        public String getCurrentItemType() {
            return currentItemType;
        }

        public void setCurrentItemType(String currentItemType) {
            this.currentItemType = currentItemType;
        }

        public ItemViewHolder(View view) throws ClassNotFoundException {
            mView = view;
            // Typeface
            // font=Typeface.createFromAsset(context.getAssets(),"HelveticaNeue.ttf");
            mItemIcon = (ImageView) mView.findViewById(R.id.item_icon);
            mTextViewName = (TextView) mView.findViewById(R.id.item_name);

            mItemIcon.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // setIconOrNameClicked(true);

                    Log.v(TAG, Utils.getMethodName() + "mItemIcon ");
                    
                    if (mCallback != null)

                        mCallback.itemIconOrNameClickedEvent(mItem);
                }
            });

            // mTextViewName.setTypeface(font);
            mTextViewName.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // setIconOrNameClicked(true);
                    Log.v(TAG, Utils.getMethodName() + "mTextViewName ");
                    if (mCallback != null) {

                        mCallback.itemIconOrNameClickedEvent(mItem);
                    }
                }
            });
            mDeletedBanner  = (ImageView) mView.findViewById(id.deleted_icon);
            mButtonRecycle = (ImageButton) mView.findViewById(R.id.b_trash_recycle);

            mButtonRecycle.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // setIconOrNameClicked(true);
                    Log.v(TAG, Utils.getMethodName() + "mButtonRecycle ");

                    setInEditMode(false);

                    if (mCallback != null)
                        mCallback.itemRecycleClickedEvent(mItem);
                }
            });

            mButtonDelete = (ImageButton) mView.findViewById(id.b_delete);

            mButtonDelete.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // setIconOrNameClicked(true);
                    Log.v(TAG, Utils.getMethodName() + "mButtonDelete ");

                    setInEditMode(false);

                    if (mCallback != null)
                        mCallback.itemDeleteClickedEvent(mItem);
                }
            });
            mButtonRestore = (ImageButton) mView.findViewById(id.b_restore);
            mButtonRestore.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // setIconOrNameClicked(true);
                    Log.v(TAG, Utils.getMethodName() + "mButtonRestore ");

                    if (mCallback != null){
                        setInEditMode(false);
                        mCallback.itemRestoreClickedEvent(mItem);
                    }
                }
            });
            
            mButtonDownload = (ImageButton) mView.findViewById(R.id.b_invite_user);
            mButtonDownload.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Log.v(TAG, Utils.getMethodName() + "mButtonDownload ");

                    if (mCallback != null)
                        mCallback.itemDownloadClickedEvent(mItem);
                }
            });

            mButtonCut = (ImageButton) mView.findViewById(R.id.b_cut);
            mButtonCut.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Log.v(TAG, Utils.getMethodName() + "mButtonCut ");

                    if (mCallback != null)
                        mCallback.itemCutClickedEvent(mItem);
                }
            });

            mButtonOk = (ImageButton) mView.findViewById(R.id.b_ok);

            mButtonOk.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Log.v(TAG, Utils.getMethodName() + "mButtonOk ");
                    setInEditMode(false);

                    if(isItemRecycled(mItem))
                    {
                        showOpenView(mItem);
                        return;
                    }
                    
                    
                    if (mCallback != null) {
                        if (isDocument(mItem))
                        {
                            Document doc = (Document) mItem;
                            doc.setName(mEditTextName.getText().toString());
                            doc.setDescription(mEditTextDescription.getText().toString());
                            doc.setTags(Arrays.asList(mEditTextTags.getText().toString()
                                    .split("\\s*,\\s*")));
                            mCallback.itemOkClickedEvent(doc);
                        }
                        else
                        {
                            Folder folder = (Folder) mItem;
                            folder.setName(mEditTextName.getText().toString());
                            folder.setDescription(mEditTextDescription.getText().toString());
                            folder.setTags(Arrays.asList(mEditTextTags.getText().toString()
                                    .split("\\s*,\\s*")));
                            mCallback.itemOkClickedEvent(folder);
                        }

                        if (!mItem.getName().equals("")) {
                            editButtonSetLayout.setVisibility(View.GONE);
                            setEditTextFieldsGone();
                            setTextViewFieldsVisible();


                            mButtonCut.setVisibility(View.VISIBLE);
                            mButtonEdit.setVisibility(View.VISIBLE);
                        }
                    }
                }
            });

            mTextViewDescription = (TextView) mView.findViewById(R.id.item_description);
            // mTextViewDescription.setTypeface(font);
            mTextViewTags = (TextView) mView.findViewById(R.id.item_tags);
            buttonSetLayout = (LinearLayout) mView.findViewById(R.id.item_button_set);
            editButtonSetLayout = (LinearLayout) mView.findViewById(R.id.item_edit_button_set);
            mCharArrayBufferName = new CharArrayBuffer(20);
            mCharArrayBufferDescription = new CharArrayBuffer(20);
            mCharArrayBufferStates = new CharArrayBuffer(20);

            mEditTextName = (EditText) mView.findViewById(R.id.item_edit_name);
            mEditTextDescription = (EditText) mView.findViewById(R.id.item_edit_description);
            mEditTextTags = (EditText) mView.findViewById(R.id.item_edit_tags);

            mButtonCut = (ImageButton) mView.findViewById(R.id.b_cut);
            mButtoOpenLinkDialog = (ImageButton) mView.findViewById(R.id.b_open_link_dialog);
            mButtoOpenLinkDialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.v(TAG, Utils.getMethodName() + "mButtoOpenLinkDialog open links of " + mItem.getName() + " "+ mItem.getId());
                    
                    mCallback.itemOpenLinkDialogClickedEvent(context, (Document) mItem);
                }

            });
            mButtonDownload = (ImageButton) mView.findViewById(R.id.b_invite_user);

            mTextViewDate = (TextView) mView.findViewById(R.id.item_time);
            mTextViewSize = (TextView) mView.findViewById(R.id.item_size);

            mLinkListIcon = (ImageView) mView.findViewById(R.id.b_list_link);
            mCommentIcon = (ImageView) mView.findViewById(R.id.b_comment);

            mButtonEdit = (ImageButton) mView.findViewById(R.id.b_edit);
            mButtonEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
            
                        setInEditMode(true);
                        showEditMode();
                    }

            });

            mButtonCancel = (ImageButton) mView.findViewById(R.id.b_cancel);
            mButtonCancel.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Log.v(TAG, Utils.getMethodName() + "mButtonEdit ");
                    setInEditMode(false);
                    if (getmCurrentSelectedItemId().equals("")) {
                        remove((ItemType) mItem);
                        return;
                    }
                    
                    showOpenView(mItem);
                }
            });

        }
        
        private boolean isDocument(SpacedItem mItem){

            String itemType = ((SpacedItem) mItem).getType();

            return (itemType.equals(Constants.DOCUMENT_TYPE)) ;

        }

        
        private boolean isItemRecycled(SpacedItem mItem){

            String itemType = ((SpacedItem) mItem).getType();
            boolean itemIsRecycled = false;

            if (itemType.equals(Constants.FOLDER_TYPE)) {

                if ( ((Folder) mItem).isRecycled() )
                    itemIsRecycled = true;

            } else if (itemType.equals(Constants.DOCUMENT_TYPE)) {

                if ( ((Document) mItem).isRecycled() )
                    itemIsRecycled = true;

            }
            return itemIsRecycled;
        }

        private void setEditTextFieldsGone(){
            mEditTextName.setEnabled(true);
            mEditTextName.setVisibility(View.GONE);

            mEditTextDescription.setEnabled(true);
            mEditTextDescription.setVisibility(View.GONE);
            mEditTextTags.setVisibility(View.GONE);
        }

        private void setEditTextFieldsVisible(){
            mEditTextName.setEnabled(true);
            mEditTextName.setVisibility(View.VISIBLE);

            mEditTextDescription.setEnabled(true);
            mEditTextDescription.setVisibility(View.VISIBLE);
            mEditTextTags.setVisibility(View.VISIBLE);
        }

        private void setTextViewFieldsVisible(){
            mTextViewName.setVisibility(View.VISIBLE);
            mTextViewDescription.setVisibility(View.VISIBLE);
            mTextViewTags.setVisibility(View.VISIBLE);
        }


        private void setTextViewFieldsGone(){
            mTextViewName.setVisibility(View.GONE);
            mTextViewDescription.setVisibility(View.GONE);
            mTextViewTags.setVisibility(View.GONE);
        }
        
        private void showEditMode() {
            editButtonSetLayout.setVisibility(View.VISIBLE);

            mTextViewDate.setVisibility(View.GONE);
            mTextViewSize.setVisibility(View.GONE);
            

            mButtonDownload.setVisibility(View.GONE);
            mButtonCut.setVisibility(View.GONE);
            mButtonEdit.setVisibility(View.GONE);
            mButtonRestore.setVisibility(View.GONE);
            mButtoOpenLinkDialog.setVisibility(View.GONE);
            mButtonDelete.setVisibility(View.GONE);

            
            if ( isItemRecycled(mItem)){
                buttonSetLayout.setVisibility(View.GONE);
                mButtonRestore.setVisibility(View.VISIBLE);
                mButtonDelete.setVisibility(View.VISIBLE);
                mButtonRecycle.setVisibility(View.GONE);
                setTextViewFieldsVisible();
                setEditTextFieldsGone();
            }
            else {
                mButtonRestore.setVisibility(View.GONE);
                mButtonRecycle.setVisibility(View.VISIBLE);
                setEditTextFieldsVisible();
                setTextViewFieldsGone();
            }
            
            mEditTextName.setText(mTextViewName.getText().toString());
            mEditTextDescription.setText(mTextViewDescription.getText().toString());
            String tagText = mTextViewTags.getText().toString();
            String shownText = "";
            if (!tagText.equals("") || tagText.equals(null)) {
                shownText = tagText.replace("    ", ",");
            }
            mEditTextTags.setText(shownText);

        }

        private void showCreateItemMode() {
            editButtonSetLayout.setVisibility(View.VISIBLE);
            buttonSetLayout.setVisibility(View.GONE);           
            mButtonRecycle.setVisibility(View.GONE);
            mDeletedBanner.setVisibility(View.INVISIBLE);
            mItemIcon.setImageResource(R.drawable.closed_a);
            setTextViewFieldsGone();
            setEditTextFieldsVisible();

        }
        
        public boolean isIconOrNameClicked() {
            return isIconAndNameClicked;
        }

        public void setIconOrNameClicked(boolean isIconAndNameClicked) {
            this.isIconAndNameClicked = isIconAndNameClicked;
        }

        public void showOpenView(SpacedItem item) {

            mItem = item;
            currentItemType = mItem.getType();

            if (mItem.getId().equals("")) {
                showCreateItemMode();
                return;
            }

            mTextViewDate.setVisibility(View.GONE);
            
            buttonSetLayout.setVisibility(View.VISIBLE);
            editButtonSetLayout.setVisibility(View.GONE);
            
            setTextViewFieldsVisible();
           // mTextViewName.setTextColor(getContext().getResources().getColor(R.color.orange));
            mTextViewName.setText(mItem.getName());
            mTextViewDescription.setText(mItem.getDescription());

            if (!mItem.isEditAble(context)) {
                mButtonEdit.setAlpha(0.3f);
                mButtonEdit.setClickable(false);
                mButtonCut.setAlpha(0.3f);
                mButtonCut.setClickable(false);
            }

            if (!mItem.getId().equals("")) {
                if (mItem.getTags().isEmpty())
                    mTextViewTags.setVisibility(View.GONE);
                else {
                    mTextViewTags.setVisibility(View.VISIBLE);
                    if (mItem.getTags() != null && mItem.getTags().size() > 0
                            && !mItem.getTags().toString().equals("")) {
                        String tagText = "";
                        for (String temp : mItem.getTags()) {
                            if (!(temp.equals("") || temp.length() == 0)) {
                                tagText += temp + "     ";
                            }
                        }
                        mTextViewTags.setText(tagText);
                    }
                }
            }
            
            setEditTextFieldsGone();

            mDeletedBanner.setVisibility(View.INVISIBLE);
            mButtonCut.setVisibility(View.VISIBLE);
            mButtonEdit.setVisibility(View.VISIBLE);
            mItemIcon.setVisibility(View.VISIBLE);
            
            if (mItem.getType().equals(Constants.DOCUMENT_TYPE)) {
                
                mButtonDownload.setVisibility(View.VISIBLE);
                mButtoOpenLinkDialog.setVisibility(View.VISIBLE);
                
                mItemIcon.getLayoutParams().height = 140;
                mItemIcon.getLayoutParams().width = 140;
                mItemIcon.setImageResource(R.drawable.default_doc_a);
                String preview_file = ((Document) mItem).getPreviewAbsPath();
                if (preview_file != null) {

                    File file = new java.io.File(preview_file);
      
                    if (file.exists()) {

                        long length = file.length();
                        length = length/1024;
                        Log.v(TAG, Utils.getMethodName() + "showing preview " + preview_file + "length " + length);
                        if (length > 0) {
                            Bitmap bMap = BitmapFactory.decodeFile(preview_file);
                            mItemIcon.setImageBitmap(bMap);
                        }
                    } else {
                        mCallback.itemGetPreview((Document) mItem);
                        mItemIcon.setImageResource(R.drawable.default_doc_a);
                    }
                }

                if ( ((Document) mItem).isRecycled())
                   mDeletedBanner.setVisibility(View.VISIBLE);
                    
            } else if (mItem.getType().equals(Constants.FOLDER_TYPE)) {

                mLinkListIcon.setVisibility(View.INVISIBLE);
                mCommentIcon.setVisibility(View.INVISIBLE);
                
                mItemIcon.setImageResource(R.drawable.closed_a);

                if ( ((Folder) mItem).isRecycled()){
                    mButtonCut.setEnabled(false);
                    mButtonDownload.setEnabled(false);
                    mButtoOpenLinkDialog.setEnabled(false);
                    mDeletedBanner.setVisibility(View.VISIBLE);
                }
                else{
                    mButtonCut.setEnabled(true);
                    mButtonDownload.setEnabled(true);
                    mButtoOpenLinkDialog.setEnabled(true);
                    mDeletedBanner.setVisibility(View.GONE);
                }
            }


        }

        /*
         * Display display = getWindowManager().getDefaultDisplay(); ImageView
         * iv = (LinearLayout) findViewById(R.id.left); int width =
         * display.getWidth(); // ((display.getWidth()*20)/100) int height =
         * display.getHeight();// ((display.getHeight()*30)/100)
         * LinearLayout.LayoutParams parms = new
         * LinearLayout.LayoutParams(width,height); iv.setLayoutParams(parms);
         */
        public void populateView(SpacedItem item) {

            this.mItem = item;
            currentItemType = mItem.getType();
            spacedItemId = mItem.getId();

            mTextViewName.setText(mItem.getName());
            mTextViewName.setTextColor(Color.parseColor("#4f545a"));

            mTextViewDescription.setText(mItem.getDescription());
            setTextViewFieldsVisible();
            
            buttonSetLayout.setVisibility(View.GONE);
            editButtonSetLayout.setVisibility(View.GONE);
            buttonSetLayout.setVisibility(View.GONE);
            
            setEditTextFieldsGone();
            
            mTextViewTags.setVisibility(View.GONE);
            mDeletedBanner.setVisibility(View.INVISIBLE);
            
            LinearLayout.LayoutParams mItemIconParams = new LinearLayout.LayoutParams(100, 100);
            DateFormat date_format_to_show = DateFormat.getDateInstance(DateFormat.MEDIUM);

            // TODO convert to date
            SimpleDateFormat sdf_iso = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
            String date_string= "";
            String last_modified = mItem.getLastModified();

            if (last_modified != null && last_modified.length() > 0) {
                    date_string = com.saperion.sdb.client.utils.Utils.getTimeExtraFormat(context,last_modified);
            }
            mTextViewDate.setText(date_string);
            mTextViewDate.setVisibility(View.VISIBLE);
   
            FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,
                    FrameLayout.LayoutParams.WRAP_CONTENT);
            mItemIcon.setLayoutParams(lp);
            mItemIcon.getLayoutParams().width = 60;
            mItemIcon.getLayoutParams().height = 60;

            if (currentItemType.equals(Constants.DOCUMENT_TYPE)) {
                mLinkListIcon.setVisibility(View.VISIBLE);
                mCommentIcon.setVisibility(View.VISIBLE);

                mItemIcon.setImageResource(R.drawable.default_doc);
                String preview_file = ((Document) mItem).getPreviewAbsPath();
                if (preview_file != null && preview_file.length() > 0) {

                    File file = new java.io.File(preview_file);
                    // File file = context.getFileStreamPath(preview_file);
                    if (file.exists()) {
                        long length = file.length();
                        length = length/1024;
                        
                        if (length > 0) {
                            Log.v(TAG, Utils.getMethodName() + "showing preview " + preview_file + "length " + length);
                            Bitmap bMap = BitmapFactory.decodeFile(preview_file);
                            mItemIcon.setImageBitmap(bMap);
                        }
                    } else {
                        mItemIcon.setImageResource(R.drawable.default_doc);
                        // informs parent to get preview
                        mCallback.itemGetPreview((Document) mItem);

                    }
                } else {
                    mItemIcon.setImageResource(R.drawable.default_doc);
                    // informs parent to get preview
                    mCallback.itemGetPreview((Document) mItem);
                }

                long size_in_byte = ((Document) mItem).getSize();
                String states = ((Document) mItem).getStates().toString();

                if (states.contains(DocumentState.LINKED.toString()))
                    mLinkListIcon.setImageResource(R.drawable.list_link_a);
                if (states.contains(DocumentState.COMMENTED.toString()))
                    mCommentIcon.setImageResource(R.drawable.comment_a);

                mTextViewSize.setVisibility(View.VISIBLE);
                mTextViewSize.setText(getAsString(size_in_byte));

                if ( ((Document) mItem).isRecycled())
                    mDeletedBanner.setVisibility(View.VISIBLE);
                else
                    mDeletedBanner.setVisibility(View.GONE);

            } else if (currentItemType.equals(Constants.FOLDER_TYPE)) {
                mItemIcon.setImageResource(R.drawable.closed);
                mLinkListIcon.setVisibility(View.INVISIBLE);
                mCommentIcon.setVisibility(View.INVISIBLE);
                mTextViewSize.setVisibility(View.GONE);
                if ( ((Folder) mItem).isRecycled())
                        mDeletedBanner.setVisibility(View.VISIBLE);
            }

            if (mItem != null && !mItem.isEditAble(context)) {
                mButtonEdit.setAlpha(0.3f);
                mButtonEdit.setClickable(false);

                mButtonCut.setAlpha(0.3f);
                mButtonCut.setClickable(false);
            }
        }

        public String getAsString(long bytes) {
            for (int i = 6; i > 0; i--) {
                double step = Math.pow(1024, i);
                if (bytes > step)
                    return String.format("%3.2f %s", bytes / step, Q[i]);
            }
            return Long.toString(bytes);
        }

    }

}
