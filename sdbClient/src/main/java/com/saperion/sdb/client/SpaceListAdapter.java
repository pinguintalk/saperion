package com.saperion.sdb.client;

import android.app.Activity;
import android.content.Context;
import android.database.CharArrayBuffer;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.saperion.sdb.client.R.color;
import com.saperion.sdb.client.utils.Log;
import com.saperion.sdb.sdk.models.Space;
import com.saperion.sdb.sdk.states.SpaceState;
import com.saperion.sdb.sdk.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SpaceListAdapter extends ArrayAdapter<Space> {

    private static final String TAG = SpaceListAdapter.class.getSimpleName();

    private static LayoutInflater mInflater = null;
    private int mLayoutType = 1;
    private Context mContext;
    private int mCurrSelectedItemPosition = -1;
    private String mCurrentSelectedItemId = null;

    private View mCurrentSelectedView = null;
    private SpaceListViewInterface mCallback;

    private boolean isInEditMode = false;
    private boolean showRecycledItems = false;
    ArrayList<Space> subItems;
    ArrayList<Space> allItems;
    
    public SpaceListAdapter(Context context, SpaceListViewInterface spaceListViewCallbacks,
                            ArrayList<Space> mSpaceList) {
        super(context, 0, mSpaceList);
        this.mContext = context;
        this.mCallback = spaceListViewCallbacks;
        this.subItems = mSpaceList;
        this.allItems = this.subItems;
        
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    
    public boolean isInEditMode() {
        return isInEditMode;
    }

    public void setInEditMode(boolean isInEditMode) {
        this.isInEditMode = isInEditMode;
    }


    
    public View getmCurrentSelectedView() {
        return mCurrentSelectedView;
    }

    public void setmCurrentSelectedView(View mCurrentSelectedView) {
        this.mCurrentSelectedView = mCurrentSelectedView;
    }

    public String getmCurrentSelectedItemId() {
        return mCurrentSelectedItemId;
    }

    public int getCurrSelectedItemPosition() {
        return mCurrSelectedItemPosition;

    }

    public void setCurrSelectedItemPosition(int pos) {
        this.mCurrSelectedItemPosition = pos;

    }

    public void setmCurrentSelectedItemId(String mCurrentSelectedItemId) {
        Log.v(TAG, Utils.getMethodName() + "entry ");
        this.mCurrentSelectedItemId = mCurrentSelectedItemId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Log.v(TAG, Utils.getMethodName() + "entry ");
        View view = convertView;

        Space item = (Space) getItem(position);
      
        if (convertView == null) {

            view = mInflater.inflate(R.layout.space_cell, null);
            view.setTag(new ViewHolder(view));

            ((ViewHolder) view.getTag()).populateView(item);

        } else {
            ((ViewHolder) view.getTag()).populateView(item);

            String item_id = item.getId();

            if (mCurrentSelectedItemId != null && mCurrentSelectedItemId.equals(item_id)) {
                view.setTag(new ViewHolder(view));

                ((ViewHolder) convertView.getTag()).showOpenItemView(item);
                if (isInEditMode()) {
                    ((ViewHolder) convertView.getTag()).showEditMode(view);
                }
            }
        }


        
        return view;

    }

    @Override
    public int getItemViewType(int position) {

        Space space = (Space) this.getItem(position);

        String states = space.getStates().toString();

        if (states.contains(SpaceState.SHARED.toString())) {
            return 2;
        }

        return 1;
    }

    class ViewHolder {
        private RelativeLayout relativeLayout;
        private ImageView mSpaceIcon;
        private ImageView mDeletedBanner;
        private TextView mTextViewName;
        private TextView mTextViewDescription;
        private TextView mTextViewOwner;
        private TextView mTextViewTags;

        private LinearLayout buttonSetLayout;
        private LinearLayout editButtonSetLayout;
        private CharArrayBuffer mCharArrayBufferDescription;
        private CharArrayBuffer mCharArrayBufferStates;
        private CharArrayBuffer mCharArrayBufferName;

        private View mView = null;

        private ImageButton mButtonEdit;
        private ImageButton mButtonOk;
        private ImageButton mButtonCancel;
        private ImageButton mButtonDelete;
        private ImageButton mButtonRecycle;
        private ImageButton mButtonFavorit;
        private ImageButton mButtonShare;
        private ImageButton mButtonDownload;
        private ImageButton mButtonSync;
        private ImageButton  mButtonRestore;
        private EditText mEditTextName;
        private EditText mEditTextDescription;
        private EditText mEditTextTags;

        private Space mCurrOpenedSpace = null;
        private ImageView mImageViewFavorit;

        private Space mSpace = null;

        //The "x" and "y" position of the "Sync Button"
        Point p;

        public ViewHolder(View view) {
            this.mView = view;


            
            relativeLayout = (RelativeLayout) mView.findViewById(R.id.space_item);
            mSpaceIcon = (ImageView) mView.findViewById(R.id.space_icon);
            mSpaceIcon.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // setIconOrNameClicked(true);
                    Log.v(TAG, Utils.getMethodName() + "mSpaceIcon ");
                    if (mCallback != null) {
                        mCallback.itemIconOrNameClickedEvent(mSpace);
                    }
                }
            });
            mDeletedBanner = (ImageView) mView.findViewById(R.id.deleted_icon);
            mTextViewName = (TextView) mView.findViewById(R.id.space_name);
            mTextViewName.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // setIconOrNameClicked(true);
                    Log.v(TAG, Utils.getMethodName() + "mTextViewName ");
                    if (mCallback != null) {
                        mCallback.itemIconOrNameClickedEvent(mSpace);
                    }
                }
            });

            mTextViewDescription = (TextView) mView.findViewById(R.id.space_description);
            mTextViewOwner = (TextView) mView.findViewById(R.id.space_owner);
            mTextViewTags = (TextView) mView.findViewById(R.id.space_textview_tags);
            buttonSetLayout = (LinearLayout) mView.findViewById(R.id.space_button_set);
            mImageViewFavorit = (ImageView) mView.findViewById(R.id.fav_icon);


            editButtonSetLayout = (LinearLayout) mView.findViewById(R.id.space_edit_button_set);
            mCharArrayBufferName = new CharArrayBuffer(20);
            mCharArrayBufferDescription = new CharArrayBuffer(20);
            mCharArrayBufferStates = new CharArrayBuffer(20);

            mEditTextName = (EditText) mView.findViewById(R.id.space_edit_name);
            mEditTextDescription = (EditText) mView.findViewById(R.id.space_edit_description);
            mEditTextTags = (EditText) mView.findViewById(R.id.space_edit_tags);


            mButtonShare = (ImageButton) mView.findViewById(R.id.b_share);
            mButtonShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.v(TAG, Utils.getMethodName() + "mButtonShare ");
                    mCallback.itemOpenShareDialogClickedEvent(mContext, mCurrOpenedSpace);
                }
            });
            
            mButtonDownload = (ImageButton) mView.findViewById(R.id.b_invite_user);
            mButtonSync = (ImageButton) mView.findViewById(R.id.b_sync);
            
            mButtonSync.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    
                    getSyncButtonLocation();
                    
                    //open popup window for setting sync flags
                    if (p != null)
                        showSyncPopup((Activity)  mView.getContext(), p, mCurrOpenedSpace);
                }
            });
            mButtonEdit = (ImageButton) mView.findViewById(R.id.b_edit);

            mButtonEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.v(TAG, Utils.getMethodName() + "mButtonEdit ");

                    showEditMode(v);

                }
            });

            mButtonOk = (ImageButton) mView.findViewById(R.id.b_ok);

            mButtonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.v(TAG, Utils.getMethodName() + "mButtonOk ");
                    setInEditMode(false);
                    if(mCurrOpenedSpace.isRecycled()){
                        showOpenItemView(mCurrOpenedSpace);
                        return;
                    }
                        
                    if (mCurrOpenedSpace != null) {
                        mCurrOpenedSpace.setName(mEditTextName.getText().toString());
                        mCurrOpenedSpace.setDescription(mEditTextDescription.getText().toString());
                        mCurrOpenedSpace.setTags(Arrays.asList(mEditTextTags.getText().toString()
                                .split("\\s*,\\s*")));
                        mEditTextTags.setVisibility(View.GONE);
                        mCallback.itemOkClickedEvent(mCurrOpenedSpace);
                    }
                }
            });

             mButtonRestore = (ImageButton) mView.findViewById(R.id.b_restore);
             mButtonRestore.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {

                     Log.v(TAG, Utils.getMethodName() + "mRestoreButton ");

                     if (mCurrOpenedSpace != null) {
                         setInEditMode(false);
                         mCallback.itemRestoreClickedEvent(mCurrOpenedSpace);
                     }
                 }
             });
            
            mButtonCancel = (ImageButton) mView.findViewById(R.id.b_cancel);
            mButtonCancel.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    Log.v(TAG, Utils.getMethodName() + "mButtonCancel ");
                    setInEditMode(false);

                    if (mCurrOpenedSpace != null && mCurrOpenedSpace.getName().equals("")) {
                        Log.v(TAG, Utils.getMethodName() + "remove empty item ");
                        remove(mCurrOpenedSpace);
                        notifyDataSetChanged();
                        return;
                    }
                    showOpenItemView(mCurrOpenedSpace);
                }
            });

            mButtonDelete = (ImageButton) mView.findViewById(R.id.b_trash);

            mButtonDelete.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Log.v(TAG, Utils.getMethodName() + "mButtonDelete ");
                    // setInEditMode(false);
                    if (mCurrOpenedSpace != null) {

                        mCallback.itemDeleteClickedEvent(mCurrOpenedSpace);
                    }
                }
            });

            mButtonRecycle = (ImageButton) mView.findViewById(R.id.b_trash_recycle);

            mButtonRecycle.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Log.v(TAG, Utils.getMethodName() + "mButtonRecycle ");
                    // setInEditMode(false);
                    if (mCurrOpenedSpace != null) {

                        mCallback.itemTrashClickedEvent(mCurrOpenedSpace);
                    }
                }
            });

            mButtonFavorit = (ImageButton) mView.findViewById(R.id.b_manage_user);
            mButtonFavorit.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Log.v(TAG, Utils.getMethodName() + "mButtonFavorit");
                    if (mCurrOpenedSpace != null) {
                        mCallback.itemFavoritClickEvent(mCurrOpenedSpace);
                    }
                }
            });
        }


        private void setEditTextFieldsGone(){
            mEditTextName.setEnabled(true);
            mEditTextName.setVisibility(View.GONE);

            mEditTextDescription.setEnabled(true);
            mEditTextDescription.setVisibility(View.GONE);
            mEditTextTags.setVisibility(View.GONE);
        }

        private void setEditTextFieldsVisible(){
            mEditTextName.setEnabled(true);
            mEditTextName.setVisibility(View.VISIBLE);

            mEditTextDescription.setEnabled(true);
            mEditTextDescription.setVisibility(View.VISIBLE);
            mEditTextTags.setVisibility(View.VISIBLE);

            mEditTextName.setText(mTextViewName.getText().toString());
            mEditTextDescription.setText(mTextViewDescription.getText().toString());
        }

        private void setTextViewFieldsVisible(Space space){
            mTextViewName.setVisibility(View.VISIBLE);
            mTextViewDescription.setVisibility(View.VISIBLE);
            mTextViewTags.setVisibility(View.VISIBLE);

            mTextViewOwner.setText(space.getOwnerName());
            mTextViewName.setText(space.getName());
            mTextViewDescription.setText(space.getDescription());
        }


        private void setTextViewFieldsGone(){
            mTextViewName.setVisibility(View.GONE);
            mTextViewDescription.setVisibility(View.GONE);
            mTextViewTags.setVisibility(View.GONE);
        }
        
        public void showEditMode(View v) {

            Log.v(TAG, Utils.getMethodName() + "mButtonEdit ");
            setInEditMode(true);
            buttonSetLayout.setVisibility(View.GONE);
            editButtonSetLayout.setVisibility(View.VISIBLE);


            if (mCurrOpenedSpace.isRecycled()) {
                setEditTextFieldsGone();
                mDeletedBanner.setVisibility(View.VISIBLE);
                mButtonRestore.setVisibility(View.VISIBLE);
                mButtonDelete.setVisibility(View.VISIBLE);
                mButtonRecycle.setVisibility(View.GONE);
                setTextViewFieldsVisible(mCurrOpenedSpace);
            }

            else{
                setEditTextFieldsVisible();
                mDeletedBanner.setVisibility(View.GONE);
                mButtonRestore.setVisibility(View.GONE);
                mButtonDelete.setVisibility(View.GONE);
                mButtonRecycle.setVisibility(View.VISIBLE);
                setTextViewFieldsGone();
            }
            

            String tagText = mTextViewTags.getText().toString();
            String shownText = "";
            if (!tagText.equals("") || tagText.equals(null)) {
                shownText = tagText.replace("    ", ",");
            }
            mEditTextTags.setText(shownText);

        }

        public void showCreateNewItemMode() {

            setInEditMode(true);
            buttonSetLayout.setVisibility(View.GONE);
            editButtonSetLayout.setVisibility(View.VISIBLE);
            
            Log.v(TAG, Utils.getMethodName() + "space " + mCurrOpenedSpace.getName());
            setTextViewFieldsGone();
            setEditTextFieldsVisible();          
        }

        private void getSyncButtonLocation() {
            int[] location = new int[2];

            // Get the x, y location and store it in the location[] array
            // location[0] = x, location[1] = y.
            mButtonSync.getLocationOnScreen(location);

            //Initialize the Point with x, and y positions
            p = new Point();
            p.x = location[0];
            p.y = location[1];
        }
        
        public void showOpenItemView(Space c ) {
            this.mCurrOpenedSpace = c;
            this.mSpace = c;
            getSyncButtonLocation();
            
            mSpaceIcon.setImageResource(R.drawable.open_a);
            
            mView.setBackgroundColor(Color.WHITE);
            buttonSetLayout.setVisibility(View.VISIBLE);
            editButtonSetLayout.setVisibility(View.GONE);
            mButtonEdit.setEnabled(true);

            if (mCurrOpenedSpace.getId().equals("")){                
                showCreateNewItemMode();
                return;
            }
            
            setEditTextFieldsGone();
            setTextViewFieldsVisible(c);
            
            if (c.isRecycled()) {
                mButtonShare.setEnabled(false);
                mButtonDownload.setEnabled(false);
                mButtonSync.setEnabled(false);
                mButtonFavorit.setEnabled(false);                
                mDeletedBanner.setVisibility(View.VISIBLE);
            }
                
            else{
                mButtonShare.setEnabled(true);
                mButtonDownload.setEnabled(true);
                mButtonSync.setEnabled(true);
                mButtonFavorit.setEnabled(true);
                
                mDeletedBanner.setVisibility(View.GONE);
 
            }

            if (c.isFavorit()) {
                mImageViewFavorit.setVisibility(View.VISIBLE);
                mButtonFavorit.setImageResource(R.drawable.favorite_orange);
            } else {
                mImageViewFavorit.setVisibility(View.INVISIBLE);
                mButtonFavorit.setImageResource(R.drawable.favorite);
            }
            if (!c.getId().equals("")) {
                if (c.getTags().isEmpty())
                    mTextViewTags.setVisibility(View.GONE);
                else {
                    Log.v(TAG, "tags" + c.getTags());
                    mTextViewTags.setVisibility(View.VISIBLE);
                    List<String> tags = c.getTags();
                    if (tags != null && tags.size() > 0 && !tags.toString().equals("")) {
                        String tagText = "";
                        for (String temp : tags) {
                            if (!(temp.equals("") || temp.length() == 0)) {
                                tagText += temp + "     ";
                            }
                        }
                        mTextViewTags.setText(tagText);
                    }
                }
            }
            mTextViewName.setTextColor(getContext().getResources().getColor(color.orange));
            String states = mCurrOpenedSpace.getStates().toString();

            if (states.contains(SpaceState.SHARED.toString())) {
                mSpaceIcon.setImageResource(R.drawable.open_share_a);
                if (states.contains(SpaceState.SYNCHRONIZE_APP.toString()))
                    mSpaceIcon.setImageResource(R.drawable.open_share_sync_a);
            }
            if (states.contains(SpaceState.SYNCHRONIZE_APP.toString()))
                mSpaceIcon.setImageResource(R.drawable.open_sync_a);


        }

        public void populateView(Space c) {
            // Log.v(TAG, Utils.getMethodName() + "entry ");
            
           mSpace = c;
           mView.setBackgroundColor(getContext().getResources().getColor(color.grey));
            // default icon
            mSpaceIcon.setImageResource(R.drawable.closed);
            
            buttonSetLayout.setVisibility(View.GONE);
            editButtonSetLayout.setVisibility(View.GONE);

            setEditTextFieldsGone();
            
            mTextViewName.setTextColor(getContext().getResources().getColor(color.text_grey));

            setTextViewFieldsVisible(c);
            mTextViewOwner.setVisibility(View.GONE);
            mTextViewTags.setVisibility(View.GONE);
    
            if (c.isRecycled()){                 
                mDeletedBanner.setVisibility(View.VISIBLE);
            }
            else
                mDeletedBanner.setVisibility(View.INVISIBLE);


            if (c.isFavorit()) {
                mImageViewFavorit.setVisibility(View.VISIBLE);
            } else {
                mImageViewFavorit.setVisibility(View.GONE);
            }

            String states = c.getStates().toString();

            if (states.contains(SpaceState.SHARED.toString()))
                mSpaceIcon.setImageResource(R.drawable.closed_share);

            if (states.contains(SpaceState.SYNCHRONIZE_APP.toString())) {
                mSpaceIcon.setImageResource(R.drawable.closed_sync);
                if (states.contains(SpaceState.SHARED.toString()))
                    mSpaceIcon.setImageResource(R.drawable.closed_share_sync);

            }
        }

    }

    public void update(View mView, Space new_space) {
        if (mView != null) {
            EditText mEditTextName = (EditText) mView.findViewById(R.id.space_edit_name);
            EditText mEditTextDescription =
                    (EditText) mView.findViewById(R.id.space_edit_description);
            EditText mEditTextTags = (EditText) mView.findViewById(R.id.space_edit_tags);

            mEditTextName.setText(new_space.getName());

            mEditTextDescription.setText(new_space.getDescription());
            mEditTextTags.setText(new_space.getTags().toString());
        }
    }

    @Override
    public int getCount() {
        return subItems.size();
    }
    
    @Override
    public Filter getFilter() {

        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                subItems = (ArrayList<Space>) results.values;

                Log.e(TAG, Utils.getMethodName() +  results.values.toString());
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();
                ArrayList<Space> filteredItemList = new ArrayList<Space>();

                // perform your search here using the searchConstraint String.

                constraint = constraint.toString().toLowerCase();
                for (int i = 0; i < allItems.size(); i++) {
                    String dataNames = allItems.get(i).getName();
                    if (dataNames.toLowerCase().contains(constraint.toString()))  {
                        filteredItemList.add(allItems.get(i));
                    }
                }

                results.count = filteredItemList.size();
                results.values = filteredItemList;
                Log.e(TAG, Utils.getMethodName() +   results.values.toString());

                return results;
            }
        };

     return filter;
    }


    // The method that displays the popup.
    private void showSyncPopup(final Activity context, Point p,final Space space) {
        int popupWidth = 120;
        int popupHeight = 85;

        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.popup);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.sync_popup_layout, viewGroup);

        // Creating the PopupWindow
        final PopupWindow popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setWidth(popupWidth);
        popup.setHeight(popupHeight);
        popup.setFocusable(true);

        // Some offset to align the popup a bit to the right, and a bit down, relative to button's position.
        int OFFSET_X = -20;
        int OFFSET_Y = 45;

        // Clear the default translucent background
        popup.setBackgroundDrawable(new BitmapDrawable());

        // Displaying the popup at the specified location, + offsets.
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, p.x + OFFSET_X, p.y + OFFSET_Y);

        ImageButton mButtonSyncApp = (ImageButton) layout.findViewById(R.id.b_sync_app);
        mButtonSyncApp.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                mCallback.itemSyncAppClickedEvent(space);
                popup.dismiss();
            }
        });

        ImageButton mButtonSyncdesktop = (ImageButton) layout.findViewById(R.id.b_sync_desktop);
        mButtonSyncdesktop.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                mCallback.itemSyncDesktopClickedEvent(space);
                popup.dismiss();
            }
        });
    }

}
