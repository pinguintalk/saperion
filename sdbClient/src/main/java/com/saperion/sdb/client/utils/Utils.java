package com.saperion.sdb.client.utils;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import android.view.View;

import com.saperion.sdb.client.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class Utils {

    private static final String TAG = Utils.class.getCanonicalName();

    public static String getMethodName() {
        return (new Exception().getStackTrace()[1].getMethodName()) + "("
                + (new Exception().getStackTrace()[1].getLineNumber()) + "): ";
    }

    public static void createFolder(String space_path) {

        if (space_path == null || space_path.length() == 0) {
            return;
        }

        File space_dir = new File(space_path);

        if (space_dir.exists()) {
            Log.v(TAG, Utils.getMethodName() + space_path + " exists");
        } else {
            Log.v(TAG, Utils.getMethodName() + "creating " + space_path);
            space_dir.mkdirs();
        }

		/*
         * 
		 * 
		 * // File fileWithinMyDir = new File(space_dir, "myfile");
		 * //Getting a file within the dir. // FileOutputStream out
		 * = new FileOutputStream(fileWithinMyDir); //Use the stream
		 * as usual to write into the file.
		 */
    }

    public static String getAuthToken() {
        String email = "loadtest02@qa.de";
        String password = "together";

        if (email.equals("") || password.equals("")) {

            return null;
        }

        String authStr = "";
        try {
            authStr =
                    "Basic "
                            + android.util.Base64.encodeToString(
                            (email + ":" + password).getBytes("UTF-8"), 4);
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
        }

        //authStr.replaceAll("\n", "");
        authStr = authStr.replaceAll("\\r\\n", "");

        //Log.v(TAG,authStr);

        return authStr;
    }

    public static void fileCopy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes createInstance in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

    public static void fileCopy(String src, String dst) throws IOException {
        File file_name_with_id = new File(src);
        File human_readable_file_name = new File(dst);
        try {
            Utils.fileCopy(file_name_with_id,human_readable_file_name);
        } catch (IOException e) {
            com.saperion.sdb.sdk.utils.Log.e(TAG, Utils.getMethodName() + e.toString());
        }
    }
    
    public static void showFolders(Context context) {
        String msg = "";
		/*
		msg += "SystemInfo properties\n";
		msg += "-------------\n";
		java.util.Properties props = SystemInfo.getProperties();
		Enumeration<?> e = props.propertyNames();
		while (e.hasMoreElements()) {
		   String k = (String) e.nextElement();
		   String v = props.getProperty(k);
		   msg += k+": "+v+"\n";
		}

		msg += "\n";
		msg += "Envirionment variables\n";
		msg += "-------------\n";
		Map<String, String> envs = SystemInfo.getenv();
		Set<String> keys = envs.keySet();
		Iterator<String> i = keys.iterator();
		while (i.hasNext()) {
		   String k = (String) i.next();
		   String v = (String) envs.get(k);
		   msg += k+": "+v+"\n";
		}

		msg += "\n";
		msg += "Environment folders\n";
		msg += "-------------\n";
		msg += "Data folder: "
		   +Environment.getDataDirectory().getPath()+"\n";
		msg += "Download cache folder: "
		   +Environment.getDownloadCacheDirectory().getPath()+"\n";
		msg += "External Storage folder: "
		   +Environment.getExternalStorageDirectory().getPath()+"\n";
		msg += "Root folder: "
		   +Environment.getRootDirectory().getPath()+"\n";

		msg += "\n";
		msg += "Application context info\n";
		msg += "-------------\n";
		msg += "Cache folder: "
		   +  context.getCacheDir().getPath()+"\n";
		msg += "External cache folder: "
		   + context.getExternalCacheDir().getPath()+"\n";
		msg += "File folder: "
		   + context.getFilesDir().getPath()+"\n";
		msg += "OBB folder: "
		   + context.getObbDir().getPath()+"\n";
		msg += "Package name: "
		   + context.getPackageName()+"\n";
		msg += "Package code path: "
		   + context.getPackageCodePath()+"\n";
		msg += "Package resource path: "
		   + context.getPackageResourcePath()+"\n";
		*/

        File root_folder = context.getFilesDir();

        msg += "Root folder :" + root_folder.getName() + "\n";

        File[] spaces = root_folder.listFiles();

        for (int i = 0; i < spaces.length; i++) {
            msg += "space " + i + ": " + spaces[i].getName() + "\n";
            File child = spaces[i];
            File[] children = child.listFiles();

            for (int c = 0; c < children.length; c++) {
                msg += "child " + c + ": " + children[c].getName() + "\n";

                // File f = new File(children[i], children[i].getName());
            }

            // File f = new File(children[i], children[i].getName());
        }

        Log.v(TAG, getMethodName() + msg);
    }


    // Copy EditCopy text to the ClipBoard
    public static void copyTextToClipBoard(Context context, String text)
    {
        int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
            android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setText(text);
        } else {
            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            android.content.ClipData clip = android.content.ClipData.newPlainText("document link url", text);
            clipboard.setPrimaryClip(clip);
        }
    }

    public static void showMessageDialog(Context context, String title,String message) {

        if (context == null) return;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(title);
        builder.setMessage(message);

        builder.setPositiveButton(R.string.dialog_error_button_close_txt, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do do my action here
                dialog.dismiss();
            }

        });


        AlertDialog alert = builder.create();
        alert.show();

    }

    public static void showLoadingDialog(ProgressDialog progressDialog, Context context) {


        return;
        /*
        if (progressDialog != null) {
            progressDialog.cancel();
            progressDialog = null;
        }
      
        progressDialog = new ProgressDialog(context);

        progressDialog.setTitle("Processing...");
        progressDialog.setMessage("Please wait.");
        progressDialog.setCancelable(true);
        progressDialog.setIndeterminate(true);

        progressDialog.show();
        */
    }

    public static void cancelLoadingDialog(ProgressDialog progressDialog) {

        if (progressDialog != null) {
            progressDialog.cancel();
            progressDialog = null;
        }
    }

    public static void showErrorDialog(Context context, String message) {

        if (context == null) return;

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(R.string.dialog_service_error_title);
        builder.setMessage(message);

        builder.setPositiveButton(R.string.dialog_error_button_close_txt, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do do my action here

                dialog.dismiss();
            }

        });
        /*
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // I do not need any action here you might
                dialog.dismiss();
            }
        });
        */
        AlertDialog alert = builder.create();
        alert.show();

    }


    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @author paulburke
     */
    public static String getPath(final Context context, final Uri uri) {

       /*  final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        should support new file manager on kikat
        // DocumentProvider
        if (isKitKat) {
            if (DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        }
        // MediaStore (and general)
        else
        */
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static String getTimeExtraFormat(Context context, String last_modified) {
        String date_string = "";
        
        SimpleDateFormat sdf_iso = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        DateFormat date_format_to_show = DateFormat.getDateInstance(DateFormat.MEDIUM);

        if (last_modified != null && last_modified.length() > 0) {
            
            try {
                Date modified_date = sdf_iso.parse(last_modified);
                Date today = new Date();
                long time_diff = today.getTime() - modified_date.getTime();

                // long diffSeconds = time_diff / 1000 % 60;
                long diffMinutes = time_diff / (60 * 1000) % 60;
                long diffHours = time_diff / (60 * 60 * 1000) % 24;
                long diffDays = time_diff / (24 * 60 * 60 * 1000);
                long diffWeeks = time_diff / (7 * 24 * 60 * 60 * 1000);
                long diffYears = 0;
                if ( diffDays > 365) diffYears = diffDays/365;

                if (diffYears > 0) {
                    date_string = diffYears + " " +  context.getResources().getString(R.string.years) +
                            " " + context.getResources().getString(R.string.ago) ;
                } else if (diffWeeks > 0) {
                    date_string = diffWeeks + " " +  context.getResources().getString(R.string.weeks) +
                            " " + context.getResources().getString(R.string.ago) ;
                } else if (diffDays > 0) {
                    date_string = diffDays + " " +  context.getResources().getString(R.string.days) +
                            " " + context.getResources().getString(R.string.ago) ;
                }
                else if (diffHours > 0) {
                    date_string =  diffHours + " " +  context.getResources().getString(R.string.hours) +
                            " " + context.getResources().getString(R.string.ago) ;

                } else if (diffMinutes > 0) {
                    date_string = diffMinutes + " "  + context.getResources().getString(R.string.minutes) +
                            " " + context.getResources().getString(R.string.ago) ;
                }
                else
                    date_string = date_format_to_show.format(modified_date);

            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return date_string;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static void cancelLoadingDialog(Activity activity) {

        if (activity == null) return;
        
        ActionBar ab = activity.getActionBar();
        if (ab != null) {
            View view = ab.getCustomView();
            if (view != null)
                view.findViewById(R.id.progressBar).setVisibility(View.GONE);
        }
    }

    public static void showLoadingDialog(Activity activity) {
        if (activity == null) return;
        
        ActionBar ab = activity.getActionBar();
        if (ab != null) {
            View view = ab.getCustomView();
            if (view != null)
                view.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
        }
    }
}
