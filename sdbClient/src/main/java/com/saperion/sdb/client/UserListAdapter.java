package com.saperion.sdb.client;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.saperion.sdb.client.utils.Log;
import com.saperion.sdb.sdk.models.User;
import com.saperion.sdb.sdk.rights.UserRight;
import com.saperion.sdb.sdk.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

public class UserListAdapter extends ArrayAdapter<User> {

    private static final String TAG = UserListAdapter.class.getSimpleName();

    private static LayoutInflater mInflater = null;
    private int mLayoutType = 1;
    private Context mContext;
    private int mCurrSelectedItemPosition = -1;
    private String mCurrentSelectedItemId = null;

    private View mCurrentSelectedView = null;
    private UserListViewInterface mCallback;

    private boolean isInEditMode = false;
    private boolean showRecycledItems = false;
    ArrayList<User> subItems;
    ArrayList<User> allItems;
    
    public UserListAdapter(Context context, UserListViewInterface spaceListViewCallbacks,
                           ArrayList<User> userList) {
        super(context, 0, userList);
        this.mContext = context;
        this.mCallback = spaceListViewCallbacks;
        this.subItems = userList;
        this.allItems = this.subItems;
        
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    
    public boolean isInEditMode() {
        return isInEditMode;
    }

    public void setInEditMode(boolean isInEditMode) {
        this.isInEditMode = isInEditMode;
    }


    
    public View getmCurrentSelectedView() {
        return mCurrentSelectedView;
    }

    public void setmCurrentSelectedView(View mCurrentSelectedView) {
        this.mCurrentSelectedView = mCurrentSelectedView;
    }

    public String getmCurrentSelectedItemId() {
        return mCurrentSelectedItemId;
    }

    public int getCurrSelectedItemPosition() {
        return mCurrSelectedItemPosition;

    }

    public void setCurrSelectedItemPosition(int pos) {
        this.mCurrSelectedItemPosition = pos;

    }

    public void setmCurrentSelectedItemId(String mCurrentSelectedItemId) {
        Log.v(TAG, Utils.getMethodName() + "entry ");
        this.mCurrentSelectedItemId = mCurrentSelectedItemId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Log.v(TAG, Utils.getMethodName() + "entry ");
        View view = convertView;

        User item = getItem(position);
      
        if (convertView == null) {

            view = mInflater.inflate(R.layout.user_cell, null);
            view.setTag(new ViewHolder(view));

            ((ViewHolder) view.getTag()).populateView(item);

        } else {
            ((ViewHolder) view.getTag()).populateView(item);

            String item_id = item.getId();

            if (mCurrentSelectedItemId != null && mCurrentSelectedItemId.equals(item_id)) {
                view.setTag(new ViewHolder(view));

                if (isInEditMode()) {
                    ((ViewHolder) convertView.getTag()).showEditMode(view);
                }
            }
        }


        
        return view;

    }

    public void updateAvatar(View mView, User user) {
        if (mView == null)
            return;
        if (user == null)
            return;

        ImageView mItemIcon = (ImageView) mView.findViewById(R.id.user_avatar);
        String avatar_file = user.getAvatarAbsPath();
        if (avatar_file == null)
            return;
        File file = new java.io.File(avatar_file);
        if (file.exists()) {
            // File file = context.getFileStreamPath(preview_file);
            Log.v(TAG, Utils.getMethodName() + "updating avatar " + file.getAbsolutePath());

            Bitmap bMap = BitmapFactory.decodeFile(avatar_file);
            mItemIcon.setImageBitmap(bMap);
        } 
    }

    public void updateData(View mView, User user) {
        if (mView != null) {
            TextView displayNameTV = (TextView) mView.findViewById(R.id.user_display_name);
            TextView firstName =
                    (TextView) mView.findViewById(R.id.user_first_name);
            TextView lastName = (TextView) mView.findViewById(R.id.user_last_name);

            displayNameTV.setText(user.getDisplayname());

            firstName.setText(user.getFirstname());
            lastName.setText(user.getLastname());


            ImageView mImageViewShare = (ImageView) mView.findViewById(R.id.iv_share_user);
            ImageView mImageViewInviteUser = (ImageView) mView.findViewById(R.id.iv_invite_user);
            ImageView mImageViewManageUser = (ImageView) mView.findViewById(R.id.iv_manage_user);


            if(user.getRights().contains(UserRight.SHARE))
                mImageViewShare.setAlpha(0.9f);
            else
                mImageViewShare.setAlpha(0.4f);

            if(user.getRights().contains(UserRight.INVITE))
                mImageViewInviteUser.setAlpha(0.9f);
            else
                mImageViewInviteUser.setAlpha(0.4f);

            if(user.getRights().contains(UserRight.MANAGE))
                mImageViewManageUser.setAlpha(0.9f);
            else
                mImageViewManageUser.setAlpha(0.4f);
        }
    }
    

    class ViewHolder {
        private LinearLayout linearLayout;
        private ImageView userAvatar;
        private TextView mTextViewDisplayName;
        private TextView mTextViewFirstName;
        private TextView mTextViewEmail;
        private TextView mTextViewLastName;

        private LinearLayout settingButtonSet;
        private LinearLayout editButtonSet;

        LinearLayout imageViewSetUserRights;
        
        private View mView = null;

        private ImageButton mButtonEdit;
        private ImageButton mButtonOk;
        private ImageButton mButtonCancel;
        private ImageButton mButtonDeleteUser;

        private ImageButton mButtonShare;
        private ImageButton mButtonInviteUser;
        private ImageButton mButtonManageUser;

        private ImageView mImageViewShare;
        private ImageView mImageViewInviteUser;
        private ImageView mImageViewManageUser;
        
        
        private EditText mEditDisplayName;
        private EditText mEditTextFirstName;
        private EditText mEditTextLastName;

        private User mCurrOpenedUser = null;

        Drawable btn_selected_drawable;
        Drawable btn_not_selected_drawwable;
        
        public ViewHolder(View view) {
            this.mView = view;

            linearLayout = (LinearLayout) mView.findViewById(R.id.user_cell);

            mImageViewShare = (ImageView) mView.findViewById(R.id.iv_share_user);
            mImageViewInviteUser = (ImageView) mView.findViewById(R.id.iv_invite_user);
            mImageViewManageUser = (ImageView) mView.findViewById(R.id.iv_manage_user);
            

            btn_selected_drawable = mView.getResources().getDrawable(R.drawable.fns_bg_orange_shape_no_stroke);
            btn_not_selected_drawwable  = mView.getResources().getDrawable(R.drawable.fns_bg_grey_shape_no_stroke);
            
            userAvatar = (ImageView) mView.findViewById(R.id.user_avatar);
            mTextViewDisplayName = (TextView) mView.findViewById(R.id.user_display_name);

            mTextViewFirstName = (TextView) mView.findViewById(R.id.user_first_name);
            mTextViewEmail = (TextView) mView.findViewById(R.id.user_email);
            mTextViewLastName = (TextView) mView.findViewById(R.id.user_last_name);

            mEditDisplayName = (EditText) mView.findViewById(R.id.user_display_name_edit);
           
            
            mEditTextFirstName = (EditText) mView.findViewById(R.id.user_first_name_edit);
            mEditTextLastName = (EditText) mView.findViewById(R.id.user_last_name_edit);

            settingButtonSet = (LinearLayout) mView.findViewById(R.id.user_setting_button_set);
            settingButtonSet.setVisibility(View.GONE );

            imageViewSetUserRights =(LinearLayout) mView.findViewById(R.id.user_setting_image_set);
            editButtonSet = (LinearLayout) mView.findViewById(R.id.user_edit_button_set);

            
            
            mButtonShare = (ImageButton) mView.findViewById(R.id.b_share);
            mButtonShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Collection<UserRight> rights = mCurrOpenedUser.getRights();
                    Log.v(TAG, Utils.getMethodName() + "mButtonShare ");
                    if (mButtonShare.getBackground().equals(btn_selected_drawable)){
                        mButtonShare.setBackgroundDrawable(btn_not_selected_drawwable);
                        rights.remove(UserRight.SHARE);
                    }
                    else{
                        mButtonShare.setBackgroundDrawable(btn_selected_drawable);
                        rights.add(UserRight.SHARE);
                    }

                    mCurrOpenedUser.setRights(rights);

                }
            });
            


            mButtonInviteUser = (ImageButton) mView.findViewById(R.id.b_invite_user);
            mButtonInviteUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Collection<UserRight> rights = mCurrOpenedUser.getRights();
                    Log.v(TAG, Utils.getMethodName() + "mButtonInviteUser ");
                    if (mButtonInviteUser.getBackground().equals(btn_selected_drawable)) {
                        mButtonInviteUser.setBackgroundDrawable(btn_not_selected_drawwable);
                        Log.v(TAG, Utils.getMethodName() + "mButtonInviteUser 1");
                        rights.remove(UserRight.INVITE);
                    } else {
                        mButtonInviteUser.setBackgroundDrawable(btn_selected_drawable);
                        Log.v(TAG, Utils.getMethodName() + "mButtonInviteUser 2");
                        rights.add(UserRight.INVITE);
                    }

                    mCurrOpenedUser.setRights(rights);
                }
            });



            mButtonManageUser = (ImageButton) mView.findViewById(R.id.b_manage_user);
            mButtonManageUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Collection<UserRight> rights = mCurrOpenedUser.getRights();
                    
                    Log.v(TAG, Utils.getMethodName() + "mButtonManageUser ");
                    if (mButtonManageUser.getBackground().equals(btn_selected_drawable)) {
                        mButtonManageUser.setBackgroundDrawable(btn_not_selected_drawwable);

                        rights.remove(UserRight.MANAGE);
     
                    }
                    else {
                        mButtonManageUser.setBackgroundDrawable(btn_selected_drawable);

                        rights.add(UserRight.MANAGE);
                    }
                    mCurrOpenedUser.setRights(rights);
                }
            });




            mButtonEdit = (ImageButton) mView.findViewById(R.id.b_edit);

            mButtonEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.v(TAG, Utils.getMethodName() + "mButtonEdit ");
                    showEditMode(v);

                }
            });

            mButtonOk = (ImageButton) mView.findViewById(R.id.b_ok);

            mButtonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.v(TAG, Utils.getMethodName() + "mButtonOk ");
                    setInEditMode(false);

                    if (mCurrOpenedUser != null) {
                        // show normal view
                        populateView(mCurrOpenedUser);
                        
                        mCurrOpenedUser.setDisplayname(mEditDisplayName.getText().toString());
                        mCurrOpenedUser.setFirstname(mEditTextFirstName.getText().toString());
                        mCurrOpenedUser.setLastname(mEditTextLastName.getText().toString());

                        mCallback.onUpdateUser(mCurrOpenedUser);
                    }

                }
            });

            
            mButtonCancel = (ImageButton) mView.findViewById(R.id.b_cancel);
            mButtonCancel.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    Log.v(TAG, Utils.getMethodName() + "mButtonCancel ");
                    setInEditMode(false);

                    populateView(mCurrOpenedUser);
                }
            });


            mButtonDeleteUser = (ImageButton) mView.findViewById(R.id.b_delete_user);

            mButtonDeleteUser.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Log.v(TAG, Utils.getMethodName() + "mButtonDeleteUser ");
                    setInEditMode(false);
                    if (mCurrOpenedUser != null) {

                        mCallback.onDeleteUser(mCurrOpenedUser);
                    }
                }
            });

        }


        private void setEditTextFieldsGone(){
            mEditDisplayName.setEnabled(true);
            mEditDisplayName.setVisibility(View.GONE);

            mEditTextFirstName.setEnabled(true);
            mEditTextFirstName.setVisibility(View.GONE);
            mEditTextLastName.setVisibility(View.GONE);
        }

        private void setEditTextFieldsVisible(){
            mEditDisplayName.setEnabled(true);
            mEditDisplayName.setVisibility(View.VISIBLE);

            mEditTextFirstName.setEnabled(true);
            mEditTextFirstName.setVisibility(View.VISIBLE);
            mEditTextLastName.setVisibility(View.VISIBLE);

            mEditDisplayName.setText(mTextViewDisplayName.getText().toString());
            mEditTextFirstName.setText(mTextViewFirstName.getText().toString());
            mEditTextLastName.setText(mTextViewLastName.getText().toString());
        }

        private void setTextViewFieldsVisible(User user){
            mTextViewDisplayName.setVisibility(View.VISIBLE);
            mTextViewFirstName.setVisibility(View.VISIBLE);
            mTextViewLastName.setVisibility(View.VISIBLE);
            mTextViewEmail.setVisibility(View.VISIBLE);
            
            mTextViewEmail.setText(user.getEmail());
            mTextViewDisplayName.setText(user.getDisplayname());
            mTextViewFirstName.setText(user.getFirstname());
            mTextViewLastName.setText(user.getLastname());
        }


        private void setTextViewFieldsGone(){
            mTextViewDisplayName.setVisibility(View.GONE);
            mTextViewFirstName.setVisibility(View.GONE);
            mTextViewLastName.setVisibility(View.GONE);

        }

        
        public void showEditMode(View v) {

            Log.v(TAG, Utils.getMethodName() + "mButtonEdit ");
            setInEditMode(true);
            mButtonEdit.setVisibility(View.GONE);
            editButtonSet.setVisibility(View.VISIBLE);

            setEditTextFieldsVisible();
            setTextViewFieldsGone();

            imageViewSetUserRights.setVisibility(View.GONE);
            settingButtonSet.setVisibility(View.VISIBLE);

            Log.v(TAG, Utils.getMethodName() + "rights " +mCurrOpenedUser.getRights().toString());

            if(mCurrOpenedUser.getRights().contains(UserRight.SHARE))
                mButtonShare.setBackgroundDrawable(btn_selected_drawable);
            else
                mButtonShare.setBackgroundDrawable(btn_not_selected_drawwable);

            if(mCurrOpenedUser.getRights().contains(UserRight.INVITE))
                mButtonInviteUser.setBackgroundDrawable(btn_selected_drawable);
            else
                mButtonInviteUser.setBackgroundDrawable(btn_not_selected_drawwable);

            if(mCurrOpenedUser.getRights().contains(UserRight.MANAGE))
                mButtonManageUser.setBackgroundDrawable(btn_selected_drawable);
            else
                mButtonManageUser.setBackgroundDrawable(btn_not_selected_drawwable);

        }

        public void showCreateNewItemMode() {

            setInEditMode(true);
            settingButtonSet.setVisibility(View.VISIBLE);
            editButtonSet.setVisibility(View.VISIBLE);
            
            Log.v(TAG, Utils.getMethodName() + "user " + mCurrOpenedUser.getDisplayname());
            setTextViewFieldsGone();
            setEditTextFieldsVisible();          
        }

      
        public void populateView(User user) {
            Log.v(TAG, Utils.getMethodName() + "entry ");
            mCurrOpenedUser = user;
            imageViewSetUserRights.setVisibility(View.VISIBLE);
            settingButtonSet.setVisibility(View.GONE);
            editButtonSet.setVisibility(View.GONE);
            mButtonEdit.setVisibility(View.VISIBLE);

            Log.v(TAG, Utils.getMethodName() + "rights " +mCurrOpenedUser.getRights().toString());
            
            if(mCurrOpenedUser.getRights().contains(UserRight.SHARE))
                mImageViewShare.setAlpha(0.9f);
            else
                mImageViewShare.setAlpha(0.4f);

            if(mCurrOpenedUser.getRights().contains(UserRight.INVITE))
                mImageViewInviteUser.setAlpha(0.9f);
            else
                mImageViewInviteUser.setAlpha(0.4f);
            
            if(mCurrOpenedUser.getRights().contains(UserRight.MANAGE))
                mImageViewManageUser.setAlpha(0.9f);
            else
                mImageViewManageUser.setAlpha(0.4f);
            
            
            setEditTextFieldsGone();
            setTextViewFieldsVisible(user);

            String avatar_file_path = user.getAvatarAbsPath();
            if (avatar_file_path == null) {
                Log.v(TAG, Utils.getMethodName() + "showing standard avatar icon ");
                userAvatar.setImageResource(R.drawable.avatar);

            }
            else {
                Log.v(TAG, Utils.getMethodName() + "showing avatar " + avatar_file_path);
                Bitmap bMap = BitmapFactory.decodeFile(avatar_file_path);
                userAvatar.setImageBitmap(bMap);
            }

            // always send request to get fresh avatar !
            mCallback.onGetUserAvatar(user);

        }

    }






    @Override
    public int getCount() {
        return subItems.size();
    }
    
    @Override
    public Filter getFilter() {

        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                subItems = (ArrayList<User>) results.values;

                Log.e(TAG, Utils.getMethodName() +  results.values.toString());
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();
                ArrayList<User> filteredItemList = new ArrayList<User>();

                // perform your search here using the searchConstraint String.

                constraint = constraint.toString().toLowerCase();
                for (int i = 0; i < allItems.size(); i++) {
                    String dataNames = allItems.get(i).getDisplayname();
                    if (dataNames.toLowerCase().contains(constraint.toString()))  {
                        filteredItemList.add(allItems.get(i));
                    }
                }

                results.count = filteredItemList.size();
                results.values = filteredItemList;
                Log.e(TAG, Utils.getMethodName() +   results.values.toString());

                return results;
            }
        };

     return filter;
    }

}
