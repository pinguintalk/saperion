package com.saperion.sdb.client;


import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.ipaulpro.afilechooser.utils.FileUtils;
import com.saperion.sdb.client.ItemListActivity.FragmentInterface;
import com.saperion.sdb.client.PullDownListView.ListViewTouchEventListener;
import com.saperion.sdb.client.R.string;
import com.saperion.sdb.client.dialogs.ErrorDialogFragment.ErrorDialogFragmentBuilder;
import com.saperion.sdb.client.utils.Log;
import com.saperion.sdb.sdk.SdbApplication;
import com.saperion.sdb.sdk.models.Document;
import com.saperion.sdb.sdk.models.ErrorMessage;
import com.saperion.sdb.sdk.models.Folder;
import com.saperion.sdb.sdk.models.Item;
import com.saperion.sdb.sdk.models.Space;
import com.saperion.sdb.sdk.models.SpacedItem;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.RequestConstants;
import com.saperion.sdb.sdk.requestmanager.RequestManager.RequestListener;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.rights.SpaceRight;
import com.saperion.sdb.sdk.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cx.hell.android.pdfview.ChooseFileActivity;

/**
 * A fragment representing a single Item detail screen. This fragment is either
 * contained in a {@link MainFragmentActivity} in two-pane mode (on tablets) or
 * a {@link ItemListActivity} on handsets.
 */

public class ItemListFragment extends Fragment implements OnClickListener, RequestListener,
        ItemListViewInterface, FragmentInterface, ListViewTouchEventListener, TextWatcher {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */

    private static final String TAG = ItemListFragment.class.getSimpleName();

    public static final String CURRENT_PARENT_POSITION = "CURRENT_PARENT_POSITION";
    public static final String SPACE = "SPACE";
    public static final String ACTION_DROPDOWN_ITEM_NAME = "name";
    public static final String ACTION_DROPDOWN_ITEM = "item";
    public static final int READ_REQUEST_CODE = 42;

    Context mContext;
    private PullDownListView mListView;
    private ItemListAdapter mListAdapter;
    private ArrayList<Item> mItemList = new ArrayList<Item>();

    private ImageButton mCreateFileButton;
    private ImageButton mIBCreateFolder;
    private ImageView mInsertFileIcon;
    private RequestListener mListener;
    private LinearLayout toolbar;  
    private ProgressDialog progressDialog = null;
    private Item mSelectedItem;
    private Item mCurrentParrent;
    private boolean uploadingFromTmp = false;


    public final List<Map<String, Object>> navItemList =
            new ArrayList<Map<String, Object>>();
    private SimpleAdapter navAdapter = null;
    private ActionBar actionBar = null;
    private Spinner navSpinner = null;

    private ProgressBar listViewProgressbar;
    private ImageView listview_refresh_up;
    private EditText itemfilterText;
    private int mCurrentParentPosition = -1;

    public void updateNavList(Space space) {
        //TODO Update navList
    }

    public int getmCurrentParentPosition() {
        return mCurrentParentPosition;
    }

    public void setmCurrentParentPosition(int mCurrentParentPosition) {
        this.mCurrentParentPosition = mCurrentParentPosition;
    }
    
    @Override
    public void onListViewPullingDown(float distance) {

        Log.i(TAG, Utils.getMethodName() + "distance " + distance + "   " + listview_refresh_up.getHeight());

        FrameLayout.LayoutParams  lp = new   FrameLayout.LayoutParams( FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);

        if(distance < listview_refresh_up.getHeight())
        {

            lp.setMargins(0, (int) (distance), 0, 0);
            mListView.setLayoutParams(lp);
            mListView.bringToFront();
        }
        else if(distance >= PullDownListView.maxDiff)
        {
            listview_refresh_up.setVisibility(View.VISIBLE);

            lp.setMargins(0, listview_refresh_up.getHeight() + 2,0,0);
            mListView.setLayoutParams(lp);
            mListView.bringToFront();
        }
    }

    @Override
    public void onListViewPulledDown() {
        Log.i(TAG, Utils.getMethodName() + "entry");

        listview_refresh_up.setVisibility(View.VISIBLE);

    }


    @Override
    public void onListViewTouchUp(float distance) {
        Log.i(TAG, Utils.getMethodName() + "entry");


        if (distance > PullDownListView.maxDiff){

            listview_refresh_up.setVisibility(View.GONE);
            listViewProgressbar.setVisibility(View.VISIBLE);
            getChildren();
        }
        FrameLayout.LayoutParams  lp = new   FrameLayout.LayoutParams( FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(0,0,0,0);
        mListView.setLayoutParams(lp);
        mListView.bringToFront();
    }

    @Override
    public void onListViewResetPosition() {
        Log.i(TAG, Utils.getMethodName() + "entry");

        listview_refresh_up.setVisibility(View.GONE);
        listViewProgressbar.setVisibility(View.GONE);

        FrameLayout.LayoutParams  lp = new  FrameLayout.LayoutParams( FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(0,0,0,0);
        mListView.setLayoutParams(lp);

        mListView.bringToFront();
    }


    
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemListFragment() {
    }

    /*
    private OnClickListener mBreadCrumbListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            clickBreadCrumb(v);
            Log.v(TAG, Utils.getMethodName() + "BCListener");

        }
    };
    */
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.v(TAG, Utils.getMethodName() + "entry");
        setRetainInstance(true);
        mContext = getActivity().getApplicationContext();
        mListener = this;

        // hideKeyboard();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);


        // get parent Activity
        Context context = getActivity();
        
        Log.v(TAG, Utils.getMethodName() + "entry" + ItemListActivity.class.getSimpleName() + "  "
                + context.getClass().getSimpleName());
        // if we are on smartphone 
        if (context.getClass().getSimpleName().equals(ItemListActivity.class.getSimpleName()))
            ((ItemListActivity) context).fragmentInterface = this;

    }

    @Override
    public void passDataToFragment(String filePath) {
        if (filePath != null) {
            String fileName = isFileNameInList(mItemList, filePath);
            if (fileName.length() > 1) {
                showReplaceFileDialog(fileName, filePath);

            } else {
                showLoadingDialog();
                Document doc = new Document();
                //bc
                //doc.setParentId(mLlBreadcrumb.getCurrentLocationId());
                
                Item parentItem = (Item) navItemList.get(navSpinner.getSelectedItemPosition()).get(ACTION_DROPDOWN_ITEM);
                doc.setParentId(parentItem.getId());
                
                doc.create(filePath, this);
            }
        }
    }


    private void hideKeyboard() {

        InputMethodManager inputManager =
                (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Log.v(TAG, Utils.getMethodName() + "entry");

        // If activity recreated (such as createInstance screen rotate), restore
        // the previous article selection set by onSaveInstanceState().
        // This is primarily necessary when in the two-pane layout.
        if (savedInstanceState != null) {
            Log.v(TAG, Utils.getMethodName() + "savedInstanceState != null");
            mCurrentParentPosition = savedInstanceState.getInt(CURRENT_PARENT_POSITION);
        }

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.item_list_fragment, container, false);

    }



    @Override
    public void onResume() {
        super.onResume();

        Log.v(TAG,
                Utils.getMethodName() + "entry");
        
        // During startup, check if there are arguments passed to the fragment.
        // onStart is a good place to do this because the layout has already been
        // applied to the fragment at this point so we can safely call the method
        // below that sets the items of space.
        LinearLayout toolbar = (LinearLayout) getActivity().findViewById(R.id.toolbar_layout);
        
        Bundle bundle = getArguments();
        if (bundle != null) {
            // Set article based on argument passed in
            Space space = bundle.getParcelable(SPACE);
            
            toolbar.setVisibility(View.VISIBLE);

            mCurrentParrent = space;
            initNavList(space);
            if (!space.getRights().toString().contains(SpaceRight.WRITE.toString())) {
                disableToolBarButtons();
            }
            
            showLoadingDialog();
            ArrayList<? extends SpacedItem> cached_items = space.getChildren(mContext,this);
            refreshItemList(cached_items);
            mListAdapter.notifyDataSetChanged();
            
        } else if (navItemList.size() > 0 && mCurrentParentPosition >= 0) {
            Log.v(TAG,
                    Utils.getMethodName() + "nav to mCurrentParentPosition " + mCurrentParentPosition);
            navigateToItem(mCurrentParentPosition);
        } 
        else {
            toolbar.setVisibility(View.INVISIBLE);        
        }

        initDropDownNav();
    }
   
    
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        Log.v(TAG, Utils.getMethodName() + "entry");

        mIBCreateFolder = (ImageButton) view.findViewById(R.id.b_create_folder);
        mCreateFileButton = (ImageButton) view.findViewById(R.id.b_upload_file);
        mInsertFileIcon = (ImageView) view.findViewById(R.id.insert_icon);

        if (SdbApplication.getInstance().getmClipboardItem() != null)
            mInsertFileIcon.setVisibility(View.VISIBLE);
        else
            mInsertFileIcon.setVisibility(View.INVISIBLE);

  
        mListView = (PullDownListView) view.findViewById(R.id.item_list_view);
        mListView.setListViewTouchListener(this);
        if (mListView == null)
            return;

        itemfilterText = (EditText) view.findViewById(R.id.item_filter_field);
        itemfilterText.addTextChangedListener(this);
        itemfilterText.setClickable(true);
        listViewProgressbar = (ProgressBar) view.findViewById(R.id.list_view_progressbar);
        listview_refresh_up = (ImageView) view.findViewById(R.id.listview_refresh_up);

        mListView.setListViewTouchListener(this);
        mListView.setPulledDown(true);

        mListView.setVisibility(View.VISIBLE);

        toolbar = (LinearLayout) getActivity().findViewById(R.id.toolbar_layout);
       // toolbar.setVisibility(View.VISIBLE);
        
        mIBCreateFolder.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //bc
                //if (mLlBreadcrumb == null)
                //    return;

                if (!isNewSpaceInEditMode()) {
                    // set selected to empty item once
                    Folder folder = new Folder();
                    folder.setId("");
                    Item parentItem = (Item) navItemList.get(navSpinner.getSelectedItemPosition()).get(ACTION_DROPDOWN_ITEM);
                    //bc
                    //folder.setParentId(mLlBreadcrumb.getCurrentLocationId());
                    folder.setParentId(parentItem.getId());
                    folder.setName("");

                    mListAdapter.setmCurrentSelectedItemId(folder.getId());
                    mListAdapter.insert(folder, 0);
                    mListAdapter.notifyDataSetChanged();

                    mListView.scrollTo(0, 0);
                }

            }
        });

        mListAdapter = new ItemListAdapter(getActivity(), this, mItemList);
        mListView.setAdapter(mListAdapter);

        mListView.setAnimationCacheEnabled(false);
        mListView.setScrollingCacheEnabled(false);
        mListView.setClickable(true);
        mListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
                Log.v(TAG, Utils.getMethodName() + "Item clicked: " + position);

                Item selected_item = (Item) mListAdapter.getItem(position);

                // check if there was a new folder not saved before
                Item folder_may_to_delete = (Item) mListAdapter.getItem(0);
                if (folder_may_to_delete.getName().equals("")) { // remove empty
                    // item
                    // first, if
                    // create
                    // process
                    // canceled
                    mListAdapter.remove((Item) folder_may_to_delete);
                }
                mSelectedItem = selected_item;
                mListAdapter.setmOpenItemPosition(position);
                mListAdapter.setmCurrentSelectedItemId(selected_item.getId());
                mListAdapter.setInEditMode(false);
                // refresh the list
                refreshListView();

            }
        });
        
        actionBar = getActivity().getActionBar();

        mCreateFileButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO we have to check if we are in edit mode ?
                // if (isNewSpaceInEditMode()) return;

                if (SdbApplication.getInstance().getmClipboardItem() != null) {
                    showPopupMenu(v);
                } else {


//                    if ( Build.VERSION.SDK_INT >= VERSION_CODES.KITKAT)
//                    {
//                    // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file
//                    // browser.
//                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
//
//                    // Filter to only show results that can be "opened", such as a
//                    // file (as opposed to a list of contacts or timezones)
//                    intent.addCategory(Intent.CATEGORY_OPENABLE);
//
//                    // Filter to show only images, using the image MIME data type.
//                    // If one wanted to search for ogg vorbis files, the type would be "audio/ogg".
//                    // To search for all documents available via installed storage providers.
//                   intent.setType("*/*");
//
//                    startActivityForResult(intent, READ_REQUEST_CODE);
//                    }
//                    else 
//                    {
//                        getActivity().startActivityForResult(
//                                new Intent(mContext, ChooseFileActivity.class),
//                               MainFragmentActivity.PICK_FILE_REQUEST_CODE); 
//                    }
                    
                    // use aFileChooser lib
                    // Use the GET_CONTENT intent from the utility class
                    Intent target = FileUtils.createGetContentIntent();
                    // Create the chooser Intent
                    Intent intent = Intent.createChooser(
                            target, getString(R.string.choose_file_to_upload_title));
                    try {
                        startActivityForResult(intent, MainFragmentActivity.PICK_FILE_REQUEST_CODE);
                    } catch (ActivityNotFoundException e) {
                        // The reason for the existence of aFileChooser
                    }
                    
                }
            }
        });


        Log.v(TAG,
                Utils.getMethodName() + "navItemList.size " + navItemList.size());
        
        if (navItemList.size() > 0) {     
            mListView.setVisibility(View.VISIBLE);
            
            Space mCurrSpace = (Space) navItemList.get(0).get(ACTION_DROPDOWN_ITEM);
                
            if (!mCurrSpace.getRights().toString().contains(SpaceRight.WRITE.toString())) {
                Log.v(TAG,
                        Utils.getMethodName() + "space rights " + mCurrSpace.getRights().toString());

                disableToolBarButtons();

                if (mInsertFileIcon != null && (mInsertFileIcon.getVisibility() == View.VISIBLE)) {
                    mInsertFileIcon.setAlpha(Constants.TRANSPARENT_GRADE);
                    mInsertFileIcon.setClickable(false);
                }
            }
        } else {
            mListView.setVisibility(View.GONE);
           // toolbar.setVisibility(View.GONE);
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.v(TAG, Utils.getMethodName() + "entry mCurrentParentPosition = "+ mCurrentParentPosition);
        // Save the current  selection in case we need to recreate the fragment
        outState.putInt(CURRENT_PARENT_POSITION, mCurrentParentPosition);

    }

    private void initNavList(Space space){

        Log.v(TAG, Utils.getMethodName() + "entry");

        if(space != null){
            navItemList.clear();
            Map<String, Object> map = new HashMap<String, Object>();
            map.put(ACTION_DROPDOWN_ITEM_NAME, space.getName());
            map.put(ACTION_DROPDOWN_ITEM, space);
            navItemList.add(map);
            mCurrentParentPosition = 0;
        }
    }
    
    private void initDropDownNav(){

        Log.v(TAG, Utils.getMethodName() + "entry");

        navAdapter  = new SimpleAdapter(this.getActivity(), navItemList ,
                R.layout.actionbar_spinner_cell,
                new String[] { ACTION_DROPDOWN_ITEM_NAME },
                new int[] { R.id.text } );
        
        navSpinner = (Spinner) actionBar.getCustomView().findViewById(R.id.dropdown_list);
        navSpinner.setAdapter(navAdapter);
        if (mCurrentParentPosition >=0)
            navSpinner.setSelection(mCurrentParentPosition,true);
        
        navSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                Log.v(TAG, Utils.getMethodName() + "Item clicked: " + position);

                    if (mCurrentParentPosition != position ) {
                        mCurrentParentPosition = position;
                        Log.v(TAG, Utils.getMethodName() + "navigating to " + position);
                        navigateToItem( position);
                    }
                
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        /*
        actionBar.setListNavigationCallbacks( navAdapter,
                new OnNavigationListener()
                {
                    @Override
                    public boolean onNavigationItemSelected(
                            int itemPosition,
                            long itemId )
                    {
                        Map<String, Object> map =
                                navItemList.get( itemPosition );
                        String name  = (String) map.get(ACTION_DROPDOWN_ITEM_NAME);
                        Item item = (Item) map.get(ACTION_DROPDOWN_ITEM);
                        Log.v(TAG, Utils.getMethodName() + "navigating to " + name);

                        navigateToItem(item, itemPosition);
      
                        return true;
                    }
                }
        );
        */
    }
    



    public void getChildren() {

        Log.d(TAG, Utils.getMethodName() + "entry");
        if (navItemList.size() < 1)
            return;
        Item item = (Item)  navItemList.get(navSpinner.getSelectedItemPosition()).get(ACTION_DROPDOWN_ITEM);

        ArrayList<? extends SpacedItem> cached_items = null;
        if (item.getType().equals(Constants.FOLDER_TYPE)) {
            Folder folder = ((Folder) item);
            cached_items = folder.getChildren(mContext, this);
            refreshItemList(cached_items);
            
            Log.v(TAG, Utils.getMethodName() + "get children of folder " + folder.getName());
        } else if (item.getType().equals(Constants.SPACE_TYPE)) {
            Space space = (Space) item;
            cached_items =  space.getChildren(mContext, mListener);
           

            Log.v(TAG,
                    Utils.getMethodName() + "get children of space " + space.getName());
        }
        
        refreshItemList(cached_items);
        mListAdapter.notifyDataSetChanged();
        showLoadingDialog();

    }
    
    public void refreshListView() {

        String parentType = ((Item) navItemList.get(navSpinner.getSelectedItemPosition()).get(ACTION_DROPDOWN_ITEM)).getType();
        ArrayList<Item> currList  = new ArrayList<Item>();
        currList.addAll(mItemList);
        LinearLayout toolbar = (LinearLayout) getActivity().findViewById(R.id.toolbar_layout);
        
        if (!SdbApplication.getInstance().isShowRecycledItems()){

            for (int i = 0; i < currList.size(); i++) {
                Item item = currList.get(i);
                String type = item.getType();
                
                Log.v(TAG, Utils.getMethodName() + "item name" + item.getName() + " type"  + type );
                 
                if ( type.equals(Constants.FOLDER_TYPE)) {
                    if ( ((Folder) item).isRecycled()){

                        Log.v(TAG, Utils.getMethodName() +  item.getName()  + " is recycled");                        
                        mListAdapter.remove(currList.get(i));
                        
                    }
                }
                if ( type.equals(Constants.DOCUMENT_TYPE)) {
                    Document doc = (Document) item;
                    if ( doc.isRecycled()) {
                        Log.v(TAG, Utils.getMethodName() +  item.getName()  + " is recycled");
                        mListAdapter.remove(currList.get(i));
                    }
                }

            }
     
            
            if (parentType.equals(Constants.SPACE_TYPE)) {

                Space  space = ((Space) navItemList.get(navSpinner.getSelectedItemPosition()).get(ACTION_DROPDOWN_ITEM));
                
                if (space.isRecycled()){
                    actionBar.setDisplayHomeAsUpEnabled(false);
                    toolbar.setVisibility(View.GONE);
                    mListView.setVisibility(View.GONE);
                    actionBar.getCustomView().findViewById(R.id.dropdown_list).setVisibility(View.GONE);
                } else {
                    actionBar.setDisplayHomeAsUpEnabled(true);
                    toolbar.setVisibility(View.VISIBLE);
                    mListView.setVisibility(View.VISIBLE);
                    actionBar.getCustomView().findViewById(R.id.dropdown_list).setVisibility(View.VISIBLE);
                }
            }
            else if ( parentType.equals(Constants.FOLDER_TYPE)) {

                Folder  parentFolder = ((Folder) navItemList.get(navSpinner.getSelectedItemPosition()).get(ACTION_DROPDOWN_ITEM));

                if (parentFolder.isRecycled()){
                    actionBar.setDisplayHomeAsUpEnabled(false);
                    toolbar.setVisibility(View.GONE);
                    mListView.setVisibility(View.GONE);
                    actionBar.getCustomView().findViewById(R.id.dropdown_list).setVisibility(View.GONE);
                } else {
                    actionBar.setDisplayHomeAsUpEnabled(true);
                    toolbar.setVisibility(View.VISIBLE);
                    mListView.setVisibility(View.VISIBLE);
                    actionBar.getCustomView().findViewById(R.id.dropdown_list).setVisibility(View.VISIBLE);
                }
            }
        }
        else
        {
            toolbar.setVisibility(View.VISIBLE);
            mListView.setVisibility(View.VISIBLE);
            actionBar.getCustomView().findViewById(R.id.dropdown_list).setVisibility(View.VISIBLE);

            if (parentType.equals(Constants.SPACE_TYPE) ) {

                Space  space = ((Space) navItemList.get(navSpinner.getSelectedItemPosition()).get(ACTION_DROPDOWN_ITEM));

                if (space.isRecycled()){
                    disableToolBarButtons();
                }
                else
                    enableToolBarButtons();
            }
            else if ( parentType.equals(Constants.FOLDER_TYPE)) {

            Folder  parentFolder = ((Folder) navItemList.get(navSpinner.getSelectedItemPosition()).get(ACTION_DROPDOWN_ITEM));

                if (parentFolder.isRecycled()){
                    disableToolBarButtons();
                }
                else
                    enableToolBarButtons();
            }

        }
        mListAdapter.notifyDataSetChanged();
    }
    
    private void disableToolBarButtons() {
        
        mIBCreateFolder = (ImageButton) getActivity().findViewById(R.id.b_create_folder);
        mCreateFileButton = (ImageButton) getActivity().findViewById(R.id.b_upload_file);
        mInsertFileIcon = (ImageView) getActivity().findViewById(R.id.insert_icon);

        //mCreateFileButton.setAlpha(Constants.TRANSPARENT_GRADE);
        mCreateFileButton.setClickable(false);


        //mIBCreateFolder.setAlpha(Constants.TRANSPARENT_GRADE);
        mIBCreateFolder.setClickable(false);

        //mInsertFileIcon.setAlpha(Constants.TRANSPARENT_GRADE);
        mInsertFileIcon.setClickable(false);
        
        itemfilterText.setClickable(false);
    }

    private void enableToolBarButtons() {

        mIBCreateFolder = (ImageButton) getActivity().findViewById(R.id.b_create_folder);
        mCreateFileButton = (ImageButton) getActivity().findViewById(R.id.b_upload_file);
        mInsertFileIcon = (ImageView) getActivity().findViewById(R.id.insert_icon);
   
        mCreateFileButton.setClickable(true);
        mIBCreateFolder.setClickable(true);
        mInsertFileIcon.setClickable(true);
        itemfilterText.setClickable(true);
    }
    
    private void showPopupMenu(View v) {

        PopupMenu popupMenu = new PopupMenu(getActivity(), v);
        popupMenu.getMenuInflater().inflate(R.menu.insert_file_popupmenu, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Toast.makeText(getActivity(), item.toString(), Toast.LENGTH_LONG).show();
    
                if (item.getItemId() == R.id.create_file_menu_item) {
                    getActivity().startActivityForResult(
                            new Intent(mContext, ChooseFileActivity.class),
                            MainFragmentActivity.PICK_FILE_REQUEST_CODE);
                } else if (item.getItemId() == R.id.insert_file_menu_item) {

                    // TODO check if temp folder has more then one files
                    showLoadingDialog();
                    SpacedItem itemToInsert = SdbApplication.getInstance().getmClipboardItem();
                    //bc
                    //String currParentId = mLlBreadcrumb.getCurrentLocationId();
                    String currParentId = ((Item) navItemList.get(navSpinner.getSelectedItemPosition())
                            .get(ACTION_DROPDOWN_ITEM)).getId();
                    
                    if (itemToInsert.getParentId() != null
                            && currParentId.equals(itemToInsert.getParentId())) { // we have some thing to insert in same folder, so break it
                        SdbApplication.getInstance().clearClipboardItem();
                        mInsertFileIcon.setVisibility(View.INVISIBLE);
                        return true;
                    }

                    if (itemToInsert.getType().equals(Constants.DOCUMENT_TYPE)) {
                        Document document = (Document) itemToInsert;

                        document.setParentId(currParentId);

                        File[] tmp_files = SdbApplication.getInstance().getTempFiles();
                        if (tmp_files!=null && tmp_files.length > 0) {
                            Log.v(TAG, Utils.getMethodName() + "file to upload"
                                    + tmp_files[0].getAbsolutePath().toString());
                            uploadingFromTmp = true;
                            document.create(tmp_files[0].getAbsolutePath(), mListener);
                        } else {
                            document.save(mListener);
                        }
                    } else {
                        Folder folder = (Folder) itemToInsert;
                        folder.setParentId(currParentId);
                        folder.save(mListener);
                    }

                }

                return true;
            }
        });

        popupMenu.show();
    }

    private boolean isNewSpaceInEditMode() {
        boolean is_empty_space_opened = false;

        if (mListAdapter.getCount() > 0) {
            if ((((Item) mListAdapter.getItem(0)).getName()).equals("")) {
                // that means user
                // is
                // currently
                // creating
                // new
                // space
                is_empty_space_opened = true;
            }
        }
        Log.v(TAG, Utils.getMethodName() + "entry " + is_empty_space_opened);
        return is_empty_space_opened;
    }

    public void onSpaceSelected(Space space) {
        Log.v(TAG, Utils.getMethodName() + "entry ");

        if (space == null){
            
            clearList();
        } else {
            initNavList(space);
            initDropDownNav();
            getChildren();
        }
        
       
    }
    
    /*
    @Override
    public void onResume() {
        super.onResume();

        Log.v(TAG, Utils.getMethodName() + "entry ");
        if (navItemList.size() > 0 && mCurrentParentPosition >= 0){
            initDropDownNav();
            navigateToItem(mCurrentParentPosition);
        }
    }
    */

    @Override
    public void onClick(View view) {

        Log.v(TAG, Utils.getMethodName() + "view : ");

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.v(TAG, Utils.getMethodName() + "entry");

    }


    @Override
    public void itemIconOrNameClickedEvent(SpacedItem item) {

        Log.v(TAG, Utils.getMethodName() + "should go deeper ");

        String itemId = item.getId();
        String itemType = item.getType();

        // a folder is clicked
        if (itemType.equals(Constants.FOLDER_TYPE)) {

            showLoadingDialog();
            mCurrentParrent = item;
            
            //bc
            // insert new element to breadcrumb
            //mLlBreadcrumb.addElement(mCurrentParrent, mBreadCrumbListener);
            
            addItemToDropDownNav(item);
            // request to get children createInstance server
            mItemList.clear();
            mListAdapter.clear();

            ArrayList<? extends SpacedItem> items = ((Folder) item).getChildren(mContext, this);
            refreshItemList(items);
            mListAdapter.notifyDataSetChanged();

        } else if (itemType.equals(Constants.DOCUMENT_TYPE)) {

            String mimeType = ((Document) item).getMimeType();
            int docVersion = ((Document) item).getVersionNumber();
            Log.v(TAG, "mimeType: " + mimeType);

            Log.v(TAG, Utils.getMethodName() + " doc ver " + docVersion);
            
            Bundle bundle = new Bundle();
            bundle.putParcelable(DocumentFragmentActivity.PARAM_DOCUMENT,  item);
            bundle.putInt(DocumentFragmentActivity.PARAM_DOCUMENT_VERSION,  docVersion);
            
            //Intent intent = new Intent(getActivity().getApplicationContext(), DocumentFragmentActivity.class);
            Intent intent = new Intent(getActivity().getApplicationContext(), DocumentViewerActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);

            /*
			String mimeType = ((Document) item).getMimeType();
			Log.v(TAG, "mimeType: " + mimeType);
			showLoadingDialog();
			Log.v(TAG, Utils.getMethodName() + " dowloading preview and file " + itemId);

			((Document) item).getFile(this);
			**/
        }

    }

    private void addItemToDropDownNav(SpacedItem item) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put(ACTION_DROPDOWN_ITEM_NAME, item.getName());
        map.put(ACTION_DROPDOWN_ITEM, item);
        navItemList.add(map);
        navAdapter.notifyDataSetChanged();
        // getActivity().getActionBar().setSelectedNavigationItem(navItemList.size()-1);
        navSpinner.setSelection(navItemList.size() - 1, true);
    }

    @Override
    public void itemRecycleClickedEvent(SpacedItem item) {
        Log.v(TAG, Utils.getMethodName() + "entry ");

        String itemId = ((SpacedItem) item).getId();
        String itemType = ((SpacedItem) item).getType();

        if (itemType.equals(Constants.FOLDER_TYPE)) {

            Folder folder = (Folder) item;

            if (folder.getId().equals("")) {
                mListAdapter.remove(folder);
                return;
            }

            folder.recycle(this);

        } else if (itemType.equals(Constants.DOCUMENT_TYPE)) {

            Log.v(TAG, Utils.getMethodName() + " deleting doc " + itemId);

            Document doc = (Document) item;

            if (doc.getId().equals("")) {
                mListAdapter.remove(doc);
                return;
            }

            doc.recycle(this);
        }
        showLoadingDialog();
    }

    @Override
    public void itemOkClickedEvent(SpacedItem item) {

        Log.v(TAG, Utils.getMethodName() + "entry ");

        String itemId = item.getId();
        String itemType = item.getType();

        if (itemType.equals(Constants.FOLDER_TYPE)) {

            Log.v(TAG, Utils.getMethodName() + " creating or updating folder " + itemId);
            Folder folder = (Folder) item;

            if (folder.getName().equals("")) {
                Toast.makeText(getActivity(), "Enter Name", Toast.LENGTH_LONG).show();
                return;
            }
            if (folder.getId().equals("")) {
                mListAdapter.remove(folder);
            }

            folder.save(this);

        } else if (itemType.equals(Constants.DOCUMENT_TYPE)) {

            Log.v(TAG, Utils.getMethodName() + " updating doc " + itemId);
            Document doc = (Document) item;
            doc.save(this);

        }
        showLoadingDialog();
    }

    @Override
    public void itemDownloadClickedEvent(SpacedItem item) {
        showLoadingDialog();
        Document doc = (Document) item;
        doc.download(this);
    }

    @Override
    public void itemOpenLinkDialogClickedEvent(Activity activity, Parcelable docParcelable) {
        Document doc = (Document) docParcelable;
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment newFragment = new LinkDialogFragment(activity, doc);
        newFragment.show(ft, "dialog");
        
    }

    @Override
    public void itemCutClickedEvent(SpacedItem mItem) {
        SdbApplication.getInstance().setmClipboardItem(mItem);

        mInsertFileIcon.setVisibility(View.VISIBLE);

		/*
		ImageButton drawingImageView = (ImageButton) getActivity().findViewById(R.id.b_upload_file);
		Bitmap bitmap = new Bitmap();
		Canvas canvas = new Canvas(bitmap);
		drawingImageView.setImageBitmap(bitmap);

		// Circle

		Paint paint = new Paint();
		paint.setColor(Color.GREEN);
		paint.setStyle(Paint.Style.STROKE);
		float x = 50;
		float y = 50;
		float radius = 20;
		canvas.drawCircle(x, y, radius, paint);
		*/
    }

    @Override
    public void itemGetPreview(Document document) {
        document.getPreview(this);
    }

    @Override
    public void itemRestoreClickedEvent(SpacedItem item) {

        Log.v(TAG, Utils.getMethodName() + "entry ");

        String itemId = item.getId();
        String itemType = item.getType();

        if (itemType.equals(Constants.FOLDER_TYPE)) {

            Folder folder = (Folder) item;
            folder.restore(this);

        } else if (itemType.equals(Constants.DOCUMENT_TYPE)) {

            Log.v(TAG, Utils.getMethodName() + " restoring doc " + itemId);
            Document doc = (Document) item;
            doc.restore(this);

        }
        showLoadingDialog();
    }

    @Override
    public void itemDeleteClickedEvent(SpacedItem item) {
        Log.v(TAG, Utils.getMethodName() + "entry ");

        String itemId = item.getId();
        String itemType = item.getType();

        if (itemType.equals(Constants.FOLDER_TYPE)) {

            Folder folder = (Folder) item;
            folder.delete(this);

        } else if (itemType.equals(Constants.DOCUMENT_TYPE)) {
          
            Document doc = (Document) item;
            doc.delete(this);

        }
        showLoadingDialog();
    }

    /*
    public int navigateToParent() {

        Log.v(TAG, Utils.getMethodName());

        if (mLlBreadcrumb == null) {
            return 3;
        }

        if (mLlBreadcrumb.getCurrentItemList().isEmpty()) {
            return 3;
        } else if (mLlBreadcrumb.getCurrentItemList().size() == 1) {
            mLlBreadcrumb.getCurrentItemList()
                    .remove(mLlBreadcrumb.getCurrentItemList().size() - 1);
            clearList();
            return 2;
        } else {
            mLlBreadcrumb.removeLastElement();

            // request to get new contents createInstance
            showLoadingDialog();
            if (mLlBreadcrumb.getCurrentLocationType().equals(Constants.SPACE_TYPE))
                // request to get children createInstance server
                ((Space) mLlBreadcrumb.getCurrentItem()).getChildren(this);
            else if (mLlBreadcrumb.getCurrentLocationType().equals(Constants.FOLDER_TYPE))
                // request to get children createInstance server
                ((Folder) mLlBreadcrumb.getCurrentItem()).getChildren(this);

            return 1;
        }
    }
    */

    
    public void navigateToItem(int pos) {
        Log.i(TAG, Utils.getMethodName() + "entry ");

        if(pos < 0) {
            clearList();
            return;
        }
        Item item = (Item) navItemList.get(pos).get(ACTION_DROPDOWN_ITEM);
        mCurrentParrent = item;
        
        int i = navItemList.size() - 1;
        while(i > pos)
        {
            navItemList.remove(i);
            i--;
        }

        // String positionId = new Integer(id).toString();
        String itemId = item.getId();
        String itemName = item.getName();
        String itemType = item.getType();

        mCurrentParrent = item;
       //bc
        // set Breadcrumb
        //mLlBreadcrumb.navigateToItem(pos);
        
        // stop all lowest request in the queues 
        SdbApplication.getInstance().stopAllRequest();

        showLoadingDialog();

        // request to get new content
        if (item.getType().equals(Constants.FOLDER_TYPE)) {
            ArrayList<? extends SpacedItem> items =  ((Folder) item).getChildren(mContext, this);
            refreshItemList(items);
        }
        else if (item.getType().equals(Constants.SPACE_TYPE)) {
            ArrayList<? extends SpacedItem> items =  ((Space) item).getChildren(mContext, this);
            refreshItemList(items);
        }

        navAdapter.notifyDataSetChanged();
    }

    public void clearList() {
        View view = actionBar.getCustomView();
        view.findViewById(R.id.dropdown_list).setVisibility(View.GONE);
        LinearLayout toolbar = (LinearLayout) getActivity().findViewById(R.id.toolbar_layout);
        toolbar.setVisibility(View.GONE);
        
        mListAdapter.clear();
        mListAdapter.notifyDataSetChanged();
        navItemList.clear();
        navAdapter.notifyDataSetChanged();
        
        disableToolBarButtons();
    }
/*
    public void updateBreadcrumbSpace(Space space) {
        if (mLlBreadcrumb != null)
            mLlBreadcrumb.updateSpace(space, mBreadCrumbListener);
    }

    private void clickBreadCrumb(View v) {
        navigateToItem((Item) mLlBreadcrumb.getCurrentItemList().get(v.getId()), v.getId());
    }
'*/
    
    
    private void refreshItemList(ArrayList<? extends SpacedItem> items) 
    {
        if (items == null) return;
        mItemList.clear();
        if (SdbApplication.getInstance().isShowRecycledItems())
            mItemList.addAll(items);
        else {
            for (SpacedItem item:items ){
                if (item.getType().equals(Constants.DOCUMENT_TYPE)) {
                    Document doc = (Document) item;
                    if (!doc.isRecycled())
                        mItemList.add(item);

                } else{
                    Folder folder = (Folder) item;
                    if (!folder.isRecycled()) {
                        mItemList.add(item);
                    }
                }
            }
        }
    }
    @Override
    public void onRequestFinished(Request request, Bundle resultData) {
        // TODO to handle request here

        if (getActivity() == null)
            return;

        int result_code = resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);
        int requestType = request.getRequestType();
        String errorMessage = resultData.getString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE);

        cancelLoadingDialog();
        listViewProgressbar.setVisibility(View.GONE);
        //bc
        //if (mLlBreadcrumb == null)
        //    return;

        //String parent_id = mLlBreadcrumb.getCurrentLocationId();


        String parentId = null;
        if (navItemList.size() > 0 && mCurrentParentPosition >=0)
             parentId = ((Item) navItemList.get(mCurrentParentPosition).get(ACTION_DROPDOWN_ITEM)).getId();
                
        if (parentId == null || TextUtils.isEmpty(parentId))
            return;

        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " resultCode = "
                + result_code);

        switch (result_code) {

            case ResponseConstants.RESPONSE_CODE_OK:
            case ResponseConstants.RESPONSE_CODE_CREATED:
            case ResponseConstants.RESPONSE_CODE_NO_CONTENT:
                break;
            case ResponseConstants.RESPONSE_CODE_UNKOWN_ERROR:
            case ResponseConstants.RESPONSE_CODE_BAD_REQUEST:
            case ResponseConstants.RESPONSE_CODE_ERROR:
            case ResponseConstants.RESPONSE_CODE_FORBIDDEN:
            case ResponseConstants.RESPONSE_CODE_INTERNAL_SERVER_ERROR:
            case ResponseConstants.RESPONSE_CODE_INTERNAL_SERVICE_UNAVAILABLE:
            case ResponseConstants.RESPONSE_CODE_METHOD_NOT_ALLOWED:
            case ResponseConstants.RESPONSE_CODE_NOT_FOUND:
                return;
            case ResponseConstants.RESPONSE_CODE_PRECONDITION_FAILED:
            case ResponseConstants.RESPONSE_CODE_TOO_MANY_REQUESTS:
                showErrorDialog(errorMessage);
                return;
            default: {
                showErrorDialog("unkown error !"); // should not come here !
                return;
            }
        }

        switch (requestType) {

            case RequestConstants.REQUEST_TYPE_FOLDER_CREATE:

                Folder newFolder =
                        resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);

                // set selection to new created
                mListAdapter.setmCurrentSelectedItemId(newFolder.getId());
                mItemList.add(newFolder);

                break;
            case RequestConstants.REQUEST_TYPE_FOLDER_GET_CHILDREN:
            case RequestConstants.REQUEST_TYPE_SPACE_GET_CHILDREN:

                listViewProgressbar.setVisibility(View.GONE);
                
                ArrayList<? extends SpacedItem> items =
                        resultData.getParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);

                if (items == null) {

                    Log.v(TAG, Utils.getMethodName() + " items =null");

                    mItemList.clear();

                    break;
                }

                // check if this response createInstance older request
                if (items.size() > 0 && !items.get(0).getParentId().equals(parentId)) {
                    Log.i(TAG, Utils.getMethodName() + " old response request");
                    break;
                }

                Log.v(TAG, Utils.getMethodName() + " items = " + items.toString());
                refreshItemList(items);

                break;

            case RequestConstants.REQUEST_TYPE_FOLDER_UPDATE:
                Folder updateFolder =
                        resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);

                if (updateFolder == null) return;

                // check if responses of older request is coming
                if (!updateFolder.getParentId().equals(parentId))
                    break;


                if (!updateItem(mItemList, updateFolder)) { // if document could not updated because we are in other fragment list
                    // that indicate the insert action is
                    // done, so insert new_doc to this new list and delete clipboard
                    mItemList.add(updateFolder);

                    SpacedItem spacedItem = SdbApplication.getInstance().getmClipboardItem();
                    if (spacedItem != null && spacedItem.getId().equals(updateFolder.getId())) {
                        SdbApplication.getInstance().clearClipboardItem();
                        mInsertFileIcon.setVisibility(View.INVISIBLE);
                    }
                }

                Log.v(TAG, Utils.getMethodName() + " folder updated = " + updateFolder.toString());


                break;

            case RequestConstants.REQUEST_TYPE_FOLDER_DELETE:

                Folder folder = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                removeItem(mItemList, folder);
                mListAdapter.remove(folder);

                break;

            case RequestConstants.REQUEST_TYPE_DOCUMENT_DELETE: {
                Document doc = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                if (doc == null)
                    return;
                removeItem(mItemList, doc);
                mListAdapter.remove(doc);

                // mListAdapter.notifyDataSetChanged();
			/*
			mListAdapter.remove(mListAdapter.getItem(mListAdapter
			    .getmOpenItemPosition()));
			*/
            }
            break;

            case RequestConstants.REQUEST_TYPE_DOCUMENT_UPDATE: {
                Document new_doc =
                        resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                
                
                if (!updateItem(mItemList, new_doc)) { // if document could not updated because we are in other fragment list
                    // that indicate the insert action is
                    // done, so insert new_doc to this new list and delete clipboard

                    mItemList.add(new_doc);
                    SpacedItem mClipboardItem = SdbApplication.getInstance().getmClipboardItem();
                    if (mClipboardItem != null && mClipboardItem.getId().equals(new_doc.getId())) {
                        SdbApplication.getInstance().clearClipboardItem();
                        mInsertFileIcon.setVisibility(View.INVISIBLE);
                    }
                }

            }
            break;
            case RequestConstants.REQUEST_TYPE_DOCUMENT_GET_PREVIEW: {

                Document new_doc =
                        resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                if (new_doc == null)
                    break;

                // check if this response createInstance of older request
                if (!new_doc.getParentId().equals(parentId))
                    break;

                updateItem(mItemList, new_doc);

                View doc_view =
                        mListView.getChildAt(mListAdapter.getPosition(new_doc)
                                - mListView.getFirstVisiblePosition());
                if (doc_view != null && mListAdapter != null) {
                    Log.v(TAG, Utils.getMethodName() + " updating preview of " + new_doc.getName());
                    mListAdapter.updatePreview(doc_view, new_doc);
                }

            }
            break;
            case RequestConstants.REQUEST_TYPE_DOCUMENT_CREATE: {
                Document doc = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                if (doc == null) return;

                if (getCurrentDocFromListView(mItemList, doc.getId()) == null)
                    mItemList.add(doc);
                else
                    updateItem(mItemList, doc);
                if (doc.getId() != null)
                    mListAdapter.setmCurrentSelectedItemId(doc.getId());

                if (uploadingFromTmp) {
                    uploadingFromTmp = false;
                    mInsertFileIcon.setVisibility(View.INVISIBLE);
                    // we sould 
                    File[] tmp_files = SdbApplication.getInstance().getTempFiles();
                    if (tmp_files != null) {
                        for (int i = 0; i < tmp_files.length; i++) {
                            tmp_files[i].delete();
                        }
                    }
                }
                
                doc.getPreview(this);

            }
            break;

            case RequestConstants.REQUEST_TYPE_DOCUMENT_DOWNLOAD: {
                String file_name = resultData.getString(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                Toast.makeText(getActivity(), file_name + " saved", Toast.LENGTH_LONG).show();
            }
            break;


        }

        refreshListView();
    }

    /*
     *  updateData cached list
    */
    private boolean updateItem(ArrayList<? super SpacedItem> itemList, Item item) {


        String itemType = item.getType();

        for (int i = 0; i < itemList.size(); i++) {
            if (((SpacedItem) itemList.get(i)).getId().equals(item.getId())) {
                if (itemType.equals(Constants.FOLDER_TYPE))
                    itemList.set(i, (Folder) item);
                else if (itemType.equals(Constants.DOCUMENT_TYPE))
                    itemList.set(i, (Document) item);

                return true;
            }
        }

        return false;

    }

    /*
    *  remove item createInstance list
    */
    private void removeItem(ArrayList<? super SpacedItem> itemList, Item item) {
        for (int i = 0; i < itemList.size(); i++)
            if (((SpacedItem) itemList.get(i)).getId().equals(item.getId())) {
                itemList.remove(i);
                return;
            }

    }


    private Document getDocumentByName(ArrayList<? super SpacedItem> itemList, String itemName) {
        for (Object anItemList : itemList) {
            if (((SpacedItem) anItemList).getName().equals(itemName)
                    && ((SpacedItem) anItemList).getType().equals(Constants.DOCUMENT_TYPE)) {
                return ((Document) anItemList);
            }

        }
        return null;
    }


    private String isFileNameInList(ArrayList<? extends Item> itemList, String filePath) {
        for (Object anItemList : itemList) {
            String fileName = ((SpacedItem) anItemList).getName();
            if (filePath.contains(fileName)) {

                return fileName;
            }

        }
        return "";
    }

    private Document getCurrentDocFromListView(ArrayList<? super SpacedItem> itemList, String id) {
        for (Object anItemList : itemList) {
            if (((SpacedItem) anItemList).getId().equals(id)) {
                return (Document) anItemList;
            }
        }
        return null;
    }


    private int getItemPosition(ArrayList<? super SpacedItem> itemList, String id) {
        for (int i = 0; i < itemList.size(); i++) {
            if (((SpacedItem) itemList.get(i)).getId().equals(id)) {
                return i;
            }

        }
        return -1;
    }

    private void showErrorDialog(String message) {
        new ErrorDialogFragmentBuilder(getActivity())
                .setTitle(R.string.dialog_data_error_title).setMessage(message).show();
        return;
    }

    @Override
    public void onRequestConnectionError(Request request, int statusCode) {

        cancelLoadingDialog();
        int requestType = request.getRequestType();
        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " statusCode = "
                + statusCode);
        showServiceErrorDialog(getResources().getString(string.dialog_connection_error_message));


    }

    @Override
    public void onRequestDataError(Request request) {
       
        cancelLoadingDialog();
        int requestType = request.getRequestType();
        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " statusCode = ");

        showServiceErrorDialog(getResources().getString(R.string.dialog_data_error_message));
    }

    @Override
    public void onRequestServiceError(Request request, Bundle resultData) {
        cancelLoadingDialog();
        int requestType = request.getRequestType();

        int error_code = resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);
        
        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " error_type = "
                + error_code);

        switch (error_code){
            case ResponseConstants.RESPONSE_CODE_UNAUTHORIZED:
                showLoadingDialog();
                SdbApplication.getInstance().updateAccessToken();
                return;
        }
        
        ErrorMessage errMsg = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE);

        showServiceErrorDialog(errMsg.getLocalizedMessage());
    }

    private void showServiceErrorDialog(String message) {

        Context context = getActivity();
        if (context == null) return;

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(string.dialog_service_error_title);
        builder.setMessage(message);

        builder.setPositiveButton(string.dialog_error_button_close_txt, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do do my action here

                dialog.dismiss();
            }

        });
        /*
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // I do not need any action here you might
                dialog.dismiss();
            }
        });
        */
        AlertDialog alert = builder.create();
        alert.show();

    }

    private void showReplaceFileDialog(final String docName, final String filePath) {

        Context context = getActivity();
        if (context == null) return;

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(string.dialog_create_new_file_or_replace);
        builder.setMessage("Do you want to replace document " + docName + " or create new once ?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {

                showLoadingDialog();
                Document doc = getDocumentByName(mItemList, docName);
                doc.create(filePath, mListener);

                dialog.dismiss();
            }

        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showLoadingDialog();
                String parentId = ((Item) navItemList.get(navSpinner.getSelectedItemPosition()).get(ACTION_DROPDOWN_ITEM)).getId();
                Document doc = new Document();
                doc.setParentId(parentId);
                doc.create(filePath, mListener);

                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();

    }


    public void showLoadingDialog() {

        /*
        if (progressDialog != null) {
            progressDialog.cancel();
            progressDialog = null;
        }

        progressDialog = new ProgressDialog(this.getActivity());

        progressDialog.setTitle(getResources().getString(string.progress_dialog_title));
        progressDialog.setMessage(getResources().getString(string.progress_dialog_message));
        progressDialog.setCancelable(true);
        progressDialog.setIndeterminate(true);

        progressDialog.show();
        */

        
        if (getActivity() != null)
           getActivity().getActionBar().getCustomView().findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
    }

    public void cancelLoadingDialog() {
    /*
        if (progressDialog != null) {
            progressDialog.cancel();
            progressDialog = null;
        }
    */
        if (getActivity() != null) {
            
        View view = getActivity().getActionBar().getCustomView();
        if (view != null)
            view.findViewById(R.id.progressBar).setVisibility(View.GONE);
        }
    }


    public void uploadFile(String filePath) {

        Log.v(TAG, Utils.getMethodName() + "entry ");

        //bc
        //if (mLlBreadcrumb == null || mLlBreadcrumb.getCurrentLocationId() == null)
        //    return;

        String fileName = isFileNameInList(mItemList, filePath);
        if (fileName.length() > 1) {
            showReplaceFileDialog(fileName, filePath);
        } 
        else  {
            String parentId = ((Item) navItemList.get(navSpinner.getSelectedItemPosition()).get(ACTION_DROPDOWN_ITEM)).getId();
            showLoadingDialog();
            Document doc = new Document();
            doc.setParentId(parentId);
            doc.create(filePath, this);
        }


    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        String prefix = charSequence.toString();
        Log.v(TAG, Utils.getMethodName() + "prefix " + prefix);
        if(mCurrentParentPosition < 0 || navItemList.size() < 1) return;
            Item item = (Item)  navItemList.get(mCurrentParentPosition).get(ACTION_DROPDOWN_ITEM);

        String parentId = item.getId();
        if(parentId == null) return;
        
        ArrayList<? extends SpacedItem> currentList = SdbApplication.getInstance().getLocalChildrenByParentId(parentId);

        boolean isShowRecycled = SdbApplication.getInstance().isShowRecycledItems();
        mItemList.clear();
        for (SpacedItem aCurrentList : currentList) {

            String itemName = aCurrentList.getName().toLowerCase();
            String type = aCurrentList.getType();
            if (itemName.contains(prefix.toLowerCase())) {
         
                mItemList.add(aCurrentList);
            }
        }
        
        // removing recycled items if we dont want to show
 
        for (int y = 0; y < mItemList.size(); y++) {
      
            String type = ((SpacedItem) mItemList.get(y)).getType();

                if (type.equals(Constants.DOCUMENT_TYPE)){
                    Document doc = (Document)  mItemList.get(y);
                    if (!isShowRecycled && doc.isRecycled())
                        mItemList.remove(y);

                }
                else if (type.equals(Constants.FOLDER_TYPE)){
                    Folder folder = (Folder) mItemList.get(y);
                    if (!isShowRecycled && folder.isRecycled())
                        mItemList.remove(y);

                }
            
        }
        
        mListAdapter.notifyDataSetChanged();
    }

    @Override
    public void afterTextChanged(Editable editable) {
        Log.v(TAG, Utils.getMethodName() + "entry");
    }
}
