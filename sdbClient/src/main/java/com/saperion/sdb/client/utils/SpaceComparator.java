package com.saperion.sdb.client.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

import com.saperion.sdb.sdk.models.Space;

public class SpaceComparator implements Comparator<Space> {

    private static final String TAG = SpaceComparator.class.getSimpleName();

    @Override
    public int compare(Space lhs, Space rhs) {

        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        Date dateLhs = null;
        Date dateRhs = null;
        try {
            dateLhs = fmt.parse(lhs.getLastModified());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            dateRhs = fmt.parse(rhs.getLastModified());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int i = 0;

        if (lhs.isFavorit() && rhs.isFavorit()) {
            if (dateLhs.getTime() < dateRhs.getTime())
                i = 1;
            else
                i = -1;
        } else if (lhs.isFavorit()) {
            i = -1;
        } else if (rhs.isFavorit()) {
            i = 1;
        } else {
            if (dateLhs.getTime() < dateRhs.getTime())
                i = 1;
            else
                i = -1;
        }

        return i;

    }

}
