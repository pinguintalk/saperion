package com.saperion.sdb.client;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.saperion.sdb.client.R.id;
import com.saperion.sdb.client.R.layout;
import com.saperion.sdb.client.utils.Log;
import com.saperion.sdb.sdk.models.Comment;
import com.saperion.sdb.sdk.models.Document;
import com.saperion.sdb.sdk.models.Space;
import com.saperion.sdb.sdk.models.Version;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.RequestConstants;
import com.saperion.sdb.sdk.requestmanager.RequestManager.RequestListener;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.utils.Utils;

import java.util.ArrayList;

/**
 * Created by nnn on 31.07.13.
 */
public class DocumentInfoFragment extends SdbBasicFragment implements DocumentInfoListViewInterface, RequestListener {

    private static final String TAG = DocumentInfoFragment.class.getSimpleName();

    private Context mContext;
    private Document document;

    private EditText commentEditText;
    private RequestListener mListener;
    private DocumentInfoActivityInterface activityCallbacks = sDummyCallbacks;


    private Drawable btn_not_selected;
    private Drawable btn_selected;

    private Button buttonComments;
    private Button buttonVersion;
    private Button buttonActivity;
    private ArrayList<Comment> docCommentList = new ArrayList<Comment>();
    private ArrayList<Version> docVersionList = new ArrayList<Version>();
    private ArrayList<com.saperion.sdb.sdk.models.Activity> docActivitiesList = new ArrayList<com.saperion.sdb.sdk.models.Activity>();
    private DocumentVersionListAdapter versionListAdapter;
    private DocumentActivityListAdapter activityListAdapter;
    private DocumentCommentListAdapter commentListAdapter;
    private ListView mListView;
    private String currentListType = LIST_TYPE.ACTIVITY.toString();
    public enum LIST_TYPE {
        COMMENT,
        VERSION,
        ACTIVITY;
    }

    
    public DocumentInfoFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        Log.v(TAG, Utils.getMethodName() + "entry");
        mContext = getActivity();
        mListener = this;

       
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.v(TAG, Utils.getMethodName() + "orientation ch  = " + newConfig.orientation);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

        }
    }

    @Override
    public void setRetainInstance(boolean value) {
        super.setRetainInstance(value);

        Log.v(TAG, Utils.getMethodName() + "entry");

    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (activity == null) return;

        Log.v(TAG, Utils.getMethodName() + "entry ");
        try {
            activityCallbacks = (DocumentInfoActivityInterface) activity;

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement DocumentInfoActivityInterface");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(layout.document_info_fragment, container, false);

        Log.v(TAG, Utils.getMethodName() + "entry");

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        Log.v(TAG, Utils.getMethodName() + "entry");

        if (view != null) {
            mListView = (ListView) view.findViewById(id.document_info_list_view);
        }

        if (mListView == null)
            return;

        btn_selected = getActivity().getResources().getDrawable(R.drawable.fns_orange_shape_selected);
        btn_not_selected = getActivity().getResources().getDrawable(R.drawable.fns_btn_not_selected);
        
        activityListAdapter = new DocumentActivityListAdapter(mContext, this, docActivitiesList);
        versionListAdapter = new DocumentVersionListAdapter(mContext, this, docVersionList);
        commentListAdapter = new DocumentCommentListAdapter(mContext, this, docCommentList);

        mListView.setAdapter(commentListAdapter);

        mListView.setAnimationCacheEnabled(false);
        mListView.setScrollingCacheEnabled(false);
        mListView.setClickable(false);
        mListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {

                if ( currentListType.equals(LIST_TYPE.VERSION.toString())){
                Version selectedVersion = versionListAdapter.getItem(position);

                activityCallbacks.onVersionClicked(selectedVersion);

                }

            }
        });

        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> av, View v, int position, long id) {
                if ( currentListType.equals(LIST_TYPE.COMMENT.toString())){
                    Comment comment = commentListAdapter.getItem(position);
        
                    showContextMenu(v,comment);

                }

                return true;
            }
        }); 

        /*
        this.document = ContentProviderHelpers.documentGetById(getActivity().getApplicationContext(),
                "prmwayYADE-wGMSgGQbbevOULg4u7_Evpsict-wjioQePHpOGc3dS-xF3fbyl78LVeUrdYOBUVGu5cP_k96Dtw");
                
        */


        buttonComments = (Button) view.findViewById(id.b_doc_comment);

        buttonComments.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.v(TAG, Utils.getMethodName() + "buttonComments clicked ");
                setButtonsBackGroundColorToDefault();
                setButtonSelected(buttonComments);
                mListView.setClickable(true);
                // set new adapterEmailList
                mListView.setAdapter(commentListAdapter);
                currentListType = LIST_TYPE.COMMENT.toString();
                
                if (document != null) {
                    Log.v(TAG, Utils.getMethodName() + "request get comments list of " + document.getName());

                    showLoadingDialog();
                    document.getComments(mListener);
                }
            }
        });

        buttonVersion = (Button) view.findViewById(id.b_doc_version);
        buttonVersion.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.v(TAG, Utils.getMethodName() + "buttonVersion clicked ");
                setButtonsBackGroundColorToDefault();
                setButtonSelected(buttonVersion);
                mListView.setClickable(true);
                // set new adapterEmailList
                mListView.setAdapter(versionListAdapter);
                currentListType = LIST_TYPE.VERSION.toString();
                if (document != null) {
                    Log.v(TAG, Utils.getMethodName() + "request get version list of " + document.getName());

                    showLoadingDialog();
                    document.getVersions(mListener);
                }
            }
        });

        buttonActivity = (Button) view.findViewById(id.b_doc_activity);
        buttonActivity.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.v(TAG, Utils.getMethodName() + "buttonActivity clicked ");
                setButtonsBackGroundColorToDefault();
                setButtonSelected(buttonActivity);
                mListView.setClickable(false);
                // set new adapterEmailList
                mListView.setAdapter(activityListAdapter);
                currentListType = LIST_TYPE.ACTIVITY.toString();
                if (document != null) {
                    Log.v(TAG, Utils.getMethodName() + "request get activitiy list of " + document.getName());

                    showLoadingDialog();
                    document.getActivities(mListener);
                }
            }
        });

        setButtonsBackGroundColorToDefault();
        setButtonSelected(buttonComments);

        Bundle bundle = getArguments();
        if (bundle == null) {
            Log.e(TAG, Utils.getMethodName() + "bundle = null ");
            return;
        }
        this.document = bundle.getParcelable(DocumentFragmentActivity.PARAM_DOCUMENT);

        // this.document = activityCallbacks.getCurrentDocument();

        if (this.document != null) {
            Log.v(TAG, Utils.getMethodName() + "request get comments list of " + document.getName());

            // get file of current version
            // document.getRenditionVersioned(this, document.getVersionNumber());

            showLoadingDialog();
            document.getComments(this);
        }

        currentListType = LIST_TYPE.COMMENT.toString();
    }

    private void showContextMenu(final View v,final Comment comment) {

        PopupMenu popupMenu = new PopupMenu(getActivity(), v);
        popupMenu.getMenuInflater().inflate(R.menu.comment_edit_contextmenu, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {

                if (item.getItemId() == R.id.edit_menu_item) {
                    commentListAdapter.edit(v,comment);
                } else if (item.getItemId() == R.id.remove_menu_item) {
                    showLoadingDialog();
                    comment.delete(mListener); 
                }
                    

                return true;
            }
        });

        popupMenu.show();
    }
    
    private void setButtonSelected(Button button){
        button.setBackgroundDrawable(btn_selected);
        button.setTextColor(Color.WHITE);
    }

    
    private void setButtonsBackGroundColorToDefault(){

        //Button buttonComments = (Button) getActivity().findViewById(id.b_doc_comment);
        //Button buttonVersion = (Button) getActivity().findViewById(id.b_doc_version);
       // Button buttonActivity = (Button) getActivity().findViewById(id.b_doc_activity);
        
        if (buttonVersion != null){
            buttonVersion.setBackgroundDrawable(btn_not_selected);
            buttonVersion.setTextColor(Color.BLACK);
        }
        if (buttonActivity != null) {
            buttonActivity.setBackgroundDrawable(btn_not_selected);
            buttonActivity.setTextColor(Color.BLACK);
        }
        if (buttonComments != null){
            buttonComments.setBackgroundDrawable(btn_not_selected);
            buttonComments.setTextColor(Color.BLACK);
        }
    }
    
    /*
    *  updateData cached list
    */
    private void updateItem(ArrayList<Space> itemList, Space item) {
        for (int i = 0; i < itemList.size(); i++) {
            if ((itemList.get(i)).getId().equals(item.getId())) {
                itemList.set(i, item);

                return;
            }

        }

    }

    /*
    *  remove item createInstance list
    */
    private void removeItem(ArrayList<Space> itemList, Space item) {
        for (int i = 0; i < itemList.size(); i++) {

            if ((itemList.get(i)).getId().equals(item.getId())) {
                itemList.remove(i);
                return;
            }

        }

    }

    public void clickHandler(View view) {

        switch (view.getId()) {
            case id.b_doc_comment:
                Log.v(TAG, com.saperion.sdb.client.utils.Utils.getMethodName() + "b_doc_comment");

                break;
            case id.b_doc_version:

                Log.v(TAG, com.saperion.sdb.client.utils.Utils.getMethodName() + "b_doc_version");

                break;
            case id.b_doc_activity:
                Log.v(TAG, com.saperion.sdb.client.utils.Utils.getMethodName() + "b_doc_activity");

                break;
        }
    }

    @Override
    public void onRequestFinished(Request request, Bundle resultData) {

        if (getActivity() == null)
            return;

        int result_code = resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);
        int requestType = request.getRequestType();
        String errorMessage = resultData.getString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE);

        cancelLoadingDialog();

        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " resultCode = "
                + result_code);

        switch (result_code) {

            case ResponseConstants.RESPONSE_CODE_OK:
            case ResponseConstants.RESPONSE_CODE_CREATED:
                break;
            case ResponseConstants.RESPONSE_CODE_UNKOWN_ERROR:
            case ResponseConstants.RESPONSE_CODE_BAD_REQUEST:
            case ResponseConstants.RESPONSE_CODE_ERROR:
            case ResponseConstants.RESPONSE_CODE_FORBIDDEN:
            case ResponseConstants.RESPONSE_CODE_INTERNAL_SERVER_ERROR:
            case ResponseConstants.RESPONSE_CODE_INTERNAL_SERVICE_UNAVAILABLE:
            case ResponseConstants.RESPONSE_CODE_METHOD_NOT_ALLOWED:
            case ResponseConstants.RESPONSE_CODE_NOT_FOUND:
            case ResponseConstants.RESPONSE_CODE_UNAUTHORIZED:
            case ResponseConstants.RESPONSE_CODE_PRECONDITION_FAILED:
            case ResponseConstants.RESPONSE_CODE_TOO_MANY_REQUESTS:
                showErrorDialog(mContext, errorMessage);
                return;
            default: {
                showErrorDialog(mContext, "unbekannter Fehler");
                return;
            }
        }

        switch (requestType) {

            case RequestConstants.REQUEST_TYPE_DOCUMENT_GET_VERSIONS:
                if (currentListType.equals(LIST_TYPE.VERSION.toString())){
                
                docVersionList.clear();
                this.docVersionList =
                        resultData.getParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);

                versionListAdapter.clear();
                versionListAdapter.addAll(docVersionList);
                versionListAdapter.notifyDataSetChanged();
                }


                break;
            case RequestConstants.REQUEST_TYPE_DOCUMENT_GET_ACTIVITIES:

                if (currentListType.equals(LIST_TYPE.ACTIVITY.toString())){

                    docActivitiesList.clear();
                this.docActivitiesList =
                        resultData.getParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);

                Log.d(TAG, Utils.getMethodName() + " docActivitiesList.toString() = " + docActivitiesList.toString());
                activityListAdapter.clear();
                activityListAdapter.addAll(docActivitiesList);
                activityListAdapter.notifyDataSetChanged();
                }

                break;
            case RequestConstants.REQUEST_TYPE_DOCUMENT_GET_COMMENTS:

                if (currentListType.equals(LIST_TYPE.COMMENT.toString())){
                    
                    commentListAdapter.clear();
                    docCommentList.clear();
                    docCommentList =
                            resultData.getParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);

                    Log.d(TAG, Utils.getMethodName() + " docCommentList = " + docCommentList.toString());

                    // set empty comment at position 0
                    Comment comment = new Comment();
                    comment.setComment("");
                    comment.setOwnerId("");
                    comment.setId("");
                    comment.setCreationDate("");
                    commentListAdapter.insert(comment, 0);

                    commentListAdapter.addAll(docCommentList);
                    mListView.scrollTo(0, 0);
                    commentListAdapter.notifyDataSetChanged();

                }

                break;

            case RequestConstants.REQUEST_TYPE_COMMENT_ADD:
            case RequestConstants.REQUEST_TYPE_COMMENT_DELETE:
            case RequestConstants.REQUEST_TYPE_COMMENT_UPDATE:
            case RequestConstants.REQUEST_TYPE_DOCUMENT_ADD_COMMENT:
                Log.d(TAG, Utils.getMethodName() + " REQUEST_TYPE_DOCUMENT_ADD_COMMENT = ");

                commentListAdapter.setCurrEditedComment(null);
                if (currentListType.equals(LIST_TYPE.COMMENT.toString())){
                    showLoadingDialog();
                    // send new request to refresh comment list
                    document.getComments(this);                
                }

                break;

            case RequestConstants.REQUEST_TYPE_DOCUMENT_GET_FILE:
            case RequestConstants.REQUEST_TYPE_DOCUMENT_GET_RENDITION_BY_VERSION:
                Document doc = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                if (doc != null) {

                    // Uri.parse(doc.getFileAbsPath());

                    Uri fileUri = doc.getFileContentUri(mContext);
                    Log.v(TAG, Utils.getMethodName() + "opening " + fileUri.toString());

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(fileUri, "application/pdf");
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    try {
                        startActivity(intent);
                    } catch (Exception e) {
                        showErrorDialog(mContext, "Could not show file");
                        Log.e("error", "" + e);
                    }

                }
                break;
            case RequestConstants.REQUEST_TYPE_DOCUMENT_RESTORE_VERSION:

                Document docu = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                if (docu != null) {

                    Toast.makeText(getActivity(), "document setted to version "
                            + docu.getVersionNumber(), Toast.LENGTH_LONG).show();

                    showLoadingDialog();
                    document.getVersions(this);
                }
                break;

            default:
                break;

        }

    }

    @Override
    public void onRequestConnectionError(Request request, int statusCode) {

    }

    @Override
    public void onRequestDataError(Request request) {

    }

    @Override
    public void onRequestServiceError(Request request, Bundle resultData) {

    }

    @Override
    public void onCommentAdded(String comment) {

        showLoadingDialog();
        document.addComments(comment,this);
    }

    @Override
    public void onVersionClicked(Document document) {

    }

    @Override
    public void onCommentUpdate(Comment currEditedComment) {
        showLoadingDialog();
        currEditedComment.update(this);
    }


    public void showDocVersions(Document doc) {

    }


    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface DocumentInfoActivityInterface {
        /**
         * Callback for when an item has been selected.
         */

        public Document getCurrentDocument();

        public void onVersionClicked(Version version);
    }

    /**
     * A dummy implementation of the {@link DocumentInfoActivityInterface} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private static DocumentInfoActivityInterface sDummyCallbacks = new DocumentInfoActivityInterface() {
        @Override
        public Document getCurrentDocument() {
            return null;
        }

        @Override
        public void onVersionClicked(Version version) {

        }


    };
}
