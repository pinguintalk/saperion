package com.saperion.sdb.client;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.saperion.sdb.client.PullDownListView.ListViewTouchEventListener;
import com.saperion.sdb.client.R.string;
import com.saperion.sdb.client.dialogs.ProgressDialogFragment;
import com.saperion.sdb.client.utils.Log;
import com.saperion.sdb.client.utils.Utils;
import com.saperion.sdb.sdk.SdbApplication;
import com.saperion.sdb.sdk.models.Space;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.RequestConstants;
import com.saperion.sdb.sdk.requestmanager.RequestManager.RequestListener;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.states.SpaceState;

import java.util.ArrayList;



/**
 * A fragment representing a single Item detail screen. This fragment is either
 * contained in a {@link MainFragmentActivity} in two-pane mode (on tablets) or a
 * {@link ItemListActivity} on handsets.
 */
public class SpacesFragment extends Fragment implements SpaceListViewInterface, RequestListener, ListViewTouchEventListener, TextWatcher {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */

    private static final String TAG = SpacesFragment.class.getSimpleName();

    public static final String SELECTED_ITEM = "SELECTED_SPACE";
    private Context mContext;


    private ImageButton mIBCreateSpace;
 //   private ListView mListView;
    private PullDownListView mListView;
    private SpaceListAdapter mAdapter;
    private ProgressDialog progressDialog = null;



    private Space mSelectedSpace = null;
    private RequestListener mListener;
    private final static ArrayList<Space> spaceList = new ArrayList<Space>();

    private ProgressBar listViewProgressbar;
    private ImageView listview_refresh_up;
    private EditText filterTextField;
    private int selectedPos = -1;
    /**
     * The fragment's current callback object, which is notified of list item
     * clicks or contents are modified
     */

    private Callbacks mCallbacks = sDummyCallbacks;

    @Override
    public void onListViewPullingDown(float distance) {
        
        Log.i(TAG, Utils.getMethodName() + "distance " + distance + "   " + listview_refresh_up.getHeight());

    /*
        
        TranslateAnimation animate = new TranslateAnimation(0,0,0,listview_refresh_up.getHeight()/PullDownListView.maxDiff*distance);
        animate.setDuration(0);

        animate.setFillAfter(true);
        listview_refresh_up.startAnimation(animate);
        //listview_refresh_up.setVisibility(View.VISIBLE);
        if(distance >= PullDownListView.maxDiff)
        {
            listview_refresh_up.setVisibility(View.GONE);
            listview_release_to_refresh.setVisibility(View.VISIBLE);
        }
    */
 
        FrameLayout.LayoutParams  lp = new   FrameLayout.LayoutParams( FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        
        if(distance < listview_refresh_up.getHeight())
        {
            lp.setMargins(0, (int) (distance), 0, 0);
            mListView.setLayoutParams(lp);
        }
        else if(distance >= PullDownListView.maxDiff)
        {
            listview_refresh_up.setVisibility(View.VISIBLE);

            lp.setMargins(0, listview_refresh_up.getHeight() + 2,0,0);
            mListView.setLayoutParams(lp);
        }        
    }


    public Space getmSelectedSpace() {
        return mSelectedSpace;
    }

    public void setmSelectedSpace(Space mSelectedSpace) {
        this.mSelectedSpace = mSelectedSpace;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.v(TAG, com.saperion.sdb.client.utils.Utils.getMethodName() + "entry");

        getSpaces();
        refreshListView();
    }
    
    
    @Override
    public void onListViewPulledDown() {
        Log.i(TAG, Utils.getMethodName() + "entry");

        listview_refresh_up.setVisibility(View.VISIBLE);

    }


    @Override
    public void onListViewTouchUp(float distance) {
        Log.i(TAG, Utils.getMethodName() + "entry");


        if (distance > PullDownListView.maxDiff){

            listview_refresh_up.setVisibility(View.GONE);
            listViewProgressbar.setVisibility(View.VISIBLE);
            SdbApplication.getInstance().getSpaces(mListener);
        }
        FrameLayout.LayoutParams  lp = new   FrameLayout.LayoutParams( FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(0,0,0,0);
        mListView.setLayoutParams(lp);
    }

    @Override
    public void onListViewResetPosition() {
        Log.i(TAG, Utils.getMethodName() + "entry");

        listview_refresh_up.setVisibility(View.GONE);
        listViewProgressbar.setVisibility(View.GONE);
        
        FrameLayout.LayoutParams  lp = new   FrameLayout.LayoutParams( FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(0,0,0,0);
        mListView.setLayoutParams(lp);

        listview_refresh_up.setVisibility(View.VISIBLE);


    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        String prefix = charSequence.toString();
        Log.v(TAG, Utils.getMethodName() + "prefix " + prefix);
        

        ArrayList<Space> currentList = SdbApplication.getInstance().getLocalSpaces();

        spaceList.clear();
        for (Space aCurrentList : currentList) {
            String spaceName = aCurrentList.getName().toLowerCase();
            if (spaceName.contains(prefix.toLowerCase())) {
                spaceList.add(aCurrentList);

            }
        }

        boolean isShowRecycled = SdbApplication.getInstance().isShowRecycledItems();
        // removing recycled items if we dont want to show

        for (int y = 0; y < spaceList.size(); y++) {

                if (!isShowRecycled && spaceList.get(y).isRecycled())
                    spaceList.remove(y);
        }
        
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void afterTextChanged(Editable editable) {
    }


    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface Callbacks {
        /**
         * Callback for when an item has been selected.
         */
    
        public void OnNameOrIconClickedEvent(Space space);

        public void onItemClickedEvent(Space space);

        public void onSpaceUpdate(Space space);

        public void onSpaceDelete(Space space);
    }

    /**
     * A dummy implementation of the {@link Callbacks} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private static Callbacks sDummyCallbacks = new Callbacks() {
        @Override
        public void onItemClickedEvent(Space space) {
        }

        @Override
        public void OnNameOrIconClickedEvent(Space space) {
            Log.v(TAG, Utils.getMethodName() + "sDummyCallbacks");
        }

        @Override
        public void onSpaceUpdate(Space space) {

        }

        @Override
        public void onSpaceDelete(Space space) {

        }
    };

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SpacesFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        Log.v(TAG, Utils.getMethodName() + "entry");
        mContext = getActivity();
        mListener = this;

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.v(TAG, Utils.getMethodName() + "orientation ch  = " + newConfig.orientation);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

        }
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException(
                    "MainActivity and ItemFragment must implement fragment's callbacks.");
        }

        Log.v(TAG, Utils.getMethodName() + "entry");

        mCallbacks = (Callbacks) activity;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Log.v(TAG, Utils.getMethodName() + "entry");
        View view = inflater.inflate(R.layout.spaces_fragment, container, false);
        
        if (savedInstanceState != null){
            selectedPos = savedInstanceState.getInt(SELECTED_ITEM);
        }
        
        return view;

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.v(TAG, Utils.getMethodName() + "entry");
        // Save the current  selection in case we need to recreate the fragment
        outState.putInt(SELECTED_ITEM, selectedPos);

    }
    
    
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view,savedInstanceState);
        
        Log.v(TAG, Utils.getMethodName() + "entry");
        
        if (view != null) {
            mListView = (PullDownListView) view.findViewById(R.id.space_list_view);
        }

        if (mListView == null)
            return;

        listViewProgressbar = (ProgressBar) view.findViewById(R.id.list_view_progressbar);
        listview_refresh_up = (ImageView) view.findViewById(R.id.listview_refresh_up);

        mListView.setListViewTouchListener(this);
        mListView.setPulledDown(true);
        /*
        ArrayList<Space> spaceList = new ArrayList<Space>(SdbApplication.getInstance().getLocalSpaces());

        spaceList.clear();

        if (!SdbApplication.getInstance().isShowRecycledItems()){

            for (Space anItemList : spaceList) {

                if (!anItemList.isRecycled()) {
                    spaceList.add(anItemList);
                }

            }
        }
        else
            spaceList.addAll(spaceList);
        Collections.sort(spaceList);
        */
        getSpaces();
        
        mAdapter = new SpaceListAdapter(mContext, this, spaceList);
        mListView.setAdapter(mAdapter);
        refreshListView();
        filterTextField = (EditText) view.findViewById(R.id.space_filter_text_field);
        filterTextField.addTextChangedListener(this);
        
        mIBCreateSpace = (ImageButton) view.findViewById(R.id.b_create_space);
        mIBCreateSpace.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                boolean is_empty_space_opened = false;
                Log.v(TAG, Utils.getMethodName() + "mIBCreateSpace clicked ");

                if (mAdapter.getCount() > 0) {
                    if (mAdapter.getItem(0).getName().equals("")) { // there no empty space for creating opened
                        is_empty_space_opened = true;
                    }
                }

				if (!is_empty_space_opened) {
					// create empty element
					Space space = new Space();
					space.setName("");
					space.setId("");
					space.setTags(null);
                    mSelectedSpace = space;
					// set selected to empty item once
					mAdapter.setmCurrentSelectedItemId("");
                   
					mAdapter.insert(space, 0);
					mListView.scrollTo(0, 0);
					mAdapter.notifyDataSetChanged();
                    mCallbacks.onItemClickedEvent(null);
                    mAdapter.notifyDataSetChanged();
				}

            }
        });

        mListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {

                Log.v(TAG, Utils.getMethodName() + "  entry");

                mSelectedSpace = mAdapter.getItem(position);

                Space tmp_space_may_to_delete = mAdapter.getItem(0);

                if (tmp_space_may_to_delete.getName().equals("")) { // remove empty item first, if create process canceled
                    mAdapter.remove(tmp_space_may_to_delete);
                }
                Log.v(TAG, Utils.getMethodName() + " selected_space:" + mSelectedSpace.getName());
                // show expanded space cell
                mAdapter.setmCurrentSelectedItemId(mSelectedSpace.getId());
                mAdapter.setCurrSelectedItemPosition(position);
                mAdapter.setInEditMode(false);
                mAdapter.notifyDataSetChanged();
                
                // Notify the active callbacks interface (the ItemListFragment, if the
                // fragment is attached to one) that an space item has been selected.    

                if (mCallbacks != null)
                        mCallbacks.onItemClickedEvent(mSelectedSpace);

            }
        });

     
    }


   

    public void refreshListView() {
        Log.v(TAG, Utils.getMethodName() + "entry ");
        
        if (mSelectedSpace != null)
        {           
            mAdapter.setmCurrentSelectedItemId(mSelectedSpace.getId());
        } else {
            // there are now selected space saved before 
            // the fragment starts at first time 
            mAdapter.setmCurrentSelectedItemId("");           
        }
        /*
        if (!SdbApplication.getInstance().isShowRecycledItems()){

            for (Space anItemList : spaceList) {

                if (anItemList.isRecycled()) {
                    mAdapter.remove(anItemList);
                }

            }
        }
        */
        mAdapter.notifyDataSetChanged();
    }

    public void getSpaces() {
        Log.v(TAG, Utils.getMethodName() + " entry");
        
        // get cached spaces if exist then request  spaces createInstance server
        Utils.showLoadingDialog(getActivity());
        ArrayList<Space> tmplist = SdbApplication.getInstance().getSpaces(this);
        if (tmplist !=null) {
            spaceList.clear();
            if (SdbApplication.getInstance().isShowRecycledItems()) {
                spaceList.addAll(tmplist);
            }
            else {
                for (Space space: tmplist) {
                    if (!space.isRecycled())
                        spaceList.add(space);
                }
            }
           
        }
        
        
        
    }
    


    
    @Override
    public void onRequestFinished(Request request, Bundle resultData) {

        int resp_code = resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);

        String errorMessage = resultData.getString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE);

        int requestType = request.getRequestType();
        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " resultCode = "
                + resp_code);

        Utils.cancelLoadingDialog(getActivity());

        switch (resp_code) {
            case 0:
            case ResponseConstants.RESPONSE_CODE_OK:
            case ResponseConstants.RESPONSE_CODE_CREATED:
                break;

            case ResponseConstants.RESPONSE_CODE_BAD_REQUEST:
            case ResponseConstants.RESPONSE_CODE_ERROR:
            case ResponseConstants.RESPONSE_CODE_FORBIDDEN:
            case ResponseConstants.RESPONSE_CODE_INTERNAL_SERVER_ERROR:
            case ResponseConstants.RESPONSE_CODE_INTERNAL_SERVICE_UNAVAILABLE:
            case ResponseConstants.RESPONSE_CODE_METHOD_NOT_ALLOWED:
            case ResponseConstants.RESPONSE_CODE_NOT_FOUND:
                return;
            case ResponseConstants.RESPONSE_CODE_PRECONDITION_FAILED:
            case ResponseConstants.RESPONSE_CODE_TOO_MANY_REQUESTS:
                showServiceErrorDialog(errorMessage);
                return;

            default: {
                showServiceErrorDialog("unbekannter Fehler");
                return;
            }
        }

        switch (requestType) {
            case RequestConstants.REQUEST_TYPE_SPACE_CREATE:

                Space space = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                Log.v(TAG, Utils.getMethodName() + " space = " + space.toString());
                mSelectedSpace = space;
                // set selection to new created or updated Item
                mAdapter.setmCurrentSelectedItemId(space.getId());
                spaceList.add(space);
                //Collections.sort(spaceList);

               
                // Notify the active callbacks interface (the ItemListFragment, if the
                // fragment is attached to one) that an space item has been selected.    
                mCallbacks.onItemClickedEvent(mSelectedSpace);

                break;

            case RequestConstants.REQUEST_TYPE_SPACE_DELETE:

                removeItem(spaceList, this.mSelectedSpace);
                // informs itemlistfragment
                mCallbacks.onSpaceDelete(this.mSelectedSpace);

                break;
            case RequestConstants.REQUEST_TYPE_SPACE_GET_LIST:

                listview_refresh_up.setVisibility(View.GONE);
                listViewProgressbar.setVisibility(View.GONE);
                FrameLayout.LayoutParams  lp = new   FrameLayout.LayoutParams( FrameLayout.LayoutParams.WRAP_CONTENT,
                        FrameLayout.LayoutParams.WRAP_CONTENT);
                lp.setMargins(-6,0,0,0);
                mListView.setLayoutParams(lp);


                ArrayList<Space> spaceList =
                        resultData.getParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);

                SpacesFragment.spaceList.clear();


                if (!SdbApplication.getInstance().isShowRecycledItems()){

                    for (Space anItemList : spaceList) {

                        if (!anItemList.isRecycled()) {
                            SpacesFragment.spaceList.add(anItemList);
                        }

                    }
                }
                else
                    SpacesFragment.spaceList.addAll(spaceList);
               // Collections.sort(SpacesFragment.spaceList);
                
                

                break;

            case RequestConstants.REQUEST_TYPE_SPACE_DELETE_ALL:
                SpacesFragment.spaceList.clear();
                break;

            case RequestConstants.REQUEST_TYPE_SPACE_UPDATE:

                Space new_space =
                        resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                
                if (new_space.isRecycled()) {
                    if  (!SdbApplication.getInstance().isShowRecycledItems())
                            removeItem(SpacesFragment.spaceList, new_space);
                } else
                    updateItem(SpacesFragment.spaceList, new_space);

                Log.v(TAG, Utils.getMethodName() + " space updated = " + new_space.getName());

                View cell = mListView.getChildAt(mAdapter.getCurrSelectedItemPosition()
                        - mListView.getFirstVisiblePosition());

                // inform children list if e.g. name changed
                mCallbacks.onSpaceUpdate(new_space);

                break;

        }

        refreshListView();

    }


    @Override
    public void onRequestConnectionError(Request request, int statusCode) {

        ProgressDialogFragment.dismiss(this.getActivity());
        int requestType = request.getRequestType();

        showServiceErrorDialog("Connection error or timeout");

        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " statusCode = "
                + statusCode);
    }

    @Override
    public void onRequestDataError(Request request) {

        Utils.cancelLoadingDialog(getActivity());
        int requestType = request.getRequestType();
        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType);

        showServiceErrorDialog("request or data error");

    }

    @Override
    public void onRequestServiceError(Request request, Bundle resultData) {

        Utils.cancelLoadingDialog(getActivity());
        int resp_code = resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);
        
        switch (resp_code){
        case ResponseConstants.RESPONSE_CODE_UNAUTHORIZED:
            //String errMsg = resultData.getString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE);            
            Toast.makeText(getActivity(), "Authorizing login data ...", Toast.LENGTH_SHORT).show();
           
            return;
        }

    }

    private void showServiceErrorDialog(String message) {

        Context context = getActivity();
        if (context == null) return;

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle("Service Error");
        builder.setMessage(message);

        builder.setPositiveButton(getResources().getString(string.close),
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        // Do do my action here

                        dialog.dismiss();
                    }

                });
        /*
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // I do not need any action here you might
                dialog.dismiss();
            }
        });
        */
        AlertDialog alert = builder.create();
        alert.show();

    }

    @Override
    public void itemTrashClickedEvent(Space space) {

        Utils.showLoadingDialog(getActivity());

        space.addState(SpaceState.RECYCLED);
        space.save(this);
    }

    @Override
    public void itemOkClickedEvent(Space space) {
       
        Log.v(TAG, Utils.getMethodName() + " getId " + space.getId());
        Utils.showLoadingDialog(getActivity());
        space.save(this);

        if (space.getId().equals("")) {
            // we are creating new space
            // so we have to remove empty space once createInstance  mAdapter
            mAdapter.remove(space);
            // and insert it again it if we get this space createInstance server
        }
    }

    @Override
    public void itemFavoritClickEvent(Space space) {
        boolean fav = true;
        if (space.isFavorit()) {
            fav = false;
        }
        Utils.showLoadingDialog(getActivity());
        space.setFavorit(fav, this);
    }

    @Override
    public void itemSyncAppClickedEvent(Space space) {
        Log.v(TAG, Utils.getMethodName() + " getId " + space.getId());
        Utils.showLoadingDialog(getActivity());
        space.switchSyncAppState();
        space.save(this);
    }

    @Override
    public void itemSyncDesktopClickedEvent(Space space) {
        Log.v(TAG, Utils.getMethodName() + " getId " + space.getId());
        Utils.showLoadingDialog(getActivity());
        space.switchSyncDesktopState();
        space.save(this);
    }

    @Override
    public void itemRestoreClickedEvent(Space space) {
        Log.v(TAG, Utils.getMethodName() + " getId " + space.getId());
        Utils.showLoadingDialog(getActivity());
        space.removeState(SpaceState.RECYCLED);
        space.save(this);
    }

    @Override
    public void itemDeleteClickedEvent(Space space) {
        Utils.showLoadingDialog(getActivity());
        space.delete(this);
    }

    @Override
    public void itemOpenShareDialogClickedEvent(Context mContext, Space spaceParcelable) {
        Space space =  spaceParcelable;
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment newFragment = new ShareDialogFragment(this.getActivity(), space);
        newFragment.show(ft, "dialog");
    }

    @Override
    public void itemIconOrNameClickedEvent(Space space) {
        // TODO Auto-generated method stub
        Log.v(TAG, Utils.getMethodName() + " entry");
        if (space == null) return;
        mSelectedSpace = space;

        Space tmp_space_may_to_delete = mAdapter.getItem(0);

        if (tmp_space_may_to_delete.getName().equals("")) { // remove empty item first, if create process canceled
            mAdapter.remove(tmp_space_may_to_delete);
        }
        Log.v(TAG, Utils.getMethodName() + "open space" + mSelectedSpace.getName());
        // show expanded space cell
        mAdapter.setmCurrentSelectedItemId(mSelectedSpace.getId());
        mAdapter.setInEditMode(false);
        mAdapter.notifyDataSetChanged();

        // Notify the active callbacks interface (the ItemListFragment, if the
        // fragment is attached to one) that we have go to this space
        mCallbacks.OnNameOrIconClickedEvent(mSelectedSpace);
    }




    /*
    *  updateData cached list
    */
    private void updateItem(ArrayList<Space> itemList, Space item) {
        for (int i = 0; i < itemList.size(); i++) {
            if ((itemList.get(i)).getId().equals(item.getId())) {
                itemList.set(i, item);
                return;
            }

        }

    }

    /*
    *  remove item createInstance list
    */
    private void removeItem(ArrayList<Space> itemList, Space item) {
        for (int i = 0; i < itemList.size(); i++) {

            if ((itemList.get(i)).getId().equals(item.getId())) {
                itemList.remove(i);
                return;
            }

        }
    }

}
