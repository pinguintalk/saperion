/*
 * Copyright (C) 2010 The Android Open Source Project
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.saperion.sdb.client.authenticator;

import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.saperion.sdb.client.Constants;
import com.saperion.sdb.client.R;
import com.saperion.sdb.sdk.SdbApplication;
import com.saperion.sdb.sdk.config.JSONTag;
import com.saperion.sdb.sdk.config.WSConfig;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.RequestManager.RequestListener;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.utils.Utils;

/**
 * Activity which displays login screen to the user.
 */
public class AuthenticatorActivity extends AccountAuthenticatorActivity implements RequestListener {

	public static final String PARAM_CONFIRMCREDENTIALS = "confirmCredentials";
	public static final String PARAM_PASSWORD = "password";
	public static final String PARAM_USERNAME = "username";
	public static final String PARAM_AUTHTOKEN_TYPE = "authtokenType";

	private static final String TAG = "AuthenticatorActivity";

	private AccountManager mAccountManager;
	private Thread mAuthThread;
	private String mAuthtoken;
	private String mAuthtokenType;
	protected ProgressDialog progressDialog = null;

	/**
	 * If set we are just checking that the user knows their credentials; this
	 * doesn't cause the user's password to be changed on the device.
	 */
	private Boolean mConfirmCredentials = false;

	/** for posting authentication attempts back to UI thread */
	private final Handler mHandler = new Handler();
	private TextView mMessage;
	private String mPassword;
	private EditText mPasswordEdit;

	/** Was the original caller asking for an entirely new account? */
	protected boolean mRequestNewAccount = false;

	private String mUsername;
	private EditText mUsernameEdit;

	private Button mloginButton;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onCreate(Bundle icicle) {
		Log.v(TAG, "onCreate(" + icicle + ")");
		super.onCreate(icicle);
		mAccountManager = AccountManager.get(this);
		Log.v(TAG, "loading data createInstance Intent");
		final Intent intent = getIntent();
		mUsername = intent.getStringExtra(PARAM_USERNAME);
		mAuthtokenType = intent.getStringExtra(PARAM_AUTHTOKEN_TYPE);
		mRequestNewAccount = mUsername == null;
		mConfirmCredentials = intent.getBooleanExtra(PARAM_CONFIRMCREDENTIALS, false);

		Log.v(TAG, "    request new: " + mRequestNewAccount);
		requestWindowFeature(Window.FEATURE_LEFT_ICON);
		setContentView(R.layout.login);
		getWindow().setFeatureDrawableResource(Window.FEATURE_LEFT_ICON,
				android.R.drawable.ic_dialog_alert);

		mUsernameEdit = (EditText) findViewById(R.id.etEmail);
		mPasswordEdit = (EditText) findViewById(R.id.txtPassword);

		mUsernameEdit.setText(mUsername);

		mUsernameEdit.setText(SdbApplication.getInstance().getUserName());

        mPasswordEdit.setText(SdbApplication.getInstance().getPassword());


        mloginButton = (Button) findViewById(R.id.btn_login);

		// mloginButton.setOnClickListener( new OnClickListener());
	}

	/* Login Button onClick Handler */
	private void loginButtonClicked(boolean kickOtherSessions) {

		Log.v(TAG, "loggin in");
		login();
	}

	private void login() {
		progressDialog = ProgressDialog.show(this, "Login", "Please wait...", true, false);
		mUsername = mUsernameEdit.getText().toString();
		mPassword = mPasswordEdit.getText().toString();

		String accountType = this.getIntent().getStringExtra(PARAM_AUTHTOKEN_TYPE);
		if (accountType == null) {
			accountType = Constants.ACCOUNT_TYPE;
		}

		AccountManager accMgr = AccountManager.get(this);

		// This is the magic that addes the account to the Android Account Manager  
		final Account account = new Account(mUsername, accountType);
		accMgr.addAccountExplicitly(account, mPassword, null);

		// Now we tell our caller, could be the Andreoid Account Manager or even our own application  
		// that the process was successful  

		final Intent intent = new Intent();
		intent.putExtra(AccountManager.KEY_ACCOUNT_NAME, mUsername);
		intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, accountType);
		intent.putExtra(AccountManager.KEY_AUTHTOKEN, accountType);
		this.setAccountAuthenticatorResult(intent.getExtras());
		this.setResult(RESULT_OK, intent);
		this.finish();

		/*
		// TODO check account
		account = new Account(mUsername, ACCOUNT_TYPE);
		mAccountManager.addAccountExplicitly(account, mPassword, null);
		 */
		SdbApplication.getInstance().login(WSConfig.WS_AUTHENTICATION_URL, mUsername, mPassword,
				this);

	}

	/*
	 * {@inheritDoc}
	 */
	@Override
	protected Dialog onCreateDialog(int id) {
		final ProgressDialog dialog = new ProgressDialog(this);

		dialog.setIndeterminate(true);
		dialog.setCancelable(true);
		dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
			public void onCancel(DialogInterface dialog) {
				Log.v(TAG, "dialog cancel has been invoked");
				if (mAuthThread != null) {
					mAuthThread.interrupt();
					finish();
				}
			}
		});
		return dialog;
	}

	/**
	 * Handles onClick event on the Submit button. Sends username/password to
	 * the server for authentication.
	 * 
	 * @param view The Submit button for which this method is invoked
	 */
	public void handleLogin(View view) {
		if (mRequestNewAccount) {
			mUsername = mUsernameEdit.getText().toString();
		}
		mPassword = mPasswordEdit.getText().toString();
		if (TextUtils.isEmpty(mUsername) || TextUtils.isEmpty(mPassword)) {
			mMessage.setText(getMessage());
		} else {
			showProgress();
			// Start authenticating...
			SdbApplication.getInstance().login(WSConfig.WS_AUTHENTICATION_URL, mUsername,
					mPassword, AuthenticatorActivity.this);

		}
	}

	/**
	 * Called when response is received createInstance the server for confirm credentials
	 * request. See onAuthenticationResult(). Sets the
	 * AccountAuthenticatorResult which is sent back to the caller.
	 * 
	 * @param result the confirmCredentials result.
	 */
	protected void finishConfirmCredentials(boolean result) {
		Log.v(TAG, "finishConfirmCredentials()");
		final Account account = new Account(mUsername, Constants.ACCOUNT_TYPE);
		mAccountManager.setPassword(account, mPassword);
		final Intent intent = new Intent();
		intent.putExtra(AccountManager.KEY_BOOLEAN_RESULT, result);
		setAccountAuthenticatorResult(intent.getExtras());
		setResult(RESULT_OK, intent);
		finish();
	}

	/**
	 * 
	 * Called when response is received createInstance the server for authentication
	 * request. See onAuthenticationResult(). Sets the
	 * AccountAuthenticatorResult which is sent back to the caller. Also sets
	 * the authToken in AccountManager for this account.
	 */
	protected void finishLogin() {
		Log.v(TAG, "finishLogin()");
		final Account account = new Account(mUsername, Constants.ACCOUNT_TYPE);

		if (mRequestNewAccount) {
			mAccountManager.addAccountExplicitly(account, mPassword, null);
			// Set contacts sync for this account.
			ContentResolver.setSyncAutomatically(account, ContactsContract.AUTHORITY, true);
		} else {
			mAccountManager.setPassword(account, mPassword);
		}
		final Intent intent = new Intent();
		mAuthtoken = mPassword;
		intent.putExtra(AccountManager.KEY_ACCOUNT_NAME, mUsername);
		intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, Constants.ACCOUNT_TYPE);
		if (mAuthtokenType != null && mAuthtokenType.equals(Constants.AUTHTOKEN_TYPE)) {
			intent.putExtra(AccountManager.KEY_AUTHTOKEN, mAuthtoken);
		}
		setAccountAuthenticatorResult(intent.getExtras());
		setResult(RESULT_OK, intent);
		finish();
	}

	/**
	 * Hides the progress UI for a lengthy operation.
	 */
	protected void hideProgress() {
		dismissDialog(0);
	}

	/**
	 * Called when the authentication process completes (see attemptLogin()).
	 */
	public void onAuthenticationResult(boolean result) {
		Log.v(TAG, "onAuthenticationResult(" + result + ")");
		// Hide the progress dialog
		hideProgress();
		if (result) {
			if (!mConfirmCredentials) {
				finishLogin();
			} else {
				finishConfirmCredentials(true);
			}
		} else {
			Log.e(TAG, "onAuthenticationResult: failed to authenticate");
			if (mRequestNewAccount) {
				// "Please enter a valid username/password.

			} else {
				// "Please enter a valid password." (Used when the
				// account is already in the database but the password
				// doesn't work.)

			}
		}
	}

	/**
	 * Returns the message to be displayed at the top of the login dialog box.
	 */
	private CharSequence getMessage() {
		getString(R.string.app_name);
		if (TextUtils.isEmpty(mUsername)) {
			// If no username, then we ask the user to log in using an
			// appropriate service.

			return "username empty";
		}
		if (TextUtils.isEmpty(mPassword)) {
			// We have an account but no password
			return "pass empty";
		}
		return null;
	}

	/**
	 * Shows the progress UI for a lengthy operation.
	 */
	protected void showProgress() {
		showDialog(0);
	}

	@Override
	public void onRequestFinished(Request request, Bundle resultData) {
		// TODO Auto-generated method stub
		if (progressDialog != null) {
			progressDialog.cancel();
			progressDialog = null;
		}

		if (resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE) == ResponseConstants.RESPONSE_CODE_OK) {

			Log.v(TAG, "finishLogin()");

			// Set contacts sync for this account.
			// ContentResolver.setSyncAutomatically(account, ContactsContract.AUTHORITY, true);

			String result_data = resultData.getString(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);

			// Log.v(TAG, Utils.getMethodName() + "result_data "  + result_data);

			if (result_data != null && result_data.length() > 0) {
				JSONObject authObject;
				try {
					authObject = new JSONObject(result_data);

					Log.v(TAG, Utils.getMethodName() + result_data);

					if (authObject.has(JSONTag.AUTHENTICATION_ACCES_TOKEN)) {
						String token = authObject.getString(JSONTag.AUTHENTICATION_ACCES_TOKEN);

						String expires =
								authObject.getString(JSONTag.AUTHENTICATION_ACCES_TOKEN_EXPIRES);

						Log.v(TAG, Utils.getMethodName() + "access_token " + token + " expires "
								+ expires);

						final Account account = new Account(mUsername, Constants.ACCOUNT_TYPE);

						if (mRequestNewAccount) {
							mAccountManager.addAccountExplicitly(account, mPassword, null);
							// Set contacts sync for this account.
							// ContentResolver.setSyncAutomatically(account,
							//     ContactsContract.AUTHORITY, true);
						} else {
							mAccountManager.setPassword(account, mPassword);
						}
						final Intent intent = new Intent();
						mAuthtoken = mPassword;
						intent.putExtra(AccountManager.KEY_ACCOUNT_NAME, mUsername);
						intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, Constants.ACCOUNT_TYPE);
						if (mAuthtokenType != null
								&& mAuthtokenType.equals(Constants.AUTHTOKEN_TYPE)) {
							intent.putExtra(AccountManager.KEY_AUTHTOKEN, mAuthtoken);
						}
						setAccountAuthenticatorResult(intent.getExtras());
						setResult(RESULT_OK, intent);
						finish();

					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		} else {
			// TODO show error
		}

		finish();

	}

	@Override
	public void onRequestConnectionError(Request request, int statusCode) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onRequestDataError(Request request) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onRequestServiceError(Request request, Bundle resultData) {
		// TODO Auto-generated method stub

	}
}
