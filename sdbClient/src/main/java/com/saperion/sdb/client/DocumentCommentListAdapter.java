package com.saperion.sdb.client;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.saperion.sdb.client.R.color;
import com.saperion.sdb.client.R.id;
import com.saperion.sdb.client.R.layout;
import com.saperion.sdb.client.utils.Log;
import com.saperion.sdb.sdk.SdbApplication;
import com.saperion.sdb.sdk.models.Comment;
import com.saperion.sdb.sdk.models.User;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.RequestConstants;
import com.saperion.sdb.sdk.requestmanager.RequestManager.RequestListener;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.utils.Utils;

import java.util.ArrayList;

public class DocumentCommentListAdapter extends ArrayAdapter<Comment> {

    private static final String TAG = DocumentCommentListAdapter.class.getSimpleName();

    private static LayoutInflater mInflater = null;
    private int mLayoutType = 1;
    private Context mContext;
    private int mCurrSelectedItemPosition = -1;
    private String mCurrentSelectedItemId = null;



    private Comment currEditedComment = null;
    private View mCurrentSelectecurrSelectedCommentdView = null;
    private DocumentInfoListViewInterface mCallback;

    private boolean isInEditMode = false;

    public DocumentCommentListAdapter(Context context, DocumentInfoListViewInterface documentInfoListViewInterface,
                                      ArrayList<Comment> documentCommentList) {
        super(context, 0, documentCommentList);
        this.mContext = context;
        this.mCallback = documentInfoListViewInterface;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public Comment getCurrEditedComment() {
        return currEditedComment;
    }

    public void setCurrEditedComment(Comment currEditedComment) {
        this.currEditedComment = currEditedComment;
    }
    
    public String getmCurrentSelectedItemId() {
        return mCurrentSelectedItemId;
    }

    public int getCurrSelectedItemPosition() {
        return mCurrSelectedItemPosition;

    }

    public void setCurrSelectedItemPosition(int pos) {
        this.mCurrSelectedItemPosition = pos;

    }

    public void setmCurrentSelectedItemId(String mCurrentSelectedItemId) {
        this.mCurrentSelectedItemId = mCurrentSelectedItemId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.v(TAG, Utils.getMethodName() + "entry ");

        Comment item = getItem(position);

        Log.v(TAG, Utils.getMethodName() + "item id " + item.getId());

        if (convertView == null) {

            convertView = mInflater.inflate(layout.document_comment_list_cell, null);
            convertView.setTag(new ViewHolder(convertView));
        }

        ((ViewHolder) convertView.getTag()).populateView(item, position);

        return convertView;

    }

    public void edit(View view, Comment comment) {
        ((ViewHolder) view.getTag()).edit(comment);
    }


    class ViewHolder {
        private RelativeLayout relativeLayout;
        View mView;
        private TextView userNameTextView;
        private TextView commentsTextView;
        private EditText commentsEditText;
        private TextView timeTextView;
        private LinearLayout tvGroup;
        private LinearLayout buttonSetLayout;
        private ImageView avatar;
        RequestListener listener;

        private ImageView mImageViewFavorit;

        public ViewHolder(View view) {
            this.mView = view;

            avatar = (ImageView) mView.findViewById(id.avatar);
            listener = new RequestListener() {

                @Override
                public void onRequestFinished(Request request, Bundle resultData) {

                    int resp_code = resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);

                    int requestType = request.getRequestType();
                    Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " resultCode = "
                            + resp_code);
                    switch (requestType) {
                        case RequestConstants.REQUEST_TYPE_USER_GET_AVATAR:
                            User user = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                            String avatarPath = user.getAvatarAbsPath();
                            if (avatarPath !=null && avatarPath.length() > 0) {
                                Bitmap bitmap = BitmapFactory.decodeFile(user.getAvatarAbsPath());
                                avatar.setImageBitmap(bitmap);
                            }
                            break;
                    }
                }

                @Override
                public void onRequestConnectionError(Request request, int statusCode) {

                }

                @Override
                public void onRequestDataError(Request request) {

                }

                @Override
                public void onRequestServiceError(Request request, Bundle resultData) {

                }
            };
            
            relativeLayout = (RelativeLayout) mView.findViewById(id.document_comment_cell);
            userNameTextView = (TextView) mView.findViewById(id.username_txt);
            commentsTextView = (TextView) mView.findViewById(id.comments_txt);

            tvGroup = (LinearLayout) mView.findViewById(id.tv_group);
            commentsEditText = (EditText) mView.findViewById(id.comments_edit);
            timeTextView = (TextView) mView.findViewById(id.time_txt);

            commentsEditText.setVisibility(View.GONE);

            /*
            commentsEditText.setOnEditorActionListener(new OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                    Log.v(TAG, Utils.getMethodName() + "actionId " + actionId);
                    
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        mCallback.onCommentAdded(mCurrOpenedDocument,commentsEditText.getText().toString());
                    }
                    return false;
                }
            });
            */


            commentsEditText.setImeActionLabel("Done", KeyEvent.KEYCODE_ENTER);
            commentsEditText.setOnKeyListener(new OnKeyListener() {
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    // If the event is a key-down event on the "enter" button
                    Log.v(TAG, Utils.getMethodName() + "event.getAction()  " + event.getAction() +" keyCode " +keyCode);
                    
                    if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                            (keyCode == KeyEvent.KEYCODE_ENTER)) {
                        String comment = commentsEditText.getText().toString();
                       
                        //hide keyboard
                        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(
                                Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(commentsEditText.getWindowToken(), 0);
                        // Perform action on key press
                        if (currEditedComment !=null){
                            currEditedComment.setComment(comment);
                            mCallback.onCommentUpdate(currEditedComment);

                        }
                        else {
                            mCallback.onCommentAdded(comment);
                            clear();
                        }
   
                        return true;
                    }
                    return false;
                }
            });


            commentsEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    commentsEditText.post(new Runnable() {
                        @Override
                        public void run() {
                            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.showSoftInput(commentsEditText, InputMethodManager.SHOW_IMPLICIT);
                        }
                    });

                    commentsEditText.setVisibility(View.VISIBLE);
                }
            });

        }

        public void populateView(Comment item, int position) {

            User user = SdbApplication.getInstance().getUser(item.getOwnerId());
            if (user != null)  {
                user.getAvatar(listener);
            }
            
           if (position == 0) {
                commentsEditText.setVisibility(View.VISIBLE);
                tvGroup.setVisibility(View.GONE);       
           } else {
               
                if (currEditedComment == null) {
                    commentsEditText.setVisibility(View.GONE);
                    tvGroup.setVisibility(View.VISIBLE);
                }
                else {
                    if (currEditedComment.getId().equals(item.getId()))
                        tvGroup.setVisibility(View.GONE);
                    else {
                        commentsEditText.setVisibility(View.GONE);
                        tvGroup.setVisibility(View.VISIBLE);
                    }
                }
                   
           }
            userNameTextView.setText(item.getOwnerName());
            userNameTextView.setTextColor(getContext().getResources().getColor(color.orange));
            commentsTextView.setText(item.getComment());
            timeTextView.setText(item.getCreationDate());

            String date_string= "";

            String last_modified =item.getCreationDate();
            if (last_modified != null && last_modified.length() > 0) {
                date_string= com.saperion.sdb.client.utils.Utils.getTimeExtraFormat(mContext,last_modified);
            }
            timeTextView.setText(date_string);
        }

        public void edit(Comment item) {
            currEditedComment = item;
            tvGroup.setVisibility(View.GONE);
            
            commentsEditText.setText(item.getComment());
            commentsEditText.setVisibility(View.VISIBLE);
            commentsEditText.requestFocus();

        }
        }
}
