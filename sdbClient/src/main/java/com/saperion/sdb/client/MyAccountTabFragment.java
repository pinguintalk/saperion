package com.saperion.sdb.client;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.saperion.sdb.client.utils.Log;
import com.saperion.sdb.sdk.SdbApplication;
import com.saperion.sdb.sdk.models.Account;
import com.saperion.sdb.sdk.models.User;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.RequestConstants;
import com.saperion.sdb.sdk.requestmanager.RequestManager.RequestListener;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.utils.Utils;

import cx.hell.android.pdfview.ChooseFileActivity;


/**
 * Created by nnn on 14.11.13.
 */
public class MyAccountTabFragment extends Fragment implements RequestListener{


    private static final String TAG = MyAccountTabFragment.class.getSimpleName();
    private ProgressDialog progressDialog = null;
    protected RequestListener requestListener;
    LinearLayout nameExpandableLayout;
    LinearLayout avatarExpandableLayout;
    LinearLayout passwordExpandableLayout;
    ImageView nameExpandSwitcher;
    ImageView avatarExpandSwitcher;
    ImageView passwordExpandSwitcher;
    ValueAnimator mAnimator;
    
    // values
    EditText etDisplayName;
    EditText etFirstName;
    EditText etLastName;
    EditText etEmail;
    EditText etOldPassword;
    EditText etNewPassword;
    EditText etNewPasswordConfirm;
    
    ImageView avatar;
    
    ImageButton btnEditDetails;
    LinearLayout btnSetEditPersonalDetails;

    ImageButton btnOk;
    ImageButton btnCancel;
    ImageButton btnUpdatePassword ;
    ImageButton btnSetAvatar;
    ImageButton btnDeleteAvatar;


    
    User currentUser = null;
    /** (non-Javadoc)
     * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
     */
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container == null) {
            // We have different layouts, and in one of them this
            // fragment's containing frame doesn't exist.  The fragment
            // may still be created createInstance its saved state, but there is
            // no reason to try to create its view hierarchy because it
            // won't be displayed.  Note this is not needed -- we could
            // just run the code below, where we would create and return
            // the view hierarchy; it would just never be used.
            return null;
        }
        return (LinearLayout) inflater.inflate(R.layout.tab_my_account_layout, container, false);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.v(TAG, com.saperion.sdb.client.utils.Utils.getMethodName() + "entry ");
       
            if (resultCode == Activity.RESULT_OK) {
                String filepath = data.getStringExtra("file_path");
                Toast.makeText(getActivity(), filepath, Toast.LENGTH_SHORT).show();

                currentUser.setAvatar(requestListener,filepath);
            }
        
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestListener = this;
       
        etNewPasswordConfirm = (EditText) getActivity().findViewById(R.id.txtConfirmPassword);
                
        btnUpdatePassword  = (ImageButton) getActivity().findViewById(R.id.b_update_password);

        btnUpdatePassword.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "btnUpdatePassword ");
                String newPassString = etNewPassword.getText().toString();
                String newPassStringConfirm = etNewPasswordConfirm.getText().toString();
                
                if (! newPassString.equals(newPassStringConfirm))  {
                    showServiceErrorDialog("new passwords ar not equal");
                } else {
                SdbApplication.getInstance().updatePassword(requestListener,etOldPassword.getText().toString(),
                        newPassString );
                }
            }
        });

        etOldPassword = (EditText) getActivity().findViewById(R.id.txtOldPassword);

        etNewPassword =  (EditText) getActivity().findViewById(R.id.txtNewPassword);
                 
        btnEditDetails = (ImageButton) getActivity().findViewById(R.id.b_edit);
        btnEditDetails.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "btnEditDetails ");
                btnEditDetails.setVisibility(View.GONE);
                btnSetEditPersonalDetails.setVisibility(View.VISIBLE);  
            }
        });
        
        btnOk = (ImageButton) getActivity().findViewById(R.id.b_ok);
        btnCancel = (ImageButton) getActivity().findViewById(R.id.b_cancel);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "mButtonOk ");
                btnEditDetails.setVisibility(View.VISIBLE);
                btnSetEditPersonalDetails.setVisibility(View.GONE);

                if (currentUser != null) {

                    currentUser.setFirstname(etFirstName.getText().toString());
                    currentUser.setLastname(etLastName.getText().toString());
                    currentUser.setDisplayname(etDisplayName.getText().toString());
                    currentUser.setEmail(etEmail.getText().toString());
                    currentUser.save(requestListener);
                }

            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "mButtonCancel ");
                btnEditDetails.setVisibility(View.VISIBLE);
                btnSetEditPersonalDetails.setVisibility(View.GONE);

            }
        });
        
        btnSetEditPersonalDetails = (LinearLayout) getActivity().findViewById(R.id.personal_edit_button_set);


        btnSetAvatar  = (ImageButton) getActivity().findViewById(R.id.b_set_avatar);
        btnSetAvatar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "btnSetAvatar ");
                getActivity().startActivityForResult(
                        new Intent(getActivity().getApplicationContext(), ChooseFileActivity.class),
                        MainFragmentActivity.PICK_FILE_REQUEST_CODE);
            }
        });
        btnDeleteAvatar  = (ImageButton) getActivity().findViewById(R.id.b_delete_avatar);
        btnDeleteAvatar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "btnDeleteAvatar ");
                currentUser.deleteAvatar(requestListener);
            }
        });
        
        nameExpandableLayout = (LinearLayout) getActivity().findViewById(R.id.NameExpandableLayout);
        avatarExpandableLayout  = (LinearLayout) getActivity().findViewById(R.id.AvatarExpandableLayout);
        passwordExpandableLayout = (LinearLayout) getActivity().findViewById(R.id.PasswordExpandableLayout);
        
        nameExpandSwitcher = (ImageView) getActivity().findViewById(R.id.NameExpandableIcon);
        avatarExpandSwitcher = (ImageView) getActivity().findViewById(R.id.AvatarExpandableIcon);
        passwordExpandSwitcher = (ImageView) getActivity().findViewById(R.id.PasswordExpandableIcon);

        //Add onPreDrawListener
        nameExpandableLayout.getViewTreeObserver().addOnPreDrawListener(
                new ViewTreeObserver.OnPreDrawListener() {

                    @Override
                    public boolean onPreDraw() {
                        nameExpandableLayout.getViewTreeObserver().removeOnPreDrawListener(this);
                        nameExpandableLayout.setVisibility(View.GONE);

                        final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                        final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                        nameExpandableLayout.measure(widthSpec, heightSpec);

                        mAnimator = slideAnimator(nameExpandableLayout, 0, nameExpandableLayout.getMeasuredHeight());
                        return true;
                    }
                });

        //Add onPreDrawListener
        avatarExpandableLayout.getViewTreeObserver().addOnPreDrawListener(
                new ViewTreeObserver.OnPreDrawListener() {

                    @Override
                    public boolean onPreDraw() {
                        avatarExpandableLayout.getViewTreeObserver().removeOnPreDrawListener(this);
                        avatarExpandableLayout.setVisibility(View.GONE);

                        final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                        final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                        avatarExpandableLayout.measure(widthSpec, heightSpec);

                        mAnimator = slideAnimator(avatarExpandableLayout,0, avatarExpandableLayout.getMeasuredHeight());
                        return true;
                    }
                });

        //Add onPreDrawListener
        passwordExpandableLayout.getViewTreeObserver().addOnPreDrawListener(
                new ViewTreeObserver.OnPreDrawListener() {

                    @Override
                    public boolean onPreDraw() {
                        passwordExpandableLayout.getViewTreeObserver().removeOnPreDrawListener(this);
                        passwordExpandableLayout.setVisibility(View.GONE);

                        final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                        final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                        passwordExpandableLayout.measure(widthSpec, heightSpec);

                        mAnimator = slideAnimator(passwordExpandableLayout,0, passwordExpandableLayout.getMeasuredHeight());
                        return true;
                    }
                });
        
        nameExpandSwitcher.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (nameExpandableLayout.getVisibility() == View.GONE) {
                    nameExpandSwitcher.setImageDrawable(getResources().getDrawable(R.drawable.collapse));
                    setLayoutVisible(nameExpandableLayout);
                    setLayoutGone(avatarExpandableLayout);
                    avatarExpandSwitcher.setImageDrawable(getResources().getDrawable(R.drawable.expand));
                    setLayoutGone(passwordExpandableLayout);
                    passwordExpandSwitcher.setImageDrawable(getResources().getDrawable(R.drawable.expand));
                } else {
                    nameExpandSwitcher.setImageDrawable(getResources().getDrawable(R.drawable.expand));
                    setLayoutGone(nameExpandableLayout);
                }
            }
        });

        avatarExpandSwitcher.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (avatarExpandableLayout.getVisibility() == View.GONE) {
                    avatarExpandSwitcher.setImageDrawable(getResources().getDrawable(R.drawable.collapse));
                    setLayoutVisible(avatarExpandableLayout);
                    setLayoutGone(nameExpandableLayout);
                    nameExpandSwitcher.setImageDrawable(getResources().getDrawable(R.drawable.expand));
                    setLayoutGone(passwordExpandableLayout);
                    passwordExpandSwitcher.setImageDrawable(getResources().getDrawable(R.drawable.expand));
                } else {
                    avatarExpandSwitcher.setImageDrawable(getResources().getDrawable(R.drawable.expand));
                    setLayoutGone(avatarExpandableLayout);
                }
            }
        });


        passwordExpandSwitcher.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (passwordExpandableLayout.getVisibility() == View.GONE) {
                    passwordExpandSwitcher.setImageDrawable(getResources().getDrawable(R.drawable.collapse));
                    setLayoutVisible(passwordExpandableLayout);
                    setLayoutGone(nameExpandableLayout);
                    nameExpandSwitcher.setImageDrawable(getResources().getDrawable(R.drawable.expand));
                    setLayoutGone(avatarExpandableLayout);
                    avatarExpandSwitcher.setImageDrawable(getResources().getDrawable(R.drawable.expand));
                } else {
                    passwordExpandSwitcher.setImageDrawable(getResources().getDrawable(R.drawable.expand));
                    setLayoutGone(passwordExpandableLayout);
                }
            }
        });

        currentUser = SdbApplication.getInstance().getCurrentUser(this);
        if (currentUser==null) return;

        currentUser.getAvatar(this);
        setPersonalDetails(currentUser);

       // SdbApplication.getInstance().getLocalization(requestListener, "de");
    }

    private void setPersonalDetails(User user) {

        if (user==null) return;

        avatar = (ImageView) getActivity().findViewById(R.id.AvatarIcon);
        etDisplayName = (EditText) getActivity().findViewById(R.id.txtDisplayName);
        etFirstName = (EditText) getActivity().findViewById(R.id.etFirstName);
        etLastName = (EditText) getActivity().findViewById(R.id.etLastName);
        etEmail = (EditText) getActivity().findViewById(R.id.etEmail);
        
        if (etDisplayName != null)
            etDisplayName.setText(user.getDisplayname());
        if (etFirstName != null)
            etFirstName.setText(user.getFirstname());
        if (etLastName != null)
            etLastName.setText(user.getLastname());
        if (etEmail != null)
            etEmail.setText(user.getEmail());
        String  avatarPath = user.getAvatarAbsPath();

        if (avatar != null) {
            if (avatarPath != null && avatarPath.length() > 0) {
                Bitmap bMap = BitmapFactory.decodeFile(avatarPath);
                avatar.setImageBitmap(bMap);
            }
            else
                avatar.setImageResource(R.drawable.avatar);
        }
    }
    
    private void setLayoutVisible(LinearLayout layout) {
        //set nameExpandableLayout Visible
        layout.setVisibility(View.VISIBLE);

        final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        layout.measure(widthSpec, heightSpec);

        ValueAnimator mAnimator = slideAnimator(layout, 0, layout.getMeasuredHeight());
        mAnimator.start();
   
    }
    private void setLayoutGone(final LinearLayout layout) {
        int finalHeight = layout.getHeight();

        ValueAnimator mAnimator = slideAnimator(layout,finalHeight, 0);

        mAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationEnd(Animator animator) {
                //Height=0, but it set visibility to GONE
                layout.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });
        mAnimator.start();
    }
    private ValueAnimator slideAnimator(final LinearLayout layout, int start, int end) {

        ValueAnimator animator = ValueAnimator.ofInt(start, end);

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                //Update Height
                int value = (Integer) valueAnimator.getAnimatedValue();

                ViewGroup.LayoutParams layoutParams = layout.getLayoutParams();
                layoutParams.height = value;
                layout.setLayoutParams(layoutParams);
            }
        });
        return animator;
    }

    @Override
    public void onRequestFinished(Request request, Bundle resultData) {

        cancelLoadingDialog();
        int resp_code = resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);

        String errorMessage = resultData.getString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE);

        int requestType = request.getRequestType();
        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " resultCode = "
                + resp_code);

        switch (resp_code) {
            case 0:

            case ResponseConstants.RESPONSE_CODE_OK:
            case ResponseConstants.RESPONSE_CODE_CREATED:
                break;

            case ResponseConstants.RESPONSE_CODE_BAD_REQUEST:
            case ResponseConstants.RESPONSE_CODE_ERROR:
            case ResponseConstants.RESPONSE_CODE_FORBIDDEN:
            case ResponseConstants.RESPONSE_CODE_INTERNAL_SERVER_ERROR:
            case ResponseConstants.RESPONSE_CODE_INTERNAL_SERVICE_UNAVAILABLE:
            case ResponseConstants.RESPONSE_CODE_METHOD_NOT_ALLOWED:
            case ResponseConstants.RESPONSE_CODE_NOT_FOUND:
            case ResponseConstants.RESPONSE_CODE_PRECONDITION_FAILED:
            case ResponseConstants.RESPONSE_CODE_TOO_MANY_REQUESTS:
                showServiceErrorDialog(errorMessage);
                return;
            case ResponseConstants.RESPONSE_CODE_NO_CONTENT:
                return;

            default: {
                showServiceErrorDialog("unkown error");
                return;
            }
        }

        switch (requestType) {

            case RequestConstants.REQUEST_TYPE_ACCOUNT_GET:
                Account account = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                break;
            case RequestConstants.REQUEST_TYPE_USER_GET_AVATAR:
                User user = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                setPersonalDetails(user);
                break;
            case RequestConstants.REQUEST_TYPE_USER_UPDATE:
                User usertmp = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                setPersonalDetails(usertmp);
                break;
            case RequestConstants.REQUEST_TYPE_USER_SET_AVATAR:
                currentUser.getAvatar(this);
                break;
            case RequestConstants.REQUEST_TYPE_USER_DELETE_AVATAR:
                currentUser.setAvatarAbsPath("");   
                avatar.setImageResource(R.drawable.avatar);
                break;
            case RequestConstants.REQUEST_TYPE_USER_UPDATE_PASSWORD:
                SdbApplication.getInstance().setPassword(etNewPassword.getText().toString());
                break;



            case RequestConstants.REQUEST_TYPE_GET_LOCALIZAION:
                String localization= resultData.getString(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                com.saperion.sdb.sdk.utils.Log.v(TAG, Utils.getMethodName() + "current localization " + localization);
                break;
        }

    }

    @Override
    public void onRequestConnectionError(Request request, int statusCode) {
        cancelLoadingDialog();

        int requestType = request.getRequestType();
        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " statusCode = "
                + statusCode);
    }

    @Override
    public void onRequestDataError(Request request) {
        cancelLoadingDialog();

        int requestType = request.getRequestType();
        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType );
    }

    @Override
    public void onRequestServiceError(Request request, Bundle resultData) {
        cancelLoadingDialog();
        int resp_code = resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);

        String errorMessage = resultData.getString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE);

        int requestType = request.getRequestType();
        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " resultCode = "
                + resp_code);
    }

    private void showServiceErrorDialog(String message) {


        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());

        builder.setTitle("Service Error");
        builder.setMessage(message);

        builder.setPositiveButton(getResources().getString(R.string.close),
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        // Do do my action here

                        dialog.dismiss();
                    }

                });
        /*
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // I do not need any action here you might
                dialog.dismiss();
            }
        });
        */
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void cancelLoadingDialog() {

        if (progressDialog != null) {
            progressDialog.cancel();
            progressDialog = null;
        }
    }

    public void showLoadingDialog() {


        if (progressDialog != null) {
            progressDialog.cancel();
            progressDialog = null;
        }

        progressDialog = new ProgressDialog(this.getActivity());

        progressDialog.setTitle(getResources().getString(R.string.progress_dialog_title));
        progressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        progressDialog.setCancelable(true);
        progressDialog.setIndeterminate(true);

        progressDialog.show();

    }

    public void updateAvatar(String filepath) {
        currentUser.setAvatar(this,filepath);
    }
}
