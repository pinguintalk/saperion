package com.saperion.sdb.client;

import android.app.Activity;
import android.os.Parcelable;

import com.saperion.sdb.sdk.models.Document;
import com.saperion.sdb.sdk.models.SpacedItem;

public interface ItemListViewInterface  {
    /**
     * Callback for when an item has been selected.
     */
    
    public void  itemIconOrNameClickedEvent(SpacedItem mItem);

    public void itemRecycleClickedEvent(SpacedItem mItem);

    public void itemOkClickedEvent(SpacedItem mItem);

    public void itemDownloadClickedEvent(SpacedItem mItem);

    public void itemOpenLinkDialogClickedEvent(Activity acivity, Parcelable mItem);
    
    public void itemCutClickedEvent(SpacedItem mItem);

    public void itemGetPreview(Document mItem);

    public void itemRestoreClickedEvent(SpacedItem mItem);

    public void itemDeleteClickedEvent(SpacedItem mItem);
}
