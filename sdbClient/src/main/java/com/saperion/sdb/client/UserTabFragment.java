package com.saperion.sdb.client;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.saperion.sdb.client.InviteUserDialogFragment.CreateUserDialogListener;
import com.saperion.sdb.client.utils.Log;
import com.saperion.sdb.sdk.SdbApplication;
import com.saperion.sdb.sdk.models.User;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.RequestConstants;
import com.saperion.sdb.sdk.requestmanager.RequestManager.RequestListener;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.utils.Utils;

import java.util.ArrayList;

/**
 * Created by nnn on 14.11.13.
 */
public class UserTabFragment extends Fragment implements UserListViewInterface, RequestListener, TextWatcher, CreateUserDialogListener {

    private static final String TAG = UserTabFragment.class.getSimpleName();
    GridView gridView;

    private static ArrayList<User> userList = new ArrayList<User>();
    private UserListAdapter userListAdapter;
    private ProgressDialog progressDialog = null;

    private EditText userFilterTextField;
    private ImageButton btnInviteUser;

    
    /** (non-Javadoc)
     * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
     */
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container == null) {
            // We have different layouts, and in one of them this
            // fragment's containing frame doesn't exist.  The fragment
            // may still be created createInstance its saved state, but there is
            // no reason to try to create its view hierarchy because it
            // won't be displayed.  Note this is not needed -- we could
            // just run the code below, where we would create and return
            // the view hierarchy; it would just never be used.
            return null;
        }
        return (LinearLayout) inflater.inflate(R.layout.tab_user_gridview_layout, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        userList = SdbApplication.getInstance().getUserListCached();
        SdbApplication.getInstance().getUserList(this);

        userFilterTextField = (EditText) getActivity().findViewById(R.id.user_filter_text_field);
        userFilterTextField.addTextChangedListener(this);
        btnInviteUser = (ImageButton) getActivity().findViewById(R.id.b_invite_user);
        btnInviteUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.v(TAG, Utils.getMethodName() + "btnInviteUser ");

                showInviteUserDialog();


            }
        });
        gridView = (GridView) getActivity().findViewById(R.id.user_grid_view);

        userListAdapter = new UserListAdapter(getActivity().getApplicationContext(),this, userList);

        gridView.setAdapter(userListAdapter);

        gridView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                Toast.makeText(getActivity().getApplicationContext(),
                        ((TextView) v).getText(), Toast.LENGTH_SHORT).show();

            }
        });


    }

    void showInviteUserDialog() {

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment newFragment = new InviteUserDialogFragment(this);
        newFragment.show(ft, "dialog");
    }


    @Override
    public void onDeleteUser(User user) {
        showLoadingDialog();
        user.delete(this);
    }

    @Override
    public void onUpdateUser(User user) {
        showLoadingDialog();
        user.save(this);
    }

    @Override
    public void onGetUserAvatar(User user) {

        user.getAvatar(this);
    }

    @Override
    public void onCreateUser(User user) {

    }

    @Override
    public void onRequestFinished(Request request, Bundle resultData) {

        cancelLoadingDialog();
        int resp_code = resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);

        String errorMessage = resultData.getString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE);

        int requestType = request.getRequestType();
        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " resultCode = "
                + resp_code);

        switch (resp_code) {
            case 0:

            case ResponseConstants.RESPONSE_CODE_OK:
            case ResponseConstants.RESPONSE_CODE_CREATED:
                break;

            case ResponseConstants.RESPONSE_CODE_BAD_REQUEST:
            case ResponseConstants.RESPONSE_CODE_ERROR:
            case ResponseConstants.RESPONSE_CODE_FORBIDDEN:
            case ResponseConstants.RESPONSE_CODE_INTERNAL_SERVER_ERROR:
            case ResponseConstants.RESPONSE_CODE_INTERNAL_SERVICE_UNAVAILABLE:
            case ResponseConstants.RESPONSE_CODE_METHOD_NOT_ALLOWED:
            case ResponseConstants.RESPONSE_CODE_NOT_FOUND:

            case ResponseConstants.RESPONSE_CODE_PRECONDITION_FAILED:
            case ResponseConstants.RESPONSE_CODE_TOO_MANY_REQUESTS:
                showServiceErrorDialog(errorMessage);
                return;
            case ResponseConstants.RESPONSE_CODE_NO_CONTENT:
                return;

            default: {
                showServiceErrorDialog("unkown error");
                return;
            }
        }

        switch (requestType) {
            case RequestConstants.REQUEST_TYPE_USER_CREATE:

                User new_user = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                userList.add(new_user);
                userListAdapter.notifyDataSetChanged();
                // SdbApplication.getInstance().getUserList(this);
                break;
            
            case RequestConstants.REQUEST_TYPE_USER_GET_LIST:

                ArrayList<User> tmpUserList =
                        resultData.getParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                userList.clear();
                for (int i = 0; i < tmpUserList.size(); i++) {
                   // if (!tmpUserList.get(i).isRecycled() ) {
                        userList.add(tmpUserList.get(i));
                   // }
                }
               
                
                userListAdapter.notifyDataSetChanged();
                break;
            case RequestConstants.REQUEST_TYPE_USER_DELETE:
            case RequestConstants.REQUEST_TYPE_USER_RECYCLE:
                User deleted_user = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);

                recycleUser(userList,deleted_user);
                //userListAdapter.clear();
                //userListAdapter.addAll(userList);               
                userListAdapter.notifyDataSetChanged();
                break;

            
            case RequestConstants.REQUEST_TYPE_USER_UPDATE:
            case RequestConstants.REQUEST_TYPE_USER_DELETE_AVATAR:
            case RequestConstants.REQUEST_TYPE_USER_GET_AVATAR:
                User user = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                int pos = updateUserInList(userList,user);

                View userCellView =
                        gridView.getChildAt(pos -  gridView.getFirstVisiblePosition());
                if (userCellView != null && userListAdapter != null) {
                    if (requestType == RequestConstants.REQUEST_TYPE_USER_GET_AVATAR ||
                            requestType == RequestConstants.REQUEST_TYPE_USER_DELETE_AVATAR  )
                        userListAdapter.updateAvatar(userCellView, user);
                    else if (requestType == RequestConstants.REQUEST_TYPE_USER_UPDATE) {

                        Log.d(TAG, Utils.getMethodName() + " user = " + user.toString());
                        
                        userListAdapter.updateData(userCellView, user);
                    }
                }
                break;

        }


    }

    @Override
    public void onRequestConnectionError(Request request, int statusCode) {
        cancelLoadingDialog();

        int requestType = request.getRequestType();
        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " statusCode = "
                + statusCode);
    }

    @Override
    public void onRequestDataError(Request request) {
        cancelLoadingDialog();

        int requestType = request.getRequestType();
        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType );
    }

    @Override
    public void onRequestServiceError(Request request, Bundle resultData) {
        cancelLoadingDialog();
        int resp_code = resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);

        String errorMessage = resultData.getString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE);

        int requestType = request.getRequestType();
        Log.d(TAG, Utils.getMethodName() + " requestType = " + requestType + " resultCode = "
                + resp_code);
    }

    /*
    *  updateData cached list
   */
    private int updateUserInList(ArrayList<User> userList, User user) {

        String id = user.getId();

        for (int i = 0; i < userList.size(); i++) {
            if (userList.get(i).getId().equals(id) ) {
                userList.set(i, user);
                return i;
            }
        }

        return -1;

    }

    private void recycleUser(ArrayList<User> userList, User user) {

        String id = user.getId();

        int userToDeletePos = -1;
        for (int i = 0; i < userList.size(); i++) {
            if (userList.get(i).getId().equals(id) ) {
                userToDeletePos = i;
                break;
            }
        }
        
        if (userToDeletePos > 0)
            userList.remove(userToDeletePos);
    }
    
    



    private void showServiceErrorDialog(String message) {


        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());

        builder.setTitle("Service Error");
        builder.setMessage(message);

        builder.setPositiveButton(getResources().getString(R.string.close),
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        // Do do my action here

                        dialog.dismiss();
                    }

                });
        /*
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // I do not need any action here you might
                dialog.dismiss();
            }
        });
        */
        AlertDialog alert = builder.create();
        alert.show();

    }

    public void cancelLoadingDialog() {

        if (progressDialog != null) {
            progressDialog.cancel();
            progressDialog = null;
        }

    }

    public void showLoadingDialog() {


        if (progressDialog != null) {
            progressDialog.cancel();
            progressDialog = null;
        }

        progressDialog = new ProgressDialog(this.getActivity());

        progressDialog.setTitle(getResources().getString(R.string.progress_dialog_title));
        progressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        progressDialog.setCancelable(true);
        progressDialog.setIndeterminate(true);

        progressDialog.show();

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        String prefix = charSequence.toString();
        Log.v(TAG, Utils.getMethodName() + "prefix " + prefix);


        ArrayList<User> currentList = SdbApplication.getInstance().getUserListCached();

        userList.clear();
        for (User aUser : currentList) {
            String userDisplayName = aUser.getDisplayname().toLowerCase();
            if (userDisplayName.contains(prefix.toLowerCase())) {

                if ( !aUser.isRecycled())
                    userList.add(aUser);

            }
        }

        userListAdapter.notifyDataSetChanged();
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public void onFinishCreateUserDialog(User user){
        showLoadingDialog();
        user.save(this);

        Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            DialogFragment df = (DialogFragment) prev;
            df.dismiss();
        }
    }
}
