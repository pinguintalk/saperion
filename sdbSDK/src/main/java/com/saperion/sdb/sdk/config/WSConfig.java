/**
 * 2011 Foxykeep (http://datadroid.foxykeep.com)
 * <p>
 * Licensed under the Beerware License : <br />
 * As long as you retain this notice you can do whatever you want with this stuff. If we meet some
 * day, and you think this stuff is worth it, you can buy me a beer in return
 */

package com.saperion.sdb.sdk.config;

import com.saperion.sdb.sdk.security.CredentialManager;

public final class WSConfig {

    String serverUrl = "";

    private WSConfig() {
        // No public constructor
        serverUrl = CredentialManager.getInstance().getServerUrl();
    }

    // absolute name of client 
    public static final String APP_PACKAGE_NAME = "com.saperion.sdb.client";

    //public static final String WS_ROOT_URL = "http://213.61.60.88:8600/fns-service/api/1/";

    public static final String WS_ROOT_URL = "https://213.61.60.88:8601/fns-service/api/1/";

    public static final String WS_API_VERSION = "/api/1/";

    //public static final String WS_ROOT_URL = "https://filenshare.com/sdb-service-rest/api/1/";

    // Authentication WS
    public static final String WS_AUTHENTICATION_URL = "token/basic";

    // SpaceList WS
    public static final String WS_SPACES = "spaces";
    // FolderList WS
    public static final String WS_FOLDER = "folders";
    // DocumentList WS
    public static final String WS_DOCUMENT = "documents";
    public static final String WS_PREVIEW = "preview";
    public static final String WS_FILE = "file";
    public static final String WS_TMP = "tmp";
    public static final String WS_THUMBNAIL = "thumbnail";
    public static final String WS_RENDITION = "rendition";
    public static final String WS_USERS = "users";
    public static final String WS_SYSTEM = "system";
    public static final String WS_LOCALIZATIONS = "localizations";
    public static final String WS_COMMENTS = "comments";
    public static final String WS_AVATAR = "avatar";
    public static final String WS_ACCOUNTS= "accounts";
    public static final String WS_SETTINGS= "settings";
    
    public static final String WS_INCLUDE_MODIFICATION_ID = "?include=modificationId";

    public static String getSpaceURL() {
        return CredentialManager.getInstance().getServerUrl() + WS_SPACES;
    }

    public static String getFolderURL() {
        return CredentialManager.getInstance().getServerUrl() + WS_FOLDER;
    }

    public static String getDocumentURL() {
        return CredentialManager.getInstance().getServerUrl() + WS_DOCUMENT;
    }

    public static String getUserUrl() {
        return CredentialManager.getInstance().getServerUrl() + WS_USERS;

    }

    public static String documentCreatePreviewUrl(String docId) {
        return WSConfig.getDocumentURL() + "/" + docId + "/" + WSConfig.WS_PREVIEW;
    }

    public static String userCreateAvatarUrl(String Id) {
        return WSConfig.getUserUrl() + "/" + Id + "/" + WSConfig.WS_AVATAR;
    }
}
