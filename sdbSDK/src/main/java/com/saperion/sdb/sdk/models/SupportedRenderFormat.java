package com.saperion.sdb.sdk.models;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.providers.SupportedFormatContent.SupportedFormatDB;
import com.saperion.sdb.sdk.utils.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;


public class SupportedRenderFormat implements Parcelable {

    private static final String TAG = SupportedRenderFormat.class.getSimpleName();
    private String extension;
    private String mimetypes;

    public SupportedRenderFormat() {
       
    }


    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
    public String getMimetypes() {
        return mimetypes;
    }

    public void setMimetypes(String mimetypes) {
        this.mimetypes = mimetypes;
    }
    


    
    public String toJson() {

        ObjectMapper mapper = new ObjectMapper();

        mapper.setSerializationInclusion(Include.NON_NULL);
        try {
            return mapper.writeValueAsString(this);

        } catch (JsonProcessingException e1) {
            e1.printStackTrace();
        }
        return null;
    }

    public static SupportedRenderFormat fromJson(String json) {

        SupportedRenderFormat supportedRenderFormat = new SupportedRenderFormat();

        try {
            JSONObject settingsObject = new JSONObject(json);

            if (settingsObject.has("version")){
                supportedRenderFormat.setExtension(settingsObject.getString("extension"));
            }

            if (settingsObject.has("mimetypes")){
                supportedRenderFormat.setMimetypes(settingsObject.getString("mimetypes"));
            }

           
        } catch (JSONException e) {

            Log.e(TAG, "error parson json" + e.toString());
            e.printStackTrace();
        }

        return supportedRenderFormat;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.getExtension());
        out.writeString(this.getMimetypes());

    }

    private SupportedRenderFormat(Parcel in) {

        this.setExtension(in.readString());
        this.setMimetypes(in.readString());
  
    }

    public static final Creator CREATOR = new Creator() {
        public SupportedRenderFormat createFromParcel(Parcel in) {
            return new SupportedRenderFormat(in);
        }

        public SupportedRenderFormat[] newArray(int size) {
            return new SupportedRenderFormat[size];
        }
    };

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public String toString() {
        return "{" + "extension=" + mimetypes + ", value='" + mimetypes  + '\'' + "} ";
    }


    public static ArrayList<SupportedRenderFormat> parseJsonArray(String wsResponse) throws DataException {

        if (wsResponse == null || TextUtils.isEmpty(wsResponse))
            return null;

        ArrayList<SupportedRenderFormat> settingList = new ArrayList<SupportedRenderFormat>();

        ObjectMapper objectMapper = new ObjectMapper();
        JsonFactory jsonFactory = new JsonFactory();

        try {
            JsonParser jp = jsonFactory.createJsonParser(wsResponse);
            // advance stream to START_ARRAY first:
            jp.nextToken();
            // and then each time, advance to opening START_OBJECT
            while (jp.nextToken() == JsonToken.START_OBJECT) {
                SupportedRenderFormat setting = objectMapper.readValue(jp, SupportedRenderFormat.class);


                // process
                // after binding, stream points to closing END_OBJECT
                settingList.add(setting);

            }

        } catch (JsonParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return settingList;
    }

    public ContentValues toContentValues() {

        ContentValues cv = new ContentValues();
        cv.put(SupportedFormatDB.Columns.EXTENSION.getName(), this.getExtension());
        cv.put(SupportedFormatDB.Columns.MINE_TYPES.getName(), this.getMimetypes());

        return cv;
    }



    public static SupportedRenderFormat fromCursor(Cursor cursor) {
        SupportedRenderFormat format = new SupportedRenderFormat();
        format.setExtension(cursor.getString(SupportedFormatDB.Columns.EXTENSION.getIndex()));
        format.setMimetypes(cursor.getString(SupportedFormatDB.Columns.MINE_TYPES.getIndex()));

        return format;
    }

}
