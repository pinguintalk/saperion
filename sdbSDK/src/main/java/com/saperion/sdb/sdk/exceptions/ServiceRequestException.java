package com.saperion.sdb.sdk.exceptions;

/**
 * Created by nnn on 23.07.13.
 */

public final class ServiceRequestException extends Exception {

    private static final long serialVersionUID = 4456730128254827562L;
    private int mStatusCode = -1;

    /**
     * Constructs a new {@link ServiceRequestException} that includes the current stack trace.
     */
    public ServiceRequestException() {
        super();
    }

    /**
     * Constructs a new {@link ServiceRequestException} that includes the current stack trace, the
     * specified detail message and the specified cause.
     *
     * @param detailMessage The detail message for this exception.
     * @param throwable     The cause of this exception.
     */
    public ServiceRequestException(final String detailMessage, final Throwable throwable) {
        super(detailMessage, throwable);
    }

    /**
     * Constructs a new {@link ServiceRequestException} that includes the current stack trace and the
     * specified detail message.
     *
     * @param detailMessage The detail message for this exception.
     */
    public ServiceRequestException(final String detailMessage) {
        super(detailMessage);
    }

    /**
     * Constructs a new {@link ServiceRequestException} that includes the current stack trace and the
     * specified detail message and the error status code
     *
     * @param detailMessage The detail message for this exception.
     * @param statusCode    The HTTP status code
     */
    public ServiceRequestException(final String detailMessage, final int statusCode) {
        super(detailMessage);
        mStatusCode = statusCode;
    }

    /**
     * Constructs a new {@link ServiceRequestException} that includes the current stack trace and the
     * specified cause.
     *
     * @param throwable The cause of this exception.
     */
    public ServiceRequestException(final Throwable throwable) {
        super(throwable);
    }

    public int getStatusCode() {
        return mStatusCode;
    }
}
