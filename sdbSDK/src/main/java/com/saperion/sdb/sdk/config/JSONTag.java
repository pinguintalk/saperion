package com.saperion.sdb.sdk.config;

public final class JSONTag {
    private JSONTag() {
        // No public constructor
    }

    // Space tags
    public static final String SPACE_TAG_ID = "id";
    public static final String SPACE_TAG_NAME = "name";
    public static final String SPACE_TAG_LAST_MODIFIED = "lastModified";
    public static final String SPACE_TAG_DESCRIPTION = "description";
    public static final String SPACE_TAG_RIGHTS = "rights";
    public static final String SPACE_TAG_STATES = "states";
    public static final String SPACE_TAG_CREATION_DATE = "creationDate";
    public static final String SPACE_TAG_TAGS = "tags";
    public static final String SPACE_TAG_OWNER_ID = "ownerId";
    public static final String SPACE_TAG_OWNER_NAME = "ownerName";
    public static final String SPACE_TAG_TYPE = "type";
    public static final String SPACE_TAG_URI = "uri";
    public static final String SPACE_TAG_FILEPATHS = "filepaths";
    public static final String SPACE_TAG_SUBFOLDERPATHS = "subfolderpaths";
    public static final String SPACE_TAG_FOLDERS = "folders";
    public static final String SPACE_TAG_CHILDREN = "children";
    // Folder tags
    public static final String FOLDER_TAG_ID = "id";
    public static final String FOLDER_TAG_NAME = "name";
    public static final String FOLDER_TAG_LAST_MODIFIED = "lastModified";
    public static final String FOLDER_TAG_DESCRIPTION = "description";
    public static final String FOLDER_TAG_PARENT_ID = "parentId";
    public static final String FOLDER_TAG_SPACE_ID = "spaceId";
    public static final String FOLDER_TAG_STATES = "states";
    public static final String FOLDER_TAG_CREATION_DATE = "creationDate";
    public static final String FOLDER_TAG_TAGS = "tags";
    public static final String FOLDER_TAG_OWNER_ID = "ownerId";
    public static final String FOLDER_TAG_OWNER_NAME = "ownerName";
    public static final String FOLDER_TAG_TYPE = "type";
    public static final String FOLDER_TAG_ID_CHAIN = "idChain";
    public static final String FOLDER_TAG_CHILDREN = "children";

    // Document tags
    public static final String DOCUMENT_TAG_ID = "id";
    public static final String DOCUMENT_TAG_NAME = "name";
    public static final String DOCUMENT_TAG_LAST_MODIFIED = "lastModified";
    public static final String DOCUMENT_TAG_DESCRIPTION = "description";
    public static final String DOCUMENT_TAG_PARENT_ID = "parentId";
    public static final String DOCUMENT_TAG_SPACE_ID = "spaceId";
    public static final String DOCUMENT_TAG_STATES = "states";
    public static final String DOCUMENT_TAG_CREATION_DATE = "creationDate";
    public static final String DOCUMENT_TAG_TAGS = "tags";
    public static final String DOCUMENT_TAG_OWNER_ID = "ownerId";
    public static final String DOCUMENT_TAG_OWNER_NAME = "ownerName";
    public static final String DOCUMENT_TAG_TYPE = "type";
    public static final String DOCUMENT_TAG_SIZE = "size";
    public static final String DOCUMENT_TAG_MINE_TYPE = "mimeType";
    public static final String DOCUMENT_TAG_HASH = "hash";
    public static final String DOCUMENT_TAG_PAGES = "pages";
    public static final String DOCUMENT_TAG_ID_CHAIN = "idChain";
    public static final String DOCUMENT_TAG_VERSION_NUMBER = "versionNumber";
    public static final String DOCUMENT_TAG_FILE_URI = "fileURI";
    public static final String DOCUMENT_TAG_PREVIEW_URI = "previewURI";

    // User tags
    public static final String USER_TAG_GUEST = "guest";
    public static final String USER_TAG_INRECYCLEBIN = "inRecycleBin";

    // Authentication
    public static final String AUTHENTICATION_ACCES_TOKEN = "token";
    public static final String AUTHENTICATION_ACCES_TOKEN_TYPE = "type";
    public static final String AUTHENTICATION_ACCES_TOKEN_EXPIRES = "expires";

}
