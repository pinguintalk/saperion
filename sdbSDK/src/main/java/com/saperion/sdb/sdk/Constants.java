package com.saperion.sdb.sdk;


public final class Constants {

    public static final int MAX_REQUEST = 2;
    public static final String SDK_PACKAGE_NAME = "com.saperion.sdb.sdk";
    public static final String SPACE_TYPE = "space";
    public static final String FOLDER_TYPE = "folder";
    public static final String DOCUMENT_TYPE = "document";

    public static final String PREVIEW_EXTENSION = "png";
    public static final String AVATAR_EXTENSION = "png";
    public static final String BOUNDARY = "********AndroidFileAndShareClient2013";
}
