package com.saperion.sdb.sdk.rights;

/**
 * The class SpaceRight.
 */

public enum SpaceRight {
    READ,
    WRITE,
    SHARE;
}
