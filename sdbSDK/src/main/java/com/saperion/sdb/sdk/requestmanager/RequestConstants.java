package com.saperion.sdb.sdk.requestmanager;

/**
 * Class used to create the {@link Request}s.
 *
 * @author nnn, adr
 */
public final class RequestConstants {

    // Request types
    public static final int REQUEST_TYPE_SYNC_SPACE_LIST = 10;
    public static final int REQUEST_TYPE_GET_ACCESS_TOKEN = 170;
    public static final int REQUEST_TYPE_GET_LOCALIZAION = 150;
    public static final int REQUEST_TYPE_GET_SYSTEM_INFOS = 160;
    public static final int REQUEST_TYPE_GET_LANGUAGE = 140;
    
    public static final int REQUEST_TYPE_ITEM_UPDATE = 180;
    public static final int REQUEST_TYPE_ITEM_CREATE = 190;
    public static final int REQUEST_TYPE_ITEM_DELETE = 100;
    public static final int REQUEST_TYPE_GET_CHILDREN = 110;

    // space
    public static final int REQUEST_TYPE_SPACE_GET_LIST = 300;
    public static final int REQUEST_TYPE_SPACE_GET_CHILDREN = 320;
    public static final int REQUEST_TYPE_SPACE_GET_SUB_FOLDERS = 330;
    public static final int REQUEST_TYPE_SPACE_DELETE = 340;
    public static final int REQUEST_TYPE_SPACE_CREATE = 350;
    public static final int REQUEST_TYPE_SPACE_UPDATE = 360;
    public static final int REQUEST_TYPE_SPACE_GET_DOCUMENTS = 370;
    public static final int REQUEST_TYPE_SPACE_DELETE_ALL = 399;

    // folder
    public static final int REQUEST_TYPE_FOLDER_CREATE = 400;
    public static final int REQUEST_TYPE_FOLDER_UPDATE = 410;
    public static final int REQUEST_TYPE_FOLDER_DELETE = 420;
    public static final int REQUEST_TYPE_FOLDER_GET_SUB_FOLDERS = 430;
    public static final int REQUEST_TYPE_FOLDER_GET_DOCUMENT_LIST = 440;
    public static final int REQUEST_TYPE_FOLDER_GET_PREVIEWS = 450;
    public static final int REQUEST_TYPE_FOLDER_GET_CHILDREN = 490;

    // document
    public static final int REQUEST_TYPE_DOCUMENT_CREATE = 500;
    public static final int REQUEST_TYPE_DOCUMENT_CREATE_WITHOUT_CONTENT = 501;
    public static final int REQUEST_TYPE_DOCUMENT_UPDATE = 510;
    public static final int REQUEST_TYPE_DOCUMENT_DELETE = 520;
    public static final int REQUEST_TYPE_DOCUMENT_GET_PREVIEW = 530;
    public static final int REQUEST_TYPE_DOCUMENT_DOWNLOAD = 540;
    public static final int REQUEST_TYPE_DOCUMENT_DOWNLOAD_FILE_BY_VERSION = 541;
    public static final int REQUEST_TYPE_DOCUMENT_GET_FILE = 550;
    public static final int REQUEST_TYPE_DOCUMENT_GET_FILE_BY_VERSION = 551;
    public static final int REQUEST_TYPE_DOCUMENT_GET_VERSIONS = 560;
    public static final int REQUEST_TYPE_DOCUMENT_GET_ACTIVITIES = 562;
    public static final int REQUEST_TYPE_DOCUMENT_ADD_COMMENT = 591;
    public static final int REQUEST_TYPE_DOCUMENT_GET_COMMENTS = 590;
    public static final int REQUEST_TYPE_DOCUMENT_GET_RENDITION_BY_VERSION = 570;
    public static final int REQUEST_TYPE_DOCUMENT_DOWNLOAD_RENDITION_BY_VERSION = 571;
    public static final int REQUEST_TYPE_DOCUMENT_RESTORE_VERSION = 580;

    
    // comment
    public static final int REQUEST_TYPE_COMMENT_ADD = 600;
    public static final int REQUEST_TYPE_COMMENT_DELETE = 602;
    public static final int REQUEST_TYPE_COMMENT_UPDATE = 601;
    
    // user
    public static final int REQUEST_TYPE_USER_GET_LIST = 700;
    public static final int REQUEST_TYPE_USER_DELETE = 701;
    public static final int REQUEST_TYPE_USER_CREATE = 702;
    public static final int REQUEST_TYPE_USER_GET_AVATAR = 703;
    public static final int REQUEST_TYPE_USER_UPDATE = 704;
    public static final int REQUEST_TYPE_USER_GET_BY_ID = 705;
    public static final int REQUEST_TYPE_USER_GET_CURRENT = 706;
    public static final int REQUEST_TYPE_USER_SET_AVATAR =  707;
    public static final int REQUEST_TYPE_USER_DELETE_AVATAR = 708;
    public static final int REQUEST_TYPE_USER_UPDATE_PASSWORD = 709;
    public static final int REQUEST_TYPE_USER_RECYCLE = 710;

    //links
    public static final int REQUEST_TYPE_DOCUMENT_GET_LINKS = 800;
    public static final int REQUEST_TYPE_DOCUMENT_CREATE_LINK = 801;
    public static final int REQUEST_TYPE_DOCUMENT_DELETE_LINK = 802;
    public static final int REQUEST_TYPE_DOCUMENT_GET_LINK = 803;
    
    //others
    public static final int REQUEST_TYPE_ACCOUNT_GET = 900;
    public static final int REQUEST_TYPE_ACCOUNT_GET_SETTINGS = 901;


    //shares
    public static final int  REQUEST_TYPE_SHARE_CREATE = 1000;
    public static final int  REQUEST_TYPE_SHARE_GET_LIST = 1002;
    public static final int  REQUEST_TYPE_SHARE_DELETE = 1003;

    public static final int REQUEST_TYPE_SHARE_UPDATE= 1004;
    private RequestConstants() {
        // no public constructor
    }

}
