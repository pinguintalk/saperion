package com.saperion.sdb.sdk.operations;

import android.content.Context;
import android.os.Bundle;

import com.saperion.sdb.sdk.Constants;
import com.saperion.sdb.sdk.config.WSConfig;
import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.models.Document;
import com.saperion.sdb.sdk.network.NetworkConnection.ConnectionResult;
import com.saperion.sdb.sdk.providers.ContentProviderHelpers;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.service.RequestService.Operation;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

public final class DocumentGetPreviewOperation implements Operation {

    private static final String TAG = DocumentGetPreviewOperation.class.getSimpleName();

    public static final String PARAM_DOCUMENT = Constants.SDK_PACKAGE_NAME + ".param_doc";

    @Override
    public Bundle execute(Context context, Request request) throws ServiceRequestException, ConnectionException,
            DataException {
        Bundle bundle = new Bundle();
        Log.v(TAG, Utils.getMethodName() + "entry");
        Document cached_document = (Document) request.getParcelable(PARAM_DOCUMENT);

        ConnectionResult result =
                OperationHelpers.documentGetPreview(context, WSConfig.documentCreatePreviewUrl(cached_document.getId()),
                        OperationHelpers.documentCreatePreviewPathWithExtension(context, cached_document.getId()));

        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);
        Log.w(TAG, Utils.getMethodName() + " responseCode:" + result.responseCode);
        
        if (result.responseCode != ResponseConstants.RESPONSE_CODE_OK){
            
            bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);

            return bundle;
        }

        //TODO
        //bundle.putByte(, value);

        ContentProviderHelpers.documentUpdatePreviewPath(context, cached_document, result.body);
        bundle.putParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, cached_document);
        return bundle;
    }

}
