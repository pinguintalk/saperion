/**
 * 2011 Foxykeep (http://datadroid.foxykeep.com)
 * <p>
 * Licensed under the Beerware License : <br />
 * As long as you retain this notice you can do whatever you want with this stuff. If we meet some
 * day, and you think this stuff is worth it, you can buy me a beer in return
 */

package com.saperion.sdb.sdk.requestmanager;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.ResultReceiver;
import android.support.v4.util.LruCache;

import com.saperion.sdb.sdk.Constants;
import com.saperion.sdb.sdk.service.RequestService;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

import org.apache.http.HttpStatus;

import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

/**
 * This class allows to send requests through a {@link RequestService}.
 * <p/>
 * This class needs to be subclassed in your project.
 * <p/>
 * You can check the following page to see a tutorial on how to implement a webservice call using
 * the {@link RequestManager} : <a
 * href="http://www.datadroidlib.com/installation">http://www.datadroidlib.com/installation</a>.
 *
 * @author Foxykeep
 */
public abstract class RequestManager {

    private static final String TAG = RequestManager.class.getSimpleName();

    private static final String REQUEST = "request";
    private static final String REQUEST_RECEIVER = "request_receiver";
    private static final String REQUEST_STATE_SENT = "request_state_sent";
    /**
     * Clients may implements this interface to be notified when a request is finished.
     *
     * @author Foxykeep, nnn
     */
    public static interface RequestListener extends EventListener {

        /**
         * Event fired when a request is finished.
         *
         * @param request    The {@link Request} defining the request.
         * @param resultData The result of the service execution.
         */
        public void onRequestFinished(Request request, Bundle resultData);

        /**
         * Event fired when a request encountered a connection error.
         *
         * @param request    The {@link Request} defining the request.
         * @param statusCode The HTTP status code returned by the server (if the request succeeded
         *                   by the HTTP status code was not {@link HttpStatus#SC_OK}) or -1 if it was a
         *                   connection problem
         */
        public void onRequestConnectionError(Request request, int statusCode);

        /**
         * Event fired when a request encountered a data error.
         *
         * @param request The {@link Request} defining the request.
         */
        public void onRequestDataError(Request request);

        /**
         * Event fired when a request encountered a custom error.
         *
         * @param request    The {@link Request} defining the request.
         * @param resultData The result of the service execution.
         */
        public void onRequestServiceError(Request request, Bundle resultData);
    }

    public static final String RECEIVER_EXTRA_ERROR_TYPE = "com.saperion.sdb.sdk.extra.error";

    public static final int ERROR_TYPE_CONNEXION = 1;
    public static final int ERROR_TYPE_DATA = 2;
    public static final int ERROR_TYPE_SERVICE = 3;
    private final Context mContext;

  
    private final Class<? extends RequestService> mRequestService;
    // the map for periodic process e.g. update login token or synchronizing process
    private final LinkedHashMap<Request, RequestReceiver> highPrioRequestMap; 
    private final LinkedHashMap<Request, RequestReceiver> defaultRequestMap;
    private final LinkedHashMap<Request, RequestReceiver> lowRequestMap; // the map is ordered
    private final LruCache<Request, Bundle> mMemoryCache;
    private LinkedList<Map<String, Object>> normalRequestQueue = null;

   
    private int requestCounter = 0; // init number of current concurrent requests

    protected RequestManager(Context context, Class<? extends RequestService> requestService) {
        mContext = context.getApplicationContext();

        mRequestService = requestService;
        highPrioRequestMap = new LinkedHashMap<Request, RequestReceiver>();
        defaultRequestMap = new LinkedHashMap<Request, RequestReceiver>();
        lowRequestMap = new LinkedHashMap<Request, RequestReceiver>();
        mMemoryCache = new LruCache<Request, Bundle>(30);
        normalRequestQueue = new LinkedList<Map<String, Object>>();
    }

    /**
     * Add a {@link RequestListener} to this {@link RequestManager} to a specific {@link Request}.
     * Clients may use it in order to be notified when the corresponding request is completed.
     * <p/>
     * The listener is automatically removed when the request is completed and they are notified.
     * <p/>
     * <b>Warning !! </b> If it's an {@link Activity} or a {@link Fragment} that is used as a
     * listener, it must be detached when {@link Activity#onPause} is called in an {@link Activity}.
     *
     *  @param requestReceiverMap The LinkedHashMap .
     * @param listener The listener called when the Request is completed.
     * @param request  The {@link Request} to listen to.
     */
    public final void addRequestListener(LinkedHashMap<Request,RequestReceiver> requestReceiverMap,RequestListener listener, Request request) {
        if (listener == null) {
            return;
        }
        if (request == null) {
            throw new IllegalArgumentException("Request cannot be null.");
        }

        RequestReceiver requestReceiver = requestReceiverMap.get(request);
        if (requestReceiver == null) {
            Log.w(TAG, "You tried to add a listener to a non-existing request.");
            return;
        }

        requestReceiver.addListenerHolder(new ListenerHolder(listener));
    }

    /**
     * Remove a {@link RequestListener} to this {@link RequestManager} createInstance every {@link Request}s
     * which it is listening to.
     *
     * @param listener The listener to remove.
     */
    public final void removeRequestListener(RequestListener listener) {
        removeRequestListener(listener, null);
    }

    /**
     * Remove a {@link RequestListener} to this {@link RequestManager} createInstance a specific
     * {@link Request}.
     *
     * @param listener The listener to remove.
     * @param request  The {@link Request} associated with this listener. If null, the listener will
     *                 be removed createInstance every request it is currently associated with.
     */
    public final void removeRequestListener(RequestListener listener, Request request) {
        if (listener == null) {
            return;
        }
        ListenerHolder holder = new ListenerHolder(listener);
        if (request != null) {
            RequestReceiver requestReceiver = defaultRequestMap.get(request);
            if (requestReceiver != null) {
                requestReceiver.removeListenerHolder(holder);
            }
        } else {
            for (RequestReceiver requestReceiver : defaultRequestMap.values()) {
                requestReceiver.removeListenerHolder(holder);
            }
        }
    }

    /**
     * All requests in the queues with normal and lower priorities will removed. 
     */
    public final void stopAllDefaultAndLowRequests() {

        if (!defaultRequestMap.entrySet().isEmpty()) {
            defaultRequestMap.entrySet().clear();
            Log.i(TAG, Utils.getMethodName() + "size after cleared requests = " + defaultRequestMap.entrySet().size());
        }

        if (!lowRequestMap.entrySet().isEmpty()) {
            lowRequestMap.entrySet().clear();
            Log.i(TAG, Utils.getMethodName() + "2 size after cleared requests = " + defaultRequestMap.entrySet().size());
        }
    }

    /**
     * Return whether a {@link Request} is still in progress or not.
     *
     * @param request The request.
     * @return Whether the request is still in progress or not.
     */
    public final boolean isRequestInProgress(Request request) {
        return defaultRequestMap.containsKey(request);
    }

    /**
     * Call the given listener <b>synchronously</b> with the memory cached data corresponding to the
     * request.
     * <p/>
     * The method called in the listener will be
     * {@link RequestListener#onRequestFinished(Request, Bundle)}.
     * <p/>
     * If no cached data is found, {@link RequestListener#onRequestConnectionError(Request, int)}
     * will be called instead
     *
     * @param listener The listener to call with the data if any.
     * @param request  The request associated with the memory cached data.
     */
    public final void callListenerWithCachedData(RequestListener listener, Request request) {
        if (request == null) {
            throw new IllegalArgumentException("Request cannot be null.");
        }
        if (listener == null) {
            return;
        }

        if (request.isMemoryCacheEnabled()) {
            Bundle bundle = mMemoryCache.get(request);
            if (bundle != null) {
                listener.onRequestFinished(request, bundle);
            } else {
                listener.onRequestConnectionError(request, -1);
            }
        }
    }

   

    /**
     * Execute the {@link Request}.
     *
     * @param request  The request to execute.
     * @param listener The listener called when the Request is completed.
     */
    public void execute(Request request, RequestListener listener) {
        if (request == null) {
            throw new IllegalArgumentException("Request cannot be null.");
        }

        if (listener == null) {
            throw new IllegalArgumentException("Listener cannot be null.");
        }
        
        
        addRequest(request,listener);

        // informs msgHandler to send next request
        msgHandler.sendEmptyMessage(0);
    }

    private void addRequest(Request request, RequestListener listener){
        // sort request priority
        int requestPriority = request.getPriority();
        RequestReceiver requestReceiver = new RequestReceiver(request);
        switch (requestPriority) {

            case Request.PRIORITY_LEVEL_LOW:
                if (lowRequestMap.containsKey(request)) {
                    Log.v(TAG, "This request is already in progress. Adding the new listener to it.");

                    // This exact request is already in progress. Adding the new listener.
                    addRequestListener(lowRequestMap, listener, request);
                    // Just check if the new request has the memory cache enabled.
                    if (request.isMemoryCacheEnabled()) {
                        // If true, enable it in the RequestReceiver (if it's not the case already)
                        lowRequestMap.get(request).enableMemoryCache();
                    }
                    return;
                }
                lowRequestMap.put(request, requestReceiver);

                addRequestListener(lowRequestMap, listener, request);
                break;
            case Request.PRIORITY_LEVEL_HIGH:
                if (highPrioRequestMap.containsKey(request)) {
                    Log.v(TAG, "This request is already in highPrioRequestMap. Adding the new listener to it.");

                    // This exact request is already in progress. Adding the new listener.
                    addRequestListener(highPrioRequestMap, listener, request);
                    // Just check if the new request has the memory cache enabled.
                    if (request.isMemoryCacheEnabled()) {
                        // If true, enable it in the RequestReceiver (if it's not the case already)
                        highPrioRequestMap.get(request).enableMemoryCache();
                    }
                    return;
                }
                highPrioRequestMap.put(request, requestReceiver);

                addRequestListener(highPrioRequestMap, listener, request);
                break;
            default:
                if (defaultRequestMap.containsKey(request)) {
                    Log.i(TAG, "This request nr.=" + request.hashCode() + " is already in progress. Adding the new listener to it.");

                    // This exact request is already in progress. Adding the new listener.
                    addRequestListener(defaultRequestMap, listener, request);
                    // Just check if the new request has the memory cache enabled.
                    if (request.isMemoryCacheEnabled()) {
                        // If true, enable it in the RequestReceiver (if it's not the case already)
                        defaultRequestMap.get(request).enableMemoryCache();
                    }
                    return;
                }

                
                defaultRequestMap.put(request, requestReceiver);

                addRequestListener(defaultRequestMap, listener, request);

                /*
                // build new requestMap and add to our request queue
                Map<String,Object> requestMap = new HashMap<String,Object>();
                requestMap.put(REQUEST,request);
                requestMap.put(REQUEST_RECEIVER,requestReceiver);
                requestMap.put(REQUEST_STATE_SENT,false);
                normalRequestQueue.add(requestMap);
                break;
                */
        }
    }
    
    
    private void updateRequestReceiverMap(Request request, RequestReceiver receiver){
        if (highPrioRequestMap.containsKey(request)) {
            highPrioRequestMap.put(request,receiver);
            return;
        }
        
        if (defaultRequestMap.containsKey(request)) {
            defaultRequestMap.put(request,receiver);
            return;
        }

        if (lowRequestMap.containsKey(request)) {
            lowRequestMap.put(request,receiver);       
        }
    }
    private void removeRequest(Request request){

        defaultRequestMap.remove(request);
        lowRequestMap.remove(request);
        highPrioRequestMap.remove(request);

    }

    private void sendNextRequest(){

        Request request = null;
        RequestReceiver requestReceiver = null;

        // send high prio requests first
        for (Map.Entry<Request,RequestReceiver> entry : highPrioRequestMap.entrySet()) {
            // start next request
            request = entry.getKey();
            requestReceiver = entry.getValue();

            if (request==null)
                return;

            if (requestReceiver==null)
                return;

            if (requestCounter >= Constants.MAX_REQUEST)
                return;

            // if the request is not sent before
            if (!requestReceiver.isWaitingForResponse){
                // mark as waiting for response for updating later
                requestReceiver.setWaitingForResponse(true);

                Intent intent = new Intent(mContext, mRequestService);
                intent.putExtra(RequestService.INTENT_EXTRA_RECEIVER, requestReceiver);
                intent.putExtra(RequestService.INTENT_EXTRA_REQUEST, request);

                highPrioRequestMap.put(request,requestReceiver);
                
                requestCounter++;
                Log.w(TAG, Utils.getMethodName() + "requestCounter = " + requestCounter + " sending request"
                        + request.hashCode() +
                        " type = " + request.getRequestType());
                // put request to service
                mContext.startService(intent);
                return;
            }
        }
/*
        if (request != null){
            // and update request again
            highPrioRequestMap.put(request,requestReceiver);
        }
        */

        for (Map.Entry<Request,RequestReceiver> entry : defaultRequestMap.entrySet()) {
            // start next request
            request = entry.getKey();
            requestReceiver = entry.getValue();

            if (request==null)
                return;

            if (requestReceiver==null)
                return;

            if (requestCounter >= Constants.MAX_REQUEST)
                return;

            // if the request is not sent before
            if (!requestReceiver.isWaitingForResponse){
                // mark as waiting for response for updating later
                requestReceiver.setWaitingForResponse(true);
                // and update request again
                defaultRequestMap.put(request,requestReceiver);
                Intent intent = new Intent(mContext, mRequestService);
                intent.putExtra(RequestService.INTENT_EXTRA_RECEIVER, requestReceiver);
                intent.putExtra(RequestService.INTENT_EXTRA_REQUEST, request);

                Log.i(TAG, Utils.getMethodName() + "sending request: " + request.hashCode() +
                        " type = " + request.getRequestType());
                requestCounter++;
                // put request to service
                mContext.startService(intent);
                return;
            }
        }
        
        // then try to send next request createInstance request map with lower priority
       // if (lowRequestMap.entrySet().iterator().hasNext()) {
        for (Map.Entry<Request,RequestReceiver> entry : lowRequestMap.entrySet()) {
            // start next request
            request = entry.getKey();
            requestReceiver = entry.getValue();

            if (request==null)
                return;

            if (requestReceiver==null)
                return;

            if (requestCounter >= Constants.MAX_REQUEST)
                return;

            // if the request is not sent before
            if (!requestReceiver.isWaitingForResponse){
                // mark as waiting for response for updating later
                requestReceiver.setWaitingForResponse(true);
                // and update request again
                lowRequestMap.put(request,requestReceiver);
                
                Intent intent = new Intent(mContext, mRequestService);
                intent.putExtra(RequestService.INTENT_EXTRA_RECEIVER, requestReceiver);
                intent.putExtra(RequestService.INTENT_EXTRA_REQUEST, request);

                Log.i(TAG, Utils.getMethodName() + "sending request: " + request.hashCode() +
                    " type = " + request.getRequestType());
                requestCounter++;
                // put request to service
                mContext.startService(intent);
                return;
            }
        }
        
    }
    


    private final class RequestReceiver extends ResultReceiver {

        private final Request mRequest;
        private final Set<ListenerHolder> mListenerHolderSet;
        private boolean mMemoryCacheEnabled;
        private boolean isWaitingForResponse;

        public boolean isWaitingForResponse() {
            return isWaitingForResponse;
        }

        public void setWaitingForResponse(boolean waitingForResponse) {
            isWaitingForResponse = waitingForResponse;
        }


        /* package */RequestReceiver(Request request) {
            super(new Handler(Looper.getMainLooper()));

            mRequest = request;
            mListenerHolderSet = Collections.synchronizedSet(new HashSet<ListenerHolder>());
            mMemoryCacheEnabled = request.isMemoryCacheEnabled();

            // Clear the old memory cache if any
            mMemoryCache.remove(request);

            isWaitingForResponse = false;
        }

        /* package */void enableMemoryCache() {
            mMemoryCacheEnabled = true;
        }

        /* package */void addListenerHolder(ListenerHolder listenerHolder) {
            synchronized (mListenerHolderSet) {
                mListenerHolderSet.add(listenerHolder);
            }
        }

        /* package */void removeListenerHolder(ListenerHolder listenerHolder) {
            synchronized (mListenerHolderSet) {
                mListenerHolderSet.remove(listenerHolder);
            }
        }

        // the request is completed
        @Override
        public void onReceiveResult(int resultCode, Bundle resultData) {
 
            if (mMemoryCacheEnabled) {
                mMemoryCache.put(mRequest, resultData);
            }

            int statusCode =
                    resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);
            
            Log.i(TAG, Utils.getMethodName() + "request nr. = " + mRequest.hashCode() + 
                    " type = " + mRequest.getRequestType() + " resultCode= " + resultCode 
                    + " statusCode = " + statusCode);

            Log.w(TAG, Utils.getMethodName() + "statusCode = " + statusCode);

            requestCounter--;
            // we handle extra the status code "to many request" message
            // the request will not moved createInstance msg queue
            // this request sent again !
            if (statusCode == ResponseConstants.RESPONSE_CODE_TOO_MANY_REQUESTS) {
                Log.w(TAG, Utils.getMethodName() + "RESPONSE_CODE_TOO_MANY_REQUESTS ! ");
                // informs msgHandler to send next request
                msgHandler.sendEmptyMessage(0);
                return;
            }
            /*
            // access token is expired, so we have to get new access token e.g after resume
            // and we have to execute this request later again
            if (statusCode == ResponseConstants.RESPONSE_CODE_UNAUTHORIZED) {
                Log.w(TAG, Utils.getMethodName() + "RESPONSE_CODE_UNAUTHORIZED ! ");
               
                // set state of this request back and updating request
                isWaitingForResponse = false;
                 
                // and update request map
                updateRequestReceiverMap(mRequest,this);
                               
                // Call the available listeners to handle response
                synchronized (mListenerHolderSet) {
                    for (ListenerHolder listenerHolder : mListenerHolderSet) {
                        listenerHolder.onRequestFinished(mRequest, resultCode, resultData);
                    }
                }
                

                // informs msgHandler to send next request
                msgHandler.sendEmptyMessage(0);
                return;
            }     
                 */
            
            removeRequest(mRequest);

            // informs msgHandler to send next request
            msgHandler.sendEmptyMessage(0);
            
            // Call the available listeners to handle response
            synchronized (mListenerHolderSet) {
                for (ListenerHolder listenerHolder : mListenerHolderSet) {
                    listenerHolder.onRequestFinished(mRequest, resultCode, resultData);
                }
            }
        }
    }

    Handler msgHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            sendNextRequest();

        }
    };

    private final class ListenerHolder {

        private final WeakReference<RequestListener> mListenerRef;
        private final int mHashCode;

        ListenerHolder(RequestListener listener) {
            mListenerRef = new WeakReference<RequestListener>(listener);
            mHashCode = 31 + listener.hashCode();
        }

        void onRequestFinished(Request request, int resultCode, Bundle resultData) {

            int statusCode =
                    resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);
            
            // handle response createInstance last request
            RequestListener listener = mListenerRef.get();
            if (listener != null) {
                if (resultCode == RequestService.ERROR_CODE) {
                    switch (resultData.getInt(RECEIVER_EXTRA_ERROR_TYPE)) {
                        case ERROR_TYPE_DATA:
                            listener.onRequestDataError(request);
                            break;
                        case ERROR_TYPE_CONNEXION:

                            listener.onRequestConnectionError(request, statusCode);
                            break;
                        case ERROR_TYPE_SERVICE:

                            listener.onRequestServiceError(request, resultData);
                            break;
                    }
                } else {
                    listener.onRequestFinished(request, resultData);
                }
            }
      }

        @Override
        public boolean equals(Object o) {
            if (o instanceof ListenerHolder) {
                ListenerHolder oHolder = (ListenerHolder) o;
                return mListenerRef != null && oHolder.mListenerRef != null
                        && mHashCode == oHolder.mHashCode;
            }
            return false;
        }

        @Override
        public int hashCode() {
            return mHashCode;
        }
    }
}
