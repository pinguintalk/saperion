package com.saperion.sdb.sdk.models;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.saperion.sdb.sdk.factory.UserJsonFactory;
import com.saperion.sdb.sdk.operations.UserDeleteAvatarOperation;
import com.saperion.sdb.sdk.operations.UserDeleteOperation;
import com.saperion.sdb.sdk.operations.UserGetAvatarOperation;
import com.saperion.sdb.sdk.operations.UserRecycleOperation;
import com.saperion.sdb.sdk.operations.UserSaveOperation;
import com.saperion.sdb.sdk.operations.UserSetAvatarOperation;
import com.saperion.sdb.sdk.providers.UserContent.UserDB;
import com.saperion.sdb.sdk.providers.UserContent.UserDB.Columns;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.RequestConstants;
import com.saperion.sdb.sdk.requestmanager.RequestManager.RequestListener;
import com.saperion.sdb.sdk.requestmanager.RequestManagerSingleton;
import com.saperion.sdb.sdk.rights.UserRight;
import com.saperion.sdb.sdk.states.UserState;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;
import java.util.EnumSet;

@JsonIgnoreProperties({ "avatarAbsPath","recycled"}) 

public class User extends TypedIdentifiable implements Parcelable {

    private String displayname;

    private String userclassid = "";
    private String firstname;
    private String lastname;
    private String email;
    private String company;
    private EnumSet<UserRight> rights;
    private StateHolder<UserState> stateHolder;
    private String avatarAbsPath;

    public User() {
        super(ModelType.USER);
        rights = EnumSet.noneOf(UserRight.class);
        this.stateHolder = new StateHolder<UserState>(EnumSet.noneOf(UserState.class));
    }

    public String getUserclassid() {
        return userclassid;
    }

    public void setUserclassid(String userclassid) {
        this.userclassid = userclassid;
    }

    public String getDisplayname() {
        return displayname;
    }

    public User setDisplayname(String displayname) {
        this.displayname = displayname;
        return this;
    }

    public String getFirstname() {
        return firstname;
    }

    public User setFirstname(String firstname) {
        this.firstname = firstname;
        return this;
    }

    public String getLastname() {
        return lastname;
    }

    public User setLastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getCompany() {
        return company;
    }

    public User setCompany(String company) {
        this.company = company;
        return this;
    }



    public User setRights(Collection<UserRight> userRights) {
        if (userRights == null || userRights.isEmpty()) {
            this.rights = EnumSet.noneOf(UserRight.class);
        } else {
            this.rights = EnumSet.copyOf(userRights);
        }
        return this;
    }

    public User addRight(UserRight right) {
        rights.add(right);
        return this;
    }

    public User removeRight(UserRight right) {
        rights.remove(right);
        if (rights == null)
            rights = EnumSet.noneOf(UserRight.class);
        return this;
    }

    public Collection<UserRight> getRights() {
        return this.rights;
    }

    public User addState(UserState state) {
        this.stateHolder.addState(state);
        return this;
    }

    public User removeState(UserState state) {
        this.stateHolder.removeState(state);
        return this;
    }

    public Collection<UserState> getStates() {
        return this.stateHolder.getStates();
    }

    public boolean isRecycled() {
        return stateHolder.getStates().toString().toLowerCase().contains(UserState.RECYCLED.toString().toLowerCase());
    }
    
    public User setStates(Collection<UserState> states) {
        this.stateHolder.setStates(states);
        return this;
    }

    public void getAvatar(RequestListener listener) {

        Request request = new Request(RequestConstants.REQUEST_TYPE_USER_GET_AVATAR);
        request.setPriority(Request.PRIORITY_LEVEL_LOW);
        request.put(UserGetAvatarOperation.PARAM_USER, this);

        if (RequestManagerSingleton.isCreated() && listener != null)
            RequestManagerSingleton.getInstance().execute(request, listener);

    }

    public void setAvatar(RequestListener listener,String avatarAbsPath) {

        Request request = new Request(RequestConstants.REQUEST_TYPE_USER_SET_AVATAR);
        request.put(UserSetAvatarOperation.PARAM_USER, this);
        request.put(UserSetAvatarOperation.PARAM_AVATAR_PATH, avatarAbsPath);

        if (RequestManagerSingleton.isCreated() && listener != null)
            RequestManagerSingleton.getInstance().execute(request, listener);

    }
    
    public void deleteAvatar(RequestListener listener) {

        Request request = new Request(RequestConstants.REQUEST_TYPE_USER_DELETE_AVATAR);
        request.put(UserDeleteAvatarOperation.PARAM_USER, this);

        if (RequestManagerSingleton.isCreated() && listener != null)
            RequestManagerSingleton.getInstance().execute(request, listener);

    }

    public void delete(RequestListener listener) {

        Request request = new Request(RequestConstants.REQUEST_TYPE_USER_DELETE);
        request.put(UserDeleteOperation.PARAM_ITEM_ID, this.getId());

        if (RequestManagerSingleton.isCreated() && listener != null)
            RequestManagerSingleton.getInstance().execute(request, listener);

    }
    

    public void recycle(RequestListener listener) {
        Request request = new Request(RequestConstants.REQUEST_TYPE_USER_RECYCLE);
        request.put(UserRecycleOperation.PARAM_ITEM_ID, this.getId());
        this.addState(UserState.RECYCLED);
        String payload = UserJsonFactory.toJson(this);

        request.put(UserRecycleOperation.PARAM_PAYLOAD,payload);
        if (RequestManagerSingleton.isCreated() && listener != null)
            RequestManagerSingleton.getInstance().execute(request, listener);
    }

    public void save(RequestListener listener) {


        Request request = new Request(RequestConstants.REQUEST_TYPE_USER_CREATE);
      
        String payload = "";
        // is exists so update user
        if (!TextUtils.isEmpty(this.id)) {
            request = new Request(RequestConstants.REQUEST_TYPE_USER_UPDATE);
            payload = UserJsonFactory.toJson(this);
                        
        } else {
            String displayName = this.getDisplayname();
            if (displayName !=null && displayName.length() < 1)
                displayName = this.getEmail();
            JSONObject jo = new JSONObject();
            try {
                jo.put(Columns.TYPE.getName(), this.getType());
                
                jo.put(Columns.DISPLAY_NAME.getName(), displayName);
                jo.put(Columns.FIRST_NAME.getName(), this.getFirstname());
                jo.put(Columns.LAST_NAME.getName(), this.getLastname());
                jo.put(Columns.EMAIL.getName(), this.getEmail());
                JSONArray ja = new JSONArray();
                for (UserRight right:this.getRights()) {
                    ja.put(right.toString());
                }
                
                jo.put(Columns.RIGHTS.getName(), ja);
                payload =jo.toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        
        request.put(UserSaveOperation.PARAM_USER, this);
        request.put(UserSaveOperation.PARAM_PAYLOAD,payload);
        if (RequestManagerSingleton.isCreated())
            RequestManagerSingleton.getInstance().execute(request, listener);
    }

    
    
    @Override
    public String toString() {
        return "User{" + "name='" + lastname + '\'' + ", email='" + email + '\'' + ", company='"
                + company + '\'' + ", rights=" + rights + ", states=" + getStates() + "} "
                + super.toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }


    public ContentValues toContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(UserDB.Columns.ID.getName(), this.getId());
        cv.put(UserDB.Columns.TYPE.getName(), this.getType());
        cv.put(UserDB.Columns.USER_CLASS_ID.getName(), this.getUserclassid());
        cv.put(UserDB.Columns.DISPLAY_NAME.getName(), this.getDisplayname());
        cv.put(UserDB.Columns.COMPANY.getName(), this.getCompany());
        cv.put(UserDB.Columns.FIRST_NAME.getName(), this.getFirstname());
        cv.put(UserDB.Columns.LAST_NAME.getName(), this.getLastname());
        cv.put(UserDB.Columns.EMAIL.getName(), this.getEmail());
        cv.put(UserDB.Columns.AVATAR_PATH.getName(), this.getAvatarAbsPath());
        
        String rights = "";
        for (UserRight userRight : this.getRights()) {
            if (!TextUtils.isEmpty(rights)) {
                rights = rights + "," + userRight.toString();
            } else
                rights = userRight.toString();
        }

        cv.put(UserDB.Columns.RIGHTS.getName(), rights);

        String states = "";
        for (UserState userStates : this.getStates()) {
            if (!TextUtils.isEmpty(states)) {
                states = states + "," + userStates.toString();
            } else
                states = userStates.toString();
        }

        cv.put(UserDB.Columns.STATES.getName(), states);
       
        
        return cv;
    }

    public static User fromCursor(Cursor cursor) {
        User user = new User();

        user.setId(cursor.getString(UserDB.Columns.ID.getIndex()));
        user.setType(cursor.getString(UserDB.Columns.TYPE.getIndex()));
        user.setUserclassid(cursor.getString(Columns.USER_CLASS_ID.getIndex()));
        user.setDisplayname(cursor.getString(Columns.DISPLAY_NAME.getIndex()));
        user.setCompany(cursor.getString(Columns.COMPANY.getIndex()));
        user.setEmail(cursor.getString(Columns.EMAIL.getIndex()));
        user.setFirstname(cursor.getString(Columns.FIRST_NAME.getIndex()));
        user.setLastname(cursor.getString(Columns.LAST_NAME.getIndex()));

        user.setAvatarAbsPath(cursor.getString(Columns.AVATAR_PATH.getIndex()));

        String rights = cursor.getString(UserDB.Columns.RIGHTS.getIndex());

        if (rights != null && !TextUtils.isEmpty(rights)) {

            if (rights.contains(",")) {
                String[] right_set = rights.split(",");

                for (int i = 0; i < right_set.length; i++) {
                    user.rights.add(UserRight.valueOf(right_set[i]));
                }

            } else {
                user.rights.add(UserRight.valueOf(rights));
            }
        }

        String states = cursor.getString(UserDB.Columns.STATES.getIndex());

        if (states != null && !TextUtils.isEmpty(states)) {
            if (states.contains(",")) {
                String[] states_array = states.split(",");
                for (int i = 0; i < states_array.length; i++) {
                    user.addState(UserState.valueOf(states_array[i]));
                }
            } else
                user.addState(UserState.valueOf(states));
        }


        return user;
    }
    
    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.getId());
        out.writeString(this.getType());
        out.writeString(this.getUserclassid());
        out.writeString(this.getDisplayname());
        out.writeString(this.getCompany());
        out.writeString(this.getFirstname());
        out.writeString(this.getLastname());
        out.writeString(this.getEmail());
        out.writeString(this.getAvatarAbsPath());
        out.writeSerializable(this.rights);
        if (this.getStates() != null) {
            out.writeInt(this.getStates().size());
            for (UserState s : this.getStates()) {

                out.writeString(s.toString());
            }
        }
        else
            out.writeInt(0);
            
    }

    private User(Parcel in) {
        this.setId(in.readString());
        this.setType(in.readString());
        this.setUserclassid(in.readString());
        this.setDisplayname(in.readString());
        this.setCompany(in.readString());
        this.setFirstname(in.readString());
        this.setLastname(in.readString());
        this.setEmail(in.readString());
        this.setAvatarAbsPath(in.readString());
        this.setRights((EnumSet<UserRight>) in.readSerializable());
        
        int state_size = in.readInt();

        if (stateHolder == null) {
            this.stateHolder = new StateHolder<UserState>(EnumSet.noneOf(UserState.class));
        }
        
        if (state_size > 0)
        {
            for (int i = 0; i < state_size; i++) {
                String state = in.readString();
                addState(UserState.valueOf(state));
            }
        }     
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getAvatarAbsPath() {
        return avatarAbsPath;
    }

    public void setAvatarAbsPath(String avatarAbsPath) {
        this.avatarAbsPath =  avatarAbsPath;
    }


}
