package com.saperion.sdb.sdk.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.saperion.sdb.sdk.Constants;
import com.saperion.sdb.sdk.config.JSONTag;
import com.saperion.sdb.sdk.factory.FolderJsonFactory;
import com.saperion.sdb.sdk.operations.DocumentCreateNoContentOperation;
import com.saperion.sdb.sdk.operations.FolderCreateOperation;
import com.saperion.sdb.sdk.operations.FolderDeleteOperation;
import com.saperion.sdb.sdk.operations.FolderGetChildrenOperation;
import com.saperion.sdb.sdk.operations.FolderGetSubfoldersOperation;
import com.saperion.sdb.sdk.operations.FolderUpdateOperation;
import com.saperion.sdb.sdk.providers.ContentProviderHelpers;
import com.saperion.sdb.sdk.providers.FolderContent.FolderDB;
import com.saperion.sdb.sdk.providers.SpacesContent.SpaceDB;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.RequestConstants;
import com.saperion.sdb.sdk.requestmanager.RequestManager.RequestListener;
import com.saperion.sdb.sdk.requestmanager.RequestManagerSingleton;
import com.saperion.sdb.sdk.states.FolderState;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * The class Folder.
 */

@JsonIgnoreProperties({JSONTag.FOLDER_TAG_ID_CHAIN, JSONTag.FOLDER_TAG_CHILDREN, "mEtag","recycled",
        "mChildrenEtag", "mSubfoldersEtag", "mDocumentsEtag"})
public class Folder extends SpacedItem implements Parcelable {

    private StateHolder<FolderState> stateHolder;

    private String mEtag; // eTag of this folder
    private String mChildrenEtag; // eTag of folder/{id}/children?include=modificationid
    private String mDocumentsEtag; // eTag of folder/{id}/document?include=modificationid
    private String mSubfoldersEtag; // eTag of folder/{id}/subfolders?include=modificationid

    public boolean isRecycled() {
        return getStates().toString().contains(FolderState.RECYCLED.toString().toUpperCase());
    }

    
    public String getmEtag() {
        return mEtag;
    }

    public void setmEtag(String mEtag) {
        this.mEtag = mEtag;
    }

    public String getmChildrenEtag() {
        return mChildrenEtag;
    }

    public void setmChildrenEtag(String mChildrenEtag) {
        this.mChildrenEtag = mChildrenEtag;
    }

    public String getmDocumentsEtag() {
        return mDocumentsEtag;
    }

    public void setmDocumentsEtag(String mDocumentsEtag) {
        this.mDocumentsEtag = mDocumentsEtag;
    }

    public String getmSubfoldersEtag() {
        return mSubfoldersEtag;
    }

    public void setmSubfoldersEtag(String mSubfoldersEtag) {
        this.mSubfoldersEtag = mSubfoldersEtag;
    }



    public Folder() {
        super(ModelType.FOLDER);
        this.stateHolder = new StateHolder<FolderState>();
    }

    public Folder addState(FolderState state) {
        this.stateHolder.addState(state);
        return this;
    }

    public Folder removeState(FolderState state) {
        this.stateHolder.removeState(state);
        return this;
    }

    public Collection<FolderState> getStates() {
        return this.stateHolder.getStates();
    }

    public Item setStates(Collection<FolderState> states) {
        this.stateHolder.setStates(states);
        return this;

    }

    public ContentValues toContentValues() {
        // TODO 
        ContentValues cv = new ContentValues();
        cv.put(FolderDB.Columns.ID.getName(), this.getId());
        cv.put(FolderDB.Columns.NAME.getName(), this.getName());
        cv.put(FolderDB.Columns.CREATION_DATE.getName(), this.getCreationDate());
        cv.put(FolderDB.Columns.DESCRIPTION.getName(), this.getDescription());
        cv.put(FolderDB.Columns.LAST_MODIFIED.getName(), this.getLastModified());
        cv.put(FolderDB.Columns.OWNER_ID.getName(), this.getOwnerId());
        cv.put(FolderDB.Columns.OWNER_NAME.getName(), this.getOwnerName());

        List<String> tag_list = this.getTags();

        if (tag_list != null && tag_list.size() > 0) { // TODO hier muss richtig gemacht werden
            String tags = tag_list.toString();
            tags = tags.substring(1, tags.length() - 1);
            cv.put(SpaceDB.Columns.TAGS.getName(), tags);
        }
        cv.put(FolderDB.Columns.TYPE.getName(), this.getType());
        cv.put(FolderDB.Columns.PARENT_ID.getName(), this.getParentId());
        cv.put(FolderDB.Columns.SPACE_ID.getName(), this.getSpaceId());
        cv.put(FolderDB.Columns.ID_CHAIN.getName(), this.getIdChain());
        cv.put(FolderDB.Columns.MODIFICATION_ID.getName(), this.getModificationId());
        cv.put(FolderDB.Columns.ETAG.getName(), this.getmEtag());
        cv.put(FolderDB.Columns.CHILDREN_ETAG.getName(), this.getmChildrenEtag());
        cv.put(FolderDB.Columns.SUBFOLDERS_ETAG.getName(), this.getmSubfoldersEtag());
        cv.put(FolderDB.Columns.DOCUMENTS_ETAG.getName(), this.getmDocumentsEtag());

        String states = "";
        for (FolderState folderStates : this.getStates()) {
            if (!TextUtils.isEmpty(states)) {
                states = states + "," + folderStates.toString();
            } else
                states = folderStates.toString();
        }

        cv.put(FolderDB.Columns.STATES.getName(), states);

        return cv;
    }

    public static Folder fromCursor(Cursor cursor) {
        Folder folder = new Folder();
        folder.setName(cursor.getString(FolderDB.Columns.NAME.getIndex()));
        folder.setId(cursor.getString(FolderDB.Columns.ID.getIndex()));
        folder.setCreationDate(cursor.getString(FolderDB.Columns.CREATION_DATE.getIndex()));
        folder.setDescription(cursor.getString(FolderDB.Columns.DESCRIPTION.getIndex()));
        folder.setLastModified(cursor.getString(FolderDB.Columns.LAST_MODIFIED.getIndex()));
        folder.setOwnerId(cursor.getString(FolderDB.Columns.OWNER_ID.getIndex()));
        folder.setOwnerName(cursor.getString(FolderDB.Columns.OWNER_NAME.getIndex()));
        folder.setModificationId(cursor.getString(FolderDB.Columns.MODIFICATION_ID.getIndex()));
        folder.setmEtag(cursor.getString(FolderDB.Columns.ETAG.getIndex()));
        folder.setmChildrenEtag(cursor.getString(FolderDB.Columns.CHILDREN_ETAG.getIndex()));
        folder.setmSubfoldersEtag(cursor.getString(FolderDB.Columns.SUBFOLDERS_ETAG.getIndex()));
        folder.setmDocumentsEtag(cursor.getString(FolderDB.Columns.DOCUMENTS_ETAG.getIndex()));

        String states = cursor.getString(FolderDB.Columns.STATES.getIndex());

        if (!TextUtils.isEmpty(states)) if (states != null && states.contains(",")) {
            String[] states_array = states.split(",");

            for (String aStates_array : states_array) {
                folder.addState(FolderState.valueOf(aStates_array));
            }
        } else {
            folder.addState(FolderState.valueOf(states));
        }


        folder.addTag(cursor.getString(FolderDB.Columns.TAGS.getIndex()));
        folder.setType(cursor.getString(FolderDB.Columns.TYPE.getIndex()));
        folder.setParentId(cursor.getString(FolderDB.Columns.PARENT_ID.getIndex()));
        folder.setSpaceId(cursor.getString(FolderDB.Columns.SPACE_ID.getIndex()));
        folder.setIdChain(cursor.getString(FolderDB.Columns.ID_CHAIN.getIndex()));
        return folder;
    }

    public void delete(RequestListener listener) {
        Request request = new Request(RequestConstants.REQUEST_TYPE_FOLDER_DELETE);
        request.put(FolderDeleteOperation.PARAM_ITEM_ID, id);

        if (RequestManagerSingleton.isCreated())
            RequestManagerSingleton.getInstance().execute(request, listener);
    }

    public void restore(RequestListener listener) {
        removeState(FolderState.RECYCLED);
        save(listener);
    }
    
    /**
     * get children createInstance server
     *
     * @param listener
     */
    public ArrayList<? extends SpacedItem> getChildren(Context context, RequestListener listener) {

        Request request = new Request(RequestConstants.REQUEST_TYPE_FOLDER_GET_CHILDREN);
        request.put(FolderGetChildrenOperation.PARAM_FOLDER, this);

        if (RequestManagerSingleton.isCreated())
            RequestManagerSingleton.getInstance().execute(request, listener);

        return ContentProviderHelpers.getChildrenByParentId(context, this.getId());
    }

    public void getSubfolders(RequestListener listener) {
        
        Request request = new Request(RequestConstants.REQUEST_TYPE_FOLDER_GET_SUB_FOLDERS);
        request.put(FolderGetSubfoldersOperation.PARAM_FOLDER, this);

        if (RequestManagerSingleton.isCreated() && listener != null) {
            RequestManagerSingleton.getInstance().execute(request, listener);
        }
    }
    
    
    public void save(RequestListener listener) {
        if (!TextUtils.isEmpty(this.id)) {

            Request request = new Request(RequestConstants.REQUEST_TYPE_FOLDER_UPDATE);

            request.put(FolderUpdateOperation.PARAM_PAYLOAD, FolderJsonFactory.toJson(this));
            request.put(FolderUpdateOperation.PARAM_ITEM_ID, this.getId());

            if (RequestManagerSingleton.isCreated())
                RequestManagerSingleton.getInstance().execute(request, listener);

        }
        else
        {
            Request request = new Request(RequestConstants.REQUEST_TYPE_FOLDER_CREATE);
            request.put(FolderCreateOperation.PARAM_PAYLOAD, FolderJsonFactory.toJson(this));

            if (RequestManagerSingleton.isCreated())
                RequestManagerSingleton.getInstance().execute(request, listener);
        }
    }

    public <ChildItem extends SpacedItem> void addChild(ChildItem child, RequestListener listener) {
        
        if (child.getType().equals(Constants.FOLDER_TYPE)) {
            Folder subfolder = (Folder) child;
            subfolder.setParentId(this.id);
            subfolder.save(listener);
        } else if (child.getType().equals(Constants.DOCUMENT_TYPE)) {
            Document doc = (Document) child;
            doc.setParentId(this.id);
            Request request = new Request(RequestConstants.REQUEST_TYPE_DOCUMENT_CREATE_WITHOUT_CONTENT);
            request.put(DocumentCreateNoContentOperation.PARAM_DOCUMENT, doc);

            if (RequestManagerSingleton.isCreated())
                RequestManagerSingleton.getInstance().execute(request, listener);
        }



    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.getId());
        out.writeString(this.getType());
        out.writeString(this.getOwnerId());
        out.writeString(this.getOwnerName());
        out.writeString(this.getCreationDate());
        out.writeString(this.getLastModified());
        out.writeString(this.getName());
        out.writeString(this.getDescription());
        out.writeString(this.getModificationId());
        out.writeString(this.getParentId());
        out.writeString(this.getSpaceId());
        out.writeString(this.getIdChain());
        out.writeString(this.getmEtag());
        out.writeString(this.getmChildrenEtag());
        out.writeString(this.getmSubfoldersEtag());
        out.writeString(this.getmDocumentsEtag());
        out.writeStringList(this.getTags());
        out.writeParcelable(this.stateHolder, flags);

    }

    private Folder(Parcel in) {
        this.setId(in.readString());
        this.setType(in.readString());
        this.setOwnerId(in.readString());
        this.setOwnerName(in.readString());
        this.setCreationDate(in.readString());
        this.setLastModified(in.readString());
        this.setName(in.readString());
        this.setDescription(in.readString());
        this.setModificationId(in.readString());
        this.setParentId(in.readString());
        this.setSpaceId(in.readString());
        this.setIdChain(in.readString());
        this.setmEtag(in.readString());
        this.setmChildrenEtag(in.readString());
        this.setmSubfoldersEtag(in.readString());
        this.setmDocumentsEtag(in.readString());
        List<String> tags = new ArrayList<String>();
        in.readStringList(tags);
        this.setTags(tags);

        this.stateHolder = in.readParcelable(FolderState.class
                .getClassLoader());

    }


    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Folder createFromParcel(Parcel in) {
            return new Folder(in);
        }

        public Folder[] newArray(int size) {
            return new Folder[size];
        }
    };

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }


    public void recycle(RequestListener listener) {
        addState(FolderState.RECYCLED);
        save(listener);
    }
}
