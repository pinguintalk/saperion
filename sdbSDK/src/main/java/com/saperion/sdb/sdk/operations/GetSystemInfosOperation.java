
/**
 * Created by nnn on 06.08.13.
 */

package com.saperion.sdb.sdk.operations;

import android.content.Context;
import android.os.Bundle;

import com.saperion.sdb.sdk.config.WSConfig;
import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.models.SystemInfo;
import com.saperion.sdb.sdk.network.NetworkConnection;
import com.saperion.sdb.sdk.network.NetworkConnection.ConnectionResult;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.security.CredentialManager;
import com.saperion.sdb.sdk.service.RequestService.Operation;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

import org.apache.http.HttpStatus;
import org.apache.http.auth.UsernamePasswordCredentials;

import java.util.HashMap;

public class GetSystemInfosOperation implements Operation {

    private static final String TAG = GetSystemInfosOperation.class.getSimpleName();

    @Override
    public Bundle execute(Context context, Request request) throws ConnectionException,
            DataException, ServiceRequestException {
        Bundle bundle = new Bundle();

        NetworkConnection networkConnection =
                new NetworkConnection(context, CredentialManager.getInstance().getServerUrl()
                        + WSConfig.WS_SYSTEM);

        HashMap<String, String> accept_header = new HashMap<String, String>();
        accept_header.put("ACCEPT", "application/json");

        networkConnection.setRequestParams(accept_header);
        networkConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager
                .getInstance().getUserName(), CredentialManager.getInstance().getmAccessToken()));
        ConnectionResult result = networkConnection.execute();

        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);

        if (result.responseCode != HttpStatus.SC_OK) {
            Log.w(TAG, Utils.getMethodName() + " responseCode:" + result.responseCode);

            bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
            return bundle;
        }
        SystemInfo systemInfo = SystemInfo.fromJson(result.body);

        bundle.putParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, systemInfo);

        return bundle;
    }

}

