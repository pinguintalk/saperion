package com.saperion.sdb.sdk.states;

public enum UserState {
    GUEST,
    RECYCLED,
    READONLY
}
