package com.saperion.sdb.sdk.operations;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;

import com.saperion.sdb.sdk.Constants;
import com.saperion.sdb.sdk.config.WSConfig;
import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.factory.DocumentJsonFactory;
import com.saperion.sdb.sdk.models.Document;
import com.saperion.sdb.sdk.network.NetworkConnection;
import com.saperion.sdb.sdk.network.NetworkConnection.ConnectionResult;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.security.CredentialManager;
import com.saperion.sdb.sdk.service.RequestService.Operation;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

import org.apache.http.auth.UsernamePasswordCredentials;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public final class DocumentDownloadFileByVersionOperation implements Operation {

    private static final String TAG = DocumentDownloadFileByVersionOperation.class.getSimpleName();

    public static final String PARAM_DOCUMENT = Constants.SDK_PACKAGE_NAME + ".param_document";
    public static final String PARAM_DOCUMENT_VERSION = Constants.SDK_PACKAGE_NAME + ".param_doc_version";

    @Override
    public Bundle execute(Context context, Request request) throws ServiceRequestException, ConnectionException,
            DataException {
        Bundle bundle = new Bundle();
        Log.v(TAG, Utils.getMethodName() + "entry");

        Document  document = (Document) request.getParcelable(PARAM_DOCUMENT);
        int version =  request.getInt(PARAM_DOCUMENT_VERSION); // version to download
        String id = document.getId();

        String absFilePath = Utils.getDocumentsDirectory(context) + "/" + id + version;
        File cached_file = new File(absFilePath);

        File storageDir = Environment.getExternalStorageDirectory();

        String fileToSave =  storageDir.getAbsolutePath() +"/"+Environment.DIRECTORY_DOWNLOADS + "/" +  document.getName();
        
        if (cached_file.exists()) { // we have a cached file stored, so we need to check if the file is really up to date

            String url = WSConfig.getDocumentURL() + "/" + id + WSConfig.WS_INCLUDE_MODIFICATION_ID;
            NetworkConnection getDocuConnection = new NetworkConnection(context, url);

            HashMap<String, String> header_list = new HashMap<String, String>();
            header_list.put("ACCEPT", "application/json");
            getDocuConnection.setHeaderList(header_list);

            getDocuConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager.getInstance().getUserName(),
                    CredentialManager.getInstance().getmAccessToken()));

            ConnectionResult result = getDocuConnection.execute();

            bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE,
                    result.responseCode);
            if (result.responseCode != ResponseConstants.RESPONSE_CODE_OK) {
                bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
                return bundle;
            }

            Document new_document = DocumentJsonFactory.jsonToDocument(result.body);

            if (new_document.getModificationId().equals(document.getModificationId())) { // cached file is up to date, so we can uses it    


                try {
                    Utils.fileCopy(absFilePath,fileToSave);
                } catch (IOException e) {
                    throw  new DataException();
                }
                bundle.putString(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, fileToSave);
                return bundle;
            }
        }

        // cached file is NOT up to date or NOT exists, we have to download it
        bundle = new Bundle();
        String url = WSConfig.getDocumentURL() + "/" + id + "/" + WSConfig.WS_FILE +"?version=" + version;

        NetworkConnection networkConnection = new NetworkConnection(context, url);

        HashMap<String, String> requestParams = new HashMap<String, String>();
        //String contentfile_content_uri = ContentProviderHelpers.documentGetFileUri(context,id).toString();

        
        requestParams.put(NetworkConnection.FILE_PATH, absFilePath);
        requestParams.put(NetworkConnection.REQUEST_TYPE,
                NetworkConnection.REEQUEST_TYPE_DOWNLOAD);
        networkConnection.setRequestParams(requestParams);

        HashMap<String, String> header_list = new HashMap<String, String>();
        header_list.put("ACCEPT", "application/octet-stream");
        networkConnection.setHeaderList(header_list);

        Log.v(TAG, Utils.getMethodName() + "downloaded file: "
                + fileToSave );

        networkConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager
                .getInstance().getUserName(), CredentialManager.getInstance().getmAccessToken()));

        ConnectionResult result = networkConnection.execute();

        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);

        if (result.responseCode != ResponseConstants.RESPONSE_CODE_OK) {
            Log.w(TAG, Utils.getMethodName() + " responseCode:" + result.responseCode);

            bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
            return bundle;
        }
        //TODO
        //bundle.putByte(, value);

        try {
            Utils.fileCopy(absFilePath,fileToSave);
            bundle.putString(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, fileToSave);
        } catch (IOException e) {
            throw  new DataException();
        }

        return bundle;

    }

}
