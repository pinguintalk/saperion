package com.saperion.sdb.sdk.operations;

import android.content.Context;
import android.os.Bundle;

import com.saperion.sdb.sdk.Constants;
import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.service.RequestService.Operation;

public class FolderCreateOperation implements Operation {

    public static final String PARAM_PAYLOAD = "com.saperion.sdb.sdk.folder.param_payload";

    @Override
    public Bundle execute(Context context, Request request) throws ConnectionException,
            DataException, ServiceRequestException {

        return OperationHelpers.itemCreate(context, request.getString(PARAM_PAYLOAD),
                Constants.FOLDER_TYPE);

    }

}
