package com.saperion.sdb.sdk.operations;

import android.content.Context;
import android.os.Bundle;

import com.saperion.sdb.sdk.Constants;
import com.saperion.sdb.sdk.config.WSConfig;
import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.factory.DocumentJsonFactory;
import com.saperion.sdb.sdk.models.Document;
import com.saperion.sdb.sdk.network.NetworkConnection;
import com.saperion.sdb.sdk.network.NetworkConnection.ConnectionResult;
import com.saperion.sdb.sdk.providers.ContentProviderHelpers;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.security.CredentialManager;
import com.saperion.sdb.sdk.service.RequestService.Operation;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

import org.apache.http.auth.UsernamePasswordCredentials;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public final class DocumentGetFileByVersionOperation implements Operation {

    private static final String TAG = DocumentGetFileByVersionOperation.class.getSimpleName();

    public static final String PARAM_DOCUMENT = Constants.SDK_PACKAGE_NAME + ".param_document";
    public static final String PARAM_DOCUMENT_VERSION = Constants.SDK_PACKAGE_NAME + ".param_doc_version";

    @Override
    public Bundle execute(Context context, Request request) throws ServiceRequestException, ConnectionException,
            DataException {
        Bundle bundle = new Bundle();
        Log.v(TAG, Utils.getMethodName() + "entry");

        Document  document = (Document) request.getParcelable(PARAM_DOCUMENT);
        int version =  request.getInt(PARAM_DOCUMENT_VERSION); // version to download
        String id = document.getId();

        String file_name_with_id_and_version = Utils.getDocumentsDirectory(context)  + id + version;
        String ab_file_path_name = Utils.getDocumentsDirectory(context)  + document.getName();
        
        File cached_file = new File(file_name_with_id_and_version);

        if (cached_file.exists()) { 
        // we have a cached file stored, so we need get file meta data with modification id to check if the file is really up to date

            String url = WSConfig.getDocumentURL() + "/" + id + WSConfig.WS_INCLUDE_MODIFICATION_ID;
            NetworkConnection getDocuConnection = new NetworkConnection(context, url);

            HashMap<String, String> header_list = new HashMap<String, String>();
            header_list.put("ACCEPT", "application/json");
            getDocuConnection.setHeaderList(header_list);

            getDocuConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager.getInstance().getUserName(),
                    CredentialManager.getInstance().getmAccessToken()));

            ConnectionResult result = getDocuConnection.execute();

            bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE,
                    result.responseCode);
            if (result.responseCode != ResponseConstants.RESPONSE_CODE_OK) {
                bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
                return bundle;
            }

            Document new_document = DocumentJsonFactory.jsonToDocument(result.body);

            if (new_document.getModificationId().equals(document.getModificationId())) { // modificatin id is not changed, cached file is up to date, so we can uses it    

                Log.v(TAG, Utils.getMethodName() 
                        + file_name_with_id_and_version +
                       " is not changed");
                try {
                    Utils.fileCopy(file_name_with_id_and_version,ab_file_path_name);
    
                    new_document.setPreviewAbsPath(document.getPreviewAbsPath());
                    new_document.setRenditionAbsPath(document.getRenditionAbsPath());
                    new_document.setFileAbsPath(ab_file_path_name);
                    new_document.setIdChain(document.getIdChain());

                    ContentProviderHelpers.documentReplace(context, new_document);

                    bundle.putParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, new_document);
                    
                } catch (IOException e) {
                    Log.v(TAG, Utils.getMethodName()
                            + e.toString());
                }
                
                return bundle;
            }
        }

        // cached file is NOT up to date or NOT exists, we have to download it
        bundle = new Bundle();
        String url = WSConfig.getDocumentURL() + "/" + id + "/" + WSConfig.WS_FILE +"?version=" + version;

        NetworkConnection networkConnection = new NetworkConnection(context, url);

        HashMap<String, String> requestParams = new HashMap<String, String>();
        //String contentfile_content_uri = ContentProviderHelpers.documentGetFileUri(context,id).toString();
        
        requestParams.put(NetworkConnection.FILE_PATH, file_name_with_id_and_version);
        requestParams.put(NetworkConnection.REQUEST_TYPE,
                NetworkConnection.REEQUEST_TYPE_DOWNLOAD);
        networkConnection.setRequestParams(requestParams);

        HashMap<String, String> header_list = new HashMap<String, String>();
        header_list.put("ACCEPT", "application/octet-stream");
        networkConnection.setHeaderList(header_list);

        Log.v(TAG, Utils.getMethodName() + "file_name_with_id_and_version:"
                + file_name_with_id_and_version);

        networkConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager
                .getInstance().getUserName(), CredentialManager.getInstance().getmAccessToken()));

        ConnectionResult result = networkConnection.execute();

        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);

        if (result.responseCode != ResponseConstants.RESPONSE_CODE_OK) {
            Log.w(TAG, Utils.getMethodName() + " responseCode:" + result.responseCode);

            bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
            return bundle;
        }
        //TODO
        //bundle.putByte(, value);

        try {
            Utils.fileCopy(file_name_with_id_and_version, ab_file_path_name);
            
            ContentProviderHelpers.documentUpdateFilePath(context, document, result.body);

            bundle.putParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, document);
        } catch (IOException e) {
            Log.v(TAG, Utils.getMethodName()
                    + e.toString() );
        }
        
       
        return bundle;
    }


}
