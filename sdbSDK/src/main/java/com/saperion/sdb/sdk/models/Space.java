package com.saperion.sdb.sdk.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.saperion.sdb.sdk.config.JSONTag;
import com.saperion.sdb.sdk.factory.SpaceJsonFactory;
import com.saperion.sdb.sdk.operations.SpaceCreateOperation;
import com.saperion.sdb.sdk.operations.SpaceDeleteOperation;
import com.saperion.sdb.sdk.operations.SpaceGetChildrenOperation;
import com.saperion.sdb.sdk.operations.SpaceGetDocumentsOperation;
import com.saperion.sdb.sdk.operations.SpaceGetSubfoldersOperation;
import com.saperion.sdb.sdk.operations.SpaceUpdateOperation;
import com.saperion.sdb.sdk.providers.ContentProviderHelpers;
import com.saperion.sdb.sdk.providers.SpacesContent.SpaceDB;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.RequestConstants;
import com.saperion.sdb.sdk.requestmanager.RequestManager.RequestListener;
import com.saperion.sdb.sdk.requestmanager.RequestManagerSingleton;
import com.saperion.sdb.sdk.rights.SpaceRight;
import com.saperion.sdb.sdk.states.SpaceState;
import com.saperion.sdb.sdk.utils.Utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;

/**
 * The class Space.
 */

@JsonIgnoreProperties({JSONTag.SPACE_TAG_URI, JSONTag.SPACE_TAG_FILEPATHS,
        JSONTag.SPACE_TAG_SUBFOLDERPATHS, JSONTag.SPACE_TAG_FOLDERS, JSONTag.SPACE_TAG_CHILDREN,
        "favorit",  "recycled", "mEtag", "mChildrenEtag", "mSubfoldersEtag", "mDocumentsEtag"})
public class Space extends Item implements Parcelable, Comparable<Space> {

    private static final String TAG = Space.class.getSimpleName();
    String uri;


    private String secret;
    boolean isSynchronized;
    private EnumSet<SpaceRight> rights;
    private StateHolder<SpaceState> stateHolder;

    private String mEtag; // eTag of this space
    private String mChildrenEtag; // eTag of Spaces/{id}/children?include=modificationid
    private String mDocumentsEtag; // eTag of Spaces/{id}/document?include=modificationid
    private String mSubfoldersEtag; // eTag of Spaces/{id}/subfolders?include=modificationid


    public Space() {
        super(ModelType.SPACE);
        rights = EnumSet.noneOf(SpaceRight.class);
        this.stateHolder = new StateHolder<SpaceState>(EnumSet.noneOf(SpaceState.class));
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getmSubfoldersEtag() {
        return mSubfoldersEtag;
    }

    public void setmSubfoldersEtag(String mSubfoldersEtag) {
        this.mSubfoldersEtag = mSubfoldersEtag;
    }

    public String getmEtag() {
        return mEtag;
    }

    public void setmEtag(String mEtag) {
        this.mEtag = mEtag;
    }

    public String getmChildrenEtag() {
        return mChildrenEtag;
    }

    public void setmChildrenEtag(String mChildrenEtag) {
        this.mChildrenEtag = mChildrenEtag;
    }

    public String getmDocumentsEtag() {
        return mDocumentsEtag;
    }

    public void setmDocumentsEtag(String mDocumentsEtag) {
        this.mDocumentsEtag = mDocumentsEtag;
    }

    public boolean isSynchronized() {
        return isSynchronized;
    }

    @JsonIgnore
    public void setSynchronized(boolean isSynchronized) {
        this.isSynchronized = isSynchronized;
    }

    public Space setRights(Collection<SpaceRight> rights) {
        if (rights == null || rights.isEmpty()) {
            this.rights = EnumSet.noneOf(SpaceRight.class);
        } else {
            this.rights = EnumSet.copyOf(rights);
        }
        return this;
    }

    public Space addRight(SpaceRight right) {
        if (this.rights == null) {
            this.rights = EnumSet.of(right);
        } else {
            this.rights.add(right);
        }
        return this;
    }

    public Collection<SpaceRight> getRights() {
        return this.rights;
    }

    public Space addState(SpaceState state) {
        this.stateHolder.addState(state);
        return this;
    }

    public Space removeState(SpaceState state) {
        this.stateHolder.removeState(state);
        return this;
    }

    public Collection<SpaceState> getStates() {
        return this.stateHolder.getStates();
    }

    public boolean isRecycled() {
        return stateHolder.getStates().toString().toLowerCase().contains(SpaceState.RECYCLED.toString().toLowerCase());
    }
    
    public Item setStates(Collection<SpaceState> states) {
        this.stateHolder.setStates(states);
        return this;
    }

    public ContentValues toContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(SpaceDB.Columns.ID.getName(), this.getId());
        cv.put(SpaceDB.Columns.NAME.getName(), this.getName());
        cv.put(SpaceDB.Columns.CREATION_DATE.getName(), this.getCreationDate());
        cv.put(SpaceDB.Columns.DESCRIPTION.getName(), this.getDescription());
        cv.put(SpaceDB.Columns.LAST_MODIFIED.getName(), this.getLastModified());
        cv.put(SpaceDB.Columns.OWNER_ID.getName(), this.getOwnerId());
        cv.put(SpaceDB.Columns.OWNER_NAME.getName(), this.getOwnerName());
        cv.put(SpaceDB.Columns.MODIFICATION_ID.getName(), this.getModificationId());
        cv.put(SpaceDB.Columns.SECRET.getName(), this.getSecret());
        cv.put(SpaceDB.Columns.ETAG.getName(), this.getmEtag());
        cv.put(SpaceDB.Columns.CHILDREN_ETAG.getName(), this.getmChildrenEtag());
        cv.put(SpaceDB.Columns.SUBFOLDERS_ETAG.getName(), this.getmSubfoldersEtag());
        cv.put(SpaceDB.Columns.DOCUMENTS_ETAG.getName(), this.getmDocumentsEtag());

        cv.put(SpaceDB.Columns.IS_SYNCHRONIZED.getName(), this.isSynchronized() ? 1 : 0);

        String rights = "";
        for (SpaceRight spaceRight : this.getRights()) {
            if (!TextUtils.isEmpty(rights)) {
                rights = rights + "," + spaceRight.toString();
            } else
                rights = spaceRight.toString();
        }

        cv.put(SpaceDB.Columns.RIGHTS.getName(), rights);

        String states = "";
        for (SpaceState spaceStates : this.getStates()) {
            if (!TextUtils.isEmpty(states)) {
                states = states + "," + spaceStates.toString();
            } else
                states = spaceStates.toString();
        }

        cv.put(SpaceDB.Columns.STATES.getName(), states);

        List<String> tag_list = this.getTags();
        if (tag_list != null && tag_list.size() > 0) { // TODO muss richtig  gemacht werden !
            String tags = tag_list.toString();
            tags = tags.substring(1, tags.length() - 1);
            cv.put(SpaceDB.Columns.TAGS.getName(), tags);
        }

        cv.put(SpaceDB.Columns.TYPE.getName(), this.getType());
        return cv;
    }

    public static Space fromCursor(Cursor cursor) {
        Space space = new Space();
        space.setName(cursor.getString(SpaceDB.Columns.NAME.getIndex()));
        space.setId(cursor.getString(SpaceDB.Columns.ID.getIndex()));

        space.setCreationDate(cursor.getString(SpaceDB.Columns.CREATION_DATE.getIndex()));
        space.setDescription(cursor.getString(SpaceDB.Columns.DESCRIPTION.getIndex()));
        space.setLastModified(cursor.getString(SpaceDB.Columns.LAST_MODIFIED.getIndex()));
        space.setOwnerId(cursor.getString(SpaceDB.Columns.OWNER_ID.getIndex()));
        space.setOwnerName(cursor.getString(SpaceDB.Columns.OWNER_NAME.getIndex()));
        space.setModificationId(cursor.getString(SpaceDB.Columns.MODIFICATION_ID.getIndex()));
        space.setSynchronized(cursor.getInt(SpaceDB.Columns.IS_SYNCHRONIZED.getIndex()) != 0);
        space.setmEtag(cursor.getString(SpaceDB.Columns.ETAG.getIndex()));
        space.setmChildrenEtag(cursor.getString(SpaceDB.Columns.CHILDREN_ETAG.getIndex()));
        space.setmSubfoldersEtag(cursor.getString(SpaceDB.Columns.SUBFOLDERS_ETAG.getIndex()));
        space.setmDocumentsEtag(cursor.getString(SpaceDB.Columns.DOCUMENTS_ETAG.getIndex()));
        space.setSecret(cursor.getString(SpaceDB.Columns.SECRET.getIndex()));
        String rights = cursor.getString(SpaceDB.Columns.RIGHTS.getIndex());

        if (rights != null && !TextUtils.isEmpty(rights)) {

            if (rights.contains(",")) {
                String[] right_set = rights.split(",");

                for (int i = 0; i < right_set.length; i++) {
                    space.rights.add(SpaceRight.valueOf(right_set[i]));
                }

            } else {
                space.rights.add(SpaceRight.valueOf(rights));
            }
        }

        String states = cursor.getString(SpaceDB.Columns.STATES.getIndex());

        if (states != null && !TextUtils.isEmpty(states)) {
            if (states.contains(",")) {
                String[] states_array = states.split(",");
                for (int i = 0; i < states_array.length; i++) {
                    space.addState(SpaceState.valueOf(states_array[i]));
                }
            } else
                space.addState(SpaceState.valueOf(states));
        }

        space.addTag(cursor.getString(SpaceDB.Columns.TAGS.getIndex()));
        space.setType(cursor.getString(SpaceDB.Columns.TYPE.getIndex()));

        return space;
    }

    /**
     * Retrieve the uri.
     *
     * @return the uri
     */
    public String getUri() {
        return uri;
    }

    /**
     * Modify the uri.
     *
     * @param aUri the path to set
     */
    public void setUri(String aUri) {
        uri = aUri;
        if (uri != null) {
            // Remove any parameter part
            int qm = uri.indexOf('?');
            if (qm >= 0)
                uri = uri.substring(0, qm);
        }
    }

    // Methods for Serialization

    public int describeContents() {
        return 0;
    }

    public void delete(RequestListener listener) {

        Request request = new Request(RequestConstants.REQUEST_TYPE_SPACE_DELETE);
        request.put(SpaceDeleteOperation.PARAM_ITEM_ID, this.getId());

        if ((RequestManagerSingleton.isCreated()) && listener != null)
            RequestManagerSingleton.getInstance().execute(request, listener);

    }

    public void save(RequestListener listener) {
        if (TextUtils.isEmpty(this.getId())) {
            Request request = new Request(RequestConstants.REQUEST_TYPE_SPACE_CREATE);
            request.put(SpaceCreateOperation.PARAM_PAYLOAD, SpaceJsonFactory.toJson(this));

            if ((RequestManagerSingleton.isCreated()) && listener != null)
                RequestManagerSingleton.getInstance().execute(request, listener);

        } else {
            // or update
            // SdbApplication.getInstance().ws_space_update(this);
            Request request = new Request(RequestConstants.REQUEST_TYPE_SPACE_UPDATE);
            request.put(SpaceUpdateOperation.PARAM_PAYLOAD, SpaceJsonFactory.toJson(this));
            request.put(SpaceUpdateOperation.PARAM_ITEM_ID, this.id);

            if (RequestManagerSingleton.isCreated() && listener != null)
                RequestManagerSingleton.getInstance().execute(request, listener);
        }

    }


    public void restore(RequestListener listener) {
        if (!this.isRecycled()) return;
        
        Request request = new Request(RequestConstants.REQUEST_TYPE_SPACE_UPDATE);
        this.removeState(SpaceState.RECYCLED);
        request.put(SpaceUpdateOperation.PARAM_PAYLOAD, SpaceJsonFactory.toJson(this));
        request.put(SpaceUpdateOperation.PARAM_ITEM_ID, this.id);

        if (RequestManagerSingleton.isCreated() && listener != null)
            RequestManagerSingleton.getInstance().execute(request, listener);
    }


    /**
     * get sub folders of this space
     *
     * @param listener
     */
    public void getSubfolders(RequestListener listener) {

        Request request = new Request(RequestConstants.REQUEST_TYPE_SPACE_GET_SUB_FOLDERS);
        request.put(SpaceGetSubfoldersOperation.PARAM_SPACE_ID, this.getId());

        if (RequestManagerSingleton.isCreated() && listener != null) {
            Log.d(TAG, Utils.getMethodName() + "get sub folder of  " + this.getName());
            RequestManagerSingleton.getInstance().execute(request, listener);
        }
    }

    /**
     * get documents of this space
     *
     * @param @link RequestListener
     * 
     */
    public void getDocuments(RequestListener listener) {

        Request request = new Request(RequestConstants.REQUEST_TYPE_SPACE_GET_DOCUMENTS);
        request.put(SpaceGetDocumentsOperation.PARAM_SPACE_ID, this.getId());

        if (RequestManagerSingleton.isCreated() && listener != null) {
            RequestManagerSingleton.getInstance().execute(request, listener);
        }
    }

    /**
     * get children of this space
     *
     * @param @link RequestListener
     */
    public ArrayList<? extends SpacedItem> getChildren(Context context, RequestListener listener) {

        Request request = new Request(RequestConstants.REQUEST_TYPE_SPACE_GET_CHILDREN);
        request.put(SpaceGetChildrenOperation.PARAM_SPACE_ID, this.getId());
      
        if (RequestManagerSingleton.isCreated() && listener != null) {
            Log.v(TAG, Utils.getMethodName() + "get children of  " + this.getName());
            RequestManagerSingleton.getInstance().execute(request, listener);
        }

        return ContentProviderHelpers.getChildrenByParentId(context, this.getId());
    }

    public <ChildItem extends SpacedItem> void addFolder(Folder folder, RequestListener listener) {

            folder.setParentId(this.id);

            folder.save(listener);
    }

    public int compareTo(Space other_space) {
        return (this.getName()).compareTo(other_space.getName());
    }

    public void setFavorit(boolean favorit, RequestListener listener) {
        if (favorit) {
            this.addState(SpaceState.FAVORED);
        } else {
            this.removeState(SpaceState.FAVORED);
        }
        this.save(listener);
    }

    public boolean isFavorit() {
        if (this.getStates().contains(SpaceState.FAVORED)) {
            return true;
        }
        return false;

    }

    public static final Parcelable.Creator<Space> CREATOR = new Parcelable.Creator<Space>() {
        public Space createFromParcel(Parcel in) {
            return new Space(in);
        }

        public Space[] newArray(int size) {
            return new Space[size];
        }
    };

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.getId());
        out.writeString(this.getType());
        out.writeString(this.getOwnerId());
        out.writeString(this.getOwnerName());
        out.writeString(this.getCreationDate());
        out.writeString(this.getLastModified());
        out.writeString(this.getName());
        out.writeString(this.getDescription());
        out.writeString(this.getModificationId());
        out.writeStringList(this.getTags());
        out.writeString(this.getUri());
        out.writeString(this.getSecret());
        out.writeString(this.getmEtag());
        out.writeString(this.getmChildrenEtag());
        out.writeString(this.getmSubfoldersEtag());
        out.writeString(this.getmDocumentsEtag());

        out.writeInt(this.isSynchronized ? 1 : 0);

        out.writeSerializable(this.rights);
        /*
		out.writeSerializable((EnumSet) this.getStates());

		out.writeInt(this.getStates().size());
		for (SpaceState s : this.getStates()) {
		    out.writeSerializable(s);
		}
		*/
        out.writeInt(this.getStates().size());
        for (SpaceState s : this.getStates()) {

            out.writeString(s.toString());
        }
    }

    private Space(Parcel in) {
        this.setId(in.readString());
        this.setType(in.readString());
        this.setOwnerId(in.readString());
        this.setOwnerName(in.readString());
        this.setCreationDate(in.readString());
        this.setLastModified(in.readString());
        this.setName(in.readString());
        this.setDescription(in.readString());
        this.setModificationId(in.readString());
        List<String> tags = new ArrayList<String>();
        in.readStringList(tags);
        this.setTags(tags);
        this.setUri(in.readString());
        this.setSecret(in.readString());
        this.setmEtag(in.readString());
        this.setmChildrenEtag(in.readString());
        this.setmSubfoldersEtag(in.readString());
        this.setmDocumentsEtag(in.readString());

        this.setSynchronized(in.readInt() == 1);
        this.setRights((EnumSet<SpaceRight>) in.readSerializable());
        int state_size = in.readInt();

        for (int i = 0; i < state_size; i++) {
            String state = in.readString();
            Log.v(TAG, Utils.getMethodName() + "SpaceState " + state.toString());

            if (stateHolder == null) {
                this.stateHolder = new StateHolder<SpaceState>(EnumSet.noneOf(SpaceState.class));
            }

            addState(SpaceState.valueOf(state));
        }

        //this.setStates((Collection<SpaceState>) in.readParcelable(SpaceState.class.getClassLoader()));

    }


    @Override
    public String toString() {
        StringBuilder strbuffer = new StringBuilder("");
        for (SpaceState s : this.getStates()) {
            strbuffer.append(s.toString());
            strbuffer.append(",");
        }

        return "Space{" + "rights='" + rights + '\'' + ", stateHolder='[" + strbuffer.toString()
                + "]'} " + super.toString();
    }

    public ArrayList<Share> getShares(Context context, RequestListener requestlistener) {
        return null;
    }


    public void switchSyncAppState() {
        if (stateHolder.getStates().contains(SpaceState.SYNCHRONIZE_APP))
            stateHolder.removeState(SpaceState.SYNCHRONIZE_APP);
        else
            stateHolder.addState(SpaceState.SYNCHRONIZE_APP);
    }

    public void switchSyncDesktopState() {
        if (stateHolder.getStates().contains(SpaceState.SYNCHRONIZE_DESKTOP))
            stateHolder.removeState(SpaceState.SYNCHRONIZE_DESKTOP);
        else
            stateHolder.addState(SpaceState.SYNCHRONIZE_DESKTOP);
    }
}
