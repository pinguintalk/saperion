package com.saperion.sdb.sdk.factory;

import android.text.TextUtils;
import android.util.Log;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.models.Document;
import com.saperion.sdb.sdk.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;

public final class DocumentJsonFactory {

    private static final String TAG = DocumentJsonFactory.class.getSimpleName();

    private static boolean LOG_ALL = false;

    private DocumentJsonFactory() {
        // No public constructor
    }

    public static String toJson(Document document) {

        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(Include.NON_NULL);
        try {
            String f = mapper.writeValueAsString(document);
            return f;
        } catch (JsonProcessingException e1) {
            e1.printStackTrace();
        }
        return null;
    }

    public static Document jsonToDocument(String string) {
        Document document = new Document();
        ObjectMapper mapper = new ObjectMapper();
        try {
            document = mapper.readValue(string, Document.class);
            return document;
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static ArrayList<Document> parseResult(String wsResponse) throws DataException {

        if (wsResponse == null || TextUtils.isEmpty(wsResponse))
            return null;

        ArrayList<Document> documentList = new ArrayList<Document>();

        ObjectMapper objectMapper = new ObjectMapper();
        JsonFactory jsonFactory = new JsonFactory();

        try {
            JsonParser jp = jsonFactory.createJsonParser(wsResponse);
            // advance stream to START_ARRAY first:
            jp.nextToken();
            // and then each time, advance to opening START_OBJECT
            while (jp.nextToken() == JsonToken.START_OBJECT) {
                Document document = objectMapper.readValue(jp, Document.class);

                if (LOG_ALL)
                    Log.v(TAG, Utils.getMethodName() + document.toString());

                // process
                // after binding, stream points to closing END_OBJECT
                documentList.add(document);

            }

        } catch (JsonParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return documentList;
    }
}
