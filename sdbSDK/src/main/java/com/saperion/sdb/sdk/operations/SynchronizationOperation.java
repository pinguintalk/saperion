package com.saperion.sdb.sdk.operations;

import android.content.Context;
import android.os.Bundle;

import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.factory.SpaceJsonFactory;
import com.saperion.sdb.sdk.models.Space;
import com.saperion.sdb.sdk.network.NetworkConnection.ConnectionResult;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.service.RequestService.Operation;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

import java.util.ArrayList;

// TODO noch nicht fertig

/**
 * @author nnn
 */
public final class SynchronizationOperation implements Operation {

    private static final String TAG = SynchronizationOperation.class.getSimpleName();

    @Override
    public Bundle execute(Context context, Request request) throws ServiceRequestException, ConnectionException,
            DataException {

        Log.v(TAG, Utils.getMethodName() + "entry");
        Bundle bundle = new Bundle();
        int  synchronized_item_count = 0;
        // fetch current space list createInstance server 
        // waiting for response and parse it
        ConnectionResult result = OperationHelpers.spaceGetList(context);

        int responseCode = result.responseCode;
        ArrayList<Space> spaceList = new ArrayList<Space>();
        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE,responseCode);
        
        if (responseCode != ResponseConstants.RESPONSE_CODE_OK) {

            bundle.putString(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT,result.body);
        }

        spaceList = SpaceJsonFactory.parseJsonList(result.body);
        
        if (spaceList == null || spaceList.size() < 1) {
            bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT,synchronized_item_count);
            return bundle;
        }
        //and sync it with cp
        ArrayList<Space> modifiedSpaces =  OperationHelpers.spacesSyncContentProvider(context, spaceList);

        bundle.putParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT,modifiedSpaces);
        return bundle;
    }

}
