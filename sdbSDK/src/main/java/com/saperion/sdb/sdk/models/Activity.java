package com.saperion.sdb.sdk.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"id", "type"})
public class Activity extends CreatedTypedIdentifiable implements Parcelable{

    private String activity;
    private String activity_type;

    private String documentId;


    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }
    
    public Activity() {
        super(ModelType.ACTIVITY);
    }

    public String getActivityType() {
        return activity_type;
    }

    public void setActivityType(String type) {
        this.activity_type = type;
    }

    /**
     * Returns the activity of this activity.
     *
     * @return the activity of this activity.
     */
    public String getActivity() {
        return activity;
    }

    /**
     * Sets the activity of this activity.
     *
     * @param activity The activity to be set.
     * @return an instance of this Activity.
     */
    public Activity setActivity(String activity) {
        this.activity = activity;
        return this;
    }

    @Override
    public String toString() {
        return "Activity{" + "activity='" + activity + '\'' + "} " + super.toString();
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.getDocumentId());
        out.writeString(this.getType());
        out.writeString(this.getOwnerId());
        out.writeString(this.getOwnerName());
        out.writeString(this.getCreationDate());
        out.writeString(this.getActivity());
        out.writeString(this.getActivityType());


    }

    private Activity(Parcel in) {
        this.setDocumentId(in.readString());
        this.setType(in.readString());
        this.setOwnerId(in.readString());
        this.setOwnerName(in.readString());
        this.setCreationDate(in.readString());
        this.setActivity(in.readString());
        this.setActivityType(in.readString());


    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Activity createFromParcel(Parcel in) {
            return new Activity(in);
        }

        public Activity[] newArray(int size) {
            return new Activity[size];
        }
    };

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }
}
