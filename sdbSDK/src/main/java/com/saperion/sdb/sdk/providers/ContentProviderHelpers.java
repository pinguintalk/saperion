package com.saperion.sdb.sdk.providers;

import android.content.ContentProviderOperation;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.text.TextUtils;

import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.models.Document;
import com.saperion.sdb.sdk.models.Folder;
import com.saperion.sdb.sdk.models.Link;
import com.saperion.sdb.sdk.models.Share;
import com.saperion.sdb.sdk.models.Space;
import com.saperion.sdb.sdk.models.SpacedItem;
import com.saperion.sdb.sdk.models.User;
import com.saperion.sdb.sdk.providers.DocumentContent.DocumentDB;
import com.saperion.sdb.sdk.providers.DocumentContent.DocumentDB.Columns;
import com.saperion.sdb.sdk.providers.FolderContent.FolderDB;
import com.saperion.sdb.sdk.providers.LinkContent.LinkDB;
import com.saperion.sdb.sdk.providers.ShareContent.ShareDB;
import com.saperion.sdb.sdk.providers.SpacesContent.SpaceDB;
import com.saperion.sdb.sdk.providers.UserContent.UserDB;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public final class ContentProviderHelpers {

    private static final String TAG = ContentProviderHelpers.class.getSimpleName();

    private static boolean ALLOW_LOG = false;

    /**
     * children of space or folder has the same types (folder or document)
     *
     * @param context
     * @param parentID
     * @return
     */
    public static ArrayList<? extends SpacedItem> getChildrenByParentId(Context context, String parentID) {
        // Load the spaced items specified by the fragment
        // arguments. Should use a Loader

        ArrayList<SpacedItem> tmpItemList = new ArrayList<SpacedItem>();

        if (TextUtils.isEmpty(parentID))
            return tmpItemList;

        Log.v(TAG, Utils.getMethodName() + "parentID = " + parentID);

        // get folders
        Cursor folderCursor =
                context.getContentResolver().query(FolderDB.CONTENT_URI, null,
                        FolderDB.Columns.PARENT_ID.getName() + "='" + parentID + "'", null, null);

        if (folderCursor != null) {

            Log.v(TAG, Utils.getMethodName() + "count " + folderCursor.getCount());

            if (folderCursor.moveToFirst()) {
                int name_idx = FolderDB.Columns.NAME.getIndex();

                do {
                    String foldername = folderCursor.getString(name_idx);

                    if (ALLOW_LOG)
                        Log.v(TAG, Utils.getMethodName() + foldername);

                    tmpItemList.add(Folder.fromCursor(folderCursor));

                } while (folderCursor.moveToNext());
            }

            folderCursor.close();
        }
        if (ALLOW_LOG)
            Log.v(TAG, Utils.getMethodName() + "get documents ");
        // get documents
        Cursor documentsCursor =
                context.getContentResolver().query(DocumentDB.CONTENT_URI, null,
                        DocumentDB.Columns.PARENT_ID.getName() + "='" + parentID + "'", null, null);

        if (documentsCursor != null) {

            if (documentsCursor.moveToFirst()) {
                int name_idx = DocumentDB.Columns.NAME.getIndex();

                do {
                    String docName = documentsCursor.getString(name_idx);

                    if (ALLOW_LOG)
                        Log.v(TAG, Utils.getMethodName() + docName);
                    tmpItemList.add(Document.fromCursor(documentsCursor));

                } while (documentsCursor.moveToNext());
            }

            documentsCursor.close();

        }
        /*
		for  ( Object item : tmpItemList)
		{
			
			Log.v(TAG, Utils.getMethodName() + ((SpacedItem) item).getName() + "type " +((SpacedItem) item).getType() );    
			
			if (((SpacedItem) item).getType().equals("document"))
			{
				Log.v(TAG, Utils.getMethodName() + "size " + ((Document) item).getStates().toString() + "size " +((Document) item).getSize() );    
			}
		}
		*/

        return tmpItemList;
    }

    public static Uri documentGetFileUri(Context context, Document document) {

        ContentValues cv = document.toContentValues();
        // update _data field to current file path 
        cv.put(DocumentDB.Columns.BINARY_DATA.getName(), document.getFileAbsPath());

        String docId = document.getId();
        context.getContentResolver().update(DocumentDB.CONTENT_URI, cv,
                DocumentDB.Columns.ID.getName() + "='" + docId + "'", null);

        Cursor docCursor =
                context.getContentResolver().query(DocumentDB.CONTENT_URI, null,
                        DocumentDB.Columns.ID.getName() + "='" + docId + "'", null, null);

        long docDBId = -1;

        if (docCursor != null) {

            if (docCursor.moveToFirst()) {
                docDBId = docCursor.getLong(0);
            }

            docCursor.close();
        }

        if (docDBId < 0)
            return null;

        return ContentUris.withAppendedId(DocumentDB.CONTENT_URI, docDBId);
    }


    public static OutputStream documnentGetFileOutputStream(Context context, Uri content_uri)
            throws FileNotFoundException {

        return context.getContentResolver().openOutputStream(content_uri, "w");
    }

    public static InputStream documentGetFileInputStream(Context context, Document document)
            throws FileNotFoundException {

        Uri fileUri = documentGetFileUri(context, document);

        if (fileUri == null) return null;


        InputStream fileStream = null;
        try {
            fileStream = context.getContentResolver().openInputStream(fileUri);
        } catch (FileNotFoundException e) {
            Log.d(TAG, "could not open provider thumb: " + e.toString());
        }

        return fileStream;
    }

    public static String getSpaceIdBySpaceName(Context context, String spaceName) {
        String spaceID = "";

        Cursor spacesCursor =
                context.getContentResolver().query(SpaceDB.CONTENT_URI, null,
                        SpaceDB.Columns.NAME.getName() + "='" + spaceName + "'", null, null);
        if (spacesCursor == null)
            return null;

        try {
            if (spacesCursor.moveToFirst()) {
                int idx = SpaceDB.Columns.ID.getIndex();
                spaceID = spacesCursor.getString(idx);
            }
        } finally {
            spacesCursor.close();

        }

        return spaceID;
    }

    public static Uri spaceInsert(Context context, Space space) {
        return context.getContentResolver().insert(SpaceDB.CONTENT_URI, space.toContentValues());
    }

    public static String spaceGetIdFromName(Context context, String spaceName) {
        String spaceID = "";

        Cursor spacesCursor =
                context.getContentResolver().query(SpaceDB.CONTENT_URI, null,
                        SpaceDB.Columns.NAME.getName() + "='" + spaceName + "'", null, null);
        if (spacesCursor == null)
            return null;

        try {
            if (spacesCursor.moveToFirst()) {
                int idx = SpaceDB.Columns.ID.getIndex();
                spaceID = spacesCursor.getString(idx);
            }
        } finally {

            spacesCursor.close();

        }

        return spaceID;
    }

    public static boolean spaceRemoveEmpties(Context context) {

        int count =
                context.getContentResolver().delete(SpaceDB.CONTENT_URI,
                        SpaceDB.Columns.ID.getName() + "=''", null);
        return count > 0;

    }

    public static void spaceReplace(Context context, Space tmpSpace) {

        context.getContentResolver().update(SpaceDB.CONTENT_URI, tmpSpace.toContentValues(),
                SpaceDB.Columns.ID.getName() + "='" + tmpSpace.getId() + "'", null);
    }

    public static void spaceUpdate(Context context, Space tmpSpace) {

        Space old_space = spaceGetById(context, tmpSpace.getId());

        tmpSpace.setmDocumentsEtag(old_space.getmDocumentsEtag());
        tmpSpace.setmEtag(old_space.getmEtag());
        tmpSpace.setmChildrenEtag(old_space.getmChildrenEtag());
        tmpSpace.setmSubfoldersEtag(old_space.getmSubfoldersEtag());
        // tmpSpace.setSynchronized(old_space.isSynchronized());

        spaceReplace(context, tmpSpace);
    }


    public static void spaceUpdateSubfolderEtag(Context context, Space tmpSpace, String etag) {

        tmpSpace.setmSubfoldersEtag(etag);

        spaceReplace(context, tmpSpace);

    }

    public static void spaceUpdateChildrenEtag(Context context, Space tmpSpace, String etag) {

        tmpSpace.setmChildrenEtag(etag);

        spaceReplace(context, tmpSpace);
    }

    public static void spaceUpdateDocumentsEtag(Context context, Space tmpSpace, String etag) {

        tmpSpace.setmDocumentsEtag(etag);

        spaceReplace(context, tmpSpace);
    }

    public static boolean spaceDelete(Context context, Space space) {
        int count =
                context.getContentResolver().delete(SpaceDB.CONTENT_URI,
                        SpaceDB.Columns.ID.getName() + "='" + space.getId() + "'", null);
        return count > 0;
    }


    public static boolean spaceDeleteByName(Context context, String spaceName) {

        Space space = spaceGetByName(context, spaceName);
        if (space == null) return false;
        int count =
                context.getContentResolver().delete(SpaceDB.CONTENT_URI,
                        SpaceDB.Columns.NAME.getName() + "='" + spaceName + "'", null);

        folderDeleteBySpaceID(context, space.getId());

        documentDeleteBySpaceID(context, space.getId());

        return count > 0;
    }

    public static boolean spaceDeleteAll(Context context) {

        int count =
                context.getContentResolver().delete(SpaceDB.CONTENT_URI,
                        null, null);


        context.getContentResolver().delete(FolderDB.CONTENT_URI,
                null, null);

        context.getContentResolver().delete(DocumentDB.CONTENT_URI,
                null, null);

        return count > 0;
    }

    public static boolean spaceDeleteById(Context context, String itemId) {

        int count =
                context.getContentResolver().delete(SpaceDB.CONTENT_URI,
                        SpaceDB.Columns.ID.getName() + "='" + itemId + "'", null);


        Log.v(TAG, Utils.getMethodName() + count + " space deleted");

        folderDeleteBySpaceID(context, itemId);

        documentDeleteBySpaceID(context, itemId);

        return count > 0;
    }

    // TODO has to use later
    public List<Space> spaceGetSortedByNameList(Context context) {
        // TODO continue ...
        List<Space> list = new ArrayList<Space>();
        Space space = null;
        Cursor spacesCursor =
                context.getContentResolver().query(SpaceDB.CONTENT_URI, null, null, null, null);
        if (spacesCursor == null)
            return null;

        try {
            if (spacesCursor.moveToFirst()) {
                space = Space.fromCursor(spacesCursor);
            }
        } finally {
            spacesCursor.close();
        }

        return list;
    }

    public static Space spaceGetById(Context context, String spaceId) {

        Space space = null;
        Cursor spacesCursor =
                context.getContentResolver().query(SpaceDB.CONTENT_URI, null,
                        SpaceDB.Columns.ID.getName() + "='" + spaceId + "'", null, null);
        if (spacesCursor == null)
            return null;

        try {
            if (spacesCursor.moveToFirst()) {
                space = Space.fromCursor(spacesCursor);
            }
        } finally {

            spacesCursor.close();

        }

        return space;
    }

    public static Space spaceGetByName(Context context, String spaceName) {

        Space space = null;
        Cursor spacesCursor =
                context.getContentResolver().query(SpaceDB.CONTENT_URI, null,
                        SpaceDB.Columns.NAME.getName() + "='" + spaceName + "'", null, null);
        if (spacesCursor == null)
            return null;

        try {
            if (spacesCursor.moveToFirst()) {
                space = Space.fromCursor(spacesCursor);
            }
        } finally {

            spacesCursor.close();

        }

        return space;
    }

    public static ArrayList<Space> spaceGetList(Context context) {
        // Load the spaced items specified by the fragment
        // arguments. Should use a Loader

        ArrayList<Space> tmpSpaceList = new ArrayList<Space>();
        // get folders
        Cursor cursor =
                context.getContentResolver().query(SpaceDB.CONTENT_URI, null, null, null, null);

        if (cursor != null) {

            Log.v(TAG, Utils.getMethodName() + "count " + cursor.getCount());

            if (cursor.moveToFirst()) {

                do {
                    // String foldername = cursor.getString(name_idx);                
                    // Log.v(TAG, Utils.getMethodName() + foldername);           		
                    tmpSpaceList.add(Space.fromCursor(cursor));

                } while (cursor.moveToNext());
            }


            cursor.close();


        }

        return tmpSpaceList;
    }

    public static boolean spaceIsExists(Context context, Space space) {
        boolean value;

        Cursor cursor =
                context.getContentResolver().query(SpaceDB.CONTENT_URI, null,
                        SpaceDB.Columns.ID.getName() + "='" + space.getId() + "'", null, null);

        if (cursor == null)
            return false;

        value = cursor.moveToFirst();

        cursor.close();


        return value;
    }

    public static Uri folderInsert(Context context, Folder folder) {
        return context.getContentResolver().insert(FolderDB.CONTENT_URI, folder.toContentValues());

    }

    public static void folderReplace(Context context, Folder new_folder) {

        context.getContentResolver().update(FolderDB.CONTENT_URI, new_folder.toContentValues(),
                FolderDB.Columns.ID.getName() + "='" + new_folder.getId() + "'", null);
    }

    public static void folderUpdate(Context context, Folder old_folder, Folder new_folder) {
        
        if (old_folder.getmDocumentsEtag() != null) 
            new_folder.setmDocumentsEtag(old_folder.getmDocumentsEtag());        
        if (old_folder.getmSubfoldersEtag() != null)
            new_folder.setmSubfoldersEtag(old_folder.getmSubfoldersEtag());
        if (old_folder.getmEtag() != null)
            new_folder.setmEtag(old_folder.getmEtag());
        if (old_folder.getmChildrenEtag() != null)
             new_folder.setmChildrenEtag(old_folder.getmChildrenEtag());

        String oldIdChain = old_folder.getIdChain();
        
        new_folder.setIdChain(oldIdChain);
        
        // folder is moved
        if(!new_folder.getParentId().equals(old_folder.getParentId()))
        {
            String oldParentIdChain = getIdChainByItemId(context,old_folder.getParentId());
            String newParentIdChain = getIdChainByItemId(context,new_folder.getParentId());
            
            String newIdChain = old_folder.getIdChain().replace(oldParentIdChain, newParentIdChain);

            new_folder.setIdChain(newIdChain);

            changeIdChainAndSpaceIdInSubItems(context, oldIdChain, newIdChain, new_folder.getSpaceId());
        }    

        folderReplace(context, new_folder);
    }

    public static String getIdChainByItemId(Context context,String Id) {
        String idChain = "";

        Cursor cursor =
                context.getContentResolver().query(SpaceDB.CONTENT_URI, null,
                        SpaceDB.Columns.ID.getName() + "='" + Id + "'", null, null);

        try {
            if (cursor.moveToFirst()) {
                idChain = Space.fromCursor(cursor).getId();
            }
        } finally {
            cursor.close();
        }
        
        if ( idChain.length() > 0 )
            return idChain;
        else 
        {
        cursor =
                context.getContentResolver().query(FolderDB.CONTENT_URI, null,
                        FolderDB.Columns.ID.getName() + "='" + Id + "'", null, null);

        try {
            if (cursor.moveToFirst()) {
                idChain = Folder.fromCursor(cursor).getIdChain();
            }
        } finally {

            cursor.close();

        }
        }
        return idChain;

    }

    private static void changeIdChainAndSpaceIdInSubItems(Context context, String oldParentIdChain, String newParentIdChain, String newSpaceId) {

        List<Folder> folderList = new ArrayList<Folder>();
        List<Document> documentList = new ArrayList<Document>();

        // Log.w(TAG, Utils.getMethodName() + " find subfolders IdChain contains  " + oldParentIdChain);
        
        // search all item contains oldParentIdChain
        Cursor cursor = context.getContentResolver().query(
                FolderDB.CONTENT_URI,
                null,
                FolderDB.Columns.ID_CHAIN.getName() +" LIKE ?",
                new String[] { oldParentIdChain + ";%" },
                null);
        
        if (cursor.moveToFirst()) {
            do {
                Folder folder = Folder.fromCursor(cursor);

                folder.getIdChain().replace(oldParentIdChain, newParentIdChain);
                folder.setSpaceId(newSpaceId);
               // Log.w(TAG, Utils.getMethodName() + " new subfolder spaceid " + folder.getSpaceId());
                folderList.add(folder);

            } while (cursor.moveToNext());
        }

        cursor.close();

        Log.w(TAG, Utils.getMethodName() + " find docus IdChain contains  " + oldParentIdChain);
        cursor =
                context.getContentResolver().query(DocumentDB.CONTENT_URI,null,
                        DocumentDB.Columns.ID_CHAIN.getName() +" LIKE ?",
                        new String[] { oldParentIdChain + ";%" },
                        null);


        if (cursor.moveToFirst()) {
            do {
                Document document = Document.fromCursor(cursor);
                document.getIdChain().replace(oldParentIdChain,newParentIdChain);

               // Log.w(TAG, Utils.getMethodName() + " replace document spaceid " + document.getSpaceId());
                
                document.setSpaceId(newSpaceId);

               // Log.w(TAG, Utils.getMethodName() + " replace document spaceid 2 " + document.getSpaceId());
                documentList.add(document);
               

            } while (cursor.moveToNext());
        }


        
        cursor.close();

        for (Folder folder: folderList)
        {
            folderReplace(context,folder);
        }

        for (Document document: documentList)
        {
            documentReplace(context, document);
        }
        
    }
    

    public static void folderUpdateSubfolderEtag(Context context, Folder folder, String Etag) {
        folder.setmSubfoldersEtag(Etag);

        folderReplace(context, folder);
    }

    public static void folderUpdateDocumentsEtag(Context context, Folder folder, String Etag) {
        folder.setmDocumentsEtag(Etag);

        folderReplace(context, folder);
    }


    public static boolean folderDeleteByName(Context context, String name) {

        Folder folder = folderGetByName(context, name);

        if (folder == null) return false;

        String id = folder.getId();

        return folderDeleteById(context, id);

    }

    public static boolean folderDeleteById(Context context, String id) {
		/*
		// TODO delete all items of folder
		// search for existed folders item in this folder
		 * */
        boolean result = false;
        // delete all sub folders contains this id 
        int count =
                context.getContentResolver().delete(FolderDB.CONTENT_URI,
                        FolderDB.Columns.ID_CHAIN.getName() + " LIKE '%" + id + "%'", null);

        result = count > 0;

        // delete all documents contain this id 
        count =
                context.getContentResolver().delete(DocumentDB.CONTENT_URI,
                        DocumentDB.Columns.ID_CHAIN.getName() + " LIKE '%" + id + "%'", null);

        result = count > 0;

        // delete his self
        count =
                context.getContentResolver().delete(FolderDB.CONTENT_URI,
                        FolderDB.Columns.ID.getName() + "='" + id + "'", null);

        result = count > 0;

        return result;
    }

    public static boolean folderDeleteById_OLD(Context context, String id) {

        int count =
                context.getContentResolver().delete(FolderDB.CONTENT_URI,
                        FolderDB.Columns.ID.getName() + "='" + id + "'", null);

        return count > 0;
    }

    public static int folderDeleteBySpaceID(Context context, String space_id) {
        return
                context.getContentResolver().delete(FolderDB.CONTENT_URI,
                        FolderDB.Columns.SPACE_ID.getName() + "='" + space_id + "'", null);

    }

    public static int folderDeleteByIdChain(Context context, String id_chain) {
        return
                context.getContentResolver().delete(FolderDB.CONTENT_URI,
                        FolderDB.Columns.ID_CHAIN.getName() + "='" + id_chain + "'", null);

    }

    public static Folder folderGetByName(Context context, String folderName) {

        Folder folder = null;
        Cursor cursor =
                context.getContentResolver().query(FolderDB.CONTENT_URI, null,
                        FolderDB.Columns.NAME.getName() + "='" + folderName + "'", null, null);

        if (cursor == null)
            return null;

        try {
            if (cursor.moveToFirst()) {
                folder = Folder.fromCursor(cursor);
            }
        } finally {

            cursor.close();

        }

        return folder;
    }

    public static String folderGetIdChainById(Context context, String id) {
        String folderIdChain = "";

        Cursor spacesCursor =
                context.getContentResolver().query(FolderDB.CONTENT_URI, null,
                        FolderDB.Columns.ID.getName() + "='" + id + "'", null, null);
        if (spacesCursor == null)
            return null;

        try {
            if (spacesCursor.moveToFirst()) {
                int idx = FolderDB.Columns.ID_CHAIN.getIndex();
                folderIdChain = spacesCursor.getString(idx);
            }
        } finally {

            spacesCursor.close();

        }

        return folderIdChain;
    }

    /**
     * children of space or folder has the same types (folder and document)
     *
     * @param context
     * @param itemName
     * @return
     */
    public static String folderGetIdByName(Context context, String itemName) {
        String folderID = "";

        Cursor spacesCursor =
                context.getContentResolver().query(FolderDB.CONTENT_URI, null,
                        FolderDB.Columns.NAME.getName() + "='" + itemName + "'", null, null);
        if (spacesCursor == null)
            return null;

        try {
            if (spacesCursor.moveToFirst()) {
                int idx = FolderDB.Columns.ID.getIndex();
                folderID = spacesCursor.getString(idx);
            }
        } finally {

            spacesCursor.close();

        }

        return folderID;
    }

    public static List<Folder> folderGetListByParentID(Context context, String parentID) {

        Cursor cursor =
                context.getContentResolver().query(FolderDB.CONTENT_URI, null,
                        FolderDB.Columns.PARENT_ID.getName() + "='" + parentID + "'", null, null);

        List<Folder> folders = new ArrayList<Folder>();

        if (cursor == null)
            return null;

        if (cursor.moveToFirst()) {

            do {
                folders.add(Folder.fromCursor(cursor));

            } while (cursor.moveToNext());
        }

        cursor.close();

        return folders;
    }

    public static ArrayList<Folder> folderGetListByParentId(Context context, String parentId) {

        ArrayList<Folder> folderList = new ArrayList<Folder>();

        // search for existed folders in this folder/space
        Cursor folderCursor =
                context.getContentResolver().query(FolderDB.CONTENT_URI, null,
                        FolderDB.Columns.PARENT_ID.getName() + "='" + parentId + "'", null, null);

        if (folderCursor == null)
            return null;

        // delete local folder which not exists on server and create ID list of existing folders in this parent
        if (folderCursor.moveToFirst()) {
            do {
                // Log.v(TAG, Utils.getMethodName() + space_id);    
                folderList.add(Folder.fromCursor(folderCursor));

            } while (folderCursor.moveToNext());
        }

        folderCursor.close();

        return folderList;
    }

    public static Boolean folderIsExists(Context context, Folder folder) {

        boolean value;

        Cursor cursor =
                context.getContentResolver().query(FolderDB.CONTENT_URI, null,
                        FolderDB.Columns.ID.getName() + "='" + folder.getId() + "'", null, null);
        if (cursor == null)
            return null;

        value = cursor.moveToFirst();

        cursor.close();


        return value;
    }

    public static Folder folderGetById(Context context, String id) {

        Folder folder = null;

        Cursor folderCursor =
                context.getContentResolver().query(FolderDB.CONTENT_URI, null,
                        FolderDB.Columns.ID.getName() + "='" + id + "'", null, null);
        if (folderCursor == null)
            return null;
        if (folderCursor.moveToFirst()) {
            folder = Folder.fromCursor(folderCursor);
        }

        folderCursor.close();

        return folder;
    }

    public static String folderGetIdFromName(Context context, String itemName) {
        String folderID = "";

        Cursor spacesCursor =
                context.getContentResolver().query(SpaceDB.CONTENT_URI, null,
                        SpaceDB.Columns.NAME.getName() + "='" + itemName + "'", null, null);
        if (spacesCursor == null)
            return null;

        try {
            if (spacesCursor.moveToFirst()) {
                int idx = SpaceDB.Columns.ID.getIndex();
                folderID = spacesCursor.getString(idx);
            }
        } finally {

            spacesCursor.close();

        }

        return folderID;
    }

    public static Uri documentInsert(Context context, Document document) {
        return context.getContentResolver().insert(DocumentDB.CONTENT_URI,
                document.toContentValues());
    }

    /**
     * replace document with new once
     *
     * @param context
     * @param new_document
     */
    public static void documentReplace(Context context, Document new_document) {

        context.getContentResolver().update(DocumentDB.CONTENT_URI, new_document.toContentValues(),
                DocumentDB.Columns.ID.getName() + "='" + new_document.getId() + "'", null);
    }

    /**
     * update document
     *
     * @param context
     * @param oldDocument
     * @param new_document
     */
    public static void documentUpdate(Context context, Document oldDocument, Document new_document) {
  

        new_document.setFileAbsPath(oldDocument.getFileAbsPath());
        new_document.setPreviewAbsPath(oldDocument.getPreviewAbsPath());
        String idChainOfNewDocument = new_document.getIdChain();
        if (idChainOfNewDocument == null)
            new_document.setIdChain(oldDocument.getIdChain());
        
        // document is moved
        if(!new_document.getParentId().equals(oldDocument.getParentId()))
        {
            String oldIdChain = oldDocument.getIdChain();
            
            String oldParentIdChain = getIdChainByItemId(context,oldDocument.getParentId());
            String newParentIdChain = getIdChainByItemId(context,new_document.getParentId());
            String newIdChain = oldIdChain.replace(oldParentIdChain, newParentIdChain);

            new_document.setIdChain(newIdChain);
        }

        documentReplace(context,new_document);

    }

    public static void documentUpdatePreviewPath(Context context, Document local_doc, String path) {
        local_doc.setPreviewAbsPath(path);

        context.getContentResolver().update(DocumentDB.CONTENT_URI, local_doc.toContentValues(),
                DocumentDB.Columns.ID.getName() + "='" + local_doc.getId() + "'", null);
    }

    public static void documentUpdateFilePath(Context context, Document document, String path) {
        document.setFileAbsPath(path);

        context.getContentResolver().update(DocumentDB.CONTENT_URI, document.toContentValues(),
                DocumentDB.Columns.ID.getName() + "='" + document.getId() + "'", null);
    }

    public static int documentDeleteById(Context context, String id) {

        // TODO have to remove files on file system too
        return context.getContentResolver().delete(DocumentDB.CONTENT_URI,
                DocumentDB.Columns.ID.getName() + "='" + id + "'", null);
    }

    public static int documentDeleteBySpaceID(Context context, String space_id) {

        // TODO have to remove files on file system too
        return context.getContentResolver().delete(DocumentDB.CONTENT_URI,
                DocumentDB.Columns.SPACE_ID.getName() + "='" + space_id + "'", null);
    }

    public static boolean documentDeleteByIdChain(Context context, String id) {

        int count =
                context.getContentResolver().delete(DocumentDB.CONTENT_URI,
                        DocumentDB.Columns.ID_CHAIN.getName() + "='" + id + "'", null);

        return count > 0;
    }

    public static int documentDeleteByParentID(Context context, String parent_id) {

        // TODO have to remove files on file system too
        return context.getContentResolver().delete(DocumentDB.CONTENT_URI,
                DocumentDB.Columns.PARENT_ID.getName() + "='" + parent_id + "'", null);
    }

    public static ArrayList<Document> documentGetListBySpaceId(Context context, String spaceId) {

        Cursor docCursor =
                context.getContentResolver().query(DocumentDB.CONTENT_URI, null,
                        DocumentDB.Columns.SPACE_ID.getName() + "='" + spaceId + "'", null, null);

        ArrayList<Document> currentDocIDList = new ArrayList<Document>();

        if (docCursor == null)
            return null;

        if (docCursor.moveToFirst()) {
            do {
                currentDocIDList.add(Document.fromCursor(docCursor));

            } while (docCursor.moveToNext());
        }

        docCursor.close();


        return currentDocIDList;
    }
    
    public static ArrayList<Document> documentGetListByParentID(Context context, String parentID) {

        Cursor docCursor =
                context.getContentResolver().query(DocumentDB.CONTENT_URI, null,
                        DocumentDB.Columns.PARENT_ID.getName() + "='" + parentID + "'", null, null);

        ArrayList<Document> currentDocIDList = new ArrayList<Document>();

        if (docCursor == null)
            return null;

        if (docCursor.moveToFirst()) {
            do {
                currentDocIDList.add(Document.fromCursor(docCursor));

            } while (docCursor.moveToNext());
        }

        docCursor.close();


        return currentDocIDList;
    }

    public static String documentGetModificationIdById(Context context, String tmpDocumentID) {
        String modId = null;

        Cursor tmpDocumentCursor =
                context.getContentResolver().query(DocumentDB.CONTENT_URI, null,
                        DocumentDB.Columns.ID.getName() + "='" + tmpDocumentID + "'", null, null);

        if (tmpDocumentCursor == null)
            return null;

        if (tmpDocumentCursor.moveToFirst()) {
            modId = tmpDocumentCursor.getString(DocumentDB.Columns.MODIFICATION_ID.getIndex());

        }

        tmpDocumentCursor.close();

        return modId;
    }

    public static Document documentGetById(Context context, String Id) {

        Document doc = null;
        Cursor tmpDocumentCursor =
                context.getContentResolver().query(DocumentDB.CONTENT_URI, null,
                        DocumentDB.Columns.ID.getName() + "='" + Id + "'", null, null);

        if (tmpDocumentCursor == null)
            return null;

        if (tmpDocumentCursor.moveToFirst()) {
            doc = Document.fromCursor(tmpDocumentCursor);

        }

        tmpDocumentCursor.close();

        return doc;
    }

    public static boolean documentIsExists(Context context, Document document) {

        boolean value = false;

        Cursor cursor =
                context.getContentResolver()
                        .query(DocumentDB.CONTENT_URI, null,
                                DocumentDB.Columns.ID.getName() + "='" + document.getId() + "'",
                                null, null);
        if (cursor == null)
            return value;

        value = cursor.moveToFirst();


        cursor.close();


        return value;
    }

    public static String documentGetFileAbsPath(Context context, String doc_id) {

        Cursor docCursor =
                context.getContentResolver().query(DocumentDB.CONTENT_URI, null,
                        DocumentDB.Columns.ID.getName() + "='" + doc_id + "'", null, null);

        String path = "";

        if (docCursor != null) {

            if (docCursor.moveToFirst()) {
                path = docCursor.getString(Columns.BINARY_DATA.getIndex());
            }

            docCursor.close();
        }

        return path;

    }

    public static ArrayList<User> userGetList(Context context) {
        // Load the spaced items specified by the fragment
        // arguments. Should use a Loader

        ArrayList<User> tmpUserList = new ArrayList<User>();
        // get folders
        Cursor cursor =
                context.getContentResolver().query(UserDB.CONTENT_URI, null, null, null, null);

        if (cursor != null) {

            Log.v(TAG, Utils.getMethodName() + "count " + cursor.getCount());

            if (cursor.moveToFirst()) {

                do {

                    tmpUserList.add(User.fromCursor(cursor));

                } while (cursor.moveToNext());
            }

            cursor.close();

        }

        return tmpUserList;
    }

    public static boolean userDeleteById(Context context, String itemId) {

        int count =
                context.getContentResolver().delete(UserDB.CONTENT_URI,
                        UserDB.Columns.ID.getName() + "='" + itemId + "'", null);

        return count > 0;
    }

    public static User userGetByEmail(Context context, String email) {

        User user = null;
        Cursor cursor =
                context.getContentResolver().query(UserDB.CONTENT_URI, null,
                        UserDB.Columns.EMAIL.getName() + "='" + email + "'", null, null);

        if (cursor == null)
            return null;

        if (cursor.moveToFirst()) {
            user = User.fromCursor(cursor);

        }

        cursor.close();

        return user;
    }
    


    public static void userInsertBulk(Context context, ArrayList<User> userList) throws DataException {
        // Clear the table first
        context.getContentResolver().delete(UserDB.CONTENT_URI, null, null);

        if (userList == null) return;
        int userListSize = userList.size();
        // Add new users to database

        if (userListSize > 0)
        {

            ArrayList<ContentProviderOperation> operationList =
                    new ArrayList<ContentProviderOperation>();
            
            for (int i = 0; i < userListSize; i++){
            
                operationList.add(ContentProviderOperation.newInsert(UserDB.CONTENT_URI)
                        .withValues(userList.get(i).toContentValues()).build());

            }
        
            try {
                context.getContentResolver().applyBatch(SdbContentProvider.AUTHORITY, operationList);
            } catch (RemoteException e) {
                Log.e(TAG, Utils.getMethodName() + "error " + e.toString());
                throw new DataException(e);
            } catch (OperationApplicationException e) {
                Log.e(TAG, Utils.getMethodName() +  "error " + e.toString());
                throw new DataException(e);
            }
            catch (RuntimeException e) {
                Log.e(TAG, Utils.getMethodName() +  "error " + e.toString());
                throw new DataException(e);
            }
        }
    }

    public static Uri userInsert(Context context, User user) {
        return context.getContentResolver().insert(UserDB.CONTENT_URI, user.toContentValues());
    }

    public static void userUpdate(Context context, User user) {

        context.getContentResolver().update(UserDB.CONTENT_URI, user.toContentValues(),
                UserDB.Columns.ID.getName() + "='" + user.getId() + "'", null);
    }

    public static void userDelete(Context context, User user) {
        int count =
                context.getContentResolver().delete(UserDB.CONTENT_URI,
                        UserDB.Columns.ID.getName() + "='" + user.getId() + "'", null);
    }

    public static void userDelete(Context context, String userId) {
        int count =
                context.getContentResolver().delete(UserDB.CONTENT_URI,
                        UserDB.Columns.ID.getName() + "='" + userId + "'", null);
    }


    public static User userGetById(Context context, String userId) {

        User user = null;
        Cursor cursor =
                context.getContentResolver().query(UserDB.CONTENT_URI, null,
                        UserDB.Columns.ID.getName() + "='" + userId + "'", null, null);

        if (cursor == null)
            return null;

        if (cursor.moveToFirst()) {
            user = User.fromCursor(cursor);

        }

        cursor.close();

        return user;
    }
    

    public static void linkInsertBulk(Context context, ArrayList<Link> linkList) throws DataException {
        // Clear the table first
        context.getContentResolver().delete(LinkDB.CONTENT_URI, null, null);

        if (linkList == null) return;
        int linkListSize = linkList.size();
        // Add new users to database

        if (linkListSize > 0)
        {

            ArrayList<ContentProviderOperation> operationList =
                    new ArrayList<ContentProviderOperation>();

            for (int i = 0; i < linkListSize; i++){

                operationList.add(ContentProviderOperation.newInsert(LinkDB.CONTENT_URI)
                        .withValues(linkList.get(i).toContentValues()).build());

            }

            try {
                context.getContentResolver().applyBatch(SdbContentProvider.AUTHORITY, operationList);
            } catch (RemoteException e) {
                Log.e(TAG, Utils.getMethodName() + "error " + e.toString());
                throw new DataException(e);
            } catch (OperationApplicationException e) {
                Log.e(TAG, Utils.getMethodName() +  "error " + e.toString());
                throw new DataException(e);
            }
            catch (RuntimeException e) {
                Log.e(TAG, Utils.getMethodName() +  "error " + e.toString());
                throw new DataException(e);
            }
        }
    }


    public static ArrayList<Link> linkGetListCached(Context context) {
        // Load the spaced items specified by the fragment
        // arguments. Should use a Loader

        ArrayList<Link> linkList = new ArrayList<Link>();
        // get folders
        Cursor cursor =
                context.getContentResolver().query(LinkDB.CONTENT_URI, null, null, null, null);

        if (cursor != null) {

            Log.v(TAG, Utils.getMethodName() + "count " + cursor.getCount());

            if (cursor.moveToFirst()) {

                do {

                    linkList.add(Link.fromCursor(cursor));

                } while (cursor.moveToNext());
            }

            cursor.close();

        }

        return linkList;
    }

    public static Uri shareInsert(Context context, Share share) {
        return context.getContentResolver().insert(ShareDB.CONTENT_URI, share.toContentValues());
    }

    public static void shareUpdate(Context context, Share share) {

        context.getContentResolver().update(ShareDB.CONTENT_URI, share.toContentValues(),
                ShareDB.Columns.ID.getName() + "='" + share.getId() + "'", null);
    }

    public static void shareDelete(Context context, Share share) {
        int count =
                context.getContentResolver().delete(ShareDB.CONTENT_URI,
                        ShareDB.Columns.ID.getName() + "='" + share.getId() + "'", null);
    }

    public static void shareDelete(Context context, String shareId) {
        int count =
                context.getContentResolver().delete(ShareDB.CONTENT_URI,
                        ShareDB.Columns.ID.getName() + "='" + shareId + "'", null);
    }

    
    public static void shareInsertBulk(Context context, ArrayList<Share> shareList) throws DataException {
        // Clear the table first
        context.getContentResolver().delete(ShareDB.CONTENT_URI, null, null);

        if (shareList == null) return;
        int shareListSize = shareList.size();
        // Add new users to database

        if (shareListSize > 0)
        {

            ArrayList<ContentProviderOperation> operationList =
                    new ArrayList<ContentProviderOperation>();

            for (int i = 0; i < shareListSize; i++){

                operationList.add(ContentProviderOperation.newInsert(ShareDB.CONTENT_URI)
                        .withValues(shareList.get(i).toContentValues()).build());

            }

            try {
                context.getContentResolver().applyBatch(SdbContentProvider.AUTHORITY, operationList);
            } catch (RemoteException e) {
                Log.e(TAG, Utils.getMethodName() + "error " + e.toString());
                throw new DataException(e);
            } catch (OperationApplicationException e) {
                Log.e(TAG, Utils.getMethodName() +  "error " + e.toString());
                throw new DataException(e);
            }
            catch (RuntimeException e) {
                Log.e(TAG, Utils.getMethodName() +  "error " + e.toString());
                throw new DataException(e);
            }
        }
    }
    
    public static ArrayList<Share> shareGetListCached(Context context) {
        // Load the spaced items specified by the fragment
        // arguments. Should use a Loader

        ArrayList<Share> shareList = new ArrayList<Share>();
        // get folders
        Cursor cursor =
                context.getContentResolver().query(ShareDB.CONTENT_URI, null, null, null, null);

        if (cursor != null) {

            Log.v(TAG, Utils.getMethodName() + "count " + cursor.getCount());

            if (cursor.moveToFirst()) {

                do {

                    shareList.add(Share.fromCursor(cursor));

                } while (cursor.moveToNext());
            }

            cursor.close();
        }

        return shareList;
    }
}
