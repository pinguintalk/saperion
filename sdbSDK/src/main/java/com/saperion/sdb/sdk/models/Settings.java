package com.saperion.sdb.sdk.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.saperion.sdb.sdk.utils.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@JsonIgnoreProperties({ "id"})
public class Settings extends TypedIdentifiable implements Parcelable {

    private static final String TAG = Settings.class.getSimpleName();
    
    private Setting[] list = null;
    
    public Settings() {
        super(ModelType.SETTINGS);
    }


    public Setting[] getList() {
        return list;
    }

    public void setList(Setting[] list) {
        this.list = list;
    }


    public static Settings fromJson(String json) {

        Settings settings = new Settings();
        Setting[] settingList = null;
        
        try {
            JSONObject settingsObject = new JSONObject(json);

            JSONArray list_array = settingsObject.getJSONArray("list");
            
            if (list_array.length() > 0) {
                settingList = new Setting[list_array.length()];
                for (int s=0; s < list_array.length(); s++) {
                    settingList[s] = Setting.fromJson(list_array.get(s).toString());
                }
                settings.setList(settingList);
            }
        } catch (JSONException e) {

            Log.e(TAG, "error" + e.toString());
            e.printStackTrace();
        }

        return settings;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.getType());
        out.writeParcelableArray(list, 0);
    }

    private Settings(Parcel in) {

        this.setType(in.readString());
        Parcelable[] parcels = in.readParcelableArray(Setting.class.getClassLoader());
        Setting[] list_tmp = new Setting[parcels.length];

        for (int p = 0; p < parcels.length; p++)
        {
            list_tmp[p] = (Setting) parcels[p];
           
        }
        setList(list_tmp);

    }

    public static final Creator CREATOR = new Creator() {
        public Settings createFromParcel(Parcel in) {
            return new Settings(in);
        }

        public Settings[] newArray(int size) {
            return new Settings[size];
        }
    };

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public String toString() {
        return "Settings {" + "list=" + list.toString() ;
    }
}
