package com.saperion.sdb.sdk.operations;

import android.content.Context;
import android.os.Bundle;

import com.saperion.sdb.sdk.Constants;
import com.saperion.sdb.sdk.config.WSConfig;
import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.factory.DocumentJsonFactory;
import com.saperion.sdb.sdk.models.Document;
import com.saperion.sdb.sdk.network.NetworkConnection;
import com.saperion.sdb.sdk.network.NetworkConnection.ConnectionResult;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.security.CredentialManager;
import com.saperion.sdb.sdk.service.RequestService.Operation;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

import org.apache.http.auth.UsernamePasswordCredentials;

import java.io.File;
import java.util.HashMap;

public final class DocumentGetRenditionByVersionOperation implements Operation {

    private static final String TAG = DocumentGetRenditionByVersionOperation.class.getSimpleName();

    public static final String PARAM_DOCUMENT = Constants.SDK_PACKAGE_NAME + ".param_doc";
    public static final String PARAM_DOCUMENT_VERSION = Constants.SDK_PACKAGE_NAME + ".param_doc_version";
    // public static final int FILE_SIZE_THRESHOLD = 2000000; // 2MB


    @Override
    public Bundle execute(Context context, Request request) throws ServiceRequestException, ConnectionException,
            DataException {
        Bundle bundle = new Bundle();
        Log.v(TAG, Utils.getMethodName() + "entry");

        Document cached_document = (Document) request.getParcelable(PARAM_DOCUMENT);
        String id = cached_document.getId();
        int docVersion = request.getInt(PARAM_DOCUMENT_VERSION);
        
        /*
        // if cached file is NOT up to date, NOT exists or not so big, we should to download it createInstance server
        String url = WSConfig.getDocumentURL() + "/" + id + "/" + WSConfig.WS_FILE + "?version=" + docVersion;

        String mimetype = cached_document.getMimeType();
        if (mimetype.contains("ppt") || mimetype.contains("doc") || mimetype.contains("docx")) {
            url = WSConfig.getDocumentURL() + "/" + id + "/" + WSConfig.WS_RENDITION + "?version=" + docVersion;

        }
        */

        String absRenditionFilePath = Utils.getRenditionsDirectory(context) + id + docVersion + ".pdf";

        File cached_rendition = new File(absRenditionFilePath);

        if (cached_rendition.exists()) {
            // if we have a cached file stored and the file is bigger as FILE_SIZE_THRESHOLD 

            Log.v(TAG, Utils.getMethodName() + "getModificationId: "
                    + cached_document.getModificationId() + "cached_file.length() = " + cached_rendition.length());


                // so we need to check if the file is really up to date
                String url = WSConfig.getDocumentURL() + "/" + id + WSConfig.WS_INCLUDE_MODIFICATION_ID;
                NetworkConnection getDocuConnection = new NetworkConnection(context, url);

                HashMap<String, String> header_list = new HashMap<String, String>();
                header_list.put("ACCEPT", "application/json");
                getDocuConnection.setHeaderList(header_list);

                getDocuConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager.getInstance().getUserName(),
                        CredentialManager.getInstance().getmAccessToken()));

                ConnectionResult result = getDocuConnection.execute();

                bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE,
                        result.responseCode);
                if (result.responseCode != ResponseConstants.RESPONSE_CODE_OK) {
                    bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
                    return bundle;
                }

                Document new_document = DocumentJsonFactory.jsonToDocument(result.body);

                //  Check ModificationId  

                if (new_document.getModificationId().equals(cached_document.getModificationId())) { // cached file is up to date, so we can uses it

                    Log.v(TAG, Utils.getMethodName() + " new_documen ModificationId() = " + new_document.getModificationId() +
                            " absRenditionFilePath" + absRenditionFilePath);

                    bundle.putString(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, absRenditionFilePath);
                    return bundle;
                }
            }


        String url = WSConfig.getDocumentURL() + "/" + id + "/" + WSConfig.WS_RENDITION + "?version=" + docVersion;

        NetworkConnection networkConnection = new NetworkConnection(context, url);

        HashMap<String, String> requestParams = new HashMap<String, String>();

        requestParams.put(NetworkConnection.FILE_PATH, absRenditionFilePath);
        requestParams.put(NetworkConnection.REQUEST_TYPE,
                NetworkConnection.REEQUEST_TYPE_DOWNLOAD);
        networkConnection.setRequestParams(requestParams);

        HashMap<String, String> header_list = new HashMap<String, String>();
        header_list.put("ACCEPT", "application/octet-stream");
        networkConnection.setHeaderList(header_list);

        networkConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager
                .getInstance().getUserName(), CredentialManager.getInstance().getmAccessToken()));

        ConnectionResult result = networkConnection.execute();

        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);

        if (result.responseCode != ResponseConstants.RESPONSE_CODE_OK) {
            Log.w(TAG, Utils.getMethodName() + " responseCode:" + result.responseCode);

            bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
            return bundle;
        }

        Log.v(TAG, Utils.getMethodName() + " rendition downloaded: " + result.body);

        bundle.putString(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, absRenditionFilePath);
        return bundle;
    }

}
