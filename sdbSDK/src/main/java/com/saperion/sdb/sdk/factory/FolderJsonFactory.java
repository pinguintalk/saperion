package com.saperion.sdb.sdk.factory;

import android.text.TextUtils;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.models.Folder;

import java.io.IOException;
import java.util.ArrayList;

public final class FolderJsonFactory {

    private static final String TAG = FolderJsonFactory.class.getSimpleName();

    private static boolean LOG_ALL = false;

    private FolderJsonFactory() {
        // No public constructor
    }

    public static String toJson(Folder folder) {

        ObjectMapper mapper = new ObjectMapper();
        //    	mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        mapper.setSerializationInclusion(Include.NON_NULL);
        try {
            String f = mapper.writeValueAsString(folder);
            return f;
        } catch (JsonProcessingException e1) {
            e1.printStackTrace();
        }
        return null;
    }

    public static Folder jsonToFolder(String string) {
        Folder folder = new Folder();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        try {
            folder = mapper.readValue(string, Folder.class);
            return folder;
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static ArrayList<Folder> parseResult(String wsResponse) throws DataException {

        if (wsResponse == null || TextUtils.isEmpty(wsResponse))
            return null;
        ArrayList<Folder> folderList = new ArrayList<Folder>();

        ObjectMapper objectMapper = new ObjectMapper();
        JsonFactory jsonFactory = new JsonFactory();

        try {
            JsonParser jp = jsonFactory.createJsonParser(wsResponse);
            // advance stream to START_ARRAY first:
            jp.nextToken();
            // and then each time, advance to opening START_OBJECT
            while (jp.nextToken() == JsonToken.START_OBJECT) {
                Folder folder = objectMapper.readValue(jp, Folder.class);
                // process
                // after binding, stream points to closing END_OBJECT
                folderList.add(folder);

                // Log.v(TAG, Utils.getMethodName() + folder.toString());
            }

        } catch (JsonParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return folderList;
    }
}
