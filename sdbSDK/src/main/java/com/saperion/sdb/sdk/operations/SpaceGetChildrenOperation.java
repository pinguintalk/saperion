package com.saperion.sdb.sdk.operations;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;

import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.factory.DocumentJsonFactory;
import com.saperion.sdb.sdk.factory.FolderJsonFactory;
import com.saperion.sdb.sdk.models.Document;
import com.saperion.sdb.sdk.models.Folder;
import com.saperion.sdb.sdk.models.Space;
import com.saperion.sdb.sdk.models.SpacedItem;
import com.saperion.sdb.sdk.network.NetworkConnection;
import com.saperion.sdb.sdk.providers.ContentProviderHelpers;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.service.RequestService.Operation;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public final class SpaceGetChildrenOperation implements Operation {

    private static final String TAG = SpaceGetChildrenOperation.class.getSimpleName();

    public static final String PARAM_SPACE_ID = "com.saperion.sdb.client.extra.spaceId";

    @Override
    public Bundle execute(Context context, Request request) throws ServiceRequestException, ConnectionException,
            DataException {

        Log.v(TAG, Utils.getMethodName() + "entry");
        Bundle bundle = new Bundle();
        String spaceId = request.getString(PARAM_SPACE_ID);

        List<? super SpacedItem> items = new ArrayList<SpacedItem>();

        ArrayList<Folder> folder_list = new ArrayList<Folder>();
        ArrayList<Document> document_list = new ArrayList<Document>();

        NetworkConnection.ConnectionResult result;
        /* get folder list createInstance server of current parent */
        result = OperationHelpers.folderGetSubfoldersByParentId(context, spaceId, true);

        Space space = ContentProviderHelpers.spaceGetById(context, spaceId);
        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);

        if (result.responseCode == ResponseConstants.RESPONSE_CODE_NOT_MODIFIED) {
            // get local folders
            folder_list = ContentProviderHelpers.folderGetListByParentId(context, spaceId);
            
            items.addAll(folder_list);
            
            bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE,
                    ResponseConstants.RESPONSE_CODE_OK);
        } else if (result.responseCode == ResponseConstants.RESPONSE_CODE_OK) {
            if (!result.body.equals("[]")) {
                folder_list = FolderJsonFactory.parseResult(result.body);
                items.addAll(folder_list);
            }
            List<String> etag_list = result.headerMap.get("ETag");
            String Etag = etag_list.get(0).toString();

            ContentProviderHelpers.spaceUpdateSubfolderEtag(context, space, Etag);
            OperationHelpers.foldersUpdateByParentId(context, spaceId, spaceId, folder_list);
        } else {
            Log.w(TAG, Utils.getMethodName() + " got error code createInstance network:"
                    + result.responseCode);

            // so we can only deliver local folder
            bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
            bundle.putParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT,
                    (ArrayList<? extends Parcelable>) items);
            return bundle;
        }

        result = OperationHelpers.getDocumentListByParentId(context, spaceId, true);

        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);

        if (result.responseCode == ResponseConstants.RESPONSE_CODE_NOT_MODIFIED) {
            // get local folders
            document_list = ContentProviderHelpers.documentGetListByParentID(context, spaceId);
            items.addAll(document_list);
            bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE,
                    ResponseConstants.RESPONSE_CODE_OK);
        } else if (result.responseCode == ResponseConstants.RESPONSE_CODE_OK) {
            if (!result.body.equals("[]")) {
                //waiting for response and parse it
                document_list = DocumentJsonFactory.parseResult(result.body);
                items.addAll(document_list);
            }

            List<String> etag_list = result.headerMap.get("ETag");
            String Etag = etag_list.get(0).toString();
            Log.v(TAG, Utils.getMethodName() + "doc etag_list:" + Etag);

            ContentProviderHelpers.spaceUpdateDocumentsEtag(context, space, Etag);
            OperationHelpers.documentsUpdateByParentId(context, spaceId, spaceId,
                    document_list);

        } else {
            // TODO return local doc list ?
            //return empty list
            bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
            bundle.putParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT,
                    (ArrayList<? extends Parcelable>) items);
            return bundle;
        }

        bundle.putParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT,
                (ArrayList<? extends Parcelable>) items);
        return bundle;
    }

}
