package com.saperion.sdb.sdk.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.saperion.sdb.sdk.config.WSConfig;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

public final class Utils {

    private static final String TAG = Utils.class.getCanonicalName();


    public static String getMethodName() {
        return (new Exception().getStackTrace()[1].getMethodName()) + "("
                + (new Exception().getStackTrace()[1].getLineNumber()) + "): ";
    }

    public static String getFileExtension(String mineType) {

        // TODO has convert createInstance mimetype
        String ext = ".pdf";
        return ext;

    }

    public static String getRenditionsDirectory(Context context) {

        return context.getFilesDir().getAbsolutePath() + "/" + WSConfig.WS_RENDITION + "/";
    }

    public static String getPreviewDirectory(Context context) {

        return context.getFilesDir().getAbsolutePath() + "/" + WSConfig.WS_PREVIEW + "/";
    }


    public static String getDocumentsDirectory(Context context) {
        return context.getFilesDir().getAbsolutePath() + "/" + WSConfig.WS_DOCUMENT + "/";
    }


//    public static String getFileExtension(String mineType) {
//        MimeTypes allTypes = MimeTypes.getDefaultMimeTypes();
//
//        String ext = "";
//
//        try {
//            MimeType mt = allTypes.forName(mineType);
//            ext =  mt.getExtension();
//        } catch (MimeTypeException e) {
//            e.printStackTrace();
//        }
//        return ext;
//
//    }

    public static void createFolder(String space_path) {

        if (space_path == null || space_path.length() == 0) {
            return;
        }

        File space_dir = new File(space_path);

        if (space_dir.exists()) {
            Log.v(TAG, Utils.getMethodName() + space_path + " exists");
        } else {
            Log.v(TAG, Utils.getMethodName() + "creating " + space_path);
            space_dir.mkdirs();
        }

		/*
         * 
		 * 
		 * // File fileWithinMyDir = new File(space_dir, "myfile");
		 * //Getting a file within the dir. // FileOutputStream out
		 * = new FileOutputStream(fileWithinMyDir); //Use the stream
		 * as usual to write into the file.
		 */
    }

    public static String getAuthToken() {
        String email = "loadtest02@qa.de";
        String password = "together";

        if (email.equals("") || password.equals("")) {

            return null;
        }

        String authStr = "";
        try {
            authStr =
                    "Basic "
                            + android.util.Base64.encodeToString(
                            (email + ":" + password).getBytes("UTF-8"), 4);
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
        }

        //authStr.replaceAll("\n", "");
        authStr = authStr.replaceAll("\\r\\n", "");

        //Log.v(TAG,authStr);

        return authStr;
    }

    public static void showFolders(Context context) {
        String msg = "";
		/*
		msg += "SystemInfo properties\n";
		msg += "-------------\n";
		java.util.Properties props = SystemInfo.getProperties();
		Enumeration<?> e = props.propertyNames();
		while (e.hasMoreElements()) {
		   String k = (String) e.nextElement();
		   String v = props.getProperty(k);
		   msg += k+": "+v+"\n";
		}

		msg += "\n";
		msg += "Envirionment variables\n";
		msg += "-------------\n";
		Map<String, String> envs = SystemInfo.getenv();
		Set<String> keys = envs.keySet();
		Iterator<String> i = keys.iterator();
		while (i.hasNext()) {
		   String k = (String) i.next();
		   String v = (String) envs.get(k);
		   msg += k+": "+v+"\n";
		}

		msg += "\n";
		msg += "Environment folders\n";
		msg += "-------------\n";
		msg += "Data folder: "
		   +Environment.getDataDirectory().getPath()+"\n";
		msg += "Download cache folder: "
		   +Environment.getDownloadCacheDirectory().getPath()+"\n";
		msg += "External Storage folder: "
		   +Environment.getExternalStorageDirectory().getPath()+"\n";
		msg += "Root folder: "
		   +Environment.getRootDirectory().getPath()+"\n";

		msg += "\n";
		msg += "Application context info\n";
		msg += "-------------\n";
		msg += "Cache folder: "
		   +  context.getCacheDir().getPath()+"\n";
		msg += "External cache folder: "
		   + context.getExternalCacheDir().getPath()+"\n";
		msg += "File folder: "
		   + context.getFilesDir().getPath()+"\n";
		msg += "OBB folder: "
		   + context.getObbDir().getPath()+"\n";
		msg += "Package name: "
		   + context.getPackageName()+"\n";
		msg += "Package code path: "
		   + context.getPackageCodePath()+"\n";
		msg += "Package resource path: "
		   + context.getPackageResourcePath()+"\n";
		*/

        File root_folder = context.getFilesDir();

        msg += "Root folder :" + root_folder.getName() + "\n";

        File[] spaces = root_folder.listFiles();

        for (int i = 0; i < spaces.length; i++) {
            msg += "space " + i + ": " + spaces[i].getName() + "\n";
            File child = spaces[i];
            File[] children = child.listFiles();

            for (int c = 0; c < children.length; c++) {
                msg += "child " + c + ": " + children[c].getName() + "\n";

                // File f = new File(children[i], children[i].getName());
            }

            // File f = new File(children[i], children[i].getName());
        }

        Log.v(TAG, getMethodName() + msg);
    }

    public static void fileCopy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes createInstance in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

    public static void fileCopy(String src, String dst) throws IOException {
        File file_name_with_id = new File(src);
        File human_readable_file_name = new File(dst);
        try {
            Utils.fileCopy(file_name_with_id,human_readable_file_name);
        } catch (IOException e) {
            com.saperion.sdb.sdk.utils.Log.e(TAG, Utils.getMethodName() + e.toString());
        }
    }
    

    
    public static boolean isNetworkAvailable(Context con)  {
    
    try{
        ConnectivityManager connectivityManager = (ConnectivityManager)
                con.getSystemService(Context.CONNECTIVITY_SERVICE);

        /*
        NetworkInfo info= connectivityManager.getActiveNetworkInfo();

        if(info != null && info.isConnected()) {
            Log.v("NetworkInfo","Connected State");
            return true;
        }

            Log.e("conMgr"," No Connection");
        return false;
        */
       
        NetworkInfo etherInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_ETHERNET);
        NetworkInfo wifiInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobileInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifiInfo.isConnected() || mobileInfo.isConnected() || etherInfo.isConnected()) {
            return true;
        }
        
    }
    catch(Exception e){
        e.printStackTrace();
    }
    return false;
    }

}
