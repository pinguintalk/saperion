package com.saperion.sdb.sdk.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.saperion.sdb.sdk.operations.CommentDeleteOperation;
import com.saperion.sdb.sdk.operations.CommentUpdateOperation;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.RequestConstants;
import com.saperion.sdb.sdk.requestmanager.RequestManager.RequestListener;
import com.saperion.sdb.sdk.requestmanager.RequestManagerSingleton;

public class Comment extends CreatedTypedIdentifiable implements Parcelable{

    private String comment;
    private String documentId;


    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }
    public Comment() {
        super(ModelType.COMMENT);
    }

    /**
     * Returns the comment of this model.
     *
     * @return the comment of this model.
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets the comment.
     *
     * @param comment The comment to be set.
     * @return an instance of this model.
     */
    public Comment setComment(String comment) {
        this.comment = comment;
        return this;
    }

    @Override
    public String toString() {
        return "Comment{" + "comment='" + comment + '\'' + "} " + super.toString();
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.getId());
        out.writeString(this.getType());
        out.writeString(this.getDocumentId());
        out.writeString(this.getComment());
        out.writeString(this.getOwnerId());
        out.writeString(this.getOwnerName());
        out.writeString(this.getCreationDate());
    }

    private Comment(Parcel in) {
        this.setId(in.readString());
        this.setType(in.readString());
        this.setDocumentId(in.readString());
        this.setComment(in.readString());
        this.setOwnerId(in.readString());
        this.setOwnerName(in.readString());
        this.setCreationDate(in.readString());



    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Comment createFromParcel(Parcel in) {
            return new Comment(in);
        }

        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }


    public void delete(RequestListener listener) {
        Request request = new Request(RequestConstants.REQUEST_TYPE_COMMENT_DELETE);

        request.put(CommentDeleteOperation.PARAM_COMMENT, this);

        if (RequestManagerSingleton.isCreated())
            RequestManagerSingleton.getInstance().execute(request, listener);
    }

    public void update(RequestListener listener) {

        Request request = new Request(RequestConstants.REQUEST_TYPE_COMMENT_UPDATE);

        request.put(CommentUpdateOperation.PARAM_COMMENT, this);

        if (RequestManagerSingleton.isCreated())
            RequestManagerSingleton.getInstance().execute(request, listener);
    }
}
