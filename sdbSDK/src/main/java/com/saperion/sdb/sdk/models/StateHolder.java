package com.saperion.sdb.sdk.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;

/**
 * The class StateHolder.
 *
 * @author sts
 */
public class StateHolder<T extends Enum<T>> implements Parcelable {

    private EnumSet<T> states;

    public StateHolder() {
    }

    public StateHolder(EnumSet<T> states) {
        this.states = states;
    }

    public Collection<T> getStates() {
        if (null == states) {
            return Collections.emptyList();
        }
        return states.clone();
    }

    public void addState(T state) {
        if (null == states) {
            states = EnumSet.<T>of(state);
        } else {
            states.add(state);
        }
    }

    public void removeState(T state) {
        if (null != states) {
            states.remove(state);
        }
    }

    public void setStates(Collection<T> newStates) {
        if (newStates.isEmpty()) {
            if (null != states) {
                states.clear();
            }
            return;
        }

        states = EnumSet.<T>copyOf(newStates);
    }

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.states);

    }

    @SuppressWarnings("unchecked")
    protected StateHolder(Parcel in) {
        states = (EnumSet<T>) in.readSerializable();
    }

    @SuppressWarnings("rawtypes")
    public static final Parcelable.Creator<StateHolder> CREATOR =
            new Parcelable.Creator<StateHolder>() {

                public StateHolder createFromParcel(Parcel in) {
                    return new StateHolder(in);
                }

                public StateHolder[] newArray(int size) {
                    return new StateHolder[size];
                }
            };
}
