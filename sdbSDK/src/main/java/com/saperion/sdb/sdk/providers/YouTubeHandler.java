package com.saperion.sdb.sdk.providers;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;

import android.net.Uri;

/**
 * Parses YouTube entity data and and inserts it into the finch video content
 * provider.
 */
public class YouTubeHandler implements ResponseHandler {
    public static final String MEDIA = "media";
    public static final String GROUP = "group";
    public static final String DESCRIPTION = "description";
    public static final String THUMBNAIL = "thumbnail";
    public static final String TITLE = "title";
    public static final String CONTENT = "content";

    public static final String WIDTH = "width";
    public static final String HEIGHT = "height";

    public static final String YT = "yt";
    public static final String DURATION = "duration";
    public static final String FORMAT = "format";

    public static final String URI = "uri";
    public static final String THUMB_URI = "thumb_uri";

    public static final String MOBILE_FORMAT = "1";

    public static final String ENTRY = "entry";
    public static final String ID = "id";

    private static final String FLUSH_TIME = "5 minutes";

    private SdbContentProvider mFinchVideoProvider;

    private String mQueryText;
    private boolean isEntry;

    public YouTubeHandler(SdbContentProvider restfulProvider, String queryText) {
        mFinchVideoProvider = restfulProvider;
        mQueryText = queryText;
    }

    /*
     * Handles the response createInstance the YouTube gdata server, which is in the form
     * of an RSS feed containing references to YouTube videos.
     */
    @Override
    public void handleResponse(HttpResponse response, Uri uri) {
        try {
            int newCount = parseYoutubeEntity(response.getEntity());

            // only flush old state now that new state has arrived
            if (newCount > 0) {
                deleteOld();
            }

        } catch (IOException e) {
            // use the exception to avoid clearing old state, if we can not
            // get new state.  This way we leave the application with some
            // data to work with in absence of network connectivity.

            // we could retry the request for data in the hope that the network
            // might return.
        }
    }

    private void deleteOld() {
        // delete any old elements, not just ones that match the current query.

    }

    private int parseYoutubeEntity(HttpEntity entity) throws IOException {
        InputStream youTubeContent = entity.getContent();
        InputStreamReader inputReader = new InputStreamReader(youTubeContent);

        int inserted = 0;

        // We might consider lazily downloading the
        // image so that it was only downloaded on
        // viewing.  Downloading more aggressively,
        // could also improve performance.

        //mFinchVideoProvider.
        //        cacheUri2File(String.valueOf(mediaID),
        //                thumbUri);

        return inserted;
    }
}
