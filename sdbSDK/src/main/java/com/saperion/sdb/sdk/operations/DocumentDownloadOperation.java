package com.saperion.sdb.sdk.operations;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;

import com.saperion.sdb.sdk.config.WSConfig;
import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.factory.DocumentJsonFactory;
import com.saperion.sdb.sdk.models.Document;
import com.saperion.sdb.sdk.network.NetworkConnection;
import com.saperion.sdb.sdk.network.NetworkConnection.ConnectionResult;
import com.saperion.sdb.sdk.providers.ContentProviderHelpers;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.security.CredentialManager;
import com.saperion.sdb.sdk.service.RequestService.Operation;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

import org.apache.http.auth.UsernamePasswordCredentials;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public final class DocumentDownloadOperation implements Operation {

    private static final String TAG = DocumentDownloadOperation.class.getSimpleName();

    public static final String PARAM_DOCUMENT = "com.saperion.sdb.sdk.param_doc_id";

    @Override
    public Bundle execute(Context context, Request request) throws ServiceRequestException, ConnectionException,
            DataException {
        Bundle bundle = new Bundle();
        Log.v(TAG, Utils.getMethodName() + "entry");

        Document cached_document = (Document) request.getParcelable(PARAM_DOCUMENT);
        String id = cached_document.getId();

        File dest_file =
                new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DOWNLOADS).toString()
                        + "/" + cached_document.getName());
        // TODO convert minetype to extension
        // use doc name or file name

        String url = WSConfig.getDocumentURL() + "/" + id + "?id=modificationId";
        NetworkConnection getDocuConnection = new NetworkConnection(context, url);

        getDocuConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager
                .getInstance().getUserName(), CredentialManager.getInstance().getmAccessToken()));

        ConnectionResult result = getDocuConnection.execute();

        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);
        if (result.responseCode != ResponseConstants.RESPONSE_CODE_OK) {
            bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
            return bundle;
        }

        Log.v(TAG, Utils.getMethodName() + "parse new doc ");

        Document new_document = DocumentJsonFactory.jsonToDocument(result.body);

        String filePath = cached_document.getFileAbsPath();

        if (filePath != null) {

            Log.v(TAG, Utils.getMethodName() + "cached_document.getFileAbsPath() "
                    + filePath);

            File cached_file = new File(filePath);

            if (cached_file.exists()) { // we have a cached file stored, so we need to check if the file is really up to date

                if (new_document.getModificationId().equals(cached_document.getModificationId())) { // cached file is up to date
                    Log.v(TAG,
                            Utils.getMethodName() + " new ModificationId :"
                                    + new_document.getModificationId() + "  old ModificationId :"
                                    + cached_document.getModificationId());

                    try {
                        Utils.fileCopy(new File(cached_document.getFileAbsPath()), dest_file);
                    } catch (IOException e) {
                        Log.e(TAG, Utils.getMethodName() + e.toString());
                    }

                    bundle.putString(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT,
                            dest_file.toString());
                    return bundle;
                }

            }
        }


        // cached file is NOT up to date or doesn't exist, so we have to download it again
        bundle = new Bundle();
        url = WSConfig.getDocumentURL() + "/" + id + "/" + WSConfig.WS_FILE;

        NetworkConnection networkConnection = new NetworkConnection(context, url);

        HashMap<String, String> requestParams = new HashMap<String, String>();

        String file_path_without_extension =
                OperationHelpers.documentGetFilePathWithoutExtension(context, id);

        requestParams.put(NetworkConnection.FILE_PATH, file_path_without_extension);
        requestParams.put(NetworkConnection.REQUEST_TYPE,
                NetworkConnection.REEQUEST_TYPE_DOWNLOAD);
        networkConnection.setRequestParams(requestParams);

        HashMap<String, String> dl_header_list = new HashMap<String, String>();
        dl_header_list.put("ACCEPT", "application/octet-stream");
        networkConnection.setHeaderList(dl_header_list);

        networkConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager
                .getInstance().getUserName(), CredentialManager.getInstance().getmAccessToken()));

        result = networkConnection.execute();

        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);

        if (result.responseCode != ResponseConstants.RESPONSE_CODE_OK) {
            Log.w(TAG, Utils.getMethodName() + " responseCode:" + result.responseCode);

            bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
            return bundle;
        }

        String file_path_abs = result.body;
        // update file path
        ContentProviderHelpers.documentUpdateFilePath(context, cached_document, file_path_abs);

        try {

            Log.w(TAG, Utils.getMethodName() + "trying copy to :" + dest_file.getAbsolutePath());

            Utils.fileCopy(new File(file_path_abs), dest_file);
        } catch (IOException e) {
            Log.e(TAG, Utils.getMethodName() + e.toString());
        }

        Log.w(TAG, Utils.getMethodName() + dest_file.getAbsolutePath() + " downloaded");

        bundle.putString(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, dest_file.getAbsolutePath());
        return bundle;
    }

}
