package com.saperion.sdb.sdk.factory;

import android.text.TextUtils;
import android.util.Log;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.models.User;
import com.saperion.sdb.sdk.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;

public final class UserJsonFactory {

    private static final String TAG = DocumentJsonFactory.class.getSimpleName();

    private static boolean LOG_ALL = false;

    private UserJsonFactory() {
        // No public constructor
    }

    public static String toJson(User user) {

        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(Include.NON_NULL);
        try {
            String f = mapper.writeValueAsString(user);
            return f;
        } catch (JsonProcessingException e1) {
            e1.printStackTrace();
        }
        return null;
    }

    public static User jsonToUser(String string) {
        User user = new User();
        ObjectMapper mapper = new ObjectMapper();
        try {
            user = mapper.readValue(string, User.class);
            return user;
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static ArrayList<User> parseResult(String wsResponse) throws DataException {

        if (wsResponse == null || TextUtils.isEmpty(wsResponse))
            return null;

        ArrayList<User> userList = new ArrayList<User>();

        ObjectMapper objectMapper = new ObjectMapper();
        JsonFactory jsonFactory = new JsonFactory();

        try {
            JsonParser jp = jsonFactory.createJsonParser(wsResponse);
            // advance stream to START_ARRAY first:
            jp.nextToken();
            // and then each time, advance to opening START_OBJECT
            while (jp.nextToken() == JsonToken.START_OBJECT) {
                User user = objectMapper.readValue(jp, User.class);

                if (LOG_ALL)
                    Log.v(TAG, Utils.getMethodName() + user.toString());

                // process
                // after binding, stream points to closing END_OBJECT
                userList.add(user);

            }

        } catch (JsonParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return userList;
    }
    
    
}
