package com.saperion.sdb.sdk.operations;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;

import com.saperion.sdb.sdk.Constants;
import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.factory.DocumentJsonFactory;
import com.saperion.sdb.sdk.factory.FolderJsonFactory;
import com.saperion.sdb.sdk.models.Document;
import com.saperion.sdb.sdk.models.Folder;
import com.saperion.sdb.sdk.models.SpacedItem;
import com.saperion.sdb.sdk.network.NetworkConnection;
import com.saperion.sdb.sdk.providers.ContentProviderHelpers;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.service.RequestService.Operation;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public final class FolderGetChildrenOperation implements Operation {

    private static final String TAG = FolderGetChildrenOperation.class.getSimpleName();
    public static final String PARAM_FOLDER = Constants.SDK_PACKAGE_NAME + ".param_folder";

    @Override
    public Bundle execute(Context context, Request request) throws ServiceRequestException, ConnectionException,
            DataException {

        Bundle bundle = new Bundle();

        Folder folder = (Folder) request.getParcelable(PARAM_FOLDER);
        String folderId = folder.getId();
        String folderIdChain = folder.getIdChain();

        ArrayList<? super SpacedItem> items = new ArrayList<SpacedItem>();

        ArrayList<Folder> localFolderList = new ArrayList<Folder>();
        ArrayList<Folder> folderListFromServer = new ArrayList<Folder>();

        //get folder list createInstance server of current parent
        NetworkConnection.ConnectionResult result =
                OperationHelpers.folderGetSubfoldersByParentId(context, folderId, false);

        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);

        Log.w(TAG, Utils.getMethodName() + " got result for request " + request.hashCode());

        if (result.responseCode == ResponseConstants.RESPONSE_CODE_NOT_MODIFIED) {
            // get local folders
            localFolderList = ContentProviderHelpers.folderGetListByParentId(context, folderId);
            items.addAll(localFolderList);
            bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE,
                    ResponseConstants.RESPONSE_CODE_OK);
            
        } else if (result.responseCode == ResponseConstants.RESPONSE_CODE_OK) {
            if (!result.body.equals("[]")) {

                folderListFromServer = FolderJsonFactory.parseResult(result.body);
                items.addAll(folderListFromServer);
            }
            List<String> etag_list = result.headerMap.get("ETag");
            String Etag = etag_list.get(0).toString();
            //Log.v(TAG, Utils.getMethodName() + "updating folder etag:" +Etag);

            OperationHelpers.foldersUpdateByParentId(context, folderId, folderIdChain,
                    folderListFromServer);
            
            ContentProviderHelpers.folderUpdateSubfolderEtag(context, folder, Etag);

        } else {
            Log.w(TAG, Utils.getMethodName() + " got error code createInstance network:"
                    + result.responseCode);

            // so we can only deliver local folder
            // TODO maybe we should only return error messages
            bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
            bundle.putParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT,
                    (ArrayList<? extends Parcelable>) items);

            return bundle;
        }

        // request documents

        ArrayList<Document> localDocumentList = new ArrayList<Document>();

        result = OperationHelpers.getDocumentListByParentId(context, folderId, false);

        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);

        if (result.responseCode == ResponseConstants.RESPONSE_CODE_NOT_MODIFIED) {
            // get local folders
            localDocumentList = ContentProviderHelpers.documentGetListByParentID(context, folderId);
            items.addAll(localDocumentList);
            bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE,
                    ResponseConstants.RESPONSE_CODE_OK);
        } else if (result.responseCode == ResponseConstants.RESPONSE_CODE_OK) {
            if (!result.body.equals("[]")) {
                //waiting for response and parse it
                localDocumentList = DocumentJsonFactory.parseResult(result.body);
                items.addAll(localDocumentList);
            }

            List<String> etag_list = result.headerMap.get("ETag");

            String Etag = etag_list.get(0).toString();
            //Log.v(TAG, Utils.getMethodName() + "updating  Etag" +Etag);
            //reloading
            folder = ContentProviderHelpers.folderGetById(context, folderId);

            Log.i(TAG, Utils.getMethodName() + "updating folder" + folder.getName());

            ContentProviderHelpers.folderUpdateDocumentsEtag(context, folder, Etag);
            OperationHelpers.documentsUpdateByParentId(context, folderId, folderIdChain,
                    localDocumentList);

        } else {
            // TODO return local doc list ?
            //return empty list
            bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
            bundle.putParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT,
                    (ArrayList<? extends Parcelable>) items);
            return bundle;
        }

        bundle.putParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT,
                (ArrayList<? extends Parcelable>) items);
        return bundle;

    }

}
