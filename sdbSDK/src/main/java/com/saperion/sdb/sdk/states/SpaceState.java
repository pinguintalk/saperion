package com.saperion.sdb.sdk.states;

public enum SpaceState {
    SYNCHRONIZE_WEB,
    SYNCHRONIZE_APP,
    SYNCHRONIZE_DESKTOP,
    SHARED,
    RECYCLED,
    FAVORED,
    CRYPTED
}
