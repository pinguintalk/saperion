package com.saperion.sdb.sdk.service;

import android.content.Intent;

import com.saperion.sdb.sdk.operations.AccountsGetOperation;
import com.saperion.sdb.sdk.operations.AccountsGetSettingsOperation;
import com.saperion.sdb.sdk.operations.CommentAddOperation;
import com.saperion.sdb.sdk.operations.CommentDeleteOperation;
import com.saperion.sdb.sdk.operations.CommentUpdateOperation;
import com.saperion.sdb.sdk.operations.DocumentAddCommentOperation;
import com.saperion.sdb.sdk.operations.DocumentCreateNoContentOperation;
import com.saperion.sdb.sdk.operations.DocumentCreateOperation;
import com.saperion.sdb.sdk.operations.DocumentDeleteOperation;
import com.saperion.sdb.sdk.operations.DocumentDownloadFileByVersionOperation;
import com.saperion.sdb.sdk.operations.DocumentDownloadOperation;
import com.saperion.sdb.sdk.operations.DocumentDownloadRenditionByVersionOperation;
import com.saperion.sdb.sdk.operations.DocumentGetActivitiesOperation;
import com.saperion.sdb.sdk.operations.DocumentGetCommentsOperation;
import com.saperion.sdb.sdk.operations.DocumentGetFileByVersionOperation;
import com.saperion.sdb.sdk.operations.DocumentGetFileOperation;
import com.saperion.sdb.sdk.operations.DocumentGetLinksOperation;
import com.saperion.sdb.sdk.operations.DocumentGetPreviewOperation;
import com.saperion.sdb.sdk.operations.DocumentGetRenditionByVersionOperation;
import com.saperion.sdb.sdk.operations.DocumentGetVersionsOperation;
import com.saperion.sdb.sdk.operations.DocumentRestoreVersionOperation;
import com.saperion.sdb.sdk.operations.DocumentUpdateOperation;
import com.saperion.sdb.sdk.operations.FolderCreateOperation;
import com.saperion.sdb.sdk.operations.FolderDeleteOperation;
import com.saperion.sdb.sdk.operations.FolderGetChildrenOperation;
import com.saperion.sdb.sdk.operations.FolderGetDocumentsOperation;
import com.saperion.sdb.sdk.operations.FolderGetPreviewsOperation;
import com.saperion.sdb.sdk.operations.FolderGetSubfoldersOperation;
import com.saperion.sdb.sdk.operations.FolderUpdateOperation;
import com.saperion.sdb.sdk.operations.GetAccessTokenOperation;
import com.saperion.sdb.sdk.operations.GetLanguageOperation;
import com.saperion.sdb.sdk.operations.GetSystemInfosOperation;
import com.saperion.sdb.sdk.operations.LinkDeleteOperation;
import com.saperion.sdb.sdk.operations.LinkSaveOperation;
import com.saperion.sdb.sdk.operations.ShareDeleteOperation;
import com.saperion.sdb.sdk.operations.ShareGetListOperation;
import com.saperion.sdb.sdk.operations.ShareSaveOperation;
import com.saperion.sdb.sdk.operations.ShareUpdateOperation;
import com.saperion.sdb.sdk.operations.SpaceCreateOperation;
import com.saperion.sdb.sdk.operations.SpaceDeleteAllOperation;
import com.saperion.sdb.sdk.operations.SpaceDeleteOperation;
import com.saperion.sdb.sdk.operations.SpaceGetChildrenOperation;
import com.saperion.sdb.sdk.operations.SpaceGetDocumentsOperation;
import com.saperion.sdb.sdk.operations.SpaceGetListOperation;
import com.saperion.sdb.sdk.operations.SpaceGetSubfoldersOperation;
import com.saperion.sdb.sdk.operations.SpaceUpdateOperation;
import com.saperion.sdb.sdk.operations.SynchronizationOperation;
import com.saperion.sdb.sdk.operations.UserDeleteAvatarOperation;
import com.saperion.sdb.sdk.operations.UserDeleteOperation;
import com.saperion.sdb.sdk.operations.UserGetAvatarOperation;
import com.saperion.sdb.sdk.operations.UserGetByIdOperation;
import com.saperion.sdb.sdk.operations.UserGetCurrentOperation;
import com.saperion.sdb.sdk.operations.UserGetListOperation;
import com.saperion.sdb.sdk.operations.UserRecycleOperation;
import com.saperion.sdb.sdk.operations.UserSaveOperation;
import com.saperion.sdb.sdk.operations.UserSetAvatarOperation;
import com.saperion.sdb.sdk.operations.UserUpdatePasswordOperation;
import com.saperion.sdb.sdk.requestmanager.RequestConstants;
import com.saperion.sdb.sdk.requestmanager.RequestManagerSingleton;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

/**
 * This class is called by the {@link RequestManagerSingleton} through the {@link Intent} system.
 *
 * @author nnn, adr
 */
public final class SdbService extends RequestService {

    private static final String TAG = SdbService.class.getSimpleName();

    @Override
    protected int getMaximumNumberOfThreads() {
        return 3;
    }

    @Override
    public Operation getOperationForType(int requestType) {
        switch (requestType) {
            
            // SAPCES
            case RequestConstants.REQUEST_TYPE_SYNC_SPACE_LIST:
                return new SynchronizationOperation();
            case RequestConstants.REQUEST_TYPE_SPACE_GET_LIST:
                return new SpaceGetListOperation();
            case RequestConstants.REQUEST_TYPE_SPACE_GET_CHILDREN:
                return new SpaceGetChildrenOperation();
            case RequestConstants.REQUEST_TYPE_SPACE_GET_SUB_FOLDERS:
                return new SpaceGetSubfoldersOperation();
            case RequestConstants.REQUEST_TYPE_SPACE_GET_DOCUMENTS:
                return new SpaceGetDocumentsOperation();
            case RequestConstants.REQUEST_TYPE_SPACE_CREATE:
                return new SpaceCreateOperation();
            case RequestConstants.REQUEST_TYPE_SPACE_DELETE:
                return new SpaceDeleteOperation();
            case RequestConstants.REQUEST_TYPE_SPACE_UPDATE:
                return new SpaceUpdateOperation();
            case RequestConstants.REQUEST_TYPE_SPACE_DELETE_ALL:
                return new SpaceDeleteAllOperation();


            // FOLDERS
            case RequestConstants.REQUEST_TYPE_FOLDER_UPDATE:
                return new FolderUpdateOperation();
            case RequestConstants.REQUEST_TYPE_FOLDER_CREATE:
                return new FolderCreateOperation();
            case RequestConstants.REQUEST_TYPE_FOLDER_DELETE:
                return new FolderDeleteOperation();
            case RequestConstants.REQUEST_TYPE_FOLDER_GET_SUB_FOLDERS:
                return new FolderGetSubfoldersOperation();
            case RequestConstants.REQUEST_TYPE_FOLDER_GET_DOCUMENT_LIST:
                return new FolderGetDocumentsOperation();
            case RequestConstants.REQUEST_TYPE_FOLDER_GET_CHILDREN:
                return new FolderGetChildrenOperation();
            case RequestConstants.REQUEST_TYPE_FOLDER_GET_PREVIEWS:
                return new FolderGetPreviewsOperation();

            // DOCUMENTS
            case RequestConstants.REQUEST_TYPE_DOCUMENT_CREATE:
                return new DocumentCreateOperation();
            case RequestConstants.REQUEST_TYPE_DOCUMENT_CREATE_WITHOUT_CONTENT:
                return new DocumentCreateNoContentOperation();
            case RequestConstants.REQUEST_TYPE_DOCUMENT_UPDATE:
                return new DocumentUpdateOperation();
            case RequestConstants.REQUEST_TYPE_DOCUMENT_DELETE:
                return new DocumentDeleteOperation();
            case RequestConstants.REQUEST_TYPE_DOCUMENT_GET_PREVIEW:
                return new DocumentGetPreviewOperation();
            case RequestConstants.REQUEST_TYPE_DOCUMENT_DOWNLOAD:
                return new DocumentDownloadOperation();
            case RequestConstants.REQUEST_TYPE_DOCUMENT_GET_FILE:
                return new DocumentGetFileOperation();
            case RequestConstants.REQUEST_TYPE_DOCUMENT_GET_FILE_BY_VERSION:
                return new DocumentGetFileByVersionOperation();
            case RequestConstants.REQUEST_TYPE_DOCUMENT_DOWNLOAD_FILE_BY_VERSION:
                return new DocumentDownloadFileByVersionOperation();
            case RequestConstants.REQUEST_TYPE_DOCUMENT_GET_RENDITION_BY_VERSION:
                return new DocumentGetRenditionByVersionOperation();
            case RequestConstants.REQUEST_TYPE_DOCUMENT_DOWNLOAD_RENDITION_BY_VERSION:
                return new DocumentDownloadRenditionByVersionOperation();
            case RequestConstants.REQUEST_TYPE_DOCUMENT_GET_COMMENTS:
                return new DocumentGetCommentsOperation();
            case RequestConstants.REQUEST_TYPE_DOCUMENT_ADD_COMMENT:
                return new DocumentAddCommentOperation();
            case RequestConstants.REQUEST_TYPE_DOCUMENT_GET_ACTIVITIES:
                return new DocumentGetActivitiesOperation();
            case RequestConstants.REQUEST_TYPE_DOCUMENT_GET_VERSIONS:
                return new DocumentGetVersionsOperation();
            case RequestConstants.REQUEST_TYPE_DOCUMENT_RESTORE_VERSION:
                return new DocumentRestoreVersionOperation();
            // COMMENTS

            case RequestConstants.REQUEST_TYPE_COMMENT_ADD:
                return new CommentAddOperation();
            case RequestConstants.REQUEST_TYPE_COMMENT_DELETE:
                return new CommentDeleteOperation();
            case RequestConstants.REQUEST_TYPE_COMMENT_UPDATE:
                return new CommentUpdateOperation();

            // USERS
            case RequestConstants.REQUEST_TYPE_USER_CREATE:
                return new UserSaveOperation();
            case RequestConstants.REQUEST_TYPE_USER_DELETE:
                return new UserDeleteOperation();
            case RequestConstants.REQUEST_TYPE_USER_GET_CURRENT:
                return new UserGetCurrentOperation();
            case RequestConstants.REQUEST_TYPE_USER_GET_LIST:
                return new UserGetListOperation();
            case RequestConstants.REQUEST_TYPE_USER_UPDATE:
                return new UserSaveOperation();
            case RequestConstants.REQUEST_TYPE_USER_GET_BY_ID:
                return new UserGetByIdOperation();
            case RequestConstants.REQUEST_TYPE_USER_RECYCLE:
                return new UserRecycleOperation();
            case RequestConstants.REQUEST_TYPE_USER_UPDATE_PASSWORD:
                return new UserUpdatePasswordOperation();
            case RequestConstants.REQUEST_TYPE_USER_GET_AVATAR:
                return new UserGetAvatarOperation();
            case RequestConstants.REQUEST_TYPE_USER_SET_AVATAR:
                return new UserSetAvatarOperation();
            case RequestConstants.REQUEST_TYPE_USER_DELETE_AVATAR:
                return new UserDeleteAvatarOperation();

            
            
            // SYSTEM
            case RequestConstants.REQUEST_TYPE_GET_ACCESS_TOKEN:
                return new GetAccessTokenOperation();
            case RequestConstants.REQUEST_TYPE_GET_SYSTEM_INFOS:
                return new GetSystemInfosOperation();
            case RequestConstants.REQUEST_TYPE_GET_LANGUAGE:
                return new GetLanguageOperation();

            //Acoounts
            case RequestConstants.REQUEST_TYPE_ACCOUNT_GET:
                return new AccountsGetOperation();
            case RequestConstants.REQUEST_TYPE_ACCOUNT_GET_SETTINGS:
                return new AccountsGetSettingsOperation();
            
            // Link
            case RequestConstants.REQUEST_TYPE_DOCUMENT_CREATE_LINK:
                return new LinkSaveOperation();
            case RequestConstants.REQUEST_TYPE_DOCUMENT_GET_LINKS:
                return new DocumentGetLinksOperation();
            case RequestConstants.REQUEST_TYPE_DOCUMENT_DELETE_LINK:
                return new LinkDeleteOperation();

            // Share
            case RequestConstants.REQUEST_TYPE_SHARE_CREATE:
                return new ShareSaveOperation();
            case RequestConstants.REQUEST_TYPE_SHARE_GET_LIST:
                return new ShareGetListOperation();
            case RequestConstants.REQUEST_TYPE_SHARE_DELETE:
                return new ShareDeleteOperation();
            case RequestConstants.REQUEST_TYPE_SHARE_UPDATE:
                return new ShareUpdateOperation();

            
        }

        Log.e(TAG, Utils.getMethodName() + "OPERATION NOT FOUND !");

        return null;
    }

}
