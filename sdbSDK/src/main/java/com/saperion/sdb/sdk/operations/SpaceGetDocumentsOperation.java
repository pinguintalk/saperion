package com.saperion.sdb.sdk.operations;

import android.content.Context;
import android.os.Bundle;

import com.saperion.sdb.sdk.Constants;
import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.service.RequestService.Operation;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

public final class SpaceGetDocumentsOperation implements Operation {

    private static final String TAG = SpaceGetDocumentsOperation.class.getSimpleName();

    public static final String PARAM_SPACE_ID = Constants.SDK_PACKAGE_NAME +  ".paramSpaceId";

    @Override
    public Bundle execute(Context context, Request request) throws ServiceRequestException, ConnectionException,
            DataException {

        Log.v(TAG, Utils.getMethodName() + "entry");

        String spaceID = request.getString(PARAM_SPACE_ID);


        return OperationHelpers.documentGetListByParentId(context, spaceID, spaceID,
                true);


    }

}
