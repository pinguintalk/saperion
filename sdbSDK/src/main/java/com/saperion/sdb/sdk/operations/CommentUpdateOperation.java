package com.saperion.sdb.sdk.operations;

import android.content.Context;
import android.os.Bundle;

import com.saperion.sdb.sdk.Constants;
import com.saperion.sdb.sdk.config.WSConfig;
import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.models.Comment;
import com.saperion.sdb.sdk.network.NetworkConnection;
import com.saperion.sdb.sdk.network.NetworkConnection.ConnectionResult;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.security.CredentialManager;
import com.saperion.sdb.sdk.service.RequestService.Operation;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

import org.apache.http.auth.UsernamePasswordCredentials;
import org.json.JSONException;
import org.json.JSONObject;

public final class CommentUpdateOperation implements Operation {

    private static final String TAG = CommentUpdateOperation.class.getSimpleName();

    public static final String PARAM_COMMENT = Constants.SDK_PACKAGE_NAME + ".param_comment";

    @Override
    public Bundle execute(Context context, Request request) throws ServiceRequestException, ConnectionException,
            DataException {
        Bundle bundle = new Bundle();
        Log.v(TAG, Utils.getMethodName() + "entry");
     
        Comment comment = (Comment) request.getParcelable(PARAM_COMMENT);
        String docID = comment.getDocumentId();
        String url = WSConfig.getDocumentURL() + "/" + docID + "/" + WSConfig.WS_COMMENTS;
        NetworkConnection networkConnection = new NetworkConnection(context, url);
        networkConnection.setMethod(NetworkConnection.Method.PUT);
        networkConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager
                .getInstance().getUserName(), CredentialManager.getInstance().getmAccessToken()));

       
        JSONObject jo = new JSONObject();

        try {
            jo.put("type", "comment");
            jo.put("comment", comment.getComment());
            jo.put("id", comment.getId());
        } catch (JSONException e) {
            throw  new DataException("error while creating comment");
        }
        String payload = jo.toString();
        networkConnection.setPayload(payload);
        
        ConnectionResult result = networkConnection.execute();

        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);

        if (result.responseCode != ResponseConstants.RESPONSE_CODE_CREATED) {
            bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
            return bundle;
        }
        return bundle;
    }


}
