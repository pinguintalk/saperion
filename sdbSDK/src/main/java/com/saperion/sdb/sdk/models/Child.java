package com.saperion.sdb.sdk.models;

import com.saperion.sdb.sdk.Constants;

/**
 * Created by nnn on 04.07.13.
 */
public class Child<T extends SpacedItem> {

    private T item;

    // Here's the constructor.  Note the use of the type variable T.
    public Child(T value) { this.item = value; }
    
    public void set(T item) {
        this.item = item;
    }

    public T get() {
        return  item;
    }
     
    public boolean isRecycled(){
        if (item.getType().equals(Constants.DOCUMENT_TYPE)){
            return ((Document) item).isRecycled();
        }
        else if (item.getType().equals(Constants.FOLDER_TYPE)){
            return ((Folder) item).isRecycled();
        }
        return false;
    }
}
