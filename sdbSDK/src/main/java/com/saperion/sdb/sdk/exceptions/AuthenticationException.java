package com.saperion.sdb.sdk.exceptions;

public class AuthenticationException extends Exception {
    private static final long serialVersionUID = 5732982047622811557L;

    public AuthenticationException(String message) {
        super(message);
    }

    /**
     * Constructs a new {@link ServiceRequestException} that includes the current stack trace, the
     * specified detail message and the specified cause.
     *
     * @param detailMessage The detail message for this exception.
     * @param throwable     The cause of this exception.
     */
    public AuthenticationException(final String detailMessage, final Throwable throwable) {
        super(detailMessage, throwable);
    }

    /**
     * Constructs a new {@link ServiceRequestException} that includes the current stack trace and the
     * specified detail message and the error status code
     *
     * @param detailMessage The detail message for this exception.
     * @param statusCode    The HTTP status code
     */
    public AuthenticationException(final String detailMessage, final int statusCode) {
        super(detailMessage);
    }

    /**
     * Constructs a new {@link ServiceRequestException} that includes the current stack trace and the
     * specified cause.
     *
     * @param throwable The cause of this exception.
     */
    public AuthenticationException(final Throwable throwable) {
        super(throwable);
    }

}
