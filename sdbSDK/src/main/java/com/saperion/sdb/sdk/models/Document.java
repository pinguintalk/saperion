package com.saperion.sdb.sdk.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.saperion.sdb.sdk.config.JSONTag;
import com.saperion.sdb.sdk.factory.DocumentJsonFactory;
import com.saperion.sdb.sdk.operations.DocumentAddCommentOperation;
import com.saperion.sdb.sdk.operations.DocumentCreateNoContentOperation;
import com.saperion.sdb.sdk.operations.DocumentCreateOperation;
import com.saperion.sdb.sdk.operations.DocumentDeleteOperation;
import com.saperion.sdb.sdk.operations.DocumentDownloadFileByVersionOperation;
import com.saperion.sdb.sdk.operations.DocumentDownloadOperation;
import com.saperion.sdb.sdk.operations.DocumentDownloadRenditionByVersionOperation;
import com.saperion.sdb.sdk.operations.DocumentGetActivitiesOperation;
import com.saperion.sdb.sdk.operations.DocumentGetCommentsOperation;
import com.saperion.sdb.sdk.operations.DocumentGetFileByVersionOperation;
import com.saperion.sdb.sdk.operations.DocumentGetFileOperation;
import com.saperion.sdb.sdk.operations.DocumentGetLinksOperation;
import com.saperion.sdb.sdk.operations.DocumentGetPreviewOperation;
import com.saperion.sdb.sdk.operations.DocumentGetRenditionByVersionOperation;
import com.saperion.sdb.sdk.operations.DocumentGetVersionsOperation;
import com.saperion.sdb.sdk.operations.DocumentRestoreVersionOperation;
import com.saperion.sdb.sdk.operations.DocumentUpdateOperation;
import com.saperion.sdb.sdk.providers.ContentProviderHelpers;
import com.saperion.sdb.sdk.providers.DocumentContent.DocumentDB;
import com.saperion.sdb.sdk.providers.DocumentContent.DocumentDB.Columns;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.RequestConstants;
import com.saperion.sdb.sdk.requestmanager.RequestManager.RequestListener;
import com.saperion.sdb.sdk.requestmanager.RequestManagerSingleton;
import com.saperion.sdb.sdk.states.DocumentState;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@JsonIgnoreProperties({JSONTag.DOCUMENT_TAG_FILE_URI, JSONTag.DOCUMENT_TAG_PREVIEW_URI,
        "fileAbsPath", "recycled", "renditionAbsPath", "previewAbsPath", "idChain"})
public class Document extends SpacedItem implements Parcelable {

    private int versionNumber;
    private long pages;
    private long size;
    private String hash;
    private String mimeType;
    private String creationDate;
    private String fileAbsPath;
    private String renditionAbsPath;
    private String previewAbsPath; // absolute path


    public boolean isRecycled() {
        return getStates().toString().contains(DocumentState.RECYCLED.toString().toUpperCase());
    }

    
    public String getRenditionAbsPath() {
        return renditionAbsPath;
    }

    public void setRenditionAbsPath(String renditionAbsPath) {
        this.renditionAbsPath = renditionAbsPath;
    }

    public String getFileAbsPath() {
        return fileAbsPath;
        // ContentProviderHelpers.documentGetFileAbsPath(context, this.getId());
    }

    public void setFileAbsPath(String file_path) {
        this.fileAbsPath = file_path;
    }

    public String getPreviewAbsPath() {
        return previewAbsPath;
    }

    public void setPreviewAbsPath(String previewAbsPath) {
        this.previewAbsPath = previewAbsPath;
    }

    private StateHolder<DocumentState> stateHolder;

    public Document() {
        super(ModelType.DOCUMENT);
        this.stateHolder = new StateHolder<DocumentState>();
    }

    public long getPages() {
        return pages;
    }

    public Document setPages(long pages) {
        this.pages = pages;
        return this;
    }

    public String getHash() {
        return hash;
    }

    public Document setHash(String hash) {
        this.hash = hash;
        return this;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public int getVersionNumber() {
        return versionNumber;
    }

    public Document setVersionNumber(int versionNumber) {
        this.versionNumber = versionNumber;
        return this;
    }

    /**
     * Returns the size of this documents attachment (binary content) in bytes.
     *
     * @return the size of this documents attachment in bytes.
     */
    public long getSize() {
        return size;
    }

    /**
     * Sets the size of this documents attachment in bytes.
     *
     * @param size in bytes.
     */
    public Document setSize(long size) {
        this.size = size;
        return this;
    }

    public String getMimeType() {
        return mimeType;
    }

    public Document setMimeType(String mimeType) {
        this.mimeType = mimeType;
        return this;
    }

    public Document addState(DocumentState state) {
        this.stateHolder.addState(state);
        return this;
    }

    public Document removeState(DocumentState state) {
        this.stateHolder.removeState(state);
        return this;
    }

    public Collection<DocumentState> getStates() {
        return this.stateHolder.getStates();
    }

    public Item setStates(Collection<DocumentState> states) {
        this.stateHolder.setStates(states);
        return this;
    }

    public void delete(RequestListener listener) {
        Request request = new Request(RequestConstants.REQUEST_TYPE_DOCUMENT_DELETE);
        request.put(DocumentDeleteOperation.PARAM_ITEM_ID, this.getId());

        if (RequestManagerSingleton.isCreated() && listener != null)
            RequestManagerSingleton.getInstance().execute(request, listener);

    }

    public void create(String file_path, RequestListener listener) {
        if (file_path == null || TextUtils.isEmpty(file_path))
            return;

        // create new
        String file_name = file_path.substring(file_path.lastIndexOf("/") + 1);
        this.setName(file_name);
        Request request = new Request(RequestConstants.REQUEST_TYPE_DOCUMENT_CREATE);

        request.put(DocumentCreateOperation.PARAM_DOCUMENT_PATH, file_path);
        request.put(DocumentCreateOperation.PARAM_DOCUMENT, this);
        request.setPriority(Request.PRIORITY_LEVEL_HIGH);
        
        if (RequestManagerSingleton.isCreated())
            RequestManagerSingleton.getInstance().execute(request, listener);

    }

    public void createWithoutContent(RequestListener listener) {

        // create new withou file update

        Request request = new Request(RequestConstants.REQUEST_TYPE_DOCUMENT_CREATE_WITHOUT_CONTENT);

        request.put(DocumentCreateNoContentOperation.PARAM_DOCUMENT, this);

        if (RequestManagerSingleton.isCreated())
            RequestManagerSingleton.getInstance().execute(request, listener);

    }
    
    public void save(RequestListener listener) {
        if (!TextUtils.isEmpty(this.id)) {
            Request request = new Request(RequestConstants.REQUEST_TYPE_DOCUMENT_UPDATE);

            request.put(DocumentUpdateOperation.PARAM_PAYLOAD, DocumentJsonFactory.toJson(this));
            request.put(DocumentUpdateOperation.PARAM_ITEM_ID, this.getId());
            if (RequestManagerSingleton.isCreated())
                RequestManagerSingleton.getInstance().execute(request, listener);

        }
    }

    public void restore(RequestListener listener) {
        removeState(DocumentState.RECYCLED);
        save(listener);
    }
    

    public void restoreVersion(RequestListener listener, int versionNumber) {
        Request request = new Request(RequestConstants.REQUEST_TYPE_DOCUMENT_RESTORE_VERSION);
        request.put(DocumentRestoreVersionOperation.PARAM_DOC_ID, id);

        JSONObject jo = new JSONObject();
        try {
            jo.put("type", "version");
            jo.put("number", versionNumber);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.put(DocumentRestoreVersionOperation.PARAM_PAYLOAD, jo.toString());

        if (RequestManagerSingleton.isCreated())
            RequestManagerSingleton.getInstance().execute(request, listener);

    }

    
    public void addComments(String comment, RequestListener listener) {
        Request request = new Request(RequestConstants.REQUEST_TYPE_DOCUMENT_ADD_COMMENT);
        request.put(DocumentAddCommentOperation.PARAM_DOC_ID, id);

        request.put(DocumentAddCommentOperation.PARAM_COMMENT, comment);

        if (RequestManagerSingleton.isCreated())
            RequestManagerSingleton.getInstance().execute(request, listener);
    }
    
    public void getComments(RequestListener listener) {
        Request request = new Request(RequestConstants.REQUEST_TYPE_DOCUMENT_GET_COMMENTS);
        request.put(DocumentGetCommentsOperation.PARAM_DOC_ID, id);

        if (RequestManagerSingleton.isCreated())
            RequestManagerSingleton.getInstance().execute(request, listener);

    }
    
    public void getActivities(RequestListener listener) {
        Request request = new Request(RequestConstants.REQUEST_TYPE_DOCUMENT_GET_ACTIVITIES);
        request.put(DocumentGetActivitiesOperation.PARAM_DOC_ID, id);

        if (RequestManagerSingleton.isCreated())
            RequestManagerSingleton.getInstance().execute(request, listener);

    }
    
    public void getVersions(RequestListener listener) {
        Request request = new Request(RequestConstants.REQUEST_TYPE_DOCUMENT_GET_VERSIONS);
        request.put(DocumentGetVersionsOperation.PARAM_DOC_ID, id);

        if (RequestManagerSingleton.isCreated())
            RequestManagerSingleton.getInstance().execute(request, listener);

    }

    public ArrayList<Link> getLinks(Context context, RequestListener listener) {
        Request request = new Request(RequestConstants.REQUEST_TYPE_DOCUMENT_GET_LINKS);
        request.put(DocumentGetLinksOperation.PARAM_DOC_ID, this.getId());

        if (RequestManagerSingleton.isCreated())
            RequestManagerSingleton.getInstance().execute(request, listener);

        return ContentProviderHelpers.linkGetListCached(context);
    }
    
    public void getPreview(RequestListener listener) {
        Request request = new Request(RequestConstants.REQUEST_TYPE_DOCUMENT_GET_PREVIEW);
        request.put(DocumentGetPreviewOperation.PARAM_DOCUMENT, this);
        request.put(Request.PRIORITY_LEVEL, Request.PRIORITY_LEVEL_LOW);

        if (RequestManagerSingleton.isCreated())
            RequestManagerSingleton.getInstance().execute(request, listener);

    }
    

    public void getFile(RequestListener listener) {
        Request request = new Request(RequestConstants.REQUEST_TYPE_DOCUMENT_GET_FILE);
        request.put(DocumentGetFileOperation.PARAM_DOCUMENT, this);
        request.setPriority(Request.PRIORITY_LEVEL_HIGH);
        if (RequestManagerSingleton.isCreated())
            RequestManagerSingleton.getInstance().execute(request, listener);
    }

    public void getFileByVersion(RequestListener listener,int versionNumber) {
        Request request = new Request(RequestConstants.REQUEST_TYPE_DOCUMENT_GET_FILE_BY_VERSION);
        request.put(DocumentGetFileByVersionOperation.PARAM_DOCUMENT, this);
        request.put(DocumentGetFileByVersionOperation.PARAM_DOCUMENT_VERSION, versionNumber);
        
        if (RequestManagerSingleton.isCreated())
            RequestManagerSingleton.getInstance().execute(request, listener);
    }

    public void downloadFileByVersion(RequestListener listener,int versionNumber) {
        Request request = new Request(RequestConstants.REQUEST_TYPE_DOCUMENT_DOWNLOAD_FILE_BY_VERSION);
        request.put(DocumentDownloadFileByVersionOperation.PARAM_DOCUMENT, this);
        request.put(DocumentDownloadFileByVersionOperation.PARAM_DOCUMENT_VERSION, versionNumber);

        if (RequestManagerSingleton.isCreated())
            RequestManagerSingleton.getInstance().execute(request, listener);
    }


    public void downloadPdfbyVersion(RequestListener listener, int number) {

        Request request = new Request(RequestConstants.REQUEST_TYPE_DOCUMENT_DOWNLOAD_RENDITION_BY_VERSION);
        request.put(DocumentDownloadRenditionByVersionOperation.PARAM_DOCUMENT, this);
        request.put(DocumentDownloadRenditionByVersionOperation.PARAM_DOCUMENT_VERSION, number);

        if (RequestManagerSingleton.isCreated())
            RequestManagerSingleton.getInstance().execute(request, listener);
    }
    
    public void getRenditionVersioned(RequestListener listener, int number) {

        Request request = new Request(RequestConstants.REQUEST_TYPE_DOCUMENT_GET_RENDITION_BY_VERSION);
        request.put(DocumentGetRenditionByVersionOperation.PARAM_DOCUMENT, this);
        request.put(DocumentGetRenditionByVersionOperation.PARAM_DOCUMENT_VERSION, number);

        if (RequestManagerSingleton.isCreated())
            RequestManagerSingleton.getInstance().execute(request, listener);
    }

    public void download(RequestListener listener) {
        Request request = new Request(RequestConstants.REQUEST_TYPE_DOCUMENT_DOWNLOAD);
        request.put(DocumentDownloadOperation.PARAM_DOCUMENT, this);
        request.setPriority(Request.PRIORITY_LEVEL_HIGH);
        
        if (RequestManagerSingleton.isCreated())
            RequestManagerSingleton.getInstance().execute(request, listener);
    }


    public ContentValues toContentValues() {
        // TODO 
        ContentValues cv = new ContentValues();
        cv.put(DocumentDB.Columns.ID.getName(), this.getId());
        cv.put(DocumentDB.Columns.NAME.getName(), this.getName());
        cv.put(DocumentDB.Columns.CREATION_DATE.getName(), this.getCreationDate());
        cv.put(DocumentDB.Columns.DESCRIPTION.getName(), this.getDescription());
        cv.put(DocumentDB.Columns.LAST_MODIFIED.getName(), this.getLastModified());
        cv.put(DocumentDB.Columns.MODIFICATION_ID.getName(), this.getModificationId());
        cv.put(DocumentDB.Columns.OWNER_ID.getName(), this.getOwnerId());
        cv.put(DocumentDB.Columns.OWNER_NAME.getName(), this.getOwnerName());
        String tags = this.getTags().toString();
        tags = tags.substring(1, tags.length() - 1);
        cv.put(DocumentDB.Columns.TAGS.getName(), tags);
        cv.put(DocumentDB.Columns.PARENT_ID.getName(), this.getParentId());
        cv.put(DocumentDB.Columns.SPACE_ID.getName(), this.getSpaceId());
        cv.put(DocumentDB.Columns.ID_CHAIN.getName(), this.getIdChain());
        cv.put(DocumentDB.Columns.HASH.getName(), this.getHash());
        cv.put(DocumentDB.Columns.TYPE.getName(), this.getType());
        cv.put(DocumentDB.Columns.PAGES.getName(), this.getPages());
        cv.put(DocumentDB.Columns.SIZE.getName(), this.getSize());
        cv.put(DocumentDB.Columns.MIME_TYPE.getName(), this.getMimeType());
        cv.put(DocumentDB.Columns.VERSION_NUMBER.getName(), this.getVersionNumber());
        cv.put(DocumentDB.Columns.PREVIEW_PATH.getName(), this.getPreviewAbsPath());
        cv.put(DocumentDB.Columns.RENDITION_PATH.getName(), this.getRenditionAbsPath());
        cv.put(DocumentDB.Columns.FILE_PATH.getName(), this.getFileAbsPath());
        cv.put(DocumentDB.Columns.DISPLAY_NAME.getName(), this.getName());

        String states = "";
        for (DocumentState documentStates : this.getStates()) {
            if (!TextUtils.isEmpty(states)) {
                states = states + "," + documentStates.toString();
            } else
                states = documentStates.toString();
        }

        cv.put(DocumentDB.Columns.STATES.getName(), states);

        return cv;
    }

    public static Document fromCursor(Cursor cursor) {
        Document document = new Document();
        document.setName(cursor.getString(DocumentDB.Columns.NAME.getIndex()));
        document.setId(cursor.getString(DocumentDB.Columns.ID.getIndex()));
        document.setModificationId(cursor.getString(DocumentDB.Columns.MODIFICATION_ID.getIndex()));
        document.setCreationDate(cursor.getString(DocumentDB.Columns.CREATION_DATE.getIndex()));
        document.setDescription(cursor.getString(DocumentDB.Columns.DESCRIPTION.getIndex()));
        document.setLastModified(cursor.getString(DocumentDB.Columns.LAST_MODIFIED.getIndex()));
        document.setOwnerId(cursor.getString(DocumentDB.Columns.OWNER_ID.getIndex()));
        document.setOwnerName(cursor.getString(DocumentDB.Columns.OWNER_NAME.getIndex()));
        document.setType(cursor.getString(DocumentDB.Columns.TYPE.getIndex()));
        document.setHash(cursor.getString(DocumentDB.Columns.HASH.getIndex()));
        document.setMimeType(cursor.getString(DocumentDB.Columns.MIME_TYPE.getIndex()));
        document.setPages(cursor.getLong(DocumentDB.Columns.PAGES.getIndex()));
        document.setSize(cursor.getLong(DocumentDB.Columns.SIZE.getIndex()));
        document.addTag(cursor.getString(DocumentDB.Columns.TAGS.getIndex()));
        document.setParentId(cursor.getString(DocumentDB.Columns.PARENT_ID.getIndex()));
        document.setSpaceId(cursor.getString(DocumentDB.Columns.SPACE_ID.getIndex()));
        document.setIdChain(cursor.getString(DocumentDB.Columns.ID_CHAIN.getIndex()));
        document.setVersionNumber(cursor.getInt(DocumentDB.Columns.VERSION_NUMBER.getIndex()));
        document.setRenditionAbsPath(cursor.getString(DocumentDB.Columns.PREVIEW_PATH.getIndex()));
        document.setPreviewAbsPath(cursor.getString(Columns.RENDITION_PATH.getIndex()));


        String states = cursor.getString(DocumentDB.Columns.STATES.getIndex());
        if (states != null && !TextUtils.isEmpty(states)) {
            if (states.contains(",")) {
                String[] states_array = states.split(",");

                for (int i = 0; i < states_array.length; i++) {
                    document.addState(DocumentState.valueOf(states_array[i]));
                }
            } else {
                document.addState(DocumentState.valueOf(states));
            }
        }
        return document;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.getId());
        out.writeString(this.getType());
        out.writeString(this.getOwnerId());
        out.writeString(this.getOwnerName());
        out.writeString(this.getCreationDate());
        out.writeString(this.getLastModified());
        out.writeString(this.getName());
        out.writeString(this.getDescription());
        out.writeString(this.getModificationId());
        out.writeString(this.getParentId());
        out.writeString(this.getSpaceId());
        out.writeString(this.getIdChain());
        out.writeString(this.getMimeType());
        out.writeString(this.getHash());
        out.writeString(this.getRenditionAbsPath());
        out.writeString(this.getFileAbsPath());
        out.writeString(this.getPreviewAbsPath());
        out.writeLong(this.getSize());
        out.writeLong(this.getPages());
        out.writeInt(this.getVersionNumber());
        out.writeStringList(this.getTags());
        out.writeParcelable(this.stateHolder, flags);
    }

    private Document(Parcel in) {
        this.setId(in.readString());
        this.setType(in.readString());
        this.setOwnerId(in.readString());
        this.setOwnerName(in.readString());
        this.setCreationDate(in.readString());
        this.setLastModified(in.readString());
        this.setName(in.readString());
        this.setDescription(in.readString());
        this.setModificationId(in.readString());
        this.setParentId(in.readString());
        this.setSpaceId(in.readString());
        this.setIdChain(in.readString());
        this.setMimeType(in.readString());
        this.setHash(in.readString());
        this.setRenditionAbsPath(in.readString());
        this.setFileAbsPath(in.readString());
        this.setPreviewAbsPath(in.readString());
        this.setSize(in.readLong());
        this.setPages(in.readLong());
        this.setVersionNumber(in.readInt());
        List<String> tags = new ArrayList<String>();
        in.readStringList(tags);
        this.setTags(tags);

        this.stateHolder = in.readParcelable(DocumentState.class
                .getClassLoader());

    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Document createFromParcel(Parcel in) {
            return new Document(in);
        }

        public Document[] newArray(int size) {
            return new Document[size];
        }
    };

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    public Uri getFileContentUri(Context context) {

        return ContentProviderHelpers.documentGetFileUri(context, this);
    }


    public void recycle(RequestListener listener) {
        addState(DocumentState.RECYCLED);
        save(listener);
    }


}
