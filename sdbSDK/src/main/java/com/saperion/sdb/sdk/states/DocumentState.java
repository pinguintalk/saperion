package com.saperion.sdb.sdk.states;

public enum DocumentState {
    CRYPTED,
    RECYCLED,
    DELETED,
    LINKED,
    COMMENTED;
}
