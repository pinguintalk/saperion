package com.saperion.sdb.sdk.states;

public enum FolderState {
    RECYCLED,
    DELETED;
}
