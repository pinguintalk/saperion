package com.saperion.sdb.sdk.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * Created by nnn on 22.07.13.
 */
public class ErrorMessage implements Parcelable {

    String type = "error";
    String exceptionMessage;
    String exceptionType;
    String lang;
    String localizedMessage;
    String message;
    String url;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }

    public void setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }

    public String getExceptionType() {
        return exceptionType;
    }

    public void setExceptionType(String exceptionType) {
        this.exceptionType = exceptionType;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getLocalizedMessage() {
        return localizedMessage;
    }

    public void setLocalizedMessage(String localizedMessage) {
        this.localizedMessage = localizedMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public String toJson() {

        ObjectMapper mapper = new ObjectMapper();

        mapper.setSerializationInclusion(Include.NON_NULL);
        try {
            return mapper.writeValueAsString(this);

        } catch (JsonProcessingException e1) {
            e1.printStackTrace();
        }
        return null;
    }

    public static ErrorMessage fromJson(String string) {

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        try {
            mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

            return mapper.readValue(string, ErrorMessage.class);

        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.getType());
        out.writeString(this.getExceptionMessage());
        out.writeString(this.getExceptionType());
        out.writeString(this.getLang());
        out.writeString(this.getLocalizedMessage());
        out.writeString(this.getMessage());
        out.writeString(this.getUrl());
    }

    public ErrorMessage() {
    }


    private ErrorMessage(Parcel in) {

        this.setType(in.readString());
        this.setExceptionMessage(in.readString());
        this.setExceptionType(in.readString());
        this.setLang(in.readString());
        this.setLocalizedMessage(in.readString());
        this.setMessage(in.readString());
        this.setUrl(in.readString());
    }

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }
}

