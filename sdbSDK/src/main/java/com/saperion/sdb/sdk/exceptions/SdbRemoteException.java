package com.saperion.sdb.sdk.exceptions;

import android.os.RemoteException;

/**
 * Thrown to indicate that a remote exception occurred.
 * <p/>
 * Subclass this class to create special exceptions for your needs.
 *
 * @author nnn
 */
public abstract class SdbRemoteException extends RemoteException {

    private static final long serialVersionUID = 4657308128254827562L;

    /**
     * Constructs a new {@link SdbRemoteException} that includes the current stack trace.
     */
    public SdbRemoteException() {
        super();
    }

}
