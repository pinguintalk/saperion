package com.saperion.sdb.sdk.operations;

import android.content.Context;
import android.os.Bundle;

import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.factory.SpaceJsonFactory;
import com.saperion.sdb.sdk.models.Space;
import com.saperion.sdb.sdk.network.NetworkConnection.ConnectionResult;
import com.saperion.sdb.sdk.providers.ContentProviderHelpers;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.RequestConstants;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.service.RequestService.Operation;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

import java.util.ArrayList;

public final class SpaceGetListOperation implements Operation {

    private static final String TAG = SpaceGetListOperation.class.getSimpleName();

    @Override
    public Bundle execute(Context context, Request request)
            throws ServiceRequestException, ConnectionException, DataException {

        Log.v(TAG, Utils.getMethodName() + "entry");
        Bundle bundle = new Bundle();

        // waiting for response and parse it
        ConnectionResult result = OperationHelpers.spaceGetList(context);

        int responseCode = result.responseCode;
        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, responseCode);
        ArrayList<Space> spaceList = new ArrayList<Space>();

        // Log.e(TAG, Utils.getMethodName() + result.body);
        if (responseCode == ResponseConstants.RESPONSE_CODE_NOT_MODIFIED) {
            // get local cached spaces
            spaceList = ContentProviderHelpers.spaceGetList(context);
            bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE,
                    ResponseConstants.RESPONSE_CODE_OK);
        } else if (responseCode == ResponseConstants.RESPONSE_CODE_OK) {
            if (!result.body.equals("[]")) {
                spaceList = SpaceJsonFactory.parseJsonList(result.body);
                if (spaceList.size() > 0) {
                    OperationHelpers.spacesSyncContentProvider(context, spaceList);
                }
            }
        } else {
            Log.w(TAG, Utils.getMethodName() + " responseCode:" + result.responseCode);
            bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);
            bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
            return bundle;
        }

        bundle.putParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, spaceList);
        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_REQUEST_TYPE,
                RequestConstants.REQUEST_TYPE_SPACE_GET_LIST);

        return bundle;
    }

}
