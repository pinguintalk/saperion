/**
 * 2011 Foxykeep (http://datadroid.foxykeep.com)
 * <p>
 * Licensed under the Beerware License : <br />
 * As long as you retain this notice you can do whatever you want with this stuff. If we meet some
 * day, and you think this stuff is worth it, you can buy me a beer in return
 */

package com.saperion.sdb.sdk.requestmanager;

import android.os.Bundle;

import com.saperion.sdb.sdk.SdbApplication;
import com.saperion.sdb.sdk.service.SdbService;

/**
 * This class is used as a proxy to call the Service. It provides easy-to-use methods to call the
 * service and manages the Intent creation. It also assures that a request will not be sent again if
 * an exactly identical one is already in progress.
 *
 * @author Foxykeep, nnn
 */
public final class RequestManagerSingleton extends RequestManager {

    // Singleton management
    private static RequestManagerSingleton sInstance;
    SdbApplication application;
    
    public synchronized static RequestManagerSingleton createInstance(SdbApplication application) {
        if (sInstance == null) {
            sInstance = new RequestManagerSingleton(application);
        }
        return sInstance;
    }

    public static boolean isCreated() {

        if (sInstance == null) {
            return false;
        }

        return true;
    }

    public synchronized static RequestManagerSingleton getInstance() {

        return sInstance;
    }

    private RequestManagerSingleton(SdbApplication application) {
        super(application, SdbService.class);
        this.application = application;
    }
    
    @Override
    public void execute(Request request, RequestListener listener) {

        if (sInstance == null) {
            return;
        }
        
        Bundle resultData = new Bundle();
            
        /*
        if (!Utils.isNetworkAvailable(application))
        {
            listener.onRequestConnectionError(request, ERROR_TYPE_CONNEXION);
            return;
        } 
         */
        
        if (request.getRequestType() != RequestConstants.REQUEST_TYPE_GET_ACCESS_TOKEN) {
            if  (!application.isLoggedIn()) {
                
                resultData.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, ResponseConstants.RESPONSE_CODE_UNAUTHORIZED);
                resultData.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, "You aren't logged in, please check log in data !");
                listener.onRequestServiceError(request, resultData);               
   
            }
        } 

        super.execute(request,listener);

    }
}
