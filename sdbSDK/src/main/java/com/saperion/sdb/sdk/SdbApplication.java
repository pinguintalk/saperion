package com.saperion.sdb.sdk;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import com.saperion.sdb.sdk.config.JSONTag;
import com.saperion.sdb.sdk.config.WSConfig;
import com.saperion.sdb.sdk.exceptions.AuthenticationException;
import com.saperion.sdb.sdk.models.Share;
import com.saperion.sdb.sdk.models.Space;
import com.saperion.sdb.sdk.models.SpacedItem;
import com.saperion.sdb.sdk.models.SystemInfo;
import com.saperion.sdb.sdk.models.User;
import com.saperion.sdb.sdk.operations.GetAccessTokenOperation;
import com.saperion.sdb.sdk.operations.GetLanguageOperation;
import com.saperion.sdb.sdk.operations.ShareGetListOperation;
import com.saperion.sdb.sdk.operations.UserUpdatePasswordOperation;
import com.saperion.sdb.sdk.providers.ContentProviderHelpers;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.RequestConstants;
import com.saperion.sdb.sdk.requestmanager.RequestManager.RequestListener;
import com.saperion.sdb.sdk.requestmanager.RequestManagerSingleton;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.security.CredentialManager;
import com.saperion.sdb.sdk.security.Crypto;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;

public class SdbApplication extends Application {

    private static final String TAG = SdbApplication.class.getSimpleName();

    private static SdbApplication instance;
    
    protected RequestManagerSingleton mRequestManager;
    protected ArrayList<Request> mRequestList = new ArrayList<Request>();

    protected CredentialManager mCredentialManager;

    protected SystemInfo systemInfos;
    private Context mContext;
    private Handler handler = new Handler();
    SynchronizationDataRunnable syncDataRunnable = null;
    UpdateTokenRunnable updateTokenRunnable = null;

    protected static int RUN_SYNC_TAKS_INTERVAL = 10000; // 30 seconds
    protected static int UPDATE_ACCESS_TOKEN_INTERVAL = 1500000; // 25 Minutes
    private String tmpPath = "";
    private boolean showRecycle = false;

    protected  User currentUser = null;
    private SpacedItem mClipboardItem = null;
    private RequestListener loginListener = null;
    private RequestListener requestListener = null;
    
    public static final String USER_LIST_ETAG = "USER_LIST_ETAG";
    private SharedPreferences appSharedPreferences;
    private String userListEtag;
    private boolean isLoggedIn = false;
    
    /**
     * Constructor.
     */
    public SdbApplication() {
        instance = this;
        
    }


    public SpacedItem getmClipboardItem() {
        return mClipboardItem;
    }

    public void setmClipboardItem(SpacedItem mClipboardItem) {
        Log.v(TAG, Utils.getMethodName() + "item saved in clipboard ");
        this.mClipboardItem = mClipboardItem;
    }
    
    public void clearClipboardItem() {
        this.mClipboardItem = null;
    }

    public SdbApplication(Context context) {

        Log.v(TAG, Utils.getMethodName() + "entry");

    }

    public ArrayList<User> getUserListCached() {
        return ContentProviderHelpers.userGetList(mContext);
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }


    public User getCurrentUser(RequestListener listener) {
        Log.v(TAG, Utils.getMethodName() + "entry");

        Request request = new Request(RequestConstants.REQUEST_TYPE_USER_GET_CURRENT);
        mRequestManager.execute(request, listener);
        mRequestList.add(request);

        currentUser = ContentProviderHelpers.userGetByEmail(mContext, CredentialManager.getInstance().getUserName());

        return currentUser;
    }
    
    public ArrayList<User> getUserList(RequestListener listener) {
        Log.v(TAG, Utils.getMethodName() + "entry");
        Request request = new Request(RequestConstants.REQUEST_TYPE_USER_GET_LIST);

        mRequestManager.execute(request, listener);
        mRequestList.add(request);
        
        return ContentProviderHelpers.userGetList(this);
    }

    public SystemInfo getSystemInfos() {
        return systemInfos;
    }

    public void setSystemInfos(SystemInfo systemInfos) {
        this.systemInfos = systemInfos;
    }
    
    public void getAccounts(RequestListener listener) {
        Log.v(TAG, Utils.getMethodName() + "entry");
        Request request = new Request(RequestConstants.REQUEST_TYPE_ACCOUNT_GET);

        mRequestManager.execute(request, listener);
        mRequestList.add(request);
    }


    public ArrayList<Share> getShareList(RequestListener requestListener,String spaceId) {

        Request request = new Request(RequestConstants.REQUEST_TYPE_SHARE_GET_LIST);
        request.put(ShareGetListOperation.PARAM_SPACE_ID, spaceId);

        if (RequestManagerSingleton.isCreated())
            RequestManagerSingleton.getInstance().execute(request, requestListener);
        
        return ContentProviderHelpers.shareGetListCached(this);
    }
    
    public String getUserListEtag() {
        return userListEtag;
    }


    public void setUserListEtag(String userListEtag) {
        this.userListEtag = userListEtag;

        SharedPreferences.Editor editor = appSharedPreferences.edit();
        editor.putString(USER_LIST_ETAG, userListEtag);
        editor.commit();
    }


    
    
    @Override
    public void onCreate() {
        super.onCreate();
        // TODO dont need it
        this.mContext = getApplicationContext();
        this.showRecycle = false;
        
        appSharedPreferences =
                this.mContext.getSharedPreferences("com.saperion.sdk.appSharedPreferences",
                        Context.MODE_PRIVATE);

        userListEtag  = appSharedPreferences.getString(USER_LIST_ETAG, "");
        
        mRequestManager = RequestManagerSingleton.createInstance(this); // get RequestManager instance
        mCredentialManager = CredentialManager.create(mContext);

        if (mCredentialManager.getServerUrl().isEmpty())
            mCredentialManager.setServerUrl(WSConfig.WS_ROOT_URL);
        if (mCredentialManager.getUserName().isEmpty())
            mCredentialManager.setUserName("ngocnam.nguyen@saperion.com");

        String fileSystemPath = "";

        try {
            fileSystemPath = mContext.getFilesDir().getPath();
        } catch (NullPointerException e) {
            // TODO send exception to tracker !
            Log.e(TAG, Utils.getMethodName() + "No Path found ");
        }

        String preview_path = fileSystemPath + "/" + WSConfig.WS_PREVIEW;

        Log.v(TAG, Utils.getMethodName() + "creating folder " + preview_path);
        File preview_folder = new File(preview_path);

        if (!preview_folder.exists()) {
            preview_folder.mkdir();
        }

        String document_path = fileSystemPath + "/" + WSConfig.WS_DOCUMENT;
        Log.v(TAG, Utils.getMethodName() + "creating folder " + document_path);
        File document_folder = new File(document_path);
        if (!document_folder.exists()) {
            document_folder.mkdir();
        }

        String rendition_path = fileSystemPath + "/" + WSConfig.WS_RENDITION;
        Log.v(TAG, Utils.getMethodName() + "creating folder " + rendition_path);
        File rendition_folder = new File(rendition_path);
        
        if (!rendition_folder.exists()) {
            rendition_folder.mkdir();
           
        }

        
        String avatar_path = fileSystemPath + "/" + WSConfig.WS_AVATAR;

        Log.v(TAG, Utils.getMethodName() + "creating avatar folder " + avatar_path);
        File avatar_folder = new File(avatar_path);

        if (!avatar_folder.exists()) {
            avatar_folder.mkdir();
        }
        
        tmpPath = fileSystemPath +  WSConfig.WS_TMP;
        File tmp_folder = new File(tmpPath);
       
        if (!document_folder.exists()) {
            tmp_folder.mkdir();
        }


        requestListener = new RequestListener() {

            @Override
            public void onRequestFinished(Request request, Bundle resultData) {
                int req_type = request.getRequestType();
                int resp_code = resultData.getInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE);

                Log.v(TAG, Utils.getMethodName() + "getRequestType " + req_type + " resultCode "
                        + resp_code);

                switch (req_type) {

                    case RequestConstants.REQUEST_TYPE_GET_ACCESS_TOKEN: {

                        if (resp_code != ResponseConstants.RESPONSE_CODE_OK) {
                            isLoggedIn = false;
                            return;
                        }

                        String result_data =
                                resultData.getString(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);

                        // Log.v(TAG, Utils.getMethodName() + "result_data "  + result_data);

                        if (result_data != null && result_data.length() > 0) {
                            JSONObject authObject;
                            try {
                                authObject = new JSONObject(result_data);

                                Log.v(TAG, Utils.getMethodName() + result_data);

                                if (authObject.has(JSONTag.AUTHENTICATION_ACCES_TOKEN)) {
                                    String token =
                                            authObject.getString(JSONTag.AUTHENTICATION_ACCES_TOKEN);
                                    
                                    String expires =
                                            authObject
                                                    .getString(JSONTag.AUTHENTICATION_ACCES_TOKEN_EXPIRES);

                                    CredentialManager.getInstance().setmAccessToken(token);
                                    CredentialManager.getInstance().setmExpires(expires);
                                    CredentialManager.getInstance().setmLastUpdateTimePoint(new Date().getTime());
                        
                                    
                                    Log.v(TAG, Utils.getMethodName() + "access_token " + token
                                            + " expires " + expires);
                                    
                                    isLoggedIn = true;

                                    // forward result to LoginActivity
                                    if (loginListener != null)
                                        loginListener.onRequestFinished(request, resultData);

         
                                }
                            } catch (JSONException e) {
                                Log.e(TAG, Utils.getMethodName() + "JSONException " + e.toString());
                            }
                        }

                    }
                    break;
                    case RequestConstants.REQUEST_TYPE_GET_SYSTEM_INFOS: {
                        systemInfos = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);

                        // Log.v(TAG, Utils.getMethodName() + "system_info " + system_Info_infos.toString());
                        for (String lang:systemInfos.getSupportedLanguages()){
                            if (lang.equals("de")) {
                                getLocalization(requestListener,"de");
                            }
                        }
                    }
                    break;


                    case RequestConstants.REQUEST_TYPE_USER_GET_CURRENT:
                        currentUser = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);

                        break;

                    case RequestConstants.REQUEST_TYPE_GET_LOCALIZAION:
                        String localization= resultData.getString(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                        Log.v(TAG, Utils.getMethodName() + "current localization " + localization );
                        break;

                    case RequestConstants.REQUEST_TYPE_USER_GET_AVATAR:
                        User currentUser = resultData.getParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT);
                        Log.v(TAG, Utils.getMethodName() + "user avatar updated " + currentUser.getDisplayname());

                        break;
                    
                    default:
                        break;
                }

            }

            @Override
            public void onRequestConnectionError(Request request, int statusCode) {

                int req_type = request.getRequestType();
                if (req_type == RequestConstants.REQUEST_TYPE_GET_ACCESS_TOKEN) {
                    isLoggedIn = false;
                }
                // forward result to LoginActivity
                if (loginListener != null)
                    loginListener.onRequestConnectionError(request, statusCode);

            }

            @Override
            public void onRequestDataError(Request request) {
                int req_type = request.getRequestType();
                if (req_type == RequestConstants.REQUEST_TYPE_GET_ACCESS_TOKEN) {
                    isLoggedIn = false;
                }
                // forward result to LoginActivity
                if (loginListener != null)
                    loginListener.onRequestDataError(request);

            }

            @Override
            public void onRequestServiceError(Request request, Bundle resultData) {
                int req_type = request.getRequestType();
                if (req_type == RequestConstants.REQUEST_TYPE_GET_ACCESS_TOKEN) {
                    isLoggedIn = false;
                }
                // forward result to LoginActivity
                if (loginListener != null)
                    loginListener.onRequestServiceError(request, resultData);

            }

        };


    }


    public synchronized static SdbApplication getInstance() {
        if (instance == null) {
            instance = new SdbApplication();
        }
        return instance;
    }

    public void itemCopy(SpacedItem item) {
        this.setmClipboardItem(item);
    }

    public String getServerUrl() {
        return mCredentialManager.getServerUrl();
    }

    public void setServerUrl(String url) {
        mCredentialManager.setServerUrl(url);
    }


    public void stopAllRequest() {
        mRequestManager.stopAllDefaultAndLowRequests();
    }

    public void setShowRecycle(boolean b) {
        showRecycle = b;
    }

    public boolean isShowRecycledItems() {
        return showRecycle;
    }

    public ArrayList<? extends SpacedItem> getLocalChildrenByParentId(String parentId) {
        return ContentProviderHelpers.getChildrenByParentId(this.mContext, parentId);
    }

    public User getUser(String userId) {
        
        return ContentProviderHelpers.userGetById(mContext,userId);

    }



    // get settings of current account 
    public void getCurrentSettings(RequestListener listener) {
        Request request = new Request(RequestConstants.REQUEST_TYPE_ACCOUNT_GET_SETTINGS);
        request.setMemoryCacheEnabled(true);

        mRequestManager.execute(request, listener);
        mRequestList.add(request);
    }
    
	/*
	login with current account settings

	public void login(RequestListener listener) {
		Request request = new Request(RequestConstants.REQUEST_TYPE_GET_ACCESS_TOKEN);
		request.setMemoryCacheEnabled(true);

		mRequestManager.execute(request, listener);
		mRequestList.add(request);
	}
    */


    public void updatePassword(RequestListener listener, String oldPassword, String newPassword) {

        Request request = new Request(RequestConstants.REQUEST_TYPE_USER_UPDATE_PASSWORD);

        JSONObject jo = new JSONObject();
        try {
            jo.put("password", oldPassword);
            jo.put("newPassword", newPassword);
            jo.put("newPasswordMD5", Crypto.getMD5(newPassword));

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, Utils.getMethodName() + "error " + e.toString());
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            Log.e(TAG, Utils.getMethodName() + "error " + e.toString());
        }

        request.put(UserUpdatePasswordOperation.PARAM_PAYLOAD, jo.toString());

        if (RequestManagerSingleton.isCreated() && listener != null)
            RequestManagerSingleton.getInstance().execute(request, listener);

    }

    public ArrayList<Space> getSpaces(RequestListener listener) {

        Log.v(TAG, Utils.getMethodName() + "entry");
        
        Request request = new Request(RequestConstants.REQUEST_TYPE_SPACE_GET_LIST);
        mRequestManager.execute(request, listener);
        mRequestList.add(request);

        return ContentProviderHelpers.spaceGetList(this);

    }

    public void deleteSpaces(RequestListener listener) {

        Log.v(TAG, Utils.getMethodName() + "entry");
        Request request = new Request(RequestConstants.REQUEST_TYPE_SPACE_DELETE_ALL);
        mRequestManager.execute(request, listener);
        mRequestList.add(request);

    }
    
	/*
	 * 		// SERVICE REQUESTS
	 * 
	 * 
	 */

    public void updateAccessToken() {

        Log.v(TAG, Utils.getMethodName() + "entry");

        // start runnable after ms
        final int start_in_sec = 0;
        
        if(updateTokenRunnable != null){
            updateTokenRunnable.running = false;
            updateTokenRunnable = null;
        }
        
        updateTokenRunnable = new UpdateTokenRunnable();
        handler.postDelayed(updateTokenRunnable, start_in_sec);
    }

    /**
     * Create the request to the authentication webservice.
     * <p/>
     * The returned message will be either a greeting if the authentication worked or a message
     * telling you to authenticate otherwise.
     */

    private class UpdateTokenRunnable implements Runnable {

        boolean running = true;
            UpdateTokenRunnable() {
        }

        public void run() {
            
            if (running){
            Log.v(TAG, Utils.getMethodName() + "updating access token");

                String userName = CredentialManager.getInstance().getUserName();
                String passWord = null;
                try {
                    passWord = CredentialManager.getInstance().getPassword();
                    if ( passWord.length() < 1 ) return;
                } catch (AuthenticationException e) {
                    e.printStackTrace();
                    Log.e(TAG, Utils.getMethodName() + "error " + e.toString());
                }

                if ( userName.length() < 1 ) return;
     
            Request request = new Request(RequestConstants.REQUEST_TYPE_GET_ACCESS_TOKEN);
            request.setPriority(Request.PRIORITY_LEVEL_HIGH);
            request.put(GetAccessTokenOperation.PARAM_USER_NAME,userName );
            request.put(GetAccessTokenOperation.PARAM_PASSWORD,passWord );
                
            mRequestManager.execute(request, requestListener);
            mRequestList.add(request);

            handler.postDelayed(this, UPDATE_ACCESS_TOKEN_INTERVAL);
            }
        }
    }

    public void getSystemInformations(RequestListener listener) {

        Log.v(TAG, Utils.getMethodName() + "entry");
        Request request = new Request(RequestConstants.REQUEST_TYPE_GET_SYSTEM_INFOS);
        mRequestManager.execute(request, listener);
        mRequestList.add(request);

    }

    public void getLocalization(RequestListener listener, String lang) {

        Log.v(TAG, Utils.getMethodName() + "entry");
        Request request = new Request(RequestConstants.REQUEST_TYPE_GET_LANGUAGE);
        request.put(GetLanguageOperation.PARAM_LANG,lang);
        mRequestManager.execute(request, listener);
        mRequestList.add(request);

    }

    public void startSyncTasks(RequestListener listener) {

        Log.v(TAG, Utils.getMethodName() + "entry");

        // start runnable after ms
        final int start_in_sec = 5000;
        syncDataRunnable = new SynchronizationDataRunnable(listener);
        handler.postDelayed(syncDataRunnable, start_in_sec);
    }

    private class SynchronizationDataRunnable implements Runnable {
        private final RequestListener listener;

        SynchronizationDataRunnable(RequestListener listener) {
            this.listener = listener;
        }

        public void run() {
            Log.v(TAG, Utils.getMethodName() + "checking space to sync");

            Request request = new Request(RequestConstants.REQUEST_TYPE_SYNC_SPACE_LIST);

            mRequestManager.execute(request, this.listener);
            mRequestList.add(request);

            handler.postDelayed(this, RUN_SYNC_TAKS_INTERVAL);
        }
    }

    public ArrayList<Space> getLocalSpaces() {

        return ContentProviderHelpers.spaceGetList(mContext);
    }

    public File[] getTempFiles() {

        File temp_folder = new File(tmpPath);
        return temp_folder.listFiles();
    }


    public void copyFileToTempFolder(File src) {

        File dst = new File(tmpPath + "/" + src.getName());

        Log.v(TAG, Utils.getMethodName() + "copying file createInstance  " + src.getAbsoluteFile() + "to "
                + dst.toString());

        try {
            Utils.fileCopy(src, dst);
        } catch (IOException e) {
            Log.e(TAG, Utils.getMethodName() + e.toString());
        }
    }

    public void login(String serverURL, String username, String password,
                      RequestListener loginListener) {
        Log.v(TAG, Utils.getMethodName() + "entry");
        
        if (serverURL.length() < 1 || username.isEmpty()) return;
        if (password == null || password.isEmpty()) return;        
        
        this.loginListener = loginListener;

        CredentialManager.getInstance().setServerUrl(serverURL);
        CredentialManager.getInstance().setUserName(username);
        CredentialManager.getInstance().setPassword(password);

        Request request = new Request(RequestConstants.REQUEST_TYPE_GET_ACCESS_TOKEN);
        request.setPriority(Request.PRIORITY_LEVEL_HIGH);
        request.put(GetAccessTokenOperation.PARAM_USER_NAME,username);
        request.put(GetAccessTokenOperation.PARAM_PASSWORD,password);
        
        mRequestManager.execute(request, this.requestListener);
    }


    public String getUserName() {
        // TODO Auto-generated method stub
        return mCredentialManager.getUserName();
    }

    public void setUserName(String username) {
        // TODO Auto-generated method stub
        mCredentialManager.setUserName(username);
    }

    public String getPassword() {
        // TODO Auto-generated method stub
        try {
            return mCredentialManager.getPassword();
        } catch (AuthenticationException e) {
            Log.e(TAG, Utils.getMethodName() + e.getMessage());
        }
        return "";
    }

    public void setPassword(String password) {

        mCredentialManager.setPassword(password);
    }


    public void onLowMemory() {
        super.onLowMemory();
        Log.w(TAG, Utils.getMethodName() + "onLowMemory"); // TODO: free some memory (caches) in native code
    }

}
