package com.saperion.sdb.sdk.security;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Build.VERSION_CODES;

import com.saperion.sdb.sdk.config.WSConfig;
import com.saperion.sdb.sdk.exceptions.AuthenticationException;
import com.saperion.sdb.sdk.utils.Log;

public final class CredentialManager {

    protected static final String TAG = CredentialManager.class.getCanonicalName();

    public static final String SERVER_URL = "SERVE_URL";
    public static final String USERNAME = "USERNAME";
    public static final String FIRST_NAME = "FIRST_NAME";
    public static final String LAST_NAME = "LAST_NAME";
    public static final String COMPANY = "COMPANY";
    public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    public static final String EXPIRES = "EXPIRES";
    public static final String LAST_UPDATE_TIME_POINT = "LAST_UPDATE_TIME_POINT"; 
    // the last time point in milliseconds the application has updated the access token 
    public static final String ENCRYPTED_PASSWORD = "ENCRYPTED_PASSWORD";
    private SharedPreferences mUserSharedPreferences;

    private final Context mContext;
    private String mServerUrl;
    private String mUserName;
    private String mAccessToken = "";
    private String mExpires = "";
    private long mLastUpdateTimePoint = 0;
    private boolean haveToSaveCredential = true;
    private String encryptedPassword;
    public KeyStoreHelper mKeyStoreHelper;

    boolean isKeyStoreAPIAvailable = false;

    // You can store multiple key pairs in the Key Store.  The string used to refer to the Key you
    // want to store, or later pull, is referred to as an "alias" in this case, because calling it
    // a key, when you use it to retrieve a key, would just be irritating.
    public static final String ALIAS = "com.saperion.sdb.passkey";

    // something
    private static final byte[] SALT = {
            (byte) 0xde, (byte) 0x32, (byte) 0x10, (byte) 0x11,
            (byte) 0xde, (byte) 0x33, (byte) 0x15, (byte) 0x12,
            (byte) 0xde, (byte) 0x31, (byte) 0x13, (byte) 0x13,
            (byte) 0xde, (byte) 0x32, (byte) 0x10, (byte) 0x14,
    };


    protected CredentialManager(Context context) {
        mContext = context.getApplicationContext();

        mUserSharedPreferences =
                context.getSharedPreferences("com.saperion.sdk.CMSharedPreferences",
                        Context.MODE_PRIVATE);

        mServerUrl = mUserSharedPreferences.getString(SERVER_URL, WSConfig.WS_ROOT_URL);
        mUserName = mUserSharedPreferences.getString(USERNAME, "");
        mAccessToken = mUserSharedPreferences.getString(ACCESS_TOKEN, "");
        mExpires = mUserSharedPreferences.getString(EXPIRES, "");
        encryptedPassword = mUserSharedPreferences.getString(ENCRYPTED_PASSWORD, "");

        if (isKeyStoreAPIAvailable()) {
            isKeyStoreAPIAvailable = true;
            mKeyStoreHelper = new KeyStoreHelper(context, ALIAS);
        }

    }


    private boolean isKeyStoreAPIAvailable() {
        try {
            if (Build.VERSION.SDK_INT < VERSION_CODES.ICE_CREAM_SANDWICH) {
                Log.w(TAG, "device version" + Build.VERSION.SDK_INT);
                return false;
            } else {
                return true;
            }
        } catch (ActivityNotFoundException e) {
            Log.e(TAG, "No UNLOCK activity: " + e.getMessage());
        }

        return false;
    }

    // Singleton
    private static CredentialManager sInstance;

    public synchronized static CredentialManager create(Context context) {
        if (sInstance == null) {
            sInstance = new CredentialManager(context);
        }

        return sInstance;
    }

    public synchronized static CredentialManager getInstance() {

        return sInstance;
    }

    public static boolean isCreated() {

        if (sInstance == null) {
            return false;
        }

        return true;
    }

    public long getmLastUpdateTimePoint() {
        return mLastUpdateTimePoint;
    }

    public void setmLastUpdateTimePoint(long mLastUpdateTimePoint) {
        this.mLastUpdateTimePoint = mLastUpdateTimePoint;

        if (haveToSaveCredential) {
            SharedPreferences.Editor editor = mUserSharedPreferences.edit();
            editor.putLong(LAST_UPDATE_TIME_POINT, mLastUpdateTimePoint);
            editor.commit();
        }
    }
    
    public String getServerUrl() {
        return mServerUrl;
    }


    public void setServerUrl(String serverUrl) {
        mServerUrl = serverUrl;

        SharedPreferences.Editor editor = mUserSharedPreferences.edit();
        editor.putString(SERVER_URL, serverUrl);
        editor.commit();
    }

    public final String getUserName() {
        return mUserName;
    }

    public void setUserName(String username) {
        this.mUserName = username;

        if (haveToSaveCredential) {
            SharedPreferences.Editor editor = mUserSharedPreferences.edit();
            editor.putString(USERNAME, username);
            editor.commit();
        }

    }

    public String getPassword() throws AuthenticationException {

        if (this.encryptedPassword == null || this.encryptedPassword.length() < 1) return "";
        
        if (isKeyStoreAPIAvailable) {
            return mKeyStoreHelper.decrypt(this.encryptedPassword);
        }
        return KeyStoreHelper.decryptText(SALT, this.encryptedPassword);
    }

    public void setPassword(String password) {

        if (isKeyStoreAPIAvailable) {
            this.encryptedPassword = mKeyStoreHelper.encrypt(password);

            if (haveToSaveCredential) {
                SharedPreferences.Editor editor = mUserSharedPreferences.edit();
                editor.putString(ENCRYPTED_PASSWORD, encryptedPassword);
                editor.commit();
            }
        } else {
            this.encryptedPassword = KeyStoreHelper.encryptText(SALT, password);

            if (haveToSaveCredential) {
                SharedPreferences.Editor editor = mUserSharedPreferences.edit();
                editor.putString(ENCRYPTED_PASSWORD, encryptedPassword);
                editor.commit();
            }
        }

        // Log.w(TAG, "encryptedPassword saved: " + encryptedPassword);

    }

    public String getmAccessToken() {
        return mAccessToken;
    }

    public void setmAccessToken(String mAccessToken) {
        this.mAccessToken = mAccessToken;

        if (haveToSaveCredential) {
            SharedPreferences.Editor editor = mUserSharedPreferences.edit();
            editor.putString(ACCESS_TOKEN, mAccessToken);
            editor.commit();
        }
    }

    public String getmExpires() {
        return mExpires;
    }

    public void setmExpires(String mExpires) {
        this.mExpires = mExpires;

        if (haveToSaveCredential) {
            SharedPreferences.Editor editor = mUserSharedPreferences.edit();
            editor.putString(EXPIRES, mExpires);
            editor.commit();
        }
    }

    public boolean isPasswordValid(String username) {
        return true;
    }

    public boolean isUserNameValid(String password) {
        return true;
    }


}
