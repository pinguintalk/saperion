package com.saperion.sdb.sdk.operations;

import android.content.Context;
import android.os.Bundle;

import com.saperion.sdb.sdk.config.WSConfig;
import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.models.User;
import com.saperion.sdb.sdk.network.NetworkConnection.ConnectionResult;
import com.saperion.sdb.sdk.providers.ContentProviderHelpers;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.service.RequestService.Operation;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

public final class UserDeleteOperation implements Operation {

    private static final String TAG = UserDeleteOperation.class.getSimpleName();

    public static final String PARAM_ITEM_ID = WSConfig.APP_PACKAGE_NAME + ".param_user_id";

    @Override
    public Bundle execute(Context context, Request request)
            throws ServiceRequestException, ConnectionException, DataException {

        Log.v(TAG, Utils.getMethodName() + "entry");
        Bundle bundle = new Bundle();

        String Id = request.getString(PARAM_ITEM_ID);

        ConnectionResult result = OperationHelpers.userDelete(context, Id);
    
        int responseCode = result.responseCode;
        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, responseCode);

        Log.w(TAG, Utils.getMethodName() + " responseCode:" + result.responseCode);
        if (responseCode == ResponseConstants.RESPONSE_CODE_OK) {
            User user = ContentProviderHelpers.userGetById(context,Id);
            bundle.putParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, user);
            
            ContentProviderHelpers.userDelete(context,user);

        }
        else 

            bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);

        return bundle;
    }

}
