package com.saperion.sdb.sdk.operations;

import android.content.Context;
import android.os.Bundle;

import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.providers.ContentProviderHelpers;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.service.RequestService.Operation;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

public final class FolderGetDocumentsOperation implements Operation {

    private static final String TAG = FolderGetDocumentsOperation.class.getSimpleName();
    public static final String PARAM_FOLDER_ID = "com.saperion.sdb.sdk.extra.folderId";

    @Override
    public Bundle execute(Context context, Request request) throws ServiceRequestException, ConnectionException,
            DataException {

        Log.v(TAG, Utils.getMethodName() + "entry");
        Bundle bundle = new Bundle();
        String folder_id = request.getString(PARAM_FOLDER_ID);
        String folder_id_chain = ContentProviderHelpers.folderGetIdChainById(context, folder_id);

        try {
            return OperationHelpers.documentGetListByParentId(context, folder_id,
                    folder_id_chain, false);

        } catch (ServiceRequestException e) {
            // TODO Auto-generated catch block
            Log.e(TAG, Utils.getMethodName() + "SdbRemoteException " + e.toString());
        } catch (ConnectionException e) {
            // TODO Auto-generated catch block
            Log.e(TAG, Utils.getMethodName() + "RemoteException " + e.toString());
        }

        return bundle;
    }

}
