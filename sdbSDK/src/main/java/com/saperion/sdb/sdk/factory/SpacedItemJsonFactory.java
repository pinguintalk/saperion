package com.saperion.sdb.sdk.factory;

import android.text.TextUtils;
import android.util.Log;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.models.SpacedItem;
import com.saperion.sdb.sdk.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;

public final class SpacedItemJsonFactory {

    private static final String TAG = SpacedItemJsonFactory.class.getSimpleName();

    private static boolean LOG_ALL = false;

    private SpacedItemJsonFactory() {
        // No public constructor
    }

    public static ArrayList<SpacedItem> parseResult(String wsResponse) throws DataException {

        if (wsResponse == null || TextUtils.isEmpty(wsResponse))
            return null;

        ArrayList<SpacedItem> spacedItemList = new ArrayList<SpacedItem>();
        ObjectMapper objectMapper = new ObjectMapper();
        JsonFactory jsonFactory = new JsonFactory();

        try {
            JsonParser jp = jsonFactory.createJsonParser(wsResponse);
            // advance stream to START_ARRAY first:
            jp.nextToken();
            // and then each time, advance to opening START_OBJECT
            while (jp.nextToken() == JsonToken.START_OBJECT) {
                SpacedItem spacedItem = objectMapper.readValue(jp, SpacedItem.class);
                // process
                // after binding, stream points to closing END_OBJECT
                spacedItemList.add(spacedItem);

                if (LOG_ALL)
                    Log.v(TAG, Utils.getMethodName() + spacedItem.getName());
            }

        } catch (JsonParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return spacedItemList;
    }
}
