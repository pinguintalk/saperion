/**
 * 2011 Foxykeep (http://datadroid.foxykeep.com)
 * <p>
 * Licensed under the Beerware License : <br />
 * As long as you retain this notice you can do whatever you want with this stuff. If we meet some
 * day, and you think this stuff is worth it, you can buy me a beer in return
 */

package com.saperion.sdb.sdk.service;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.saperion.sdb.sdk.exceptions.AuthenticationException;
import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.CustomRequestException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.models.ErrorMessage;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.RequestManager;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

/**
 * This class is the superclass of all the worker services you'll create.
 *
 * @author Foxykeep
 */
public abstract class RequestService extends MultiThreadedIntentService {

    /**
     * Interface to implement by your operations
     *
     * @author Foxykeep
     */
    public interface Operation {
        /**
         * Execute the request and returns a {@link Bundle} containing the data to return.
         *
         * @param context The context to use for your operation.
         * @param request The request to execute.
         * @return A {@link Bundle} containing the data to return. If no data to return, null.
         * @throws ConnectionException     Thrown when a connection error occurs. It will be propagated
         *                                 to the {@link RequestManager} as a
         *                                 {@link RequestManager#ERROR_TYPE_CONNEXION}.
         * @throws DataException           Thrown when a problem occurs while managing the data of the
         *                                 webservice. It will be propagated to the {@link RequestManager} as a
         *                                 {@link RequestManager#ERROR_TYPE_DATA}.
         * @throws ServiceRequestException Any other exception you may have to throw. A call to
         *                                 {@link RequestService #onServiceRequestException(Request,
         *                                 CustomRequestException)} will be made with the Exception thrown.
         */
        public Bundle execute(Context context, Request request) throws ConnectionException,
                DataException, ServiceRequestException, AuthenticationException;
    }

    private static final String LOG_TAG = RequestService.class.getSimpleName();

    public static final String INTENT_EXTRA_RECEIVER = "com.saperion.sdb.sdk.extra.receiver";
    public static final String INTENT_EXTRA_REQUEST = "com.saperion.sdb.sdk.extra.request";

    private static final int SUCCESS_CODE = 0;
    public static final int ERROR_CODE = -1;

    /**
     * Proxy method for {@link #sendResult(ResultReceiver, Bundle, int)} when the work is a
     * success.
     *
     * @param receiver The result receiver received inside the {@link Intent}.
     * @param data     A {@link Bundle} with the data to send back.
     */
    private void sendSuccess(ResultReceiver receiver, Bundle data) {
        sendResult(receiver, data, SUCCESS_CODE);
    }

    /**
     * Proxy method for {@link #sendResult(ResultReceiver, Bundle, int)} when the work is a failure
     * due to the network.
     *
     * @param receiver  The result receiver received inside the {@link Intent}.
     * @param exception The {@link ConnectionException} triggered.
     */
    private void sendConnexionFailure(ResultReceiver receiver, ConnectionException exception) {
        Bundle data = new Bundle();
        data.putInt(RequestManager.RECEIVER_EXTRA_ERROR_TYPE, RequestManager.ERROR_TYPE_CONNEXION);
        data.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE,
                exception.getStatusCode());
        sendResult(receiver, data, ERROR_CODE);
    }

    /**
     * Proxy method for {@link #sendResult(ResultReceiver, Bundle, int)} when the work is a failure
     * due to the data (parsing for example).
     *
     * @param receiver The result receiver received inside the {@link Intent}.
     */
    private void sendDataFailure(ResultReceiver receiver) {
        Bundle data = new Bundle();
        data.putInt(RequestManager.RECEIVER_EXTRA_ERROR_TYPE, RequestManager.ERROR_TYPE_DATA);
        sendResult(receiver, data, ERROR_CODE);
    }

    /**
     * Proxy method for {@link #sendResult(ResultReceiver, Bundle, int)} when the work is a failure
     * due to {@link CustomRequestException} being thrown.
     *
     * @param receiver The result receiver received inside the {@link Intent}.
     * @param data     A {@link Bundle} the data to send back.
     */
    private void sendServiceFailure(ResultReceiver receiver, Bundle data) {
        if (data == null) {
            data = new Bundle();
        }
        data.putInt(RequestManager.RECEIVER_EXTRA_ERROR_TYPE, RequestManager.ERROR_TYPE_SERVICE);
        sendResult(receiver, data, ERROR_CODE);
    }

    /**
     * Method used to send back the result to the {@link RequestManager}.
     *
     * @param receiver The result receiver received inside the {@link Intent}.
     * @param data     A {@link Bundle} the data to send back.
     * @param code     The success/error code to send back.
     */
    private void sendResult(ResultReceiver receiver, Bundle data, int code) {
        Log.d(LOG_TAG, "code:" + code + " sendResult : "
                + ((code == SUCCESS_CODE) ? "Success" : "Failure"));

        if (receiver != null) {
            if (data == null) {
                data = new Bundle();
            }

            receiver.send(code, data);
        }

    }

    @Override
    protected final void onHandleIntent(Intent intent) {
        Log.d(LOG_TAG, "intent:" + intent.toString() );
        Request request = intent.getParcelableExtra(INTENT_EXTRA_REQUEST);
        request.setClassLoader(getClassLoader());

        ResultReceiver receiver = intent.getParcelableExtra(INTENT_EXTRA_RECEIVER);

        Operation operation = getOperationForType(request.getRequestType());

        Bundle bundle = new Bundle();

        try {

            bundle = operation.execute(this, request);
            sendSuccess(receiver, bundle);

        } catch (ConnectionException e) {
            Log.e(LOG_TAG, Utils.getMethodName() + "ConnectionException " + e.toString() + " request nr. " + request.hashCode());
            sendConnexionFailure(receiver, e);
        } catch (DataException e) {
            Log.e(LOG_TAG, Utils.getMethodName() + "DataException " + e.toString() + " request nr. " + request.hashCode());
            sendDataFailure(receiver);
        } catch (ServiceRequestException e) {
            Log.e(LOG_TAG, Utils.getMethodName() + "ServiceRequestException status code=" + e.getStatusCode()+ 
                    " request nr. " + request.hashCode());
            ErrorMessage errMesg = ErrorMessage.fromJson(e.getMessage());
            bundle = new Bundle();
            bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, e.getStatusCode());
            bundle.putParcelable(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, errMesg);

            sendServiceFailure(receiver, bundle);
        } catch (RuntimeException e) {
            // if we get null pointer
            Log.e(LOG_TAG, Utils.getMethodName() + "RuntimeException " + e.toString() + " request nr. " + request.hashCode());
            sendDataFailure(receiver);

        } catch (AuthenticationException e) {
            Log.e(LOG_TAG, Utils.getMethodName() + "AuthenticationException " + e.toString()+ " request nr. " + request.hashCode());
            sendDataFailure(receiver);
        }
    }

    /**
     * Get the {@link Operation} corresponding to the given request type.
     *
     * @param requestType The request type (extracted createInstance {@link Request}).
     * @return The corresponding {@link Operation}.
     */
    public abstract Operation getOperationForType(int requestType);


}
