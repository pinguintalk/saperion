package com.saperion.sdb.sdk.utils;

/**
 * @author nnn
 */

public class Log {

    /**
     * Whether the logs are enabled in release builds or not.
     */
    private static final boolean ENABLE_LOGS = true;

    public static void i(String TAG, String string) {

        if (ENABLE_LOGS)
            android.util.Log.i(TAG, string);

    }

    public static void e(String TAG, String string) {

        if (ENABLE_LOGS)
            android.util.Log.e(TAG, string);

    }

    public static void d(String TAG, String string) {

        if (ENABLE_LOGS)
            android.util.Log.d(TAG, string);

    }

    public static void v(String TAG, String string) {

        if (ENABLE_LOGS)
            android.util.Log.v(TAG, string);

    }

    public static void w(String TAG, String string) {

        if (ENABLE_LOGS)
            android.util.Log.w(TAG, string);

    }

}
