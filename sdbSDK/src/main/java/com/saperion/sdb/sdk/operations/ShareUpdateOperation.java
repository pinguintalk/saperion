package com.saperion.sdb.sdk.operations;

import android.content.Context;
import android.os.Bundle;

import com.saperion.sdb.sdk.Constants;
import com.saperion.sdb.sdk.config.WSConfig;
import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.models.Share;
import com.saperion.sdb.sdk.network.NetworkConnection;
import com.saperion.sdb.sdk.network.NetworkConnection.ConnectionResult;
import com.saperion.sdb.sdk.network.NetworkConnection.Method;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.security.CredentialManager;
import com.saperion.sdb.sdk.service.RequestService.Operation;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

import org.apache.http.auth.UsernamePasswordCredentials;

public final class ShareUpdateOperation implements Operation {

    private static final String TAG = ShareUpdateOperation.class.getSimpleName();

    public static final String PARAM_SHARE = Constants.SDK_PACKAGE_NAME + ".param_share";
    public static final String PARAM_PAYLOAD = Constants.SDK_PACKAGE_NAME + ".param_payload";


    @Override
    public Bundle execute(Context context, Request request) throws ServiceRequestException, ConnectionException,
            DataException {
        Bundle bundle = new Bundle();
        Log.v(TAG, Utils.getMethodName() + "entry");
        Share share = (Share) request.getParcelable(PARAM_SHARE);
        String payload = request.getString(PARAM_PAYLOAD);

        String url = WSConfig.getSpaceURL() + "/" + share.getSpaceId() + "/shares/" + share.getId();
        NetworkConnection networkConnection = new NetworkConnection(context, url);
        networkConnection.setMethod(Method.PUT);
        networkConnection.setPayload(payload);
        networkConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager
                .getInstance().getUserName(), CredentialManager.getInstance().getmAccessToken()));

        ConnectionResult result = networkConnection.execute();

        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);

        if (result.responseCode != ResponseConstants.RESPONSE_CODE_OK) {
            bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
            return bundle;
        }
        Share sharetmp = Share.fromJson(result.body);
        bundle.putParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, sharetmp);
        return bundle;
    }


  

}
