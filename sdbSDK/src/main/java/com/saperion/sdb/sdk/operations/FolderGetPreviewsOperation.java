package com.saperion.sdb.sdk.operations;

import android.content.Context;
import android.os.Bundle;

import com.saperion.sdb.sdk.Constants;
import com.saperion.sdb.sdk.config.WSConfig;
import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.models.Document;
import com.saperion.sdb.sdk.network.NetworkConnection.ConnectionResult;
import com.saperion.sdb.sdk.providers.ContentProviderHelpers;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.service.RequestService.Operation;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

import java.util.ArrayList;

public final class FolderGetPreviewsOperation implements Operation {

    private static final String TAG = FolderGetPreviewsOperation.class.getSimpleName();

    public static final String PARAM_PARENT_ID = Constants.SDK_PACKAGE_NAME + ".param_parent_id";

    @Override
    public Bundle execute(Context context, Request request) throws ServiceRequestException, ConnectionException,
            DataException {
        Bundle bundle = new Bundle();
        Log.v(TAG, Utils.getMethodName() + "entry");
        String parent_id = request.getString(PARAM_PARENT_ID);

        ArrayList<Document> doc_list =
                ContentProviderHelpers.documentGetListByParentID(context, parent_id);

        for (int i = 0; i < doc_list.size(); i++) {
            Document cached_document = doc_list.get(i);

            if (cached_document.getPreviewAbsPath() == null) // there are no get preview request sent before
            {
                String doc_id = cached_document.getId();

                ConnectionResult result =
                        OperationHelpers.documentGetPreview(context, WSConfig
                                .documentCreatePreviewUrl(doc_id), OperationHelpers
                                .documentCreatePreviewPathWithExtension(context, doc_id));

                bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);

                if (result.responseCode != ResponseConstants.RESPONSE_CODE_OK) {
                    bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
                    return bundle;
                }

                //TODO
                //bundle.putByte(, value);

                ContentProviderHelpers.documentUpdatePreviewPath(context, cached_document,
                        result.body);
            }

        }

        return bundle;

    }

}
