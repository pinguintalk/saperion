/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.saperion.sdb.sdk.security;

import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.security.KeyPairGeneratorSpec;
import android.util.Base64;

import com.saperion.sdb.sdk.exceptions.AuthenticationException;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableEntryException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.security.auth.x500.X500Principal;


public class KeyStoreHelper {

    public static final String TAG = "KeyStoreHelper";
    private static final String CIPHER_ALGORITHM = "AES"; //"AES/CBC/PKCS5Padding";
    private static int KEY_LENGTH = 256;

    // constants for old keystore API
    public static final String OLD_UNLOCK_ACTION = "android.credentials.UNLOCK";
    public static final String UNLOCK_ACTION = "com.android.credentials.UNLOCK";
    public static final String RESET_ACTION = "com.android.credentials.RESET";


    // You can store multiple key pairs in the Key Store.  The string used to refer to the Key you
    // want to store, or later pull, is referred to as an "alias" in this case, because calling it
    // a key, when you use it to retrieve a key, would just be irritating.
    private String mAlias = null;
    private Context mContext = null;

    public KeyStoreHelper(Context context, String alias) {

        this.mAlias = alias;
        this.mContext = context;
        // create keys if not exits
        try {
            createKey(mContext);
        } catch (NoSuchAlgorithmException e) {
            Log.w(TAG, "RSA not supported");
        } catch (InvalidAlgorithmParameterException e) {
            Log.w(TAG, "No such provider: AndroidKeyStore");
        } catch (NoSuchProviderException e) {
            Log.w(TAG, "Invalid Algorithm Parameter Exception");
        }
    }

    private void createKey(Context context) throws NoSuchProviderException,
            NoSuchAlgorithmException, InvalidAlgorithmParameterException {


        if (isNewKeyStoreAPIAvailable()) {
            Log.v(TAG, Utils.getMethodName() + "trying create keys with NEW API if not exists");
            createKeysWithNewAPI(context);
        } else {
            Log.v(TAG, Utils.getMethodName() + "trying create keys with OLD API if not exists");

            KeyStoreDeprecated ksd = KeyStoreDeprecated.getInstance();
            byte[] keyBytes = ksd.get(mAlias);
            if (keyBytes == null) {
                Log.w(TAG, Utils.getMethodName() + "key not found in keystore, trying to create new once ");

                try {
                    KeyGenerator kg = KeyGenerator.getInstance(CIPHER_ALGORITHM);
                    kg.init(KEY_LENGTH);
                    SecretKey key = kg.generateKey();
                    if (!ksd.put(mAlias, key.getEncoded())) {
                        Log.e(TAG, Utils.getMethodName() + "can not create key ! ");
                    }

                } catch (NoSuchAlgorithmException e) {
                    throw new RuntimeException(e);
                }

            }
        }
    }


    /**
     * Creates a public and private key and stores it using the Android Key Store, so that only
     * this application will be able to access the keys.
     */

    @TargetApi(18)
    private void createKeysWithNewAPI(Context context) throws NoSuchProviderException,
            NoSuchAlgorithmException, InvalidAlgorithmParameterException {

        if (!isPrivateKeyCreated(mAlias)) {
            // Create a start and end time, for the validity range of the key pair that's about to be
            // generated.
            Calendar start = new GregorianCalendar();
            Calendar end = new GregorianCalendar();
            end.add(1, Calendar.YEAR);


            // The KeyPairGeneratorSpec object is how parameters for your key pair are passed
            // to the KeyPairGenerator.  For a fun home game, count how many classes in this sample
            // start with the phrase "KeyPair".
            KeyPairGeneratorSpec spec =
                    new KeyPairGeneratorSpec.Builder(context)
                            // You'll use the alias later to retrieve the key.  It's a key for the key!
                            .setAlias(mAlias)
                                    // The subject used for the self-signed certificate of the generated pair
                            .setSubject(new X500Principal("CN=" + mAlias))
                                    // The serial number used for the self-signed certificate of the
                                    // generated pair.
                            .setSerialNumber(BigInteger.valueOf(1337))
                                    // Date range of validity for the generated pair.
                            .setStartDate(start.getTime())
                            .setEndDate(end.getTime())
                            .build();

            // Initialize a KeyPair generator using the the intended algorithm (in this example, RSA
            // and the KeyStore.  This example uses the AndroidKeyStore.
            KeyPairGenerator kpGenerator = KeyPairGenerator
                    .getInstance(SecurityConstants.TYPE_RSA,
                            SecurityConstants.KEYSTORE_PROVIDER_ANDROID_KEYSTORE);
            kpGenerator.initialize(spec);
            KeyPair kp = kpGenerator.generateKeyPair();
            Log.d(TAG, Utils.getMethodName() + "Public Key is: " + kp.getPublic().toString());
        }
    }

    // for new keystore API only
    private boolean isPrivateKeyCreated(String alias) {
        KeyStore ks = null;
        try {
            ks = KeyStore.getInstance(SecurityConstants.KEYSTORE_PROVIDER_ANDROID_KEYSTORE);


            // Weird artifact of Java API.  If you don't have an InputStream to load, you still need
            // to call "load", or it'll crash.
            ks.load(null);

            // Load the key pair createInstance the Android Key Store
            KeyStore.Entry entry = ks.getEntry(alias, null);
            
                    /* If the entry is null, keys were never stored under this alias.
         * Debug steps in this situation would be:
         * -Check the list of aliases by iterating over Keystore.aliases(), be sure the alias
         *   exists.
         * -If that's empty, verify they were both stored and pulled createInstance the same keystore
         *   "AndroidKeyStore"
         */
            if (entry == null) {
                Log.w(TAG, Utils.getMethodName() + "No key found under alias: " + alias);
                Log.w(TAG, Utils.getMethodName() + "Exiting signData()...");
                return false;
            }

        /* If entry is not a KeyStore.PrivateKeyEntry, it might have gotten stored in a previous
         * iteration of your application that was using some other mechanism, or been overwritten
         * by something else using the same keystore with the same alias.
         * You can determine the type using entry.getClass() and debug createInstance there.
         */
            if (!(entry instanceof KeyStore.PrivateKeyEntry)) {
                Log.w(TAG, Utils.getMethodName() + "Not an instance of a PrivateKeyEntry");
                Log.w(TAG, Utils.getMethodName() + "Exiting signData()...");
                return false;
            }

        } catch (KeyStoreException e) {
            Log.e(TAG, Utils.getMethodName() + e.getMessage());
        } catch (CertificateException e) {
            Log.e(TAG, Utils.getMethodName() + e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, Utils.getMethodName() + e.getMessage());
        } catch (UnrecoverableEntryException e) {
            Log.e(TAG, Utils.getMethodName() + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, Utils.getMethodName() + e.getMessage());
        }

        return true;
    }


    private boolean isNewKeyStoreAPIAvailable() {
        try {
            if (Build.VERSION.SDK_INT < VERSION_CODES.JELLY_BEAN_MR2) {
                // firmware version < 4.3 
                Log.w(TAG, Utils.getMethodName() + "device version " + Build.VERSION.SDK_INT);
                return false;
            } else 
                return true;
        } catch (ActivityNotFoundException e) {
            Log.e(TAG, Utils.getMethodName() + "No UNLOCK activity: " + e.getMessage());
        }

        return false;
    }

    /**
     * Signs the data using the key pair stored in the Android Key Store.  This signature can be
     * used with the data later to verify it was signed by this application.
     *
     * @return A string encoding of the data signature generated
     */
    public String signData(String inputStr) throws KeyStoreException,
            UnrecoverableEntryException, NoSuchAlgorithmException, InvalidKeyException,
            SignatureException, IOException, CertificateException {
        byte[] data = inputStr.getBytes();


        KeyStore ks = KeyStore.getInstance(SecurityConstants.KEYSTORE_PROVIDER_ANDROID_KEYSTORE);

        // Weird artifact of Java API.  If you don't have an InputStream to load, you still need
        // to call "load", or it'll crash.
        ks.load(null);

        // Load the key pair createInstance the Android Key Store
        KeyStore.Entry entry = ks.getEntry(mAlias, null);

        /* If the entry is null, keys were never stored under this alias.
         * Debug steps in this situation would be:
         * -Check the list of aliases by iterating over Keystore.aliases(), be sure the alias
         *   exists.
         * -If that's empty, verify they were both stored and pulled createInstance the same keystore
         *   "AndroidKeyStore"
         */
        if (entry == null) {
            Log.w(TAG, Utils.getMethodName() + "No key found under alias: " + mAlias);
            Log.w(TAG, Utils.getMethodName() + "Exiting signData()...");
            return null;
        }

        /* If entry is not a KeyStore.PrivateKeyEntry, it might have gotten stored in a previous
         * iteration of your application that was using some other mechanism, or been overwritten
         * by something else using the same keystore with the same alias.
         * You can determine the type using entry.getClass() and debug createInstance there.
         */
        if (!(entry instanceof KeyStore.PrivateKeyEntry)) {
            Log.w(TAG, Utils.getMethodName() + "Not an instance of a PrivateKeyEntry");
            Log.w(TAG, Utils.getMethodName() + "Exiting signData()...");
            return null;
        }


        // This class doesn't actually represent the signature,
        // just the engine for creating/verifying signatures, using
        // the specified algorithm.
        Signature s = Signature.getInstance(SecurityConstants.SIGNATURE_SHA256withRSA);

        // Initialize Signature using specified private key
        s.initSign(((KeyStore.PrivateKeyEntry) entry).getPrivateKey());

        // Sign the data, store the result as a Base64 encoded String.
        s.update(data);
        byte[] signature = s.sign();
        String result = null;
        result = Base64.encodeToString(signature, Base64.DEFAULT);


        return result;
    }

    /**
     * Given some data and a signature, uses the key pair stored in the Android Key Store to verify
     * that the data was signed by this application, using that key pair.
     *
     * @param input        The data to be verified.
     * @param signatureStr The signature provided for the data.
     * @return A boolean value telling you whether the signature is valid or not.
     */
    public boolean verifyData(String input, String signatureStr) throws KeyStoreException,
            CertificateException, NoSuchAlgorithmException, IOException,
            UnrecoverableEntryException, InvalidKeyException, SignatureException {
        byte[] data = input.getBytes();
        byte[] signature;


        // Make sure the signature string exists.  If not, bail out, nothing to do.

        if (signatureStr == null) {
            Log.w(TAG, Utils.getMethodName() + "Invalid signature.");
            Log.w(TAG, Utils.getMethodName() + "Exiting verifyData()...");
            return false;
        }

        try {
            // The signature is going to be examined as a byte array,
            // not as a base64 encoded string.
            signature = Base64.decode(signatureStr, Base64.DEFAULT);
        } catch (IllegalArgumentException e) {
            // signatureStr wasn't null, but might not have been encoded properly.
            // It's not a valid Base64 string.
            return false;
        }


        KeyStore ks = KeyStore.getInstance("AndroidKeyStore");

        // Weird artifact of Java API.  If you don't have an InputStream to load, you still need
        // to call "load", or it'll crash.
        ks.load(null);

        // Load the key pair createInstance the Android Key Store
        KeyStore.Entry entry = ks.getEntry(mAlias, null);

        if (entry == null) {
            Log.w(TAG, Utils.getMethodName() + "No key found under alias: " + mAlias);
            Log.w(TAG, Utils.getMethodName() + "Exiting verifyData()...");
            return false;
        }

        if (!(entry instanceof KeyStore.PrivateKeyEntry)) {
            Log.w(TAG, Utils.getMethodName() + "Not an instance of a PrivateKeyEntry");
            return false;
        }

        // This class doesn't actually represent the signature,
        // just the engine for creating/verifying signatures, using
        // the specified algorithm.
        Signature s = Signature.getInstance(SecurityConstants.SIGNATURE_SHA256withRSA);


        // Verify the data.
        s.initVerify(((KeyStore.PrivateKeyEntry) entry).getCertificate());
        s.update(data);
        boolean valid = s.verify(signature);
        return valid;

    }

    private String encryptWithOldKeyStoreAPI(String plainText) {

        KeyStoreDeprecated ksd = KeyStoreDeprecated.getInstance();
        byte[] keyBytes = ksd.get(mAlias);
        if (keyBytes == null) {
            Log.w(TAG, Utils.getMethodName() + "key not found in keystore: ");

            return null;
        }

        String ciphertext = null;
        ciphertext = encryptText(keyBytes, plainText);

        // Log.w(TAG, Utils.getMethodName() + "ciphertext: " + ciphertext);
        return ciphertext;
    }

    private String encryptWithNewKeyStoreAPI(String plainText) {

        KeyStore ks = null;
        try {
            ks = KeyStore.getInstance(SecurityConstants.KEYSTORE_PROVIDER_ANDROID_KEYSTORE);
            // Weird artifact of Java API.  If you don't have an InputStream to load, you still need
            // to call "load", or it'll crash.
            ks.load(null);

            // Load the key pair createInstance the Android Key Store
            KeyStore.Entry entry = ks.getEntry(mAlias, null);


            if (entry == null) {
                Log.w(TAG, Utils.getMethodName() + "No key found under alias: " + mAlias);
                Log.w(TAG, Utils.getMethodName() + "Exiting signData()...");
                return null;
            }

            if (!(entry instanceof KeyStore.PrivateKeyEntry)) {
                Log.w(TAG, Utils.getMethodName() + "Not an instance of a PrivateKeyEntry");
                Log.w(TAG, Utils.getMethodName() + "Exiting signData()...");
                return null;
            }


            byte[] key = getEncodedPrivateKey(((KeyStore.PrivateKeyEntry) entry).getPrivateKey());

            return encryptText(key, plainText);

        } catch (KeyStoreException e) {
            Log.e(TAG, Utils.getMethodName() + e.getMessage());
        } catch (CertificateException e) {
            Log.e(TAG, Utils.getMethodName() + e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, Utils.getMethodName() + e.getMessage());
        } catch (UnrecoverableEntryException e) {
            Log.e(TAG, Utils.getMethodName() + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, Utils.getMethodName() + e.getMessage());
        }


        return null;
    }

    private String decryptWithOldKeyStoreAPI(String encryptedText) throws AuthenticationException {

        KeyStoreDeprecated ksd = KeyStoreDeprecated.getInstance();
        byte[] keyBytes = ksd.get(mAlias);
        if (keyBytes == null) {
            Log.w(TAG, Utils.getMethodName() + "key not found in keystore: ");

            return null;
        }
        String plaintext = decryptText(keyBytes, encryptedText);

        // Log.w(TAG, Utils.getMethodName() + "plaintext: " +plaintext); 
        return plaintext;
    }

    public static String encryptText(byte[] key, String plainText) {
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, CIPHER_ALGORITHM);

        Cipher c = null;
        try {
            c = Cipher.getInstance(CIPHER_ALGORITHM);

            c.init(Cipher.ENCRYPT_MODE, secretKeySpec);
            byte[] encryptedText = c.doFinal(plainText.getBytes("UTF-8"));
            return Base64.encodeToString(encryptedText, Base64.DEFAULT);
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, Utils.getMethodName() + e.getMessage());
        } catch (NoSuchPaddingException e) {
            Log.e(TAG, Utils.getMethodName() + e.getMessage());
        } catch (InvalidKeyException e) {
            Log.e(TAG, Utils.getMethodName() + e.getMessage());
        } catch (BadPaddingException e) {
            Log.e(TAG, Utils.getMethodName() + e.getMessage());
        } catch (IllegalBlockSizeException e) {
            Log.e(TAG, Utils.getMethodName() + e.getMessage());
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, Utils.getMethodName() + e.getMessage());
        }
        return null;
    }

    public static String decryptText(byte[] key, String encryptedText) throws AuthenticationException {
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, CIPHER_ALGORITHM);

        Cipher c = null;
        try {
            c = Cipher.getInstance(CIPHER_ALGORITHM);
            c.init(Cipher.DECRYPT_MODE, secretKeySpec);
            byte[] clearText = c.doFinal(Base64.decode(encryptedText, Base64.DEFAULT));
            return new String(clearText, "UTF-8");
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, Utils.getMethodName() + e.getMessage());
        } catch (NoSuchPaddingException e) {
            Log.e(TAG, Utils.getMethodName() + e.getMessage());
        } catch (InvalidKeyException e) {
            Log.e(TAG, Utils.getMethodName() + e.getMessage());
        } catch (BadPaddingException e) {
            Log.e(TAG, Utils.getMethodName() + e.getMessage());
        } catch (IllegalBlockSizeException e) {
            Log.e(TAG, Utils.getMethodName() + e.getMessage());
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, Utils.getMethodName() + e.getMessage());
        }

        return null;
    }

    private byte[] getEncodedPrivateKey(PrivateKey pprivateKey) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        String pk = pprivateKey.toString();

        String modulus = pk.substring(pk.lastIndexOf("=") + 1);
        modulus = modulus.replaceFirst(",", "");

        // Log.d(TAG, Utils.getMethodName() + Utils.getMethodName() +  "Private Key is: " + modulus);

        byte[] key = (modulus).getBytes("UTF-8");
        // create Hash value with  MD5 or SHA
        MessageDigest sha = MessageDigest.getInstance("MD5");
        key = sha.digest(key);
        // use first 256 bit
        key = Arrays.copyOf(key, KEY_LENGTH / 8);
        return key;
    }


    private String decryptWithNewKeyStoreAPI(String encryptedText) throws AuthenticationException {
        KeyStore ks = null;
        try {
            ks = KeyStore.getInstance(SecurityConstants.KEYSTORE_PROVIDER_ANDROID_KEYSTORE);
            // Weird artifact of Java API.  If you don't have an InputStream to load, you still need
            // to call "load", or it'll crash.
            ks.load(null);

            // Load the key pair createInstance the Android Key Store
            KeyStore.Entry entry = ks.getEntry(mAlias, null);


            if (entry == null) {
                Log.w(TAG, Utils.getMethodName() + "No key found under alias: " + mAlias);
                Log.w(TAG, Utils.getMethodName() + "Exiting signData()...");
                return null;
            }

            if (!(entry instanceof KeyStore.PrivateKeyEntry)) {
                Log.w(TAG, Utils.getMethodName() + "Not an instance of a PrivateKeyEntry");
                Log.w(TAG, Utils.getMethodName() + "Exiting signData()...");
                return null;
            }

            byte[] key = getEncodedPrivateKey(((KeyStore.PrivateKeyEntry) entry).getPrivateKey());

            String pass = decryptText(key, encryptedText);

            // Log.d(TAG, Utils.getMethodName() + "our pass " + pass);
            return pass;


        } catch (KeyStoreException e) {
            throw new AuthenticationException(e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            throw new AuthenticationException(e.getMessage());
        } catch (UnrecoverableKeyException e) {
            throw new AuthenticationException(e.getMessage());
        } catch (CertificateException e) {
            throw new AuthenticationException(e.getMessage());
        } catch (UnrecoverableEntryException e) {
            throw new AuthenticationException(e.getMessage());
        } catch (IOException e) {
            throw new AuthenticationException(e.getMessage());
        }
    }

    public String decrypt(final String encryptedText) throws AuthenticationException {

        if (isNewKeyStoreAPIAvailable())
            return decryptWithNewKeyStoreAPI(encryptedText);
        else
            return decryptWithOldKeyStoreAPI(encryptedText);
    }


    public String encrypt(final String plainText) {

        try {
            createKey(this.mContext);
        } catch (NoSuchProviderException e) {
            Log.e(TAG, Utils.getMethodName() + e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, Utils.getMethodName() + e.getMessage());
        } catch (InvalidAlgorithmParameterException e) {
            Log.e(TAG, Utils.getMethodName() + e.getMessage());
        }

        if (isNewKeyStoreAPIAvailable())
            return encryptWithNewKeyStoreAPI(plainText);
        else
            return encryptWithOldKeyStoreAPI(plainText);


    }

}
