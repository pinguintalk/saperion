/**
 * 2012 Foxykeep (http://datadroid.foxykeep.com)
 * <p>
 * Licensed under the Beerware License : <br />
 * As long as you retain this notice you can do whatever you want with this stuff. If we meet some
 * day, and you think this stuff is worth it, you can buy me a beer in return
 */

package com.saperion.sdb.sdk.network;

import android.content.Context;

import com.saperion.sdb.sdk.Constants;
import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.utils.Log;

import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class gives the user an API to easily call a webservice and return the received response.
 *
 * @author Foxykeep
 */
public final class NetworkConnection {

    private static final String LOG_TAG = NetworkConnection.class.getSimpleName();

    public static final String REQUEST_TYPE = "com.saperion.sdb.sdk.request_type";
    public static final String FILE_NAME = "com.saperion.sdb.sdk.file_name";
    public static final String FILE_PATH = "com.saperion.sdb.sdk.file_path";
    public static final String FILE_URI = "com.saperion.sdb.sdk.file_uri";
    
    public static final String PARAM_DOC_PARENT_ID = Constants.SDK_PACKAGE_NAME +
            ".document.param_doc_parent_id";
    
    public static final String REEQUEST_TYPE_DOWNLOAD =
            "com.saperion.sdb.sdk.type_download";

    public static enum Method {
        GET,
        POST,
        PUT,
        DELETE
    }

    /**
     * The result of a webservice call.
     * <p/>
     * Contains the headers and the body of the response as an unparsed <code>String</code>.
     *
     * @author Foxykeep
     */
    public static final class ConnectionResult {

        public final Map<String, List<String>> headerMap;
        public final String body;
        public byte[] body_stream = null;
        public final int responseCode;

        public ConnectionResult(Map<String, List<String>> headerMap, String body, int responseCode) {
            this.headerMap = headerMap;
            this.body = body;
            this.responseCode = responseCode;
        }
        /* TODO
		public ConnectionResult(Map<String, List<String>> headerMap, byte[] body_stream, int responseCode) {
		    this.headerMap = headerMap;
		    this.body_stream = body_stream;
		    this.responseCode = responseCode;
		}*/
    }

    private final Context mContext;
    private final String mUrl;
    private Method mMethod = Method.GET;
    private ArrayList<BasicNameValuePair> mParameterList = null;
    private HashMap<String, String> mHeaderMap = null;
    private HashMap<String, String> mRequestMap = null;
    private boolean mIsGzipEnabled = true;
    private String mUserAgent = null;
    private String mPayloadText = null;
    private UsernamePasswordCredentials mCredentials = null;
    private boolean mIsFileUpload = false;
    private boolean mIsSslValidationEnabled = true;
    private String ifNoneMatch;
    private String mAuthenticationType = "sect"; // prefix in Auth header

    public String getIfNoneMatch() {
        return ifNoneMatch;
    }

    public void setIfNoneMatch(String ifNoneMatch) {
        this.ifNoneMatch = ifNoneMatch;
    }

    /**
     * Create a {@link NetworkConnection}.
     * <p/>
     * The Method to use is {@link Method#GET} by default.
     *
     * @param context The context used by the {@link NetworkConnection}. Used to create the
     *                User-Agent.
     * @param url     The URL to call.
     */
    public NetworkConnection(Context context, String url) {
        if (url == null) {
            Log.e(LOG_TAG, "NetworkConnection.NetworkConnection - request URL cannot be null.");
            throw new NullPointerException("Request URL has not been set.");
        }

        //Log.v(LOG_TAG, Utils.getMethodName() + "url: " + url);

        mContext = context;
        mUrl = url;
    }

    /**
     * Set the method to use. Default is {@link Method#GET}.
     * <p/>
     * If set to another value than {@link Method#POST}, the POSTDATA text will be reset as it can
     * only be used with a POST request.
     *
     * @param method The method to use.
     * @return The networkConnection.
     */
    public NetworkConnection setMethod(Method method) {
        mMethod = method;
        if (mMethod != Method.POST){
            if (mMethod != Method.PUT)
                mPayloadText = null; 
        }
        return this;
    }

    /**
     * Set the parameters to add to the request. This is meant to be a "key" => "value" Map.
     * <p/>
     * The POSTDATA text will be reset as they cannot be used at the same time.
     *
     * @param parameterMap The parameters to add to the request.
     * @return The networkConnection.
     * @see #setPayload(String)
     * @see #setParameters(ArrayList)
     */
    public NetworkConnection setParameters(HashMap<String, String> parameterMap) {
        ArrayList<BasicNameValuePair> parameterList = new ArrayList<BasicNameValuePair>();
        for (Map.Entry<String, String> entry : parameterMap.entrySet()) {
            parameterList.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
        }

        return setParameters(parameterList);
    }

    /**
     * Set the parameters to add to the request. This is meant to be a "key" => "value" Map.
     * <p/>
     * The POSTDATA text will be reset as they cannot be used at the same time.
     * <p/>
     * This method allows you to have multiple values for a single key in contrary to the HashMap
     * version of the method ({@link #setParameters(HashMap)})
     *
     * @param parameterList The parameters to add to the request.
     * @return The networkConnection.
     * @see #setPayload(String)
     * @see #setParameters(HashMap)
     */
    public NetworkConnection setParameters(ArrayList<BasicNameValuePair> parameterList) {
        mParameterList = parameterList;
        mPayloadText = null;
        return this;
    }

    /**
     * Set the headers to add to the request.
     *
     * @param headerMap The headers to add to the request.
     * @return The networkConnection.
     */
    public NetworkConnection setHeaderList(HashMap<String, String> headerMap) {
        mHeaderMap = headerMap;
        return this;
    }

    /**
     * params createInstance request like id and file type for saving file
     *
     * @param requestMap
     * @return
     */
    public NetworkConnection setRequestParams(HashMap<String, String> requestMap) {
        this.mRequestMap = requestMap;
        return this;
    }

    /**
     * Set whether the request will use gzip compression if available on the server. Default is
     * true.
     *
     * @param isGzipEnabled Whether the request will user gzip compression if available on the
     *                      server.
     * @return The networkConnection.
     */
    public NetworkConnection setGzipEnabled(boolean isGzipEnabled) {
        mIsGzipEnabled = isGzipEnabled;
        return this;
    }

    /**
     * Set the user agent to set in the request. Otherwise a default Android one will be used.
     *
     * @param userAgent The user agent.
     * @return The networkConnection.
     */
    public NetworkConnection setUserAgent(String userAgent) {
        mUserAgent = userAgent;
        return this;
    }

    /**
     * Set the POSTDATA text that will be added in the request. Also automatically set the
     * {@link Method} to {@link Method#POST} to be able to use it.
     * <p/>
     * The parameters will be reset as they cannot be used at the same time.
     *
     * @param postText The POSTDATA text that will be added in the request.
     * @return The networkConnection.
     * @see #setParameters(HashMap)
     */
    public NetworkConnection setPayload(String postText) {
        this.mPayloadText = postText;
        mParameterList = null;
        return this;
    }

    /**
     * Set the credentials to use for authentication.
     *
     * @param credentials The credentials to use for authentication.
     * @return The networkConnection.
     */
    public NetworkConnection setCredentials(UsernamePasswordCredentials credentials) {
        mCredentials = credentials;
        return this;
    }

    /**
     * Set whether the SSL certificates validation are enabled. Default is true.
     *
     * @param enabled Whether the SSL certificates validation are enabled.
     * @return The networkConnection.
     */
    public NetworkConnection setSslValidationEnabled(boolean enabled) {
        mIsSslValidationEnabled = enabled;
        return this;
    }

    public NetworkConnection setFileUpload(boolean enabled) {
        mIsFileUpload = enabled;
        return this;
    }

    public void setAuthenticationType(String authType) {
        this.mAuthenticationType = authType;
    }

    /**
     * Execute the webservice call and return the {@link ConnectionResult}.
     *
     * @return The result of the webservice call.
     */
    public ConnectionResult execute() throws ConnectionException, ServiceRequestException {
        return NetworkConnectionImpl.execute(mContext, mUrl, mMethod, mParameterList, mHeaderMap,
                mRequestMap, mIsGzipEnabled, mUserAgent, mPayloadText, mCredentials, ifNoneMatch,
                mAuthenticationType, mIsSslValidationEnabled, mIsFileUpload);
    }

}
