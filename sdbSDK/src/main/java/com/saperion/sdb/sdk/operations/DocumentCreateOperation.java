package com.saperion.sdb.sdk.operations;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;

import com.saperion.sdb.sdk.Constants;
import com.saperion.sdb.sdk.config.WSConfig;
import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.factory.DocumentJsonFactory;
import com.saperion.sdb.sdk.models.Document;
import com.saperion.sdb.sdk.network.NetworkConnection;
import com.saperion.sdb.sdk.providers.ContentProviderHelpers;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.security.CredentialManager;
import com.saperion.sdb.sdk.service.RequestService.Operation;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

import org.apache.http.HttpStatus;
import org.apache.http.auth.UsernamePasswordCredentials;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

public class DocumentCreateOperation implements Operation {

    private static final String TAG = DocumentCreateOperation.class.getSimpleName();

    public static final String PARAM_PAYLOAD = Constants.SDK_PACKAGE_NAME + ".document.param_doc_payload";
    public static final String PARAM_DOCUMENT = Constants.SDK_PACKAGE_NAME + ".document.param_document";
    public static final String PARAM_DOCUMENT_PATH = Constants.SDK_PACKAGE_NAME + ".document.param_document_path";
    
    public static final int UPLOAD_METHOD_WITH_POST = 1;
    public static final int UPLOAD_METHOD_WITH_PUT = 2;


    @Override
    public Bundle execute(Context context, Request request) throws ConnectionException,
            DataException, ServiceRequestException {

        Bundle bundle = new Bundle();
        int uploadMethod = UPLOAD_METHOD_WITH_POST;
        NetworkConnection.ConnectionResult result = null;
        NetworkConnection networkConnection;
        String url = CredentialManager.getInstance().getServerUrl() + WSConfig.WS_DOCUMENT;
        Document document = (Document) request.getParcelable(PARAM_DOCUMENT);
        String documentId = document.getId();

        networkConnection = new NetworkConnection(context, url);

        String file_path = request.getString(PARAM_DOCUMENT_PATH);
       
        
        String md5checksum = calculateMD5(new File(file_path));
        //Uri fileURI = Uri.parse(file_path);
        //String md5checksum = calculateMD5(context,fileURI);

        HashMap<String, String> file_params = new HashMap<String, String>();
        file_params.put(NetworkConnection.FILE_PATH, file_path);
        file_params.put(NetworkConnection.FILE_NAME, document.getName());
        file_params.put(NetworkConnection.PARAM_DOC_PARENT_ID, document.getParentId());

        HashMap<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Connection", "Keep-Alive");

        // create new document
        if (documentId == null || documentId.length() < 1) {
            Log.v(TAG, Utils.getMethodName() + "uploading file with post method ");

            uploadMethod = UPLOAD_METHOD_WITH_POST;

            String boundary = Constants.BOUNDARY;

            headerMap.put("Content-Type", "multipart/form-data;boundary=" + boundary);
            headerMap.put("Content-MD5", "\"" + md5checksum + "\"");
            headerMap.put("Accept", "*/*");
            networkConnection.setMethod(NetworkConnection.Method.POST);
            networkConnection.setFileUpload(true);
            networkConnection.setRequestParams(file_params);
            networkConnection.setHeaderList(headerMap);
            networkConnection
                    .setCredentials(new UsernamePasswordCredentials(CredentialManager.getInstance()
                            .getUserName(), CredentialManager.getInstance().getmAccessToken()));
            try {
                result = networkConnection.execute();

            } catch (ConnectionException e) {
                e.printStackTrace();
            }

        } else { // replace document
            Log.v(TAG, Utils.getMethodName() + "uploading with put method ");
            url = url + "/" + document.getId() + "/file";
            uploadMethod = UPLOAD_METHOD_WITH_PUT;


            headerMap.put("Content-Type", "application/octet-stream");
            networkConnection = new NetworkConnection(context, url);

            networkConnection.setMethod(NetworkConnection.Method.PUT);
            networkConnection.setFileUpload(true);
            networkConnection.setRequestParams(file_params);
            networkConnection.setHeaderList(headerMap);

            networkConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager
                    .getInstance().getUserName(), CredentialManager.getInstance()
                    .getmAccessToken()));
            try {
                result = networkConnection.execute();

            } catch (ServiceRequestException e) {
                Log.e(TAG, Utils.getMethodName() + "uploading file without content 2. ");
            }


        }

        if (result == null)
            return bundle;

        int respcode = result.responseCode;

        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, respcode);

        if (respcode == HttpStatus.SC_OK
                || respcode == HttpStatus.SC_CREATED) {

            Document tmpDocument = DocumentJsonFactory.jsonToDocument(result.body);
            String docId = tmpDocument.getId();
            String parentIdChain = ContentProviderHelpers.getIdChainByItemId(context,tmpDocument.getParentId());
            tmpDocument.setIdChain( parentIdChain + ";" + docId);


            String preview_abs_path = OperationHelpers.documentCreatePreviewPathWithExtension(context, docId);

            /*
            // After file is successfully uploaded preview will downloaded
            result =
                    OperationHelpers.documentGetPreview(context,
                            WSConfig.documentCreatePreviewUrl(docId), preview_abs_path);

            bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);

            if (result.responseCode != ResponseConstants.RESPONSE_CODE_OK) {
                Log.w(TAG, Utils.getMethodName() + " responseCode:" + result.responseCode);

                bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
                return bundle;
            }
            // if get preview return ok, we save this path to db
            tmpDocument.setPreviewAbsPath(preview_abs_path);

            */
            
            String absFilePath = Utils.getDocumentsDirectory(context) + docId
                    + tmpDocument.getVersionNumber() + Utils.getFileExtension(tmpDocument.getMimeType());
            try {
                Utils.fileCopy(new File(file_path), new File(absFilePath));

                tmpDocument.setFileAbsPath(absFilePath);
            } catch (IOException e) {
                throw new DataException("error copying file after created document !");
            }

            if (uploadMethod == UPLOAD_METHOD_WITH_POST) {
                ContentProviderHelpers.documentInsert(context, tmpDocument);
                
                Document insertedDoc = ContentProviderHelpers.documentGetById(context,tmpDocument.getId());

                Log.w(TAG, Utils.getMethodName() + " versionNumber:" + insertedDoc.getVersionNumber());
            } else {
                ContentProviderHelpers.documentReplace(context, tmpDocument);
            }

            bundle.putParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, tmpDocument);
        } else {
            bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
        }

        return bundle;
    }

    public static String calculateMD5(File updateFile) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "Exception while getting Digest");
            return null;
        }

        InputStream is;
        try {
            is = new FileInputStream(updateFile);
        } catch (FileNotFoundException e) {
            Log.e(TAG, "Exception while getting FileInputStream");
            return null;
        }

        byte[] buffer = new byte[8192];
        int read;
        try {
            while ((read = is.read(buffer)) > 0) {
                digest.update(buffer, 0, read);
            }
            byte[] md5sum = digest.digest();
            BigInteger bigInt = new BigInteger(1, md5sum);
            String output = bigInt.toString(16);
            // Fill to 32 chars
            output = String.format("%32s", output).replace(' ', '0');
            return output;
        } catch (IOException e) {
            throw new RuntimeException("Unable to process file for MD5", e);
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                Log.e(TAG, "Exception on closing MD5 input stream");
            }
        }
    }


    public static String calculateMD5(Context context, Uri uri) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "Exception while getting Digest");
            return null;
        }

        ParcelFileDescriptor pfd = null;
        try {
            pfd = context.getContentResolver().
                    openFileDescriptor(uri, "r");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (pfd == null ) return null;
        
        FileInputStream is =
                new FileInputStream(pfd.getFileDescriptor());

        
        byte[] buffer = new byte[8192];
        int read;
        try {
            while ((read = is.read(buffer)) > 0) {
                digest.update(buffer, 0, read);
            }
            byte[] md5sum = digest.digest();
            BigInteger bigInt = new BigInteger(1, md5sum);
            String output = bigInt.toString(16);
            // Fill to 32 chars
            output = String.format("%32s", output).replace(' ', '0');
            return output;
        } catch (IOException e) {
            throw new RuntimeException("Unable to process file for MD5", e);
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                Log.e(TAG, "Exception on closing MD5 input stream");
            }
        }
    }
    public static boolean checkMD5(String md5, File updateFile) {
        if (md5 == null || md5.equals("") || updateFile == null) {
            Log.e(TAG, "MD5 String NULL or UpdateFile NULL");
            return false;
        }

        String calculatedDigest = calculateMD5(updateFile);
        if (calculatedDigest == null) {
            Log.e(TAG, "calculatedDigest NULL");
            return false;
        }

        Log.v(TAG, "Calculated digest: " + calculatedDigest);
        Log.v(TAG, "Provided digest: " + md5);

        return calculatedDigest.equalsIgnoreCase(md5);
    }

    public static String fileToMD5(String filePath) {
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(filePath);
            byte[] buffer = new byte[1024];
            MessageDigest digest = MessageDigest.getInstance("MD5");
            int numRead = 0;
            while (numRead != -1) {
                numRead = inputStream.read(buffer);
                if (numRead > 0)
                    digest.update(buffer, 0, numRead);
            }
            byte[] md5Bytes = digest.digest();
            return convertHashToString(md5Bytes);
        } catch (Exception e) {
            return null;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e) {
                }
            }
        }
    }

    private static String convertHashToString(byte[] md5Bytes) {
        String returnVal = "";
        for (byte md5Byte : md5Bytes) {
            returnVal += Integer.toString((md5Byte & 0xff) + 0x100, 16).substring(1);
        }
        return returnVal.toUpperCase();
    }

}

/*
               //if (doc.getVersionNumber() == 0)
               {

                   URI uri = null;
                   try {
                       uri = new URI(url);
                   } catch (URISyntaxException e) {
                       Log.e(TAG, Utils.getMethodName() + e.toString());
                   }

                   HttpClient httpclient = new DefaultHttpClient();

               try
               {
                   File file = new File(request.getString(PARAM_DOC_FILE_PATH));
                   HttpPut putRequest = new HttpPut(uri);
                   putRequest.addHeader("Content-Type", "application/octet-stream");
                   putRequest.addHeader("Authorization",
                           NetworkConnectionImpl.createAuthenticationHeader(new UsernamePasswordCredentials(CredentialManager.getInstance().getUserName(),
                                   CredentialManager.getInstance().getmAccessToken()), "sect"));

                   String contentLength = Long.toString(file.length());

                   InputStreamEntity reqEntity = null;


                   try {
                       FileInputStream inputStream = new FileInputStream(file);
                       int bytesAvailable = inputStream.available();

                       Log.v(TAG, Utils.getMethodName() + "Content-Length: " + bytesAvailable + "  " + contentLength);

                       reqEntity = new InputStreamEntity(inputStream
                               , bytesAvailable);
                       reqEntity.setContentType("application/octet-stream");
                       reqEntity.getContentLength();
                       reqEntity.setChunked(true);

                       Log.v(TAG, Utils.getMethodName() + "Content-Length: " + bytesAvailable + "  " + contentLength + " " + reqEntity.getContentLength());

                       putRequest.setEntity(reqEntity);
                       putRequest.removeHeaders("Content-Length");
                       putRequest.setHeader("Content-Length", contentLength);

                       //MultipartEntity entity = new MultipartEntity();
                       //ContentBody body = new FileBody(file, "application/octet-stream");
                       //entity.addPart("userfile", body);
                       //putRequest.setEntity(entity);


                   } catch (FileNotFoundException e) {
                       Log.e(TAG, Utils.getMethodName() + e.toString());
                       return bundle;
                   } catch (IOException e) {
                       e.printStackTrace();
                   }



                   HttpResponse response = null;
                   try {
                       response = httpclient.execute(putRequest);
                   } catch (IOException e) {
                       e.printStackTrace();
                       Log.e(TAG, Utils.getMethodName() + e.toString());
                   }

                   HttpEntity resEntity = response.getEntity();
                   int status_code = response.getStatusLine().getStatusCode();
                   Log.v(TAG, Utils.getMethodName() + "getStatusCode: " + status_code);
                   if (resEntity != null) {
                       Log.v(TAG, Utils.getMethodName()+ "Response content length: " + resEntity.getContentLength());
                       Log.v(TAG, Utils.getMethodName() + "getStatusLine: " + response.getStatusLine().toString());

                   }

               }
               finally {
                   // When HttpClient instance is no longer needed,
                   // shut down the connection manager to ensure
                   // immediate deallocation of all system resources
                   httpclient.getConnectionManager().shutdown();
               }


               }*/
