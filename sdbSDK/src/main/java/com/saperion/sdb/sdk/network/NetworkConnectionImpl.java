/**
 * 2011 Foxykeep (http://datadroid.foxykeep.com)
 * <p>
 * Licensed under the Beerware License : <br />
 * As long as you retain this notice you can do whatever you want with this stuff. If we meet some
 * day, and you think this stuff is worth it, you can buy me a beer in return
 */

package com.saperion.sdb.sdk.network;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Base64;

import com.saperion.sdb.sdk.Constants;
import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.models.ErrorMessage;
import com.saperion.sdb.sdk.network.NetworkConnection.ConnectionResult;
import com.saperion.sdb.sdk.network.NetworkConnection.Method;
import com.saperion.sdb.sdk.providers.ContentProviderHelpers;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

import org.apache.http.HttpStatus;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.zip.GZIPInputStream;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

//import org.apache.http.conn.ssl.SSLSocketFactory;

/**
 * Implementation of the network connection.
 *
 * @author Foxykeep
 */
public final class NetworkConnectionImpl {

    private static final String TAG = NetworkConnectionImpl.class.getSimpleName();

    private static final String ACCEPT = "Accept";
    private static final String ACCEPT_CHARSET_HEADER = "Accept-Charset";
    private static final String ACCEPT_ENCODING_HEADER = "Accept-Encoding";
    private static final String AUTHORIZATION_HEADER = "Authorization";
    private static final String CONTENT_ENCODING_HEADER = "Content-Encoding";
    private static final String CONTENT_TYPE_HEADER = "Content-Type";
    private static final String LOCATION_HEADER = "Location";
    private static final String USER_AGENT_HEADER = "User-Agent";
    private static final String IF_NONE_MATCH = "If-None-Match";

    private static final String UTF8_CHARSET = "UTF-8";

    // Default connection and socket timeout of 20 seconds. Tweak to taste.
    private static final int OPERATION_TIMEOUT = 20 * 1000;

    private NetworkConnectionImpl() {
        // No public constructor
    }

    // always verify the host - dont check for certificate
    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    /**
     * Call the webservice using the given parameters to construct the request and return the
     * result.
     *
     * @param context                The context to use for this operation. Used to generate the user agent if
     *                               needed.
     * @param urlValue               The webservice URL.
     * @param method                 The request method to use.
     * @param parameterList          The parameters to add to the request.
     * @param headerMap              The headers to add to the request.
     * @param isGzipEnabled          Whether the request will use gzip compression if available on the
     *                               server.
     * @param userAgent              The user agent to set in the request. If null, a default Android one will be
     *                               created.
     * @param Payload            The POSTDATA text to add in the request.
     * @param credentials            The credentials to use for authentication.
     * @param isSslValidationEnabled Whether the request will validate the SSL certificates.
     * @return The result of the webservice call.
     */
    public static ConnectionResult execute(Context context, String urlValue, Method method,
                                           ArrayList<BasicNameValuePair> parameterList, HashMap<String, String> headerMap,
                                           HashMap<String, String> requestParams, boolean isGzipEnabled, String userAgent,
                                           String Payload, UsernamePasswordCredentials credentials, String ifNoneMatch,
                                           String authenticationType, boolean isSslValidationEnabled, boolean isFileUpload)
            throws ConnectionException, ServiceRequestException {
        

        String file_name;
        String file_path;
        String file_parent_id;

        int responseCode = -1;
        String content_type;
        String body = "";
        HttpURLConnection connection = null;
        Context mContext;
        try {

            String payload = null;
            // Prepare the request information
            if (userAgent == null) {
                userAgent = UserAgentUtils.get(context);
            }
            if (headerMap == null) {
                headerMap = new HashMap<String, String>();
                // standard output format
                headerMap.put(ACCEPT, "application/json");

            }

            headerMap.put("x-tgr-client", "\'android client\'");
            headerMap.put(USER_AGENT_HEADER, userAgent);

            if (isGzipEnabled) {
                headerMap.put(ACCEPT_ENCODING_HEADER, "gzip");
            }

            headerMap.put(ACCEPT_CHARSET_HEADER, UTF8_CHARSET);

            if (credentials != null) {
                headerMap.put(AUTHORIZATION_HEADER,
                        createAuthenticationHeader(credentials, authenticationType));
            }

            if (ifNoneMatch != null) {
                headerMap.put(IF_NONE_MATCH, ifNoneMatch);
            }

            if (Payload != null) {
                headerMap.put(CONTENT_TYPE_HEADER, "application/json");
                payload = Payload;
            }

            StringBuilder paramBuilder = new StringBuilder();
            if (parameterList != null && !parameterList.isEmpty()) {
                for (BasicNameValuePair parameter : parameterList) {
                    paramBuilder.append(URLEncoder.encode(parameter.getName(), UTF8_CHARSET));
                    paramBuilder.append("=");
                    paramBuilder.append(URLEncoder.encode(parameter.getValue(), UTF8_CHARSET));
                    paramBuilder.append("&");
                }
                payload = paramBuilder.toString();
                headerMap.put(CONTENT_TYPE_HEADER, "application/x-www-form-urlencoded");
            }

            mContext = context;
            
            // Create the connection object
            URL url = new URL(urlValue);
            connection = (HttpURLConnection) url.openConnection();

            switch (method) {
                case GET:
                case DELETE:
                    if (!TextUtils.isEmpty(paramBuilder.toString()))
                        url = new URL(urlValue + "?" + paramBuilder.toString());
                    break;
                case PUT:
                case POST:
                    connection.setDoOutput(true);
                    break;
            }

            // Set the request method
            connection.setRequestMethod(method.toString());

            // If it's an HTTPS request and the SSL Validation is disabled
           // if (url.getProtocol().equals("https") && !isSslValidationEnabled) {

            if (url.getProtocol().equals("https")) {

                //trustAllHosts();
                HttpsURLConnection httpsConnection = (HttpsURLConnection) connection;

                try {
                    httpsConnection.setSSLSocketFactory(getAllHostsValidSocketFactory());
                } catch (NoSuchAlgorithmException e) {
                    Log.e(TAG, "error : " +  e.toString());
                } catch (KeyManagementException e) {
                    Log.e(TAG, "error : " +  e.toString());
                }

                httpsConnection.setHostnameVerifier(getAllHostsValidVerifier());
				

                //httpsConnection.setHostnameVerifier(DO_NOT_VERIFY);

                HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                    public boolean verify(String hostname, SSLSession session) {
                        return true;
                    }
                });
            }

            // Add the headers
            if (!headerMap.isEmpty()) {
                for (Entry<String, String> header : headerMap.entrySet()) {
                    connection.addRequestProperty(header.getKey(), header.getValue());
                }
            }

            // Log the request
            {
                Log.v(TAG, "Request url: " + urlValue);
                Log.v(TAG, "Method: " + method.toString());

                if (parameterList != null && !parameterList.isEmpty()) {
                    Log.v(TAG, "Parameters:");
                    for (BasicNameValuePair parameter : parameterList) {
                        String message = "- " + parameter.getName() + " = " + parameter.getValue();
                        Log.v(TAG, message);
                    }
                }

                if (headerMap != null && !headerMap.isEmpty()) {
                    Log.v(TAG, "Headers:");
                    for (Entry<String, String> header : headerMap.entrySet()) {
                        Log.v(TAG, "- " + header.getKey() + " = " + header.getValue());
                    }
                }
                if (payload != null)
                    Log.v(TAG, "payload:" + payload);
            }

            // Set the connection and read timeout
            connection.setConnectTimeout(OPERATION_TIMEOUT);
            connection.setReadTimeout(OPERATION_TIMEOUT);

            // Set the outputStream content for POST/PUT requests
            if (payload != null) {
                OutputStream output = null;
                try {
                    output = connection.getOutputStream();
                    output.write(payload.getBytes());
                } finally {
                    if (output != null) {
                        try {
                            output.close();
                        } catch (IOException e) {
                            // Already catching the first IOException so nothing to do here.
                        }
                    }
                }
            }

            if (isFileUpload) {
                Log.v(TAG, Utils.getMethodName() + "isFileUpload !");

                file_name = requestParams.get(NetworkConnection.FILE_NAME);
                file_path = requestParams.get(NetworkConnection.FILE_PATH);
                file_parent_id = requestParams.get(NetworkConnection.PARAM_DOC_PARENT_ID);
                if (method == Method.PUT)
                    uploadWithPutMethod(connection, file_path);
                else if (method == Method.POST)
                    multipartUpload(mContext, connection, file_path, file_name, file_parent_id);
            }

            responseCode = connection.getResponseCode();

            Log.v(TAG, "got response code = " + responseCode);

            if (responseCode == HttpStatus.SC_MOVED_PERMANENTLY) {
                String redirectionUrl = connection.getHeaderField(LOCATION_HEADER);
                throw new IOException();
            } else if (responseCode != HttpStatus.SC_OK && responseCode != HttpStatus.SC_CREATED
                    && responseCode != HttpStatus.SC_NOT_MODIFIED && responseCode != HttpStatus.SC_NO_CONTENT  ) {
                Log.e(TAG, Utils.getMethodName() + "got error code " + responseCode);

                throw new IOException();
            }

            String contentEncoding = connection.getHeaderField(CONTENT_ENCODING_HEADER);

            content_type = connection.getHeaderField(CONTENT_TYPE_HEADER);
            if (content_type != null) {

                if (content_type.contains("octet-stream")) {

                    // this is the total size of the file/content
                    int totalSize = connection.getContentLength();
                    if (totalSize < 0) {
                        Log.w(TAG, Utils.getMethodName() + "totalSize <0");
                    }

                    if (requestParams != null) {
                        String request_type = requestParams.get(NetworkConnection.REQUEST_TYPE);

                        Log.v(TAG,
                                "Content-Disposition : "
                                        + connection.getHeaderField("Content-Disposition"));

                        if (request_type.equalsIgnoreCase(NetworkConnection.REEQUEST_TYPE_DOWNLOAD)) {
                            body =
                                    writeStreamToFile(connection,
                                            requestParams.get(NetworkConnection.FILE_PATH));
                        }
                    }

                } else if (content_type.contains("json")) {

                    boolean isGzip = false;
                    if (contentEncoding != null && contentEncoding.equalsIgnoreCase("gzip")) {
                        isGzip = true;
                    }

                    body = convertStreamToString(connection.getInputStream(), isGzip);

                    if (body.equals("[]"))
                        body = "";
					/*
					{
						Log.v(TAG, "Response body: ");
						int pos = 0;
						int bodyLength = body.length();
						while (pos < bodyLength) {
							Log.v(TAG, body.substring(pos,
									Math.min(bodyLength - 1, pos + 200)));
							pos = pos + 200;
						}
					}*/
                }

            }

            // Log.v(TAG, Utils.getMethodName() + "responseCode " + responseCode + " body: " + body);
            return new ConnectionResult(connection.getHeaderFields(), body, responseCode);

        } catch (IOException e) {

            ErrorMessage errorMessage = new ErrorMessage();
            String error_msg = connection.getHeaderField("TGR-Message");
            errorMessage.setLocalizedMessage(connection.getHeaderField("TGR-Localizedmessage"));
            errorMessage.setExceptionMessage(connection.getHeaderField("TGR-Exceptionmessage"));
            errorMessage.setExceptionType(connection.getHeaderField("TGR-ExceptionType"));
            errorMessage.setMessage(connection.getHeaderField("TGR-Message"));
            errorMessage.setUrl(connection.getHeaderField("TGR-Url"));
            errorMessage.setLang(connection.getHeaderField("TGR-Lang"));
            errorMessage.setMessage(error_msg);

            // test
            body = errorMessage.toJson();

            Log.e(TAG, Utils.getMethodName() + "IOException  " + e.getMessage() + " msg " +
                    error_msg + " responseCode = " + responseCode);
            
            if (error_msg == null) {
                // if we couldn't get our own error messages, use default messages
                if (e.toString().contains("authentication challenges") || e.toString().contains("authentication challenge")) {
                    responseCode = HttpsURLConnection.HTTP_UNAUTHORIZED;

                } else if (e.toString().contains("Network is unreachable")) {
                    responseCode = HttpsURLConnection.HTTP_GATEWAY_TIMEOUT;
                } else {
                    responseCode = ResponseConstants.RESPONSE_CODE_UNKOWN_ERROR;
                }
                errorMessage.setMessage(e.getMessage());
                errorMessage.setLocalizedMessage(e.getMessage());

            }

            
            body = errorMessage.toJson();

            throw new ServiceRequestException(body, responseCode);

        } catch (NullPointerException e) {
            Log.e(TAG, Utils.getMethodName() + "NullPointerException " + e.toString());
            throw new ConnectionException(e);
        } finally {

            if (connection != null) {
                // Log.v(TAG, Utils.getMethodName() + "close connection");
                connection.disconnect();
            }
        }
    }

    public static String createAuthenticationHeader(UsernamePasswordCredentials credentials,
                                                    String authenticationType) {
        StringBuilder sb = new StringBuilder();
        sb.append(credentials.getUserName()).append(":").append(credentials.getPassword());

        return authenticationType + " "
                + Base64.encodeToString(sb.toString().getBytes(), Base64.NO_WRAP);

    }

    private static SSLSocketFactory sAllHostsValidSocketFactory;

    private static SSLSocketFactory getAllHostsValidSocketFactory()
            throws NoSuchAlgorithmException, KeyManagementException {
        if (sAllHostsValidSocketFactory == null) {
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }};

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            sAllHostsValidSocketFactory = sc.getSocketFactory();
        }

        return sAllHostsValidSocketFactory;
    }

    private static HostnameVerifier sAllHostsValidVerifier;

    private static HostnameVerifier getAllHostsValidVerifier() {
        if (sAllHostsValidVerifier == null) {
            sAllHostsValidVerifier = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
        }

        return sAllHostsValidVerifier;
    }

    private static String convertStreamToString(InputStream is, boolean isGzipEnabled)
            throws IOException {
        InputStream cleanedIs = is;
        if (isGzipEnabled) {
            cleanedIs = new GZIPInputStream(is);
        }


        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(cleanedIs, UTF8_CHARSET));
            StringBuilder sb = new StringBuilder();
            for (String line; (line = reader.readLine()) != null; ) {
                sb.append(line);
                sb.append("\n");
            }

            return sb.toString();
        } finally {
            if (reader != null) {
                reader.close();
            }

            //cleanedIs.close();

            if (isGzipEnabled) {
                is.close();
            }
        }
    }

    private static String getLocalizedMessageFromStream(InputStream is, boolean isGzipEnabled)
            throws IOException {
        InputStream cleanedIs = is;
        if (isGzipEnabled) {
            cleanedIs = new GZIPInputStream(is);
        }

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(cleanedIs, UTF8_CHARSET));
            StringBuilder sb = new StringBuilder();
            for (String line; (line = reader.readLine()) != null; ) {
                if (line.contains("LocalizedMessage")) {
                    sb.append(line);
                }
            }
            String localizedErrorMessage = sb.toString();
            localizedErrorMessage = localizedErrorMessage.replace("LocalizedMessage: ", "");
            return localizedErrorMessage;
        } finally {
            if (reader != null) {
                reader.close();
            }

            cleanedIs.close();

            if (isGzipEnabled) {
                is.close();
            }
        }
    }

    /**
     * Trust every server - dont check for any certificate
     */
    private static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return new java.security.cert.X509Certificate[]{};
            }

            public void checkClientTrusted(X509Certificate[] chain, String authType)
                    throws CertificateException {
            }

            public void checkServerTrusted(X509Certificate[] chain, String authType)
                    throws CertificateException {
            }
        }};

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

// load our own certfificate
    /*
    private SSLSocketFactory newSslSocketFactory(Context context) {
        try {
            KeyStore trusted = KeyStore.getInstance("BKS");
            InputStream in = context.getResources().openRawResource(android.R.raw.mystore);
            try {
                trusted.load(in, "mypassword".toCharArray());
            }
            finally {
                in.close();
            }

            org.apache.http.conn.ssl.SSLSocketFactory mySslFact = new org.apache.http.conn.ssl.SSLSocketFactory(trusted);
            //mySslFact.setHostNameVerifier(new MyHstNameVerifier());
            return mySslFact;
        } catch(Exception e) {
            throw new AssertionError(e);
        }
    }
    */

    /**
     * @param connection
     * @param absFilePath
     * @return
     */
    private static String writeStreamToFile(HttpURLConnection connection,
                                            String absFilePath) {
          /*  String filePath = absFilePath;

    
        if (!isWithExtension) {
            String content_disp_field = connection.getHeaderField("Content-Disposition");
            if (content_disp_field.contains("filename=")
                    && content_disp_field.contains("attachment")) {
                String[] split_strings = content_disp_field.split("filename=");
                String string2nd = split_strings[1];
                String file_name = string2nd.substring(0, string2nd.lastIndexOf("\""));
                if (file_name.contains(".")) {
                    String file_extension = file_name.substring(file_name.lastIndexOf("."));
                    if (file_extension != null) {
                        filePath = absFilePath + file_extension;
                    }
                }

            }
        }
        */

        // Log.v(TAG,  Utils.getMethodName() + "writing file" + file_path);

        // create a new file, specifying the path, and the filename
        // which we want to save the file as.
        File file_to_save = new File(absFilePath);

        // this will be used to write the downloaded data into the
        // file we created
        FileOutputStream fileOutput = null;
        try {
            fileOutput = new FileOutputStream(file_to_save);
        } catch (FileNotFoundException e) {
            Log.e(TAG, Utils.getMethodName() + " FileNotFoundException " + e.toString());
        }

        // this will be used in reading the data createInstance the internet
        InputStream inputStream = null;
        try {
            inputStream = connection.getInputStream();
        } catch (IOException e) {
            Log.e(TAG, Utils.getMethodName() + " IOException " + e.toString());
        }

        // variable to store total downloaded bytes
        int downloadedSize = 0;

        // create a buffer...
        byte[] buffer = new byte[1024];
        int bufferLength = 0; // used to store a temporary size of
        // the buffer

        // now, read through the input buffer and write the contents
        // to the file
        try {
            while ((bufferLength = inputStream.read(buffer)) > 0) {
                // add the data in the buffer to the file in the file
                // output stream (the file on the sd card
                fileOutput.write(buffer, 0, bufferLength);
                // add up the size so we know how much is downloaded
                downloadedSize += bufferLength;

            }
        } catch (IOException e) {
            Log.e(TAG, Utils.getMethodName() + " IOException " + e.toString());
        }
        // close the output stream when done
        try {
            if (fileOutput != null)
                fileOutput.close();
        } catch (IOException e) {
            Log.e(TAG, Utils.getMethodName() + " IOException " + e.toString());
        }
        Log.d(TAG, Utils.getMethodName() + "downloadedSize : " + downloadedSize);

        return absFilePath;
    }

    private static void writeFileToContentProvider(Context context, HttpURLConnection connection,
                                                   Uri uri) throws IOException {

        OutputStream out = null;

        //BufferedInputStream inStream = null;

        Log.v(TAG, Utils.getMethodName() + "downloading to " + uri.toString());
        try {
            out = ContentProviderHelpers.documnentGetFileOutputStream(context, uri);
        } catch (FileNotFoundException e) {
            Log.e(TAG, Utils.getMethodName() + " FileNotFoundException " + e.toString());
            throw e;
        }

        // this will be used in reading the data createInstance the internet
        InputStream inputStream;
        try {
            inputStream = connection.getInputStream();
        } catch (IOException e) {
            Log.e(TAG, Utils.getMethodName() + " IOException " + e.toString());
            throw e;
        }

        // variable to store total downloaded bytes
        int downloadedSize = 0;

        // create a buffer...
        byte[] buffer = new byte[1024];
        int bufferLength = 0; // used to store a temporary size of
        // the buffer

        // now, read through the input buffer and write the contents
        // to the file
        try {
            while ((bufferLength = inputStream.read(buffer)) > 0) {
                // add the data in the buffer to the file in the file
                // output stream (the file on the sd card
                out.write(buffer, 0, bufferLength);
                // add up the size so we know how much is downloaded
                downloadedSize += bufferLength;

            }
        } catch (IOException e) {
            Log.e(TAG, Utils.getMethodName() + " IOException " + e.toString());
        }

        Log.v(TAG, Utils.getMethodName() + "closing the output stream when done ");
        try {
            out.close();
        } catch (IOException e) {
            Log.e(TAG, Utils.getMethodName() + " IOException " + e.toString());
            throw e;
        }
        Log.d(TAG, Utils.getMethodName() + "downloadedSize : " + downloadedSize);

    }

    private static void multipartUpload(Context context,HttpURLConnection connection, String file_path,
                                        String file_name, String file_parent_id) throws IOException {

        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = Constants.BOUNDARY;
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;

        File file = new File(file_path);
        Log.v(TAG,
                com.saperion.sdb.sdk.utils.Utils.getMethodName() + "file length " + file.length()
                        + "URLConnection.guessContentTypeFromName " + URLConnection.guessContentTypeFromName(file.getName()));

        FileInputStream fileInputStream = new FileInputStream(file);

        /*
        Uri fileURI = Uri.parse(file_path);
        ParcelFileDescriptor pfd = context.getContentResolver().
                openFileDescriptor(fileURI, "r");
        FileInputStream fileInputStream =
                new FileInputStream(pfd.getFileDescriptor());
        */

        bytesAvailable = fileInputStream.available();
        bufferSize = Math.min(bytesAvailable, maxBufferSize);
        buffer = new byte[bufferSize];

        // Allow Inputs & Outputs
        connection.setDoInput(true);
        connection.setUseCaches(false);

        DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream());
        outputStream.writeBytes(twoHyphens + boundary + lineEnd);
        // write filename
        outputStream.writeBytes("Content-Disposition: form-data; name=\"filename\"" + lineEnd);
        outputStream.writeBytes("Content-Type: text/plain;charset=UTF-8" + lineEnd);
        //outputStream.writeBytes("Content-Length: " + file_name.length() + lineEnd);
        outputStream.writeBytes(lineEnd);
        outputStream.writeBytes(file_name + lineEnd);
        outputStream.writeBytes(twoHyphens + boundary + lineEnd);

        // write parentId
        outputStream.writeBytes("Content-Disposition: form-data; name=\"parentId\"" + lineEnd);
        outputStream.writeBytes("Content-Type: text/plain;charset=UTF-8" + lineEnd);
        //outputStream.writeBytes("Content-Length: " + file_parent_id.length() + lineEnd);
        outputStream.writeBytes(lineEnd);
        outputStream.writeBytes(file_parent_id + lineEnd);
        outputStream.writeBytes(twoHyphens + boundary + lineEnd);

        // write description
        //outputStream.writeBytes("Content-Disposition: form-data; name=\"filedesc\"" + lineEnd);
        //outputStream.writeBytes("Content-Type: text/plain;charset=UTF-8" + lineEnd);
        //outputStream.writeBytes("Content-Length: " + file_parent_id.length() + lineEnd);
        //outputStream.writeBytes(lineEnd);
        //outputStream.writeBytes(twoHyphens + boundary + lineEnd);

        // write file
        outputStream.writeBytes(twoHyphens + boundary + lineEnd);
        outputStream.writeBytes("Content-Disposition: form-data; name=\"file\"; filename=\""
                + file_name + "\"" + lineEnd);
        outputStream.writeBytes("Content-Type: " + URLConnection.guessContentTypeFromName(file_path) + lineEnd);
        //outputStream.writeBytes("Content-Type: application/pdf" +   lineEnd);

        Log.e(TAG, Utils.getMethodName() + "Content-Type: " + URLConnection.guessContentTypeFromName(file_path));
        
        outputStream.writeBytes("Content-Transfer-Encoding: binary" + lineEnd);
        outputStream.writeBytes(lineEnd);
        outputStream.flush();

        bytesRead = fileInputStream.read(buffer, 0, bufferSize);

        while (bytesRead > 0) {
            outputStream.write(buffer, 0, bufferSize);
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        }

        outputStream.writeBytes(lineEnd);
        outputStream.flush();
        outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

        fileInputStream.close();
        outputStream.flush();
        outputStream.close();

    }

    private static void uploadWithPutMethod(HttpURLConnection connection, String file_path) throws IOException {

        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;

        File file = new File(file_path);
        FileInputStream fileInputStream = new FileInputStream(file);

        bytesAvailable = fileInputStream.available();
        bufferSize = Math.min(bytesAvailable, maxBufferSize);
        buffer = new byte[bufferSize];
        connection.setFixedLengthStreamingMode((int) file.length());
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.connect();
        OutputStream output = connection.getOutputStream();

        bytesRead = fileInputStream.read(buffer, 0, bufferSize);

        while (bytesRead > 0) {
            output.write(buffer, 0, bufferSize);

            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        }
        output.flush();
        output.close();

    }

    private static void uploadWithPutMethod2(HttpURLConnection connection, String file_path) throws IOException {

        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        String result = null;

        File file = new File(file_path);
        FileInputStream fileInputStream = new FileInputStream(file);

        bytesAvailable = fileInputStream.available();
        bufferSize = Math.min(bytesAvailable, maxBufferSize);
        buffer = new byte[bufferSize];
        // connection.setFixedLengthStreamingMode((int) file.length());
        connection.setDoInput(true);
        connection.setDoOutput(true);

        OutputStream output = connection.getOutputStream();
        // output.writeBytes(reqbody);
        output.flush();
        output.close();

    }

    private byte[] convertFileToBase64(String filePath) {
        File file = new File(filePath);

        byte[] imageByteArray = new byte[0];
        try {
            // Reading a Image file createInstance file system
            FileInputStream imageInFile = new FileInputStream(file);
            byte imageData[] = new byte[(int) file.length()];
            imageInFile.read(imageData);

            // Converting Image byte array into Base64 String
            String imageDataString = encodeImage(imageData);

            // Converting a Base64 String into Image byte array
            imageByteArray = decodeImage(imageDataString);

            // Write a image byte array into file system
            FileOutputStream imageOutFile = new FileOutputStream(filePath);
            imageOutFile.write(imageByteArray);

            imageInFile.close();
            imageOutFile.close();

            System.out.println("Image Successfully Manipulated!");
        } catch (FileNotFoundException e) {
            System.out.println("Image not found" + e);
        } catch (IOException ioe) {
            System.out.println("Exception while reading the Image " + ioe);
        }

        return imageByteArray;
    }


    /**
     * Encodes the byte array into base64 string
     *
     * @param imageByteArray - byte array
     * @return String a {@link java.lang.String}
     */
    public static String encodeImage(byte[] imageByteArray) {
        return Base64.encodeToString(imageByteArray, Base64.NO_WRAP);
    }

    /**
     * Decodes the base64 string into byte array
     *
     * @param imageDataString - a {@link java.lang.String}
     * @return byte array
     */
    public static byte[] decodeImage(String imageDataString) {
        return Base64.decode(imageDataString, Base64.NO_WRAP);
    }
}


            /*
		    errorStream = connection.getErrorStream();

			if (errorStream != null) {
                Log.v(TAG, "we got error stream");
				//				String error = convertStreamToString(errorStream, isGzip);
				//				Log.w(TAG, "error stream: " + error);
				//				if (error != null)
				//					errorStream = connection.getErrorStream();
                body = getLocalizedMessageFromStream(errorStream, isGzip);
               
                
				// TODO
				// throw new ConnectionException(error, responseCode);
			} else 
			*/