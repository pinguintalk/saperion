package com.saperion.sdb.sdk.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class Feature extends Typed implements Parcelable {

    private String name;

    private String enabled;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public Feature() {
        super(ModelType.FEATURE);
    }



    public String toJson() {

        ObjectMapper mapper = new ObjectMapper();

        mapper.setSerializationInclusion(Include.NON_NULL);
        try {
            return mapper.writeValueAsString(this);

        } catch (JsonProcessingException e1) {
            e1.printStackTrace();
        }
        return null;
    }

    public static Feature fromJson(String string) {

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        try {
            mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

            return mapper.readValue(string, Feature.class);

        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.getType());
        out.writeString(this.getName());
        out.writeString(this.getEnabled());

    }    
   

    private Feature(Parcel in) {
        this.setType(in.readString());
        this.setName(in.readString());
        this.setEnabled(in.readString());
    }
    
    public static final Creator CREATOR = new Creator() {
        public Feature createFromParcel(Parcel in) {
            return new Feature(in);
        }

        public Feature[] newArray(int size) {
            return new Feature[size];
        }
    };

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public String toString() {
        return "{" + "name=" + name + ", isenable='"  + "} "
                + super.toString();
    }
}
