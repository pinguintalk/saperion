/**
 * 2011 Foxykeep (http://datadroid.foxykeep.com)
 * <p>
 * Licensed under the Beerware License : <br />
 * As long as you retain this notice you can do whatever you want with this stuff. If we meet some
 * day, and you think this stuff is worth it, you can buy me a beer in return
 */

package com.saperion.sdb.sdk.operations;

import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.OperationApplicationException;
import android.os.Bundle;
import android.os.RemoteException;

import com.saperion.sdb.sdk.Constants;
import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.factory.FolderJsonFactory;
import com.saperion.sdb.sdk.models.Folder;
import com.saperion.sdb.sdk.network.NetworkConnection.ConnectionResult;
import com.saperion.sdb.sdk.providers.FolderContent.FolderDB;
import com.saperion.sdb.sdk.providers.SdbContentProvider;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.service.RequestService.Operation;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

import java.util.ArrayList;

public final class SpaceGetSubfoldersOperation implements Operation {

    private static final String TAG = SpaceGetSubfoldersOperation.class.getSimpleName();

    public static final String PARAM_SPACE_ID = Constants.SDK_PACKAGE_NAME + ".param_space_id";


    @Override
    public Bundle execute(Context context, Request request) throws ServiceRequestException, ConnectionException,
            DataException {

        Log.v(TAG, Utils.getMethodName() + "entry");
        Bundle bundle = new Bundle();
        String spaceID = request.getString(PARAM_SPACE_ID);
        ArrayList<Folder> itemList = new ArrayList<Folder>();
        // Log.v(TAG, Utils.getMethodName() + "deleting DbSpace ...");
        // TODO  test : Clear the table
        context.getContentResolver().delete(FolderDB.CONTENT_URI, null, null);

        //get folder list createInstance server of current parent 
        ConnectionResult result =
                OperationHelpers.folderGetSubfoldersByParentId(context, spaceID, true);

        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);

        if (result.responseCode != ResponseConstants.RESPONSE_CODE_OK)
            return bundle;

        if (!result.body.equals("[]"))
            itemList = FolderJsonFactory.parseResult(result.body);

        if (itemList == null || itemList.size() < 1) {
            bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE,
                    ResponseConstants.RESPONSE_CODE_ERROR);
            bundle.putParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, itemList);
            return bundle;
        }

        // Adds the spaces to the database
        int itemListSize = itemList.size();
        if (itemListSize > 0) {
            ArrayList<ContentProviderOperation> operationList =
                    new ArrayList<ContentProviderOperation>();

            for (Folder anItemList : itemList) {

                operationList.add(ContentProviderOperation.newInsert(FolderDB.CONTENT_URI)
                        .withValues(anItemList.toContentValues()).build());
            }
            try {
                context.getContentResolver()
                        .applyBatch(SdbContentProvider.AUTHORITY, operationList);
            } catch (RemoteException e) {
                throw new DataException(e);
            } catch (OperationApplicationException e) {
                throw new DataException(e);
            }
        }

        bundle.putParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, itemList);
        
        return bundle;
    }

}
