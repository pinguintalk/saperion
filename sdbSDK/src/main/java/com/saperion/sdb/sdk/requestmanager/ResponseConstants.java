package com.saperion.sdb.sdk.requestmanager;

/**
 * Class used to create the {@link Request}s.
 *
 * @author nnn, adr
 */
public final class ResponseConstants {

    // Response code createInstance server 
    public static final int RESPONSE_CODE_OK = 200;
    public static final int RESPONSE_CODE_CREATED = 201;
    public static final int RESPONSE_CODE_NO_CONTENT = 204;
    public static final int RESPONSE_CODE_NOT_MODIFIED = 304;
    public static final int RESPONSE_CODE_BAD_REQUEST = 400;
    public static final int RESPONSE_CODE_UNAUTHORIZED = 401;
    public static final int RESPONSE_CODE_FORBIDDEN = 403;
    public static final int RESPONSE_CODE_NOT_FOUND = 404;
    public static final int RESPONSE_CODE_METHOD_NOT_ALLOWED = 405;
    public static final int RESPONSE_CODE_PRECONDITION_FAILED = 412;
    public static final int RESPONSE_CODE_UNSUPPORTED_MEDIA_TYPE = 415;
    public static final int RESPONSE_CODE_TOO_MANY_REQUESTS = 429;
    public static final int RESPONSE_CODE_INTERNAL_SERVER_ERROR = 500;
    public static final int RESPONSE_CODE_INTERNAL_SERVICE_UNAVAILABLE = 503;
    public static final int RESPONSE_CODE_ERROR = -1;
    public static final int RESPONSE_CODE_UNKOWN_ERROR = 0;

    // Response param type
    public static final String BUNDLE_EXTRA_SPACE_LIST_RESULT =
            "com.saperion.sdb.sdk.extra.spaceList";
    public static final String BUNDLE_EXTRA_ERROR_MESSAGE = "com.saperion.sdb.sdk.error_message";

    public static final String BUNDLE_EXTRA_REQUEST_TYPE =
            "com.saperion.sdb.sdk.extra.request_type";
    public static final String BUNDLE_EXTRA_RESULT_CODE = "com.saperion.sdb.sdk.extra.result_code";
    public static final String BUNDLE_EXTRA_RESULT_OBJECT =
            "com.saperion.sdb.sdk.extra.result_object";
    public static final String BUNDLE_EXTRA_DOCUMENT_DOWNLOAD_NAME =
            "com.saperion.sdb.sdk.extra.document.download.name";
    public static final String BUNDLE_EXTRA_DOCUMENT_DOWNLOAD_PATH =
            "com.saperion.sdb.sdk.extra.document.download.path";

    private ResponseConstants() {
        // no public constructor
    }

}
