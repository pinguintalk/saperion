package com.saperion.sdb.sdk.models;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.operations.ShareDeleteOperation;
import com.saperion.sdb.sdk.operations.ShareSaveOperation;
import com.saperion.sdb.sdk.operations.ShareUpdateOperation;
import com.saperion.sdb.sdk.providers.ShareContent.ShareDB;
import com.saperion.sdb.sdk.providers.UserContent.UserDB.Columns;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.RequestConstants;
import com.saperion.sdb.sdk.requestmanager.RequestManager.RequestListener;
import com.saperion.sdb.sdk.requestmanager.RequestManagerSingleton;
import com.saperion.sdb.sdk.rights.ShareRight;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;

/**
 * The class Share.
 *
 * @author sts
 */
/*
{
        "id":			”<the id of this share>”,
        "type":		”share”,
        "name":		”<name of the share – max allowed length: 400>”,
        "receiverId":”	“<id of the share to share with>”,
        "receiverName":	” <email of the share to share with>”,
        "rights":		[“READ”,”WRITE”,”SHARE”],
        "spaceId":		“the id of the space this share was created for”
        "creationDate":	”2012-08-06T15:19:09.243+02:00”,
        "ownerId":		”<id of the share who created this share>”,
        "ownerName":	”<name of the share who created this share”,
        “secret”:		“<the secret of the share, if the shared-space is encrypted>”
        }
*/
public class Share extends CreatedTypedIdentifiable implements Parcelable{
    private String name;
    private String receiverId;
    private String receiverName;
    private EnumSet<ShareRight> rights;
    private String spaceId;
    private String creationDate;
    private String ownerId;
    private String ownerName;
    private String secret;
    
    public Share() {
        super(ModelType.SHARE);
        rights = EnumSet.noneOf(ShareRight.class);
    }

    public String getSpaceId() {
        return spaceId;
    }

    public void setSpaceId(String spaceId) {
        this.spaceId = spaceId;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
    
    public String getName() {
        return name;
    }

    public Share setName(String name) {
        this.name = name;
        return this;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public Share setReceiverId(String receiverId) {
        this.receiverId = receiverId;
        return this;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public Share setReceiverName(String receiverName) {
        this.receiverName = receiverName;
        return this;
    }

    public Collection<ShareRight> getRights() {
        return rights;
    }

    public Share setRights(Collection<ShareRight> rights) {
        if (rights.isEmpty()) {
            this.rights = EnumSet.noneOf(ShareRight.class);
        } else {
            this.rights = EnumSet.copyOf(rights);
        }
        return this;
    }

    public Share addRight(ShareRight right) {
        if (null == right) {
            rights = EnumSet.of(right);
        } else {
            rights.add(right);
        }
        return this;
    }

    @Override
    public String toString() {
        return "Share{" + "name='" + name + '\'' + ", receiverId='" + receiverId + '\''
                + ", receiverName='" + receiverName + '\'' + ", rights=" + rights + "} "
                + super.toString();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    
    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(this.getId());
        parcel.writeString(this.getType());
        parcel.writeString(this.getName());
        parcel.writeString(this.getReceiverId());
        parcel.writeString(this.getReceiverName());
        parcel.writeString(this.getSpaceId());
        parcel.writeString(this.getCreationDate());
        parcel.writeString(this.getOwnerId());
        parcel.writeString(this.getOwnerName());
        parcel.writeString(this.getSecret());

        parcel.writeSerializable(this.rights);

    }

    private Share(Parcel in) {
        this.setId(in.readString());
        this.setType(in.readString());
        this.setName(in.readString());
        this.setReceiverId(in.readString());
        this.setReceiverName(in.readString());
        this.setSpaceId(in.readString());
        this.setCreationDate(in.readString());
        this.setOwnerId(in.readString());
        this.setOwnerName(in.readString());
        this.setSecret(in.readString());
        this.setRights((EnumSet<ShareRight>) in.readSerializable());
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Share createFromParcel(Parcel in) {
            return new Share(in);
        }

        public Share[] newArray(int size) {
            return new Share[size];
        }
    };

    public static String toJson(Share share) {

        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(Include.NON_NULL);
        try {
            String f = mapper.writeValueAsString(share);
            return f;
        } catch (JsonProcessingException e1) {
            e1.printStackTrace();
        }
        return null;
    }

    public static Share fromJson(String string) {
        Share share = new Share();
        ObjectMapper mapper = new ObjectMapper();
        try {
            share = mapper.readValue(string, Share.class);
            return share;
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static ArrayList<Share> parseJsonArray(String wsResponse) throws DataException {

        if (wsResponse == null || TextUtils.isEmpty(wsResponse))
            return null;

        ArrayList<Share> shareList = new ArrayList<Share>();

        ObjectMapper objectMapper = new ObjectMapper();
        JsonFactory jsonFactory = new JsonFactory();

        try {
            JsonParser jp = jsonFactory.createJsonParser(wsResponse);
            // advance stream to START_ARRAY first:
            jp.nextToken();
            // and then each time, advance to opening START_OBJECT
            while (jp.nextToken() == JsonToken.START_OBJECT) {
                Share share = objectMapper.readValue(jp, Share.class);

                // process
                // after binding, stream points to closing END_OBJECT
                shareList.add(share);

            }

        } catch (JsonParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return shareList;
    }

    public void save(RequestListener requestListener) {

        String payload = "";

        JSONObject jo = new JSONObject();
        try {

            jo.put("name", this.getName());
            jo.put("receiverName", this.getReceiverName());
            // jo.put("rights", this.getRights().toString());
            jo.put("type", "share");
            JSONArray ja = new JSONArray();
            ja.put(ShareRight.READ.toString());
            for (ShareRight right:this.getRights()) {
                ja.put(right.toString());
            }

            jo.put(Columns.RIGHTS.getName(), ja);

            payload = jo.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        
        Request request = new Request(RequestConstants.REQUEST_TYPE_SHARE_CREATE);
        
        if (this.getId() != null && this.getId().length() > 0){
            request = new Request(RequestConstants.REQUEST_TYPE_SHARE_UPDATE);
            request.put(ShareUpdateOperation.PARAM_SHARE,this);
            request.put(ShareUpdateOperation.PARAM_PAYLOAD,payload);
        } else {
            request.put(ShareSaveOperation.PARAM_SPACE_ID,this.getSpaceId());
            request.put(ShareSaveOperation.PARAM_PAYLOAD,payload);
        }

        if (RequestManagerSingleton.isCreated())
            RequestManagerSingleton.getInstance().execute(request, requestListener);

    }

    public void delete(RequestListener requestListener) {

        Request request = new Request(RequestConstants.REQUEST_TYPE_SHARE_DELETE);
        request.put(ShareDeleteOperation.PARAM_SPACE_ID,this.getSpaceId());
        request.put(ShareDeleteOperation.PARAM_SHARE_ID,this.getId());

        if (RequestManagerSingleton.isCreated())
            RequestManagerSingleton.getInstance().execute(request, requestListener);

    }

    public ContentValues toContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(ShareDB.Columns.ID.getName(), this.getId());
        cv.put(ShareDB.Columns.NAME.getName(), this.getName());
        cv.put(ShareDB.Columns.TYPE.getName(), this.getType());
        cv.put(ShareDB.Columns.RECEIVER_ID.getName(), this.getReceiverId());
        cv.put(ShareDB.Columns.OWNER_ID.getName(), this.getOwnerId());
        cv.put(ShareDB.Columns.OWNER_NAME.getName(), this.getOwnerName());
        cv.put(ShareDB.Columns.CREATION_DATE.getName(), this.getCreationDate());
        cv.put(ShareDB.Columns.RECEIVER_NAME.getName(), this.getReceiverName());
        cv.put(ShareDB.Columns.SPACE_ID.getName(), this.getSpaceId());
        cv.put(ShareDB.Columns.SECRET.getName(), this.getSecret());


        String rights = "";
        for (ShareRight share : this.getRights()) {
            if (!TextUtils.isEmpty(rights)) {
                rights = rights + "," +  share.toString();
            } else
                rights =  share.toString();
        }

        cv.put(ShareDB.Columns.RIGHTS.getName(), rights);

        return cv;
    }

    public static Share fromCursor(Cursor cursor) {
        Share share = new Share();
        share.setId(cursor.getString(ShareDB.Columns.ID.getIndex()));
        share.setName(cursor.getString(ShareDB.Columns.NAME.getIndex()));
        share.setType(cursor.getString(ShareDB.Columns.TYPE.getIndex()));
        share.setReceiverId(cursor.getString(ShareDB.Columns.RECEIVER_ID.getIndex()));
        share.setReceiverName(cursor.getString(ShareDB.Columns.RECEIVER_NAME.getIndex()));
        share.setOwnerId(cursor.getString(ShareDB.Columns.OWNER_ID.getIndex()));
        share.setOwnerName(cursor.getString(ShareDB.Columns.OWNER_NAME.getIndex()));
        share.setCreationDate(cursor.getString(ShareDB.Columns.CREATION_DATE.getIndex()));
        share.setSpaceId(cursor.getString(ShareDB.Columns.SPACE_ID.getIndex()));        
        share.setSecret(cursor.getString(ShareDB.Columns.SECRET.getIndex()));
        String rights = cursor.getString(ShareDB.Columns.RIGHTS.getIndex());

        if (rights != null && !TextUtils.isEmpty(rights)) {

            if (rights.contains(",")) {
                String[] right_set = rights.split(",");
                for (int i = 0; i < right_set.length; i++) {
                    share.rights.add(ShareRight.valueOf(right_set[i]));
                }

            } else {
                share.rights.add(ShareRight.valueOf(rights));
            }
        }
        return share;
    }
}
