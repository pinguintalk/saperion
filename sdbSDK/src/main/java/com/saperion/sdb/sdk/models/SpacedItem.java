package com.saperion.sdb.sdk.models;

import android.content.ContentValues;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.saperion.sdb.sdk.providers.ContentProviderHelpers;
import com.saperion.sdb.sdk.rights.SpaceRight;

import java.util.ArrayList;
import java.util.List;

/**
 * The class SpacedItem.
 *
 * @author sts
 */
public class SpacedItem extends Item implements Parcelable {

    private String spaceId;
    private String parentId;
    private String idChain;

    public String is() {
        return idChain;
    }
    
    public String getIdChain() {
        return idChain;
    }

    public void setIdChain(String idChain) {
        this.idChain = idChain;
    }

    protected SpacedItem() {
    }

    public SpacedItem(ModelType type) {
        super(type);
    }

    public String getSpaceId() {
        return spaceId;
    }

    public void setSpaceId(String spaceId) {
        this.spaceId = spaceId;
    }

    public String getParentId() {
        return parentId;
    }

    public Item setParentId(String parentId) {
        this.parentId = parentId;
        return this;
    }

    public boolean isEditAble(Context context) {
        Space parentSpace = ContentProviderHelpers.spaceGetById(context, this.getSpaceId());
        // this avoid if item have no id at the beginning of create process
        return parentSpace == null || parentSpace.getRights().toString().contains(SpaceRight.WRITE.toString());
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.getId());
        out.writeString(this.getType());
        out.writeString(this.getOwnerId());
        out.writeString(this.getOwnerName());
        out.writeString(this.getCreationDate());
        out.writeString(this.getLastModified());
        out.writeString(this.getName());
        out.writeString(this.getDescription());
        out.writeString(this.getModificationId());
        out.writeString(this.getParentId());
        out.writeString(this.getSpaceId());
        out.writeString(this.getIdChain());
        out.writeStringList(this.getTags());

    }

    private SpacedItem(Parcel in) {
        this.setId(in.readString());
        this.setType(in.readString());
        this.setOwnerId(in.readString());
        this.setOwnerName(in.readString());
        this.setCreationDate(in.readString());
        this.setLastModified(in.readString());
        this.setName(in.readString());
        this.setDescription(in.readString());
        this.setModificationId(in.readString());
        this.setParentId(in.readString());
        this.setSpaceId(in.readString());
        this.setIdChain(in.readString());
        List<String> tags = new ArrayList<String>();
        in.readStringList(tags);
        this.setTags(tags);

    }

    @Override
    public String toString() {
        return "SpacedItem{" + "spaceId='" + spaceId + '\'' + ", parentId='" + parentId + '\''
                + "} " + super.toString();
    }

    public ContentValues toContentValues() {
        ContentValues cv = new ContentValues();

        return cv;

    }

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }
}
