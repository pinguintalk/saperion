package com.saperion.sdb.sdk.operations;

import android.content.Context;
import android.os.Bundle;

import com.saperion.sdb.sdk.Constants;
import com.saperion.sdb.sdk.config.WSConfig;
import com.saperion.sdb.sdk.exceptions.AuthenticationException;
import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.network.NetworkConnection;
import com.saperion.sdb.sdk.network.NetworkConnection.ConnectionResult;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.security.CredentialManager;
import com.saperion.sdb.sdk.service.RequestService.Operation;
import com.saperion.sdb.sdk.utils.Log;

import org.apache.http.HttpStatus;
import org.apache.http.auth.UsernamePasswordCredentials;

import java.util.HashMap;

public class GetAccessTokenOperation implements Operation {

    private static final String TAG = GetAccessTokenOperation.class.getSimpleName();

    public static final String PARAM_USER_NAME = Constants.SDK_PACKAGE_NAME + ".param_user_name";
    public static final String PARAM_PASSWORD = Constants.SDK_PACKAGE_NAME + ".param_password";
    
    @Override
    public Bundle execute(Context context, Request request) throws ConnectionException, ServiceRequestException,
            DataException, AuthenticationException {
        Bundle bundle = new Bundle();

        //String userName = request.getString(PARAM_USER_NAME);
        //String passWord = request.getString(PARAM_PASSWORD);

        String userName = CredentialManager
                .getInstance().getUserName();
        String passWord = CredentialManager.getInstance().getPassword();
        
        if (userName == null || userName.isEmpty() || passWord == null || passWord.isEmpty()) {
            bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, HttpStatus.SC_UNAUTHORIZED);
            return bundle;
        }

        NetworkConnection networkConnection =
                new NetworkConnection(context, CredentialManager.getInstance().getServerUrl()
                        + WSConfig.WS_AUTHENTICATION_URL);

        HashMap<String, String> accept_header = new HashMap<String, String>();
        accept_header.put("ACCEPT", "application/json");

        networkConnection.setRequestParams(accept_header);
        networkConnection.setAuthenticationType("Basic");
        networkConnection.setCredentials(new UsernamePasswordCredentials(userName, passWord));
        ConnectionResult result = networkConnection.execute();

        Log.d(TAG, "GetAccessTokenOperation result: " + result.body);

        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);

        if (result.responseCode != HttpStatus.SC_OK) {
            bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
            return bundle;
        }

        bundle.putString(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, result.body);

        return bundle;

    }

}
