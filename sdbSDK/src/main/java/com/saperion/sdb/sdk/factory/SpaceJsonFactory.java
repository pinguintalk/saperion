package com.saperion.sdb.sdk.factory;

import java.io.IOException;
import java.util.ArrayList;

import android.text.TextUtils;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.models.Space;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

public final class SpaceJsonFactory {

    private static final String TAG = SpaceJsonFactory.class.getSimpleName();

    private static boolean LOG_ALL = false;

    private SpaceJsonFactory() {
        // No public constructor
    }

    public static String toJson(Space space) {

        ObjectMapper mapper = new ObjectMapper();
        //    	mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        mapper.setSerializationInclusion(Include.NON_NULL);
        try {
            String s = mapper.writeValueAsString(space);
            return s;
        } catch (JsonProcessingException e1) {
            e1.printStackTrace();
        }
        return null;
    }

    public static Space jsonToSpace(String string) {
        Space space = new Space();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        try {
            mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

            space = mapper.readValue(string, Space.class);
            return space;
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static ArrayList<Space> parseJsonList(String wsResponse) throws DataException {

        if (wsResponse == null || TextUtils.isEmpty(wsResponse))
            return null;
        ArrayList<Space> spaceList = new ArrayList<Space>();

        if (LOG_ALL)
            Log.v(TAG, Utils.getMethodName() + wsResponse);

        ObjectMapper objectMapper = new ObjectMapper();
        JsonFactory jsonFactory = new JsonFactory();
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

        try {
            JsonParser jp = jsonFactory.createJsonParser(wsResponse);
            // advance stream to START_ARRAY first:
            jp.nextToken();
            // and then each time, advance to opening START_OBJECT
            while (jp.nextToken() == JsonToken.START_OBJECT) {
                Space space = objectMapper.readValue(jp, Space.class);
                // process
                // after binding, stream points to closing END_OBJECT
                spaceList.add(space);

                if (LOG_ALL)
                    Log.v(TAG, Utils.getMethodName() + space.toString());
            }

        } catch (JsonParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return spaceList;
    }
}
