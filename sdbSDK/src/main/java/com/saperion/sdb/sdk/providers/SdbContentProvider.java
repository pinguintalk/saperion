package com.saperion.sdb.sdk.providers;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.provider.BaseColumns;

import com.saperion.sdb.sdk.config.WSConfig;
import com.saperion.sdb.sdk.providers.DocumentContent.DocumentDB;
import com.saperion.sdb.sdk.providers.FolderContent.FolderDB;
import com.saperion.sdb.sdk.providers.LinkContent.LinkDB;
import com.saperion.sdb.sdk.providers.ShareContent.ShareDB;
import com.saperion.sdb.sdk.providers.SpacesContent.SpaceDB;
import com.saperion.sdb.sdk.providers.UserContent.UserDB;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * @author nnn
 */

public final class SdbContentProvider extends SdbRESTfulContentProvider {

    private static final String LOG_TAG = SdbContentProvider.class.getSimpleName();

    private static final boolean LOG_ALL = false;

    protected static final String DATABASE_NAME = "sdb.db";

	public static final String AUTHORITY = "com.saperion.sdb.sdk.providers.SdbContentProvider";

    public static final Uri INTEGRITY_CHECK_URI = Uri.parse("content://" + AUTHORITY
            + "/integrityCheck");

    // TODO file case have to set in client
    public static final String FILE_CACHE_DIR = "/data/data/" + WSConfig.APP_PACKAGE_NAME
            + "file_cache";

    // Version 1 : Creation of the database
    public static final int DATABASE_VERSION = 1;

    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    private Handler handler = new Handler();

    protected FileHandlerFactory mFileHandlerFactory;

    private enum UriType {
        DB_SUPPORTED_RENDER_FORMAT(ShareDB.TABLE_NAME, ShareDB.TABLE_NAME, ShareDB.TYPE_DIR_TYPE),
        DB_SUPPORTED_RENDER_FORMAT_ID(ShareDB.TABLE_NAME+ "/#", ShareDB.TABLE_NAME, ShareDB.TYPE_ELEM_TYPE),
        DB_SHARE(ShareDB.TABLE_NAME, ShareDB.TABLE_NAME, ShareDB.TYPE_DIR_TYPE),
        DB_SHARE_ID(ShareDB.TABLE_NAME+ "/#", ShareDB.TABLE_NAME, ShareDB.TYPE_ELEM_TYPE),        
        DB_LINK(LinkDB.TABLE_NAME, LinkDB.TABLE_NAME, LinkDB.TYPE_DIR_TYPE),
        DB_LINK_ID(LinkDB.TABLE_NAME+ "/#", LinkDB.TABLE_NAME, LinkDB.TYPE_ELEM_TYPE),
        DB_USER(UserDB.TABLE_NAME, UserDB.TABLE_NAME, UserDB.TYPE_DIR_TYPE),
        DB_USER_ID(UserDB.TABLE_NAME+ "/#", UserDB.TABLE_NAME, UserDB.TYPE_ELEM_TYPE),
        DB_SPACE(SpaceDB.TABLE_NAME, SpaceDB.TABLE_NAME, SpaceDB.TYPE_DIR_TYPE),
        DB_SPACE_ID(SpaceDB.TABLE_NAME + "/#", SpaceDB.TABLE_NAME, SpaceDB.TYPE_ELEM_TYPE),
        DB_FOLDER(FolderDB.TABLE_NAME, FolderDB.TABLE_NAME, FolderDB.TYPE_DIR_TYPE),
        DB_FOLDER_ID(FolderDB.TABLE_NAME + "/#", FolderDB.TABLE_NAME, FolderDB.TYPE_ELEM_TYPE),
        DB_DOCUMENT(DocumentDB.TABLE_NAME, DocumentDB.TABLE_NAME, DocumentDB.TYPE_DIR_TYPE),
        DB_DOCUMENT_ID(DocumentDB.TABLE_NAME + "/#", DocumentDB.TABLE_NAME,
                DocumentDB.TYPE_ELEM_TYPE),
        DB_DOCUMENT_FILE(DocumentDB.TABLE_DOC_FILES + "/#", DocumentDB.TABLE_DOC_FILES,
                DocumentDB.TYPE_DIR_TYPE),
        DB_DOCUMENT_FILE_ID(DocumentDB.TABLE_DOC_FILES + "/#", DocumentDB.TABLE_DOC_FILES,
                DocumentDB.TYPE_ELEM_TYPE);

        private String mTableName;
        private String mType;

        UriType(String matchPath, String tableName, String type) {
            mTableName = tableName;
            mType = type;
            sUriMatcher.addURI(AUTHORITY, matchPath, ordinal());
        }

        String getTableName() {
            return mTableName;
        }

        String getType() {
            return mType;
        }
    }

    static {
        // Ensures UriType is initialized
        UriType.values();
    }

    private static UriType matchUri(Uri uri) {
        int match = sUriMatcher.match(uri);
        if (match < 0) {
            throw new IllegalArgumentException("Unknown URI " + uri);
        }
        return UriType.class.getEnumConstants()[match];
    }

    private SQLiteDatabase mDatabase;

    @SuppressWarnings("deprecation")
    public synchronized SQLiteDatabase getDatabase(Context context) {
        // Always return the cached database, if we've got one
        if (mDatabase == null || !mDatabase.isOpen()) {
            DatabaseHelper helper = new DatabaseHelper(context, DATABASE_NAME);
            mDatabase = helper.getWritableDatabase();
            if (mDatabase != null) {
                mDatabase.setLockingEnabled(true);
            }
        }

        return mDatabase;
    }

    private class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context, String name) {
            super(context, name, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.v(LOG_TAG, "Creating space database");

            // Create all tables here; each class has its own method
            if (LOG_ALL) {
                Log.d(LOG_TAG, "DbSpace | createTable start");
            }
            SpaceDB.createTable(db);
            FolderDB.createTable(db);
            DocumentDB.createTable(db);
            UserDB.createTable(db);
            LinkDB.createTable(db);
            ShareDB.createTable(db);
            if (LOG_ALL) {
                Log.d(LOG_TAG, "DbSpace | createTable end");
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            // Upgrade all tables here; each class has its own method
            if (LOG_ALL) {
                Log.d(LOG_TAG, "DbSpace | upgradeTable start");
            }

            // delete all data before , ONLY for test
            delete(SpaceDB.CONTENT_URI, null, null);
            delete(FolderDB.CONTENT_URI, null, null);
            delete(DocumentDB.CONTENT_URI, null, null);
            delete(UserDB.CONTENT_URI, null, null);
            delete(LinkDB.CONTENT_URI, null, null);
            delete(ShareDB.CONTENT_URI, null, null);
			/*
             SpaceDB.upgradeTable(db, oldVersion, newVersion);
			 FolderDB.upgradeTable(db, oldVersion, newVersion);
			 DocumentDB.upgradeTable(db, oldVersion, newVersion);
			 */

            if (LOG_ALL) {
                Log.d(LOG_TAG, "DbSpace | upgradeTable end");
            }
        }

        @Override
        public void onOpen(SQLiteDatabase db) {
        }
    }

    /**
     * Provides read only access to files that have been downloaded and stored
     * in the provider cache. Specifically, in this provider, clients can
     * access the files of downloaded thumbnail images.
     */
    @Override
    public ParcelFileDescriptor openFile(Uri uri, String mode) throws FileNotFoundException {
        Log.d(LOG_TAG, Utils.getMethodName() + " uri=" + uri + "mode: " + mode);
		/*
		// only support read only files
		if (!"r".equals(mode.toLowerCase())) {
		    throw new FileNotFoundException("Unsupported mode, " + mode + ", for uri: " + uri);
		}
		*/
		/*
		if (URI_MATCHER.match(uri) != PHOTO_ID) {
		    throw new IllegalArgumentException
		            ("URI invalid. Use an id-based URI only.");
		}
		*/
        return openFileHelper(uri, mode);

    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {

        UriType uriType = matchUri(uri);
        Context context = getContext();

        // Pick the correct database for this operation
        SQLiteDatabase db = getDatabase(context);
        String id = "0";

        if (LOG_ALL) {
            Log.d(LOG_TAG, Utils.getMethodName() + "delete: uri=" + uri + "selection: " + selection);
        }

        int result = -1;

        switch (uriType) {
            case DB_SHARE_ID:
            case DB_LINK_ID:
            case DB_USER_ID:
            case DB_SPACE_ID:
            case DB_FOLDER_ID:
            case DB_DOCUMENT_ID:
                Log.v(LOG_TAG, Utils.getMethodName() + "deleting : uri=" + uri + "id: " + id);

                id = uri.getPathSegments().get(1);
                result = db.delete(uriType.getTableName(), whereWithId(id, selection), selectionArgs);
                break;
            case DB_SHARE:
            case DB_LINK:
            case DB_USER:
            case DB_SPACE:
            case DB_FOLDER:
            case DB_DOCUMENT:

                result = db.delete(uriType.getTableName(), selection, selectionArgs);

                break;
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return result;
    }

    @Override
    public String getType(Uri uri) {
        return matchUri(uri).getType();
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {

        UriType uriType = matchUri(uri);
        Context context = getContext();

        // Pick the correct database for this operation
        SQLiteDatabase db = getDatabase(context);
        long id;

        if (LOG_ALL) {
            Log.v(LOG_TAG, "insert: uri=" + uri + ", match is " + uriType.name());
        }

        Uri resultUri = null;

        switch (uriType) {
            case DB_SHARE:
            case DB_LINK:
            case DB_USER:
            case DB_SPACE:
            case DB_FOLDER:
            case DB_DOCUMENT:
                id = db.insert(uriType.getTableName(), "foo", values);
                resultUri = id == -1 ? null : ContentUris.withAppendedId(uri, id);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        // Notify with the base uri, not the new uri (nobody is watching a new
        // record)
        getContext().getContentResolver().notifyChange(uri, null);
        return resultUri;
    }

    @Override
    public ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> operations)
            throws OperationApplicationException {

        if (LOG_ALL) {
            Log.v(LOG_TAG, Utils.getMethodName() + "entry");
        }
        SQLiteDatabase db = getDatabase(getContext());
        db.beginTransaction();
        try {
            int numOperations = operations.size();
            ContentProviderResult[] results = new ContentProviderResult[numOperations];
            for (int i = 0; i < numOperations; i++) {
                results[i] = operations.get(i).apply(this, results, i);
                db.yieldIfContendedSafely();
            }
            db.setTransactionSuccessful();
            return results;
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {

        Log.v(LOG_TAG, Utils.getMethodName() + "uri=" + uri);

        UriType uriType = matchUri(uri);
        Context context = getContext();

        // Pick the correct database for this operation
        SQLiteDatabase db = getDatabase(context);

        if (LOG_ALL) {
            Log.d(LOG_TAG, "bulkInsert: uri=" + uri + ", match is " + uriType.name());
        }

        int numberInserted = 0;
        SQLiteStatement insertStmt;

        db.beginTransaction();
        try {
            switch (uriType) {
                case DB_SHARE:
                    insertStmt = db.compileStatement(ShareDB.getBulkInsertString());
                    for (ContentValues value : values) {
                        ShareDB.bindValuesInBulkInsert(insertStmt, value);
                        insertStmt.execute();
                        insertStmt.clearBindings();
                    }
                    insertStmt.close();
                    db.setTransactionSuccessful();
                    numberInserted = values.length;

                    if (LOG_ALL) {
                        Log.d(LOG_TAG, "bulkInsert: uri=" + uri + " | nb inserts : " + numberInserted);
                    }
                    break;
                case DB_LINK:
                    insertStmt = db.compileStatement(LinkDB.getBulkInsertString());
                    for (ContentValues value : values) {
                        LinkDB.bindValuesInBulkInsert(insertStmt, value);
                        insertStmt.execute();
                        insertStmt.clearBindings();
                    }
                    insertStmt.close();
                    db.setTransactionSuccessful();
                    numberInserted = values.length;

                    if (LOG_ALL) {
                        Log.d(LOG_TAG, "bulkInsert: uri=" + uri + " | nb inserts : " + numberInserted);
                    }
                    break;
                case DB_USER:
                    insertStmt = db.compileStatement(UserDB.getBulkInsertString());
                    for (ContentValues value : values) {
                        UserDB.bindValuesInBulkInsert(insertStmt, value);
                        insertStmt.execute();
                        insertStmt.clearBindings();
                    }
                    insertStmt.close();
                    db.setTransactionSuccessful();
                    numberInserted = values.length;

                    if (LOG_ALL) {
                        Log.d(LOG_TAG, "bulkInsert: uri=" + uri + " | nb inserts : " + numberInserted);
                    }
                    break;
                
                case DB_SPACE:
                    insertStmt = db.compileStatement(SpaceDB.getBulkInsertString());
                    for (ContentValues value : values) {
                        SpaceDB.bindValuesInBulkInsert(insertStmt, value);
                        insertStmt.execute();
                        insertStmt.clearBindings();
                    }
                    insertStmt.close();
                    db.setTransactionSuccessful();
                    numberInserted = values.length;

                    if (LOG_ALL) {
                        Log.d(LOG_TAG, "bulkInsert: uri=" + uri + " | nb inserts : " + numberInserted);
                    }
                    break;
                case DB_FOLDER:
                    insertStmt = db.compileStatement(FolderDB.getBulkInsertString());
                    for (ContentValues value : values) {
                        FolderDB.bindValuesInBulkInsert(insertStmt, value);
                        insertStmt.execute();
                        insertStmt.clearBindings();
                    }
                    insertStmt.close();
                    db.setTransactionSuccessful();
                    numberInserted = values.length;

                    if (LOG_ALL) {
                        Log.d(LOG_TAG, "bulkInsert: uri=" + uri + " | nb inserts : " + numberInserted);
                    }
                    break;
                case DB_DOCUMENT:
                    insertStmt = db.compileStatement(DocumentDB.getBulkInsertString());
                    for (ContentValues value : values) {
                        DocumentDB.bindValuesInBulkInsert(insertStmt, value);
                        insertStmt.execute();
                        insertStmt.clearBindings();
                    }
                    insertStmt.close();
                    db.setTransactionSuccessful();
                    numberInserted = values.length;

                    if (LOG_ALL) {
                        Log.d(LOG_TAG, "bulkInsert: uri=" + uri + " | nb inserts : " + numberInserted);
                    }
                    break;

                default:
                    throw new IllegalArgumentException("Unknown URI " + uri);
            }
        } finally {
            db.endTransaction();
        }

        // Notify with the base uri, not the new uri (nobody is watching a new
        // record)
        context.getContentResolver().notifyChange(uri, null);
        return numberInserted;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {

        // Log.v(LOG_TAG, Utils.getMethodName() + "query: uri=" + uri);

        Cursor c = null;
        Uri notificationUri = SpacesContent.CONTENT_URI;
        UriType uriType = matchUri(uri);
        Context context = getContext();
        // Pick the correct database for this operation
        SQLiteDatabase db = getDatabase(context);
        String id;

        if (LOG_ALL) {
            Log.v(LOG_TAG, "query: uri=" + uri + ", match is " + uriType.name());
        }

        switch (uriType) {
            case DB_SHARE_ID:
            case DB_LINK_ID:
            case DB_USER_ID:
            case DB_SPACE_ID:
            case DB_FOLDER_ID:
            case DB_DOCUMENT_ID:
                id = uri.getPathSegments().get(1);
                c =
                        db.query(uriType.getTableName(), projection, whereWithId(id, selection),
                                selectionArgs, null, null, sortOrder);
                break;
            case DB_SHARE:
            case DB_LINK:
            case DB_USER:
            case DB_SPACE:
            case DB_FOLDER:
            case DB_DOCUMENT:
                c =
                        db.query(uriType.getTableName(), projection, selection, selectionArgs, null,
                                null, sortOrder);
                break;
        }

        if ((c != null) && !isTemporary()) {
            c.setNotificationUri(getContext().getContentResolver(), notificationUri);
        }
        return c;
    }

    private String whereWithId(String id, String selection) {
        StringBuilder sb = new StringBuilder(256);
        sb.append(BaseColumns._ID);
        sb.append(" = ");
        sb.append(id);
        if (selection != null) {
            sb.append(" AND (");
            sb.append(selection);
            sb.append(')');
        }
        return sb.toString();
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {

        UriType uriType = matchUri(uri);
        Context context = getContext();

        Log.v(LOG_TAG, Utils.getMethodName() + "entry");

        // Pick the correct database for this operation
        SQLiteDatabase db = getDatabase(context);

        if (LOG_ALL) {
            Log.v(LOG_TAG, "update: uri=" + uri + ", match is " + uriType.name());
        }

        int result = -1;

        int numberInserted = 0;


        switch (uriType) {
            case DB_SHARE_ID:
            case DB_LINK_ID:
            case DB_USER_ID:
            case DB_FOLDER_ID:
            case DB_SPACE_ID:
            case DB_DOCUMENT_ID:
                String id = uri.getPathSegments().get(1);

                //Log.v(LOG_TAG, "id " + id + " updating folder values =" + values.toString());

                result =
                        db.update(uriType.getTableName(), values, whereWithId(id, selection),
                                selectionArgs);

                // TODO We replace with this method to insert or update element in db            	
                // db.replace(uriType.getTableName(), null, values);

                break;
            case DB_SHARE:
            case DB_LINK:
            case DB_USER:
            case DB_SPACE:
            case DB_FOLDER:
            case DB_DOCUMENT:
                result = db.update(uriType.getTableName(), values, selection, selectionArgs);
                //Log.v(LOG_TAG, "updating folder values =" + values.toString());

                if (LOG_ALL) {
                    Log.v(LOG_TAG, "update: uri=" + uri + ", match is " + uriType.name());
                }
                // TODO We replace with this method to insert or update element in db            	
                // db.replace(uriType.getTableName(), "foo", values);                
                break;

        }

        getContext().getContentResolver().notifyChange(uri, null);
        return result;
    }

    @Override
    protected ResponseHandler newResponseHandler(String requestTag) {
        return null;
    }

    @Override
    public boolean onCreate() {
        super.onCreate();
        Log.v(LOG_TAG, Utils.getMethodName() + "entry");

        mFileHandlerFactory = new FileHandlerFactory(FILE_CACHE_DIR);
        return true;
    }

	/* TODO how to end cancel runnable ?!
	@Override
	public void onDestroy() {
		 Log.v(LOG_TAG, Utils.getMethodName() + "entry");
		 
		 handler.removeCallback(refreshDataRunnable);
		    
	    return true;
	}
	*/

}
