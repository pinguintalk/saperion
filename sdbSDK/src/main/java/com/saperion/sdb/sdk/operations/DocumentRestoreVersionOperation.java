package com.saperion.sdb.sdk.operations;

import android.content.Context;
import android.os.Bundle;

import com.saperion.sdb.sdk.config.WSConfig;
import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.models.Document;
import com.saperion.sdb.sdk.models.Version;
import com.saperion.sdb.sdk.network.NetworkConnection;
import com.saperion.sdb.sdk.network.NetworkConnection.ConnectionResult;
import com.saperion.sdb.sdk.network.NetworkConnection.Method;
import com.saperion.sdb.sdk.providers.ContentProviderHelpers;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.security.CredentialManager;
import com.saperion.sdb.sdk.service.RequestService.Operation;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

import org.apache.http.HttpStatus;
import org.apache.http.auth.UsernamePasswordCredentials;

import java.io.File;
import java.io.IOException;

public class DocumentRestoreVersionOperation implements Operation {

    private static final String TAG = DocumentRestoreVersionOperation.class.getSimpleName();
    public static final String PARAM_PAYLOAD = "com.saperion.sdb.sdk.document.param_payload";
    public static final String PARAM_DOC_ID = "com.saperion.sdb.sdk.document.param_doc_id";

    @Override
    public Bundle execute(Context context, Request request) throws ConnectionException,
            DataException, ServiceRequestException {

        String payload = request.getString(PARAM_PAYLOAD);
        String docId = request.getString(PARAM_DOC_ID);
        Log.v(TAG, Utils.getMethodName() + " itemId: " + docId + " payload: " + payload);

        String url = WSConfig.getDocumentURL() + "/" + docId + "/versions";

        NetworkConnection networkConnection = new NetworkConnection(context, url);
        networkConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager
                .getInstance().getUserName(), CredentialManager.getInstance().getmAccessToken()));

        networkConnection.setMethod(Method.POST);

        networkConnection.setPayload(payload);
        Bundle bundle = new Bundle();

        ConnectionResult result = networkConnection.execute();

        if (result == null) {

            Log.e(TAG, Utils.getMethodName() + " result == null ");

            return bundle;
        }

        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);

        if (result.responseCode != HttpStatus.SC_CREATED) {
            Log.w(TAG, Utils.getMethodName() + " responseCode:" + result.responseCode);
            bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);
            bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
            return bundle;
        }


        Log.v(TAG, Utils.getMethodName() + " restore document version done");
        Document local_doc = ContentProviderHelpers.documentGetById(context, docId);

        Version newVersion = Version.fromJson(result.body);
        String fileExt = Utils.getFileExtension(local_doc.getMimeType());

        String oldFilePath = Utils.getDocumentsDirectory(context) + local_doc.getId() + local_doc.getVersionNumber() + fileExt;
        File oldFile = new File(oldFilePath);
        String newFilePath = Utils.getDocumentsDirectory(context) + local_doc.getId() + newVersion.getNumber() + fileExt;

        if (oldFile.exists()) {
            File newFile = new File(newFilePath);
            try {
                Utils.fileCopy(oldFile, newFile);
            } catch (IOException e) {
                throw new DataException(e.getMessage());
            }
        }

        local_doc.setFileAbsPath(newFilePath);
        local_doc.setVersionNumber(newVersion.getNumber());
        ContentProviderHelpers.documentReplace(context, local_doc);
        bundle.putParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, local_doc);

        return bundle;

    }

}
