package com.saperion.sdb.sdk.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.saperion.sdb.sdk.utils.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SystemInfo extends Typed implements Parcelable {

    private static final String TAG = SystemInfo.class.getSimpleName();
    
    private String version;
    private String buildTime;
    private String buildNumber;
    private Feature[] features;
    private SupportedRenderFormat[] supportedRenderFormats;
    private String[] supportedLanguages;

    public SystemInfo() {
        super(ModelType.SYSTEM);
    }
    
    public String getBuildNumber() {
        return buildNumber;
    }

    public void setBuildNumber(String buildNumber) {
        this.buildNumber = buildNumber;
    }

    public Feature[] getFeatures() {
        return features;
    }

    public void setFeatures(Feature[] features) {
        this.features = features;
    }

    public SupportedRenderFormat[] getSupportedRenderFormats() {
        return supportedRenderFormats;
    }

    public void setSupportedRenderFormats(SupportedRenderFormat[] supportedRenderFormats) {
        this.supportedRenderFormats = supportedRenderFormats;
    }

    public String[] getSupportedLanguages() {
        return supportedLanguages;
    }

    public void setSupportedLanguages(String[] supportedLanguages) {
        this.supportedLanguages = supportedLanguages;
    }
   

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getBuildTime() {
        return buildTime;
    }

    public void setBuildTime(String buildTime) {
        this.buildTime = buildTime;
    }


    public static SystemInfo fromJson(String json) {

        SystemInfo systemInfo = new SystemInfo();
        Feature[] featureList = null;
        SupportedRenderFormat[] supportedRenderFormatList = null;
        String[] supportedLanguageList = null;
        try {
            JSONObject settingsObject = new JSONObject(json);

            if (settingsObject.has("version")){
                systemInfo.setVersion(settingsObject.getString("version"));
            }

            if (settingsObject.has("buildNumber")){
                systemInfo.setBuildNumber(settingsObject.getString("buildNumber"));
            }

            if (settingsObject.has("buildTime")){
                systemInfo.setBuildTime(settingsObject.getString("buildTime"));
            }
            
            if (settingsObject.has("features")){
            
                JSONArray list_array = settingsObject.getJSONArray("features");

                if (list_array.length() > 0) {
                    featureList = new Feature[list_array.length()];
                    for (int s=0; s < list_array.length(); s++) {
                        featureList[s] = Feature.fromJson(list_array.get(s).toString());
                    }
                    systemInfo.setFeatures(featureList);
                }
            }
            if (settingsObject.has("supportedRenderFormats")){

                JSONArray list_array = settingsObject.getJSONArray("supportedRenderFormats");

                if (list_array.length() > 0) {
                    supportedRenderFormatList = new SupportedRenderFormat[list_array.length()];
                    for (int s=0; s < list_array.length(); s++) {
                        supportedRenderFormatList[s] = SupportedRenderFormat.fromJson(list_array.get(s).toString());
                    }
                    systemInfo.setSupportedRenderFormats(supportedRenderFormatList);
                }
            }
            if (settingsObject.has("supportedLanguages")){

                JSONArray list_array = settingsObject.getJSONArray("supportedLanguages");

                if (list_array.length() > 0) {
                    supportedLanguageList = new String[list_array.length()];
                    for (int s=0; s < list_array.length(); s++) {
                        supportedLanguageList[s] = list_array.get(s).toString();
                    }
                    systemInfo.setSupportedLanguages(supportedLanguageList);
                }
            }
        } catch (JSONException e) {

            Log.e(TAG, "error parson json" + e.toString());
            e.printStackTrace();
        }

        return systemInfo;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.getType());
        out.writeString(this.getVersion());
        out.writeString(this.getBuildNumber());
        out.writeString(this.getBuildTime());
        out.writeParcelableArray(this.getFeatures(), 0);
        out.writeParcelableArray(this.getSupportedRenderFormats(), 0);
        out.writeStringArray(this.getSupportedLanguages());
    }

    private SystemInfo(Parcel in) {

        this.setType(in.readString());
        this.setVersion(in.readString());
        this.setBuildNumber(in.readString());
        this.setBuildTime(in.readString());
        
        Parcelable[] features_parcels = in.readParcelableArray(Feature.class.getClassLoader());
        Feature[] list_tmp = new Feature[features_parcels.length];

        for (int p = 0; p < features_parcels.length; p++)
        {
            list_tmp[p] = (Feature) features_parcels[p];

        }
        setFeatures(list_tmp);


        Parcelable[] supportedFormats_parcels = in.readParcelableArray(SupportedRenderFormat.class.getClassLoader());
        SupportedRenderFormat[] suppFormatList = new SupportedRenderFormat[supportedFormats_parcels.length];

        for (int p = 0; p < features_parcels.length; p++)
        {
            suppFormatList[p] = (SupportedRenderFormat) features_parcels[p];

        }
        setSupportedRenderFormats(suppFormatList);
        
        in.readStringArray(supportedLanguages);
        
    }

    public static final Creator CREATOR = new Creator() {
        public SystemInfo createFromParcel(Parcel in) {
            return new SystemInfo(in);
        }

        public SystemInfo[] newArray(int size) {
            return new SystemInfo[size];
        }
    };

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public String toString() {
        return "SystemInfo{" + "version='" + version + '\'' + ", buildNumber='" + buildNumber +  ", buildTime='" + buildTime + '\''
                + ", supportedLanguages='" + supportedLanguages.toString() +  ", supportedRenderFormats='" + supportedRenderFormats.toString() +
                ", features='" + features.toString() + "} " + super.toString();
    }
    
    
}
