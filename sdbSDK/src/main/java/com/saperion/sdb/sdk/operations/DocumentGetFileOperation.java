package com.saperion.sdb.sdk.operations;

import android.content.Context;
import android.os.Bundle;

import com.saperion.sdb.sdk.Constants;
import com.saperion.sdb.sdk.config.WSConfig;
import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.factory.DocumentJsonFactory;
import com.saperion.sdb.sdk.models.Document;
import com.saperion.sdb.sdk.network.NetworkConnection;
import com.saperion.sdb.sdk.network.NetworkConnection.ConnectionResult;
import com.saperion.sdb.sdk.providers.ContentProviderHelpers;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.security.CredentialManager;
import com.saperion.sdb.sdk.service.RequestService.Operation;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

import org.apache.http.auth.UsernamePasswordCredentials;

import java.io.File;
import java.util.HashMap;

public final class DocumentGetFileOperation implements Operation {

    private static final String TAG = DocumentGetFileOperation.class.getSimpleName();

    public static final String PARAM_DOCUMENT = Constants.SDK_PACKAGE_NAME + ".param_document";

    @Override
    public Bundle execute(Context context, Request request) throws ServiceRequestException, ConnectionException,
            DataException {
        Bundle bundle = new Bundle();
        Log.v(TAG, Utils.getMethodName() + "entry");

        Document cached_document = (Document) request.getParcelable(PARAM_DOCUMENT);
        String id = cached_document.getId();

        File cached_file = new File(cached_document.getFileAbsPath());

        if (cached_file.exists()) { // we have a cached file stored, so we need to check if the file is really up to date

            String url = WSConfig.getDocumentURL() + "/" + id + WSConfig.WS_INCLUDE_MODIFICATION_ID;
            NetworkConnection getDocuConnection = new NetworkConnection(context, url);

            HashMap<String, String> header_list = new HashMap<String, String>();
            header_list.put("ACCEPT", "application/json");
            getDocuConnection.setHeaderList(header_list);

            getDocuConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager.getInstance().getUserName(),
                    CredentialManager.getInstance().getmAccessToken()));

            ConnectionResult result = getDocuConnection.execute();

            bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE,
                    result.responseCode);
            if (result.responseCode != ResponseConstants.RESPONSE_CODE_OK) {
                bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
                return bundle;
            }

            Document new_document = DocumentJsonFactory.jsonToDocument(result.body);

            // TODO Check ModificationId  Why ModificationId == null


            if (new_document.getHash().equals(cached_document.getHash())) { // cached file is up to date, so we can uses it
                Log.v(TAG, Utils.getMethodName() + " new_document.getHash() :" + new_document.getHash() + " cached_document.getHash() :"
                        + cached_document.getHash());

                bundle.putParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, cached_document);
                return bundle;
            }

        }


        bundle = new Bundle();
        // cached file is NOT up to date or NOT exists, we have to download it
        String url = WSConfig.getDocumentURL() + "/" + id + "/" + WSConfig.WS_FILE;
        String minetype = cached_document.getMimeType();
        if (minetype.contains("ppt") || minetype.contains("doc") || minetype.contains("docx")) {
            url = WSConfig.getDocumentURL() + "/" + id + "/" + WSConfig.WS_RENDITION;
        }

        NetworkConnection networkConnection = new NetworkConnection(context, url);

        HashMap<String, String> requestParams = new HashMap<String, String>();
        //String contentfile_content_uri = ContentProviderHelpers.documentGetFileUri(context,id).toString();
        String file_path_without_extension =
                OperationHelpers.documentGetFilePathWithoutExtension(context, id);
        requestParams.put(NetworkConnection.FILE_PATH, file_path_without_extension);
        requestParams.put(NetworkConnection.REQUEST_TYPE,
                NetworkConnection.REEQUEST_TYPE_DOWNLOAD);
        networkConnection.setRequestParams(requestParams);

        HashMap<String, String> header_list = new HashMap<String, String>();
        header_list.put("ACCEPT", "application/octet-stream");
        networkConnection.setHeaderList(header_list);

        Log.w(TAG, Utils.getMethodName() + "downloading to file_path_without_extension:"
                + file_path_without_extension);

        networkConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager
                .getInstance().getUserName(), CredentialManager.getInstance().getmAccessToken()));

        ConnectionResult result = networkConnection.execute();

        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);

        if (result.responseCode != ResponseConstants.RESPONSE_CODE_OK) {
            Log.w(TAG, Utils.getMethodName() + " responseCode:" + result.responseCode);

            bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
            return bundle;
        }
        //TODO
        //bundle.putByte(, value);

        // update file path

        ContentProviderHelpers.documentUpdateFilePath(context, cached_document, result.body);
        bundle.putParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, cached_document);
        return bundle;
    }

}
