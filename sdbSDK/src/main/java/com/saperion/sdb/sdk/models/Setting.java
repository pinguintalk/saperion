package com.saperion.sdb.sdk.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.saperion.sdb.sdk.exceptions.DataException;

import java.io.IOException;
import java.util.ArrayList;

@JsonIgnoreProperties({ "id"})
public class Setting extends TypedIdentifiable implements Parcelable {

    public static final String MAX_ACCESS_COUNT_KEY_NAME = "externallink.accesscount.max";
    public static final String MAX_EXPIRATION_DATE_KEY_NAME = "externallink.expirationdate.days";
    public static final String PASSWORD_REQUIRED_KEY_NAME = "externallink.password.required";
    
    /*

The setting model:
        {
        "type":	"setting",
        "key":	"	<the setting’s key>",
        "value":	"<the setting’s value"
        }
        */
    
    private String key;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Setting() {
        super(ModelType.SETTING);
    }


    @JsonCreator
    public Setting(@JsonProperty("type") String type,
                   @JsonProperty("key") String key,
                   @JsonProperty("value") String value) {

        setType(type);
        this.key = key;
        this.value = value;
    }
    
    public String toJson() {

        ObjectMapper mapper = new ObjectMapper();

        mapper.setSerializationInclusion(Include.NON_NULL);
        try {
            return mapper.writeValueAsString(this);

        } catch (JsonProcessingException e1) {
            e1.printStackTrace();
        }
        return null;
    }

    public static Setting fromJson(String string) {

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        try {
            mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

            return mapper.readValue(string, Setting.class);

        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.getType());
        out.writeString(this.getKey());
        out.writeString(this.getValue());
    }

    private Setting(Parcel in) {

        this.setType(in.readString());
        this.setKey(in.readString());
        this.setValue(in.readString());

    }

    public static final Creator CREATOR = new Creator() {
        public Setting createFromParcel(Parcel in) {
            return new Setting(in);
        }

        public Setting[] newArray(int size) {
            return new Setting[size];
        }
    };

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public String toString() {
        return "Version{" + "key=" + key + ", value='" + value  + '\'' + "} "
                + super.toString();
    }


    public static ArrayList<Setting> parseJsonArray(String wsResponse) throws DataException {

        if (wsResponse == null || TextUtils.isEmpty(wsResponse))
            return null;

        ArrayList<Setting> settingList = new ArrayList<Setting>();

        ObjectMapper objectMapper = new ObjectMapper();
        JsonFactory jsonFactory = new JsonFactory();

        try {
            JsonParser jp = jsonFactory.createJsonParser(wsResponse);
            // advance stream to START_ARRAY first:
            jp.nextToken();
            // and then each time, advance to opening START_OBJECT
            while (jp.nextToken() == JsonToken.START_OBJECT) {
                Setting setting = objectMapper.readValue(jp, Setting.class);


                // process
                // after binding, stream points to closing END_OBJECT
                settingList.add(setting);

            }

        } catch (JsonParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return settingList;
    }


}
