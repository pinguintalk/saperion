package com.saperion.sdb.sdk.operations;

import android.content.Context;
import android.os.Bundle;

import com.saperion.sdb.sdk.SdbApplication;
import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.factory.UserJsonFactory;
import com.saperion.sdb.sdk.models.User;
import com.saperion.sdb.sdk.network.NetworkConnection.ConnectionResult;
import com.saperion.sdb.sdk.providers.ContentProviderHelpers;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.service.RequestService.Operation;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public final class UserGetListOperation implements Operation {

    private static final String TAG = UserGetListOperation.class.getSimpleName();

    
    @Override
    public Bundle execute(Context context, Request request)
            throws ServiceRequestException, ConnectionException, DataException {

        Log.v(TAG, Utils.getMethodName() + "entry");
        Bundle bundle = new Bundle();

        // waiting for response and parse it
        ConnectionResult result = OperationHelpers.userGetList(context);

        int responseCode = result.responseCode;
        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, responseCode);
        ArrayList<User> userList = new ArrayList<User>();

        // Log.e(TAG, Utils.getMethodName() + result.body);
        if (responseCode == ResponseConstants.RESPONSE_CODE_NOT_MODIFIED) {
            // get local cached spaces
            userList = ContentProviderHelpers.userGetList(context);
            bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE,
                    ResponseConstants.RESPONSE_CODE_OK);
        } else if (responseCode == ResponseConstants.RESPONSE_CODE_OK) {
            if (!result.body.equals("[]")) {
                userList = UserJsonFactory.parseResult(result.body);
                
                List<String> etag_list = result.headerMap.get("ETag");
                if (etag_list != null ){
                    String Etag = etag_list.get(0).toString();
                    if (Etag != null ) {
                        Log.v(TAG, Utils.getMethodName() + "updating etag:" +Etag);
                        SdbApplication.getInstance().setUserListEtag(Etag);
                    }  
                }
                Log.v(TAG, Utils.getMethodName() + "insert to CP");
                ContentProviderHelpers.userInsertBulk(context, userList);

            }
        } else {
            Log.w(TAG, Utils.getMethodName() + " responseCode:" + result.responseCode);
            bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);
            bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
            return bundle;
        }

        bundle.putParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, userList);
        // Log.w(TAG, Utils.getMethodName() + " userList:" + userList.toString());

        return bundle;
    }

}
