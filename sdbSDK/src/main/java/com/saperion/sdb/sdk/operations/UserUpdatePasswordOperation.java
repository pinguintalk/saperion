package com.saperion.sdb.sdk.operations;

import android.content.Context;
import android.os.Bundle;

import com.saperion.sdb.sdk.config.WSConfig;
import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.network.NetworkConnection;
import com.saperion.sdb.sdk.network.NetworkConnection.ConnectionResult;
import com.saperion.sdb.sdk.network.NetworkConnection.Method;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.security.CredentialManager;
import com.saperion.sdb.sdk.service.RequestService.Operation;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

import org.apache.http.auth.UsernamePasswordCredentials;

public final class UserUpdatePasswordOperation implements Operation {

    private static final String TAG = UserUpdatePasswordOperation.class.getSimpleName();

    public static final String PARAM_PAYLOAD = WSConfig.APP_PACKAGE_NAME + ".param_payload";
    public static final String PARAM_ITEM_ID = WSConfig.APP_PACKAGE_NAME + ".param_user_id";
    public static final String PARAM_NEW_PASSWORD = WSConfig.APP_PACKAGE_NAME + ".param_new_password";

    @Override
    public Bundle execute(Context context, Request request)
            throws ServiceRequestException, ConnectionException, DataException {

        Log.v(TAG, Utils.getMethodName() + "entry");
        Bundle bundle = new Bundle();
  
        String newPassword = request.getString(PARAM_NEW_PASSWORD);
        String payload =  request.getString(PARAM_PAYLOAD);
        String Id = request.getString(PARAM_ITEM_ID);
        String url = WSConfig.getUserUrl() + "/current/password";

        NetworkConnection networkConnection =
                new NetworkConnection(context, url);
        networkConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager
                .getInstance().getUserName(), CredentialManager.getInstance().getmAccessToken()));
        // Log.v(TAG, Utils.getMethodName() + "payload: " + payload);

        networkConnection.setPayload(payload);
  
        networkConnection.setMethod(Method.PUT);
        
        ConnectionResult result = networkConnection.execute();
        
        int responseCode = result.responseCode;
        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, responseCode);

        Log.w(TAG, Utils.getMethodName() + " responseCode:" + result.responseCode);
        if (responseCode == ResponseConstants.RESPONSE_CODE_OK) {

            bundle.putString(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, newPassword);
        }
        else 

            bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);

        return bundle;
    }

}
