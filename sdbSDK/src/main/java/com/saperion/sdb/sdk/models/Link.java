package com.saperion.sdb.sdk.models;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.saperion.sdb.sdk.operations.LinkDeleteOperation;
import com.saperion.sdb.sdk.operations.LinkSaveOperation;
import com.saperion.sdb.sdk.providers.LinkContent.LinkDB;
import com.saperion.sdb.sdk.providers.LinkContent.LinkDB.Columns;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.RequestConstants;
import com.saperion.sdb.sdk.requestmanager.RequestManager.RequestListener;
import com.saperion.sdb.sdk.requestmanager.RequestManagerSingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class Link extends CreatedTypedIdentifiable implements Parcelable {

    private String password;
    private String expirationDate;
    private String url;
    private String name;
    private String documentId;
    private int version;
    private int accessLimit;
    private int currentAccessCount;

    /*
    {
        "type":		"link",
            "id":			"<the id of this link>",
            "documentId":	“<the id of the document this link is related to>”,
        "password":		"<the link password>",
            "accessLimit":	<the access limit count>,
        "currentAccessCount":<the current access count>,
        "expirationDate":	<the expiration date of this link>,
            "url":			<the URL of this link>,
            "version":		<the document version to download via this link >,
            "name":		<a link can have an optional name>,
            "creationDate":	“<creation date of this link>,
            "ownerId":		“<the id of the user who created this link>”,
        "ownerName":	“<the name of the user who created this link>”
    }
    */
    public Link() {
        super(ModelType.LINK);
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public Link setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public Link setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Link setPassword(String password) {
        this.password = password;
        return this;
    }

    public int getAccessLimit() {
        return accessLimit;
    }

    public Link setAccessLimit(int accessLimit) {
        this.accessLimit = accessLimit;
        return this;
    }

    public int getCurrentAccessCount() {
        return currentAccessCount;
    }

    public Link setCurrentAccessCount(int currentAccessCount) {
        this.currentAccessCount = currentAccessCount;
        return this;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Link{" + "password='" + password + '\'' + ", expirationDate='" + expirationDate
                + '\'' + ", url='" + url + '\'' + ", name='" + name + '\'' + ", version=" + version
                + ", accessLimit=" + accessLimit + ", currentAccessCount=" + currentAccessCount
                + "} " + super.toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    
    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.getOwnerId());
        parcel.writeString(this.getOwnerName());
        parcel.writeString(this.getCreationDate());
        parcel.writeString(this.getId());
        parcel.writeString(this.getType());
        parcel.writeString(this.getPassword());
        parcel.writeString(this.getExpirationDate());
        parcel.writeString(this.getUrl());
        parcel.writeString(this.getName());
        parcel.writeString(this.getDocumentId());
        parcel.writeInt(this.getVersion());
        parcel.writeInt(this.getAccessLimit());
        parcel.writeInt(this.getCurrentAccessCount());
    }

    private Link(Parcel in) {
        this.setOwnerId(in.readString());
        this.setOwnerName(in.readString());
        this.setCreationDate(in.readString());
        this.setId(in.readString());
        this.setType(in.readString());
        this.setPassword(in.readString());
        this.setExpirationDate(in.readString());
        this.setUrl(in.readString());
        this.setName(in.readString());
        this.setDocumentId(in.readString());
        this.setVersion(in.readInt());
        this.setAccessLimit(in.readInt());
        this.setCurrentAccessCount(in.readInt());
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Link createFromParcel(Parcel in) {
            return new Link(in);
        }

        public Link[] newArray(int size) {
            return new Link[size];
        }
    };


    public static Link fromJson(String string) {
        Link link = new Link();
        ObjectMapper mapper = new ObjectMapper();
        try {
            link = mapper.readValue(string, Link.class);
            return link;
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String toJson(Link link) {

        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(Include.NON_NULL);
        try {
            String f = mapper.writeValueAsString(link);
            return f;
        } catch (JsonProcessingException e1) {
            e1.printStackTrace();
        }
        return null;
    }
    
     public void save(RequestListener listener) {


            Request request = new Request(RequestConstants.REQUEST_TYPE_DOCUMENT_CREATE_LINK);
            request.put(LinkDeleteOperation.PARAM_DOC_ID,this.getDocumentId());

            String payload = "";
 
                JSONObject jo = new JSONObject();
                try {
                    jo.put(LinkDB.Columns.TYPE.getName(), this.getType());

                    jo.put(LinkDB.Columns.DOCUMENT_ID.getName(), this.getDocumentId());
                    jo.put(LinkDB.Columns.NAME.getName(), this.getName());
                    jo.put(LinkDB.Columns.PASSWORD.getName(), this.getPassword());
                    jo.put(LinkDB.Columns.ACCESS_LIMIT.getName(), this.getAccessLimit());
                    jo.put(LinkDB.Columns.EXPIRATION_DATE.getName(), this.getExpirationDate());
                    payload =jo.toString();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            

            request.put(LinkSaveOperation.PARAM_PAYLOAD,jo.toString());
            if (RequestManagerSingleton.isCreated())
                RequestManagerSingleton.getInstance().execute(request, listener);
        
    }


    public void delete(RequestListener listener) {

        Request request = new Request(RequestConstants.REQUEST_TYPE_DOCUMENT_DELETE_LINK);
        request.put(LinkDeleteOperation.PARAM_DOC_ID,this.getDocumentId());
        request.put(LinkDeleteOperation.PARAM_LINK_ID,this.getId());
        if (RequestManagerSingleton.isCreated())
            RequestManagerSingleton.getInstance().execute(request, listener);
    }

    public ContentValues toContentValues() {
        
        ContentValues cv = new ContentValues();
        cv.put(LinkDB.Columns.ID.getName(), this.getId());
        cv.put(LinkDB.Columns.NAME.getName(), this.getName());
        cv.put(LinkDB.Columns.TYPE.getName(), this.getType());
        cv.put(LinkDB.Columns.URL.getName(), this.getUrl());
        cv.put(LinkDB.Columns.OWNER_ID.getName(), this.getOwnerId());
        cv.put(LinkDB.Columns.OWNER_NAME.getName(), this.getOwnerName());
        cv.put(LinkDB.Columns.CREATION_DATE.getName(), this.getCreationDate());
        cv.put(LinkDB.Columns.PASSWORD.getName(), this.getPassword());
        cv.put(LinkDB.Columns.EXPIRATION_DATE.getName(), this.getExpirationDate());
        cv.put(LinkDB.Columns.DOCUMENT_ID.getName(), this.getDocumentId());
        cv.put(LinkDB.Columns.VERSION.getName(), this.getVersion());
        cv.put(LinkDB.Columns.ACCESS_LIMIT.getName(), this.getAccessLimit());
        cv.put(LinkDB.Columns.CURRENT_ACCESS_COUNT.getName(), this.getCurrentAccessCount());


        return cv; 
    }


    
    public static Link fromCursor(Cursor cursor) {
        Link link = new Link();
        link.setId(cursor.getString(Columns.ID.getIndex()));
        link.setName(cursor.getString(Columns.NAME.getIndex()));
        link.setType(cursor.getString(Columns.TYPE.getIndex()));
        link.setDocumentId(cursor.getString(Columns.DOCUMENT_ID.getIndex()));
        link.setUrl(cursor.getString(Columns.URL.getIndex()));
        link.setOwnerId(cursor.getString(Columns.OWNER_ID.getIndex()));
        link.setOwnerName(cursor.getString(Columns.OWNER_NAME.getIndex()));
        link.setCreationDate(cursor.getString(Columns.CREATION_DATE.getIndex()));
        link.setPassword(cursor.getString(Columns.PASSWORD.getIndex()));
        link.setExpirationDate(cursor.getString(Columns.EXPIRATION_DATE.getIndex()));
        link.setVersion(cursor.getInt(Columns.VERSION.getIndex()));
        link.setAccessLimit(cursor.getInt(Columns.ACCESS_LIMIT.getIndex()));
        link.setCurrentAccessCount(cursor.getInt(Columns.CURRENT_ACCESS_COUNT.getIndex()));
        return link;
    }
}
