package com.saperion.sdb.sdk.providers;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.provider.BaseColumns;

import com.saperion.sdb.sdk.providers.util.ColumnMetadata;
import com.saperion.sdb.sdk.utils.Log;

/**
 * This class was generated by the ContentProviderCodeGenerator project made by Foxykeep
 * <p/>
 * (More information available https://github.com/foxykeep/ContentProviderCodeGenerator)
 */
public abstract class FolderContent {

    public static final Uri CONTENT_URI = Uri.parse("content://" + SdbContentProvider.AUTHORITY);

    private FolderContent() {
    }

    /**
     * Created in version 1
     */
    public static final class FolderDB extends FolderContent {

        private static final String LOG_TAG = FolderDB.class.getSimpleName();

        public static final String TABLE_NAME = "folder";
        public static final String TYPE_ELEM_TYPE = "vnd.android.cursor.item/folder-folder";
        public static final String TYPE_DIR_TYPE = "vnd.android.cursor.dir/folder";

        public static final Uri CONTENT_URI = Uri.parse(FolderContent.CONTENT_URI + "/"
                + TABLE_NAME);

        public static enum Columns implements ColumnMetadata {
            _ID(BaseColumns._ID, "integer"),
            ID("id", "TEXT UNIQUE"),
            NAME("name", "text"),
            MODIFICATION_ID("modificationId", "text"),
            LAST_MODIFIED("lastModified", "text"),
            DESCRIPTION("description", "text"),
            CREATION_DATE("creationDate", "text"),
            STATES("states", "text"),
            TAGS("tags", "text"),
            PARENT_ID("parentId", "text"),
            SPACE_ID("spaceId", "text"),
            ID_CHAIN("idChain", "text"),
            OWNER_ID("ownerId", "text"),
            OWNER_NAME("ownerName", "text"),
            ETAG("etag", "text"),
            CHILDREN_ETAG("children_etag", "text"),
            SUBFOLDERS_ETAG("subfolders_etag", "text"),
            DOCUMENTS_ETAG("documents_etag", "text"),
            TYPE("type", "text");

            private final String mName;
            private final String mType;

            private Columns(String name, String type) {
                mName = name;
                mType = type;
            }

            @Override
            public int getIndex() {
                return ordinal();
            }

            @Override
            public String getName() {
                return mName;
            }

            @Override
            public String getType() {
                return mType;
            }
        }

        public static final String[] PROJECTION = new String[]{Columns._ID.getName(),
                Columns.ID.getName(), Columns.NAME.getName(), Columns.MODIFICATION_ID.getName(),
                Columns.LAST_MODIFIED.getName(), Columns.DESCRIPTION.getName(),
                Columns.CREATION_DATE.getName(), Columns.STATES.getName(), Columns.TAGS.getName(),
                Columns.PARENT_ID.getName(), Columns.SPACE_ID.getName(),
                Columns.ID_CHAIN.getName(), Columns.OWNER_ID.getName(),
                Columns.OWNER_NAME.getName(), Columns.ETAG.getName(),
                Columns.CHILDREN_ETAG.getName(), Columns.SUBFOLDERS_ETAG.getName(),
                Columns.DOCUMENTS_ETAG.getName(), Columns.TYPE.getName()

        };

        private FolderDB() {
            // No private constructor
        }

        public static void createTable(SQLiteDatabase db) {

            Log.v(LOG_TAG, "creating folder tables on db...");

            db.execSQL("CREATE TABLE " + TABLE_NAME + " (" + Columns._ID.getName() + " "
                    + Columns._ID.getType() + ", " + Columns.ID.getName() + " "
                    + Columns.ID.getType() + ", " + Columns.NAME.getName() + " "
                    + Columns.NAME.getType() + ", " + Columns.MODIFICATION_ID.getName() + " "
                    + Columns.MODIFICATION_ID.getType() + ", " + Columns.LAST_MODIFIED.getName()
                    + " " + Columns.LAST_MODIFIED.getType() + ", " + Columns.DESCRIPTION.getName()
                    + " " + Columns.DESCRIPTION.getType() + ", " + Columns.CREATION_DATE.getName()
                    + " " + Columns.CREATION_DATE.getType() + ", " + Columns.STATES.getName() + " "
                    + Columns.STATES.getType() + ", " + Columns.TAGS.getName() + " "
                    + Columns.TAGS.getType() + ", " + Columns.PARENT_ID.getName() + " "
                    + Columns.PARENT_ID.getType() + ", " + Columns.SPACE_ID.getName() + " "
                    + Columns.SPACE_ID.getType() + ", " + Columns.ID_CHAIN.getName() + " "
                    + Columns.ID_CHAIN.getType() + ", " + Columns.OWNER_ID.getName() + " "
                    + Columns.OWNER_ID.getType() + ", " + Columns.OWNER_NAME.getName() + " "
                    + Columns.OWNER_NAME.getType() + ", " + Columns.ETAG.getName() + " "
                    + Columns.ETAG.getType() + ", " + Columns.CHILDREN_ETAG.getName() + " "
                    + Columns.CHILDREN_ETAG.getType() + ", " + Columns.SUBFOLDERS_ETAG.getName()
                    + " " + Columns.SUBFOLDERS_ETAG.getType() + ", "
                    + Columns.DOCUMENTS_ETAG.getName() + " " + Columns.DOCUMENTS_ETAG.getType()
                    + ", " + Columns.TYPE.getName() + " " + Columns.TYPE.getType()
                    + ", PRIMARY KEY (" + Columns._ID.getName() + ")" + ");");
            /*
			            db.execSQL("CREATE INDEX folder_id on " + TABLE_NAME + "(" + Columns.ID.getName() + ");");
			            db.execSQL("CREATE INDEX folder_name on " + TABLE_NAME + "(" + Columns.NAME.getName() + ");");
			            db.execSQL("CREATE INDEX folder_modificationId on " + TABLE_NAME + "(" + Columns.MODIFICATION_ID.getName() + ");");
			            db.execSQL("CREATE INDEX folder_lastModified on " + TABLE_NAME + "(" + Columns.LAST_MODIFIED.getName() + ");");
			            db.execSQL("CREATE INDEX folder_description on " + TABLE_NAME + "(" + Columns.DESCRIPTION.getName() + ");");
			            db.execSQL("CREATE INDEX folder_creationDate on " + TABLE_NAME + "(" + Columns.CREATION_DATE.getName() + ");");
			            db.execSQL("CREATE INDEX folder_states on " + TABLE_NAME + "(" + Columns.STATES.getName() + ");");
			            db.execSQL("CREATE INDEX folder_tags on " + TABLE_NAME + "(" + Columns.TAGS.getName() + ");");
			            db.execSQL("CREATE INDEX folder_parentId on " + TABLE_NAME + "(" + Columns.PARENT_ID.getName() + ");");
			            db.execSQL("CREATE INDEX folder_spaceId on " + TABLE_NAME + "(" + Columns.SPACE_ID.getName() + ");");
			            db.execSQL("CREATE INDEX folder_idChain on " + TABLE_NAME + "(" + Columns.ID_CHAIN.getName() + ");");
			            db.execSQL("CREATE INDEX folder_ownerId on " + TABLE_NAME + "(" + Columns.OWNER_ID.getName() + ");");
			            db.execSQL("CREATE INDEX folder_ownerName on " + TABLE_NAME + "(" + Columns.OWNER_NAME.getName() + ");");
			            db.execSQL("CREATE INDEX folder_etag on " + TABLE_NAME + "(" + Columns.ETAG.getName() + ");");
			            db.execSQL("CREATE INDEX folder_children_etag on " + TABLE_NAME + "(" + Columns.CHILDREN_ETAG.getName() + ");");
			            db.execSQL("CREATE INDEX folder_subfolders_etag on " + TABLE_NAME + "(" + Columns.SUBFOLDERS_ETAG.getName() + ");");
			            db.execSQL("CREATE INDEX folder_documents_etag on " + TABLE_NAME + "(" + Columns.DOCUMENTS_ETAG.getName() + ");");
			            db.execSQL("CREATE INDEX folder_type on " + TABLE_NAME + "(" + Columns.TYPE.getName() + ");");
			*/
        }

        // Version 1 : Creation of the table
        public static void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {

            if (oldVersion < 1) {
                Log.v(LOG_TAG, "Upgrading createInstance version " + oldVersion + " to " + newVersion
                        + ", data will be lost!");

                db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME + ";");
                createTable(db);
                return;
            }

            if (oldVersion != newVersion) {
                throw new IllegalStateException("Error upgrading the database to version "
                        + newVersion);
            }
        }

        static String getBulkInsertString() {
            return new StringBuilder("INSERT INTO ").append(TABLE_NAME).append(" ( ")
                    .append(Columns._ID.getName()).append(", ")
                    .append(Columns.ID.getName()).append(", ")
                    .append(Columns.NAME.getName()).append(", ")
                    .append(Columns.MODIFICATION_ID.getName()).append(", ")
                    .append(Columns.LAST_MODIFIED.getName()).append(", ")
                    .append(Columns.DESCRIPTION.getName()).append(", ")
                    .append(Columns.CREATION_DATE.getName()).append(", ")
                    .append(Columns.STATES.getName()).append(", ")
                    .append(Columns.TAGS.getName()).append(", ")
                    .append(Columns.PARENT_ID.getName()).append(", ")
                    .append(Columns.SPACE_ID.getName()).append(", ")
                    .append(Columns.ID_CHAIN.getName()).append(", ")
                    .append(Columns.OWNER_ID.getName()).append(", ")
                    .append(Columns.OWNER_NAME.getName()).append(", ")
                    .append(Columns.ETAG.getName()).append(", ")
                    .append(Columns.CHILDREN_ETAG.getName()).append(", ")
                    .append(Columns.SUBFOLDERS_ETAG.getName()).append(", ")
                    .append(Columns.DOCUMENTS_ETAG.getName()).append(", ")
                    .append(Columns.TYPE.getName())
                    .append(" ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
                    .toString();
        }

        static void bindValuesInBulkInsert(SQLiteStatement stmt, ContentValues values) {
            int i = 1;
            String value;
            stmt.bindLong(i++, values.getAsLong(Columns._ID.getName()));
            value = values.getAsString(Columns.ID.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.NAME.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.MODIFICATION_ID.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.LAST_MODIFIED.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.DESCRIPTION.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.CREATION_DATE.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.STATES.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.TAGS.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.PARENT_ID.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.SPACE_ID.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.ID_CHAIN.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.OWNER_ID.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.OWNER_NAME.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.ETAG.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.CHILDREN_ETAG.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.SUBFOLDERS_ETAG.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.DOCUMENTS_ETAG.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.TYPE.getName());
            stmt.bindString(i++, value != null ? value : "");

        }
    }
}
