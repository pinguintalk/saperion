package com.saperion.sdb.sdk.operations;

import android.content.Context;
import android.os.Bundle;

import com.saperion.sdb.sdk.config.WSConfig;
import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.models.Settings;
import com.saperion.sdb.sdk.network.NetworkConnection;
import com.saperion.sdb.sdk.network.NetworkConnection.ConnectionResult;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.security.CredentialManager;
import com.saperion.sdb.sdk.service.RequestService.Operation;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

import org.apache.http.auth.UsernamePasswordCredentials;

public final class AccountsGetSettingsOperation implements Operation {

    private static final String TAG = AccountsGetSettingsOperation.class.getSimpleName();
    
    @Override
    public Bundle execute(Context context, Request request)
            throws ServiceRequestException, ConnectionException, DataException {

        Log.v(TAG, Utils.getMethodName() + "entry");
        Bundle bundle = new Bundle();

        
        NetworkConnection networkConnection =
                new NetworkConnection(context, WSConfig.WS_ROOT_URL + WSConfig.WS_ACCOUNTS + "/" + WSConfig.WS_SETTINGS);
        networkConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager
                .getInstance().getUserName(), CredentialManager.getInstance().getmAccessToken()));
        
        ConnectionResult result = networkConnection.execute();
        
        int responseCode = result.responseCode;
        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, responseCode);

        Log.v(TAG, Utils.getMethodName() + " responseCode:" + result.responseCode);
        if (responseCode == ResponseConstants.RESPONSE_CODE_OK) {
            Settings settings = Settings.fromJson(result.body);

            Log.w(TAG, Utils.getMethodName() + " settings:" + settings.toString());
            
            bundle.putParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, settings);
        }
        else 
            bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
        

        return bundle;
    }

}
