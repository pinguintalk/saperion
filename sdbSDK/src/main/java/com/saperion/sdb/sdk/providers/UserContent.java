package com.saperion.sdb.sdk.providers;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

import com.saperion.sdb.sdk.providers.util.ColumnMetadata;

public abstract class UserContent {

    public static final Uri CONTENT_URI = Uri.parse("content://" + SdbContentProvider.AUTHORITY);

    private UserContent() {
    }

    /**
     * Created in version 1
     */
    public static final class UserDB extends UserContent {

        private static final String TAG = UserDB.class.getSimpleName();

        public static final String TABLE_NAME = "user";
        public static final String TYPE_ELEM_TYPE = "vnd.android.cursor.item/poc-user";
        public static final String TYPE_DIR_TYPE = "vnd.android.cursor.dir/poc-user";
        public static final Uri CONTENT_URI = Uri.parse(UserContent.CONTENT_URI + "/"
                + TABLE_NAME);




        public static enum Columns implements ColumnMetadata {
            _ID(BaseColumns._ID, "integer"),
            ID("id", "TEXT UNIQUE"),
            USER_CLASS_ID("userclassid", "text"),
            DISPLAY_NAME("displayname", "text"),
            FIRST_NAME("firstname", "text"),
            LAST_NAME("lastname", "text"),
            EMAIL("email", "text"),
            COMPANY("company", "text"),
            RIGHTS("rights", "text"),
            TYPE("type", "text"),
            AVATAR_PATH("avatar_path", "text"),
            STATES("states", "text");

            private final String mName;
            private final String mType;

            private Columns(String name, String type) {
                mName = name;
                mType = type;
            }

            @Override
            public int getIndex() {
                return ordinal();
            }

            @Override
            public String getName() {
                return mName;
            }

            @Override
            public String getType() {
                return mType;
            }

        }

        private UserDB() {
            // No private constructor
        }

        public static void createTable(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + TABLE_NAME + " (" 
                    + Columns._ID.getName() + " " + Columns._ID.getType() + ", " 
                    + Columns.ID.getName() + " " + Columns.ID.getType() + ", " 
                    + Columns.USER_CLASS_ID.getName() + " " + Columns.USER_CLASS_ID.getType() + ", " 
                    + Columns.DISPLAY_NAME.getName() + " " + Columns.DISPLAY_NAME.getType() + ", " 
                    + Columns.FIRST_NAME.getName() + " " + Columns.FIRST_NAME.getType() + ", " 
                    + Columns.LAST_NAME.getName() + " " + Columns.LAST_NAME.getType() + ", "
                    + Columns.EMAIL.getName() + " " + Columns.EMAIL.getType() + ", " 
                    + Columns.COMPANY.getName() + " " + Columns.COMPANY.getType() + ", "
                    + Columns.RIGHTS.getName() + " " + Columns.RIGHTS.getType() + ", "
                    + Columns.TYPE.getName() + " " + Columns.TYPE.getType() + ", "
                    + Columns.AVATAR_PATH.getName() + " " + Columns.AVATAR_PATH.getType() + ", "
                    
                    + Columns.STATES.getName() + " " + Columns.STATES.getType() 
                    + ", PRIMARY KEY (" + Columns._ID.getName() + ")" + ");");

            db.execSQL("CREATE INDEX dbUser_DisplayName on " + TABLE_NAME + "(" + Columns.DISPLAY_NAME.getName()
                    + ");");
        }

        

        // Version 1 : Creation of the table
        public static void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {

            if (oldVersion < 1) {
                Log.v(TAG, "Upgrading createInstance version " + oldVersion + " to " + newVersion
                        + ", data will be lost!");

                db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME + ";");
                createTable(db);
                return;
            }

            if (oldVersion != newVersion) {
                throw new IllegalStateException("Error upgrading the database to version "
                        + newVersion);
            }
        }

        public static String getBulkInsertString() {

            return new StringBuilder("INSERT INTO ").append(TABLE_NAME).append(" ( ")
                    .append(Columns._ID.getName()).append(", ").append(Columns.ID.getName())
                    .append(", ")
                    .append(Columns.ID.getName()).append(", ")
                    .append(Columns.USER_CLASS_ID.getName()).append(", ")
                    .append(Columns.DISPLAY_NAME.getName()).append(", ")
                    .append(Columns.FIRST_NAME.getName()).append(", ")
                    .append(Columns.LAST_NAME.getName()).append(", ")
                    .append(Columns.EMAIL.getName()).append(", ")
                    .append(Columns.COMPANY.getName()).append(", ")
                    .append(Columns.RIGHTS.getName()).append(", ")
                    .append(Columns.TYPE.getName()).append(", ")
                    .append(Columns.AVATAR_PATH.getName()).append(", ")
            
                    .append(Columns.STATES.getName()).append(", ")
                    .append(" ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
                    .toString();

        }
        

        static void bindValuesInBulkReplace(SQLiteStatement stmt, ContentValues values) {
            int i = 1;
            String value;
            value = values.getAsString(Columns.ID.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.USER_CLASS_ID.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.DISPLAY_NAME.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.FIRST_NAME.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.LAST_NAME.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.EMAIL.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.COMPANY.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.RIGHTS.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.TYPE.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.AVATAR_PATH.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.STATES.getName());
            stmt.bindString(i++, value != null ? value : "");
        }

        static void bindValuesInBulkInsert(SQLiteStatement stmt, ContentValues values) {
            int i = 1;
            String value;
            value = values.getAsString(Columns.ID.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.USER_CLASS_ID.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.DISPLAY_NAME.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.FIRST_NAME.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.LAST_NAME.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.EMAIL.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.COMPANY.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.RIGHTS.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.TYPE.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.AVATAR_PATH.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.STATES.getName());
            stmt.bindString(i++, value != null ? value : "");
        }
    }
}
