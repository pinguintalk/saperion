package com.saperion.sdb.sdk.models;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.saperion.sdb.sdk.providers.ContentProviderHelpers;

import java.io.IOException;

public class Version extends CreatedTypedIdentifiable implements Parcelable {

    private int number;
    private String comment;
    private String documentId;

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public Document getDocument(Context context) {
        return ContentProviderHelpers.documentGetById(context, documentId);
    }


    public Version() {
        super(ModelType.VERSION);
    }

    public Version setNumber(int number) {
        this.number = number;
        return this;
    }

    public int getNumber() {
        return this.number;
    }

    public Version setComment(String comment) {
        this.comment = comment;
        return this;
    }

    public String getComment() {
        return this.comment;
    }


    public String toJson() {

        ObjectMapper mapper = new ObjectMapper();

        mapper.setSerializationInclusion(Include.NON_NULL);
        try {
            return mapper.writeValueAsString(this);

        } catch (JsonProcessingException e1) {
            e1.printStackTrace();
        }
        return null;
    }

    public static Version fromJson(String string) {

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        try {
            mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

            return mapper.readValue(string, Version.class);

        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.getId());
        out.writeString(this.getType());
        out.writeString(this.getOwnerId());
        out.writeString(this.getOwnerName());
        out.writeString(this.getCreationDate());
        out.writeString(this.getComment());
        out.writeString(this.getDocumentId());
        out.writeInt(this.getNumber());

    }

    private Version(Parcel in) {
        this.setId(in.readString());
        this.setType(in.readString());
        this.setOwnerId(in.readString());
        this.setOwnerName(in.readString());
        this.setCreationDate(in.readString());
        this.setComment(in.readString());
        this.setDocumentId(in.readString());
        this.setNumber(in.readInt());

    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Version createFromParcel(Parcel in) {
            return new Version(in);
        }

        public Version[] newArray(int size) {
            return new Version[size];
        }
    };

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public String toString() {
        return "Version{" + "number=" + number + ", documentId='" + documentId + ", comment='" + comment + '\'' + "} "
                + super.toString();
    }
}
/*

The setting model:
        {
        "type":	"setting",
        "key":	"	<the setting’s key>",
        "value":	"<the setting’s value"
        }
        The settings model:
        {
        "type":	"settings",
        "list":		[<a list of setting models>]
        }*/