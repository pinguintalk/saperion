package com.saperion.sdb.sdk.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The class Item.
 */
public abstract class Item extends ModifiableTypedIdentifiable {
    private String name;
    private String description;
    private String modificationId;
    private List<String> tags;

    protected Item() {
    }

    public Item(ModelType type) {
        super(type);
    }

    public String getName() {
        return name;
    }

    public Item setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Item setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getModificationId() {
        return modificationId;
    }

    public Item setModificationId(String modificationId) {
        this.modificationId = modificationId;
        return this;
    }

    public List<String> getTags() {
        return tags;
    }

    public Item setTags(List<String> tags) {
        this.tags = tags;
        return this;
    }

    public Item addTag(String tag) {
        if (null == tags) {
            tags = new ArrayList<String>();
        }
        if (tag != null) {
            tags = Arrays.asList(tag.split("\\s*,\\s*"));
        }
        return this;
    }

    @Override
    public String toString() {
        return "Item{" + "name='" + name + '\'' + ", description='" + description + '\''
                + ", modificationId='" + modificationId + '\'' + ", tags=" + tags + "} "
                + super.toString();
    }

}
