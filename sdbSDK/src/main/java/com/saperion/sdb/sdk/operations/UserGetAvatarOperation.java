package com.saperion.sdb.sdk.operations;

import android.content.Context;
import android.os.Bundle;

import com.saperion.sdb.sdk.config.WSConfig;
import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.models.User;
import com.saperion.sdb.sdk.network.NetworkConnection;
import com.saperion.sdb.sdk.network.NetworkConnection.ConnectionResult;
import com.saperion.sdb.sdk.providers.ContentProviderHelpers;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.security.CredentialManager;
import com.saperion.sdb.sdk.service.RequestService.Operation;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

import org.apache.http.auth.UsernamePasswordCredentials;

import java.util.HashMap;

public final class UserGetAvatarOperation implements Operation {

    private static final String TAG = UserGetAvatarOperation.class.getSimpleName();

    public static final String PARAM_USER = WSConfig.APP_PACKAGE_NAME + ".param_user";
    
    @Override
    public Bundle execute(Context context, Request request)
            throws ServiceRequestException, ConnectionException, DataException {

        Log.v(TAG, Utils.getMethodName() + "entry");
        Bundle bundle = new Bundle();
        User user =  (User) request.getParcelable(PARAM_USER);
        String user_id = user.getId();
        String abs_path = OperationHelpers.userCreateAvatarPathWithExtension(context, user_id);
        
        NetworkConnection networkConnection =
                new NetworkConnection(context, WSConfig.userCreateAvatarUrl(user_id));
        networkConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager
                .getInstance().getUserName(), CredentialManager.getInstance().getmAccessToken()));

        HashMap<String, String> accept_header = new HashMap<String, String>();
        accept_header.put("ACCEPT", "application/octet-stream");
        networkConnection.setHeaderList(accept_header);

        HashMap<String, String> requestParams = new HashMap<String, String>();
        requestParams.put(NetworkConnection.REQUEST_TYPE,
                NetworkConnection.REEQUEST_TYPE_DOWNLOAD);
        requestParams.put(NetworkConnection.FILE_PATH, abs_path);
        networkConnection.setRequestParams(requestParams);
        
        ConnectionResult result = networkConnection.execute();
        
        int responseCode = result.responseCode;
        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, responseCode);

        Log.w(TAG, Utils.getMethodName() + " responseCode:" + result.responseCode);
        if (responseCode == ResponseConstants.RESPONSE_CODE_OK) {
            user.setAvatarAbsPath(abs_path);
            ContentProviderHelpers.userUpdate(context, user);
            
            bundle.putParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, user);
        }
        else if (responseCode == ResponseConstants.RESPONSE_CODE_NO_CONTENT) {
            user.setAvatarAbsPath(abs_path);
            bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
        }
        else 
            bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
        

        return bundle;
    }

}
