package com.saperion.sdb.sdk.operations;

import android.content.Context;
import android.os.Bundle;

import com.saperion.sdb.sdk.config.WSConfig;
import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.factory.UserJsonFactory;
import com.saperion.sdb.sdk.models.User;
import com.saperion.sdb.sdk.network.NetworkConnection.ConnectionResult;
import com.saperion.sdb.sdk.providers.ContentProviderHelpers;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.service.RequestService.Operation;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

public final class UserSaveOperation implements Operation {

    private static final String TAG = UserSaveOperation.class.getSimpleName();

    public static final String PARAM_PAYLOAD = WSConfig.APP_PACKAGE_NAME + ".param_payload";
    public static final String PARAM_USER = WSConfig.APP_PACKAGE_NAME + ".param_user";

    @Override
    public Bundle execute(Context context, Request request)
            throws ServiceRequestException, ConnectionException, DataException {

        Log.v(TAG, Utils.getMethodName() + "entry");
        Bundle bundle = new Bundle();
 
        User user = (User) request.getParcelable(PARAM_USER);
        String payload =  request.getString(PARAM_PAYLOAD);
        String Id = user.getId();

        ConnectionResult result = null;
        if (Id != null && Id.length() > 0) {
            result = OperationHelpers.userUpdate(context,Id,payload);
        } else{
            result = OperationHelpers.userCreate(context,payload);
        }
       
        
        int responseCode = result.responseCode;
        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, responseCode);

        Log.e(TAG, Utils.getMethodName() + " responseCode:" + result.responseCode);
        // Log.e(TAG, Utils.getMethodName() + " body:" + result.body);
        
        if (responseCode == ResponseConstants.RESPONSE_CODE_OK) {
            User userTmp = UserJsonFactory.jsonToUser(result.body);

           // Log.e(TAG, Utils.getMethodName() + " userTmp:" + userTmp.toString());
            
            ContentProviderHelpers.userUpdate(context,userTmp);
            bundle.putParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, userTmp);
        }
        else if (responseCode == ResponseConstants.RESPONSE_CODE_CREATED) {
            User userTmp = UserJsonFactory.jsonToUser(result.body);
            // Log.e(TAG, Utils.getMethodName() + " userTmp:" + userTmp.toString());
            ContentProviderHelpers.userInsert(context,userTmp);
            bundle.putParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, userTmp);
        }
        else 

            bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);

        return bundle;
    }

}
