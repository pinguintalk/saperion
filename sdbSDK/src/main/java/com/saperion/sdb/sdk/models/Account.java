package com.saperion.sdb.sdk.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

@JsonIgnoreProperties({ "features"}) 
public class Account extends Typed implements Parcelable{

    private String name;  // The name of the company/account>
    private int userLimit; //<user limit – currently not used>,
    private int usedUsers; //<the current guest user count – currently not used>
    private int guestLimit; //<guest limit – currently not used>
    private int usedGuest;  //<the current guest user count – currently not used>
    private long storageLimit; // <the maximum storage size in MB>
    private long uploadLimit; // <the maximum storage size in MB>
    private long usedStorage; //<used storage size in MB>

    
    public Account() {
        super(ModelType.ACCOUNT);
    }

    public String getName() {
        return name;
    }

    public Account setName(String name) {
        this.name = name;
        return this;
    }

    public int getGuestLimit() {
        return guestLimit;
    }

    public void setGuestLimit(int guestLimit) {
        this.guestLimit = guestLimit;
    }

    public int getUsedGuest() {
        return usedGuest;
    }

    public void setUsedGuest(int usedGuest) {
        this.usedGuest = usedGuest;
    }

    /**
     * @return the userLimit
     */
    public int getUserLimit() {
        return userLimit;
    }

    /**
     * @return the usedUsers
     */
    public int getUsedUsers() {
        return usedUsers;
    }

    /**
     * @return the storageLimit
     */
    public long getStorageLimit() {
        return storageLimit;
    }

    /**
     * @return the usedStorage
     */
    public long getUsedStorage() {
        return usedStorage;
    }

    /**
     * @param userLimit the userLimit to set
     */
    public final void setUserLimit(int userLimit) {
        this.userLimit = userLimit;
    }

    /**
     * @param usedUsers the usedUsers to set
     */
    public final void setUsedUsers(int usedUsers) {
        this.usedUsers = usedUsers;
    }

    /**
     * @param storageLimit the storageLimit to set
     */
    public final void setStorageLimit(long storageLimit) {
        this.storageLimit = storageLimit;
    }

    /**
     * @param usedStorage the usedStorage to set
     */
    public final void setUsedStorage(long usedStorage) {
        this.usedStorage = usedStorage;
    }

    public long getUploadLimit() {
        return uploadLimit;
    }

    public void setUploadLimit(long uploadLimit) {
        this.uploadLimit = uploadLimit;
    }

    public static Account fromJson(String string) {
        Account account = new Account();
        ObjectMapper mapper = new ObjectMapper();
        try {
            account = mapper.readValue(string, Account.class);
            return account;
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
    
    
    @Override
    public String toString() {
        return "Account{" + "name='" + name + '\'' + ", userLimit=" + userLimit + ", usedUsers="
                + usedUsers + ", guestLimit=" + guestLimit + ", usedGuest=" + usedGuest
                + ", storageLimit=" + storageLimit + ", uploadLimit=" + uploadLimit
                + ", usedStorage=" + usedStorage + "} " + super.toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    
    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.getType());
        parcel.writeString(this.getName());
        parcel.writeInt(this.getUserLimit());
        parcel.writeInt(this.getUsedUsers());
        parcel.writeInt(this.getGuestLimit());
        parcel.writeInt(this.getUsedGuest());
        parcel.writeLong(this.getStorageLimit());
        parcel.writeLong(this.getUploadLimit());
        parcel.writeLong(this.getUsedStorage());
        
    }

    private Account(Parcel in) {
        this.setType(in.readString());
        this.setName(in.readString());
        this.setUserLimit(in.readInt());
        this.setUsedUsers(in.readInt());
        this.setGuestLimit(in.readInt());
        this.setUsedGuest(in.readInt());
        this.setStorageLimit(in.readLong());
        this.setUploadLimit(in.readLong());
        this.setUsedStorage(in.readLong());

    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Account createFromParcel(Parcel in) {
            return new Account(in);
        }

        public Account[] newArray(int size) {
            return new Account[size];
        }
    };
}
