package com.saperion.sdb.sdk.operations;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import com.saperion.sdb.sdk.Constants;
import com.saperion.sdb.sdk.SdbApplication;
import com.saperion.sdb.sdk.config.WSConfig;
import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.factory.DocumentJsonFactory;
import com.saperion.sdb.sdk.factory.FolderJsonFactory;
import com.saperion.sdb.sdk.factory.SpaceJsonFactory;
import com.saperion.sdb.sdk.models.Document;
import com.saperion.sdb.sdk.models.Folder;
import com.saperion.sdb.sdk.models.ModelType;
import com.saperion.sdb.sdk.models.Space;
import com.saperion.sdb.sdk.models.User;
import com.saperion.sdb.sdk.network.NetworkConnection;
import com.saperion.sdb.sdk.network.NetworkConnection.ConnectionResult;
import com.saperion.sdb.sdk.network.NetworkConnection.Method;
import com.saperion.sdb.sdk.providers.ContentProviderHelpers;
import com.saperion.sdb.sdk.providers.FolderContent.FolderDB;
import com.saperion.sdb.sdk.requestmanager.ResponseConstants;
import com.saperion.sdb.sdk.security.CredentialManager;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

import org.apache.http.auth.UsernamePasswordCredentials;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public final class OperationHelpers {

    private static final String TAG = OperationHelpers.class.getSimpleName();

    /**
     * get current space list createInstance server and update to content provider. All space will updated independent createInstance his state
     *
     * @param context
     * @return space list
     * @throws com.saperion.sdb.sdk.exceptions.ConnectionException
     * @throws com.saperion.sdb.sdk.exceptions.DataException
     */
    public static ConnectionResult spaceGetList(Context context) throws ConnectionException,
            DataException, ServiceRequestException {

        NetworkConnection networkConnection =
                new NetworkConnection(context, WSConfig.getSpaceURL() + "?include=recycled,modificationId");
        networkConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager
                .getInstance().getUserName(), CredentialManager.getInstance().getmAccessToken()));

        return networkConnection.execute();

    }

    public static ConnectionResult getDocumentListByParentId(Context context, String parentID,
                                                             boolean parentIsSpace) throws ConnectionException, DataException, ServiceRequestException {
        Log.i(TAG, Utils.getMethodName() + "entry ");

        String url = WSConfig.getSpaceURL();
        if (!parentIsSpace)
            url = WSConfig.getFolderURL();

        url = url + "/" + parentID + "/documents?include=recycled,modificationId";

        NetworkConnection networkConnection = new NetworkConnection(context, url);
        networkConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager
                .getInstance().getUserName(), CredentialManager.getInstance().getmAccessToken()));

        if (parentIsSpace) {
            Space space = ContentProviderHelpers.spaceGetById(context, parentID);
            String if_none_match_field = space.getmDocumentsEtag();
            if (if_none_match_field != null && !TextUtils.isEmpty(if_none_match_field)) {
                networkConnection.setIfNoneMatch(if_none_match_field);
            }

        } else {
            Folder folder = ContentProviderHelpers.folderGetById(context, parentID);
            String if_none_match_field = folder.getmDocumentsEtag();
            if (if_none_match_field != null && !TextUtils.isEmpty(if_none_match_field)) {
                Log.v(TAG, Utils.getMethodName() + "if_none_match_field " + if_none_match_field);
                networkConnection.setIfNoneMatch(if_none_match_field);
            }
        }

        return networkConnection.execute();

    }

    public static ConnectionResult folderGetChildrenByParentId(Context context, String parentID,
                                                               boolean isSpace) throws ServiceRequestException, ConnectionException {
        Log.v(TAG, Utils.getMethodName() + "entry");

        String url = WSConfig.getSpaceURL();
        if (!isSpace)
            url = WSConfig.getFolderURL();

        if (!TextUtils.isEmpty(parentID)) {
            url = url + "/" + parentID + "/children?include=modificationid";
        }
        HashMap<String, String> headerList = new HashMap<String, String>();

        NetworkConnection networkConnection = new NetworkConnection(context, url);
        networkConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager
                .getInstance().getUserName(), CredentialManager.getInstance().getmAccessToken()));

        if (isSpace) {
            Space space = ContentProviderHelpers.spaceGetById(context, parentID);
            String if_none_match_field = space.getmChildrenEtag();
            if (if_none_match_field != null && !TextUtils.isEmpty(if_none_match_field)) {
                headerList.put("If-None-Match", if_none_match_field);
                networkConnection.setHeaderList(headerList);
            }

        } else {
            Folder folder = ContentProviderHelpers.folderGetById(context, parentID);
            String if_none_match_field = folder.getmChildrenEtag();
            if (if_none_match_field != null && !TextUtils.isEmpty(if_none_match_field)) {
                headerList.put("If-None-Match", if_none_match_field);
                networkConnection.setHeaderList(headerList);
            }
        }

        return networkConnection.execute();

    }

    public static ConnectionResult folderGetSubfoldersByParentId(Context context, String parentID,
                                                                 boolean isSpace) throws ServiceRequestException, ConnectionException, DataException {
        Log.v(TAG, Utils.getMethodName() + "entry");

        String url = WSConfig.getSpaceURL();
        if (!isSpace)
            url = WSConfig.getFolderURL();

        if (!TextUtils.isEmpty(parentID)) {
            url = url + "/" + parentID + "/subfolders?include=recycled,modificationId";
        }

        NetworkConnection networkConnection = new NetworkConnection(context, url);
        networkConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager
                .getInstance().getUserName(), CredentialManager.getInstance().getmAccessToken()));

        String if_none_match_field = null;
        
        if (isSpace) {
            Space space = ContentProviderHelpers.spaceGetById(context, parentID);
            if_none_match_field = space.getmSubfoldersEtag();

        } else { // is folder
            Folder folder = ContentProviderHelpers.folderGetById(context, parentID);
             if_none_match_field = folder.getmSubfoldersEtag();
        }

        if (if_none_match_field != null && !TextUtils.isEmpty(if_none_match_field)) {
            networkConnection.setIfNoneMatch(if_none_match_field);
        }
        return networkConnection.execute();

    }

    public static boolean isSpaceListContainsId(List<Space> spaceList, String ID) {
        for (Space space : spaceList) {
            if (space.getId().equals(ID))
                return true;
        }
        return false;
    }

    public static boolean isFolderListContainsID(List<Folder> folderListFromServer, String ID) {
        for (Folder folder : folderListFromServer) {
            if (folder.getId().equals(ID))
                return true;
        }
        return false;
    }

    public static boolean isDocumentListContainsId(List<Document> docListFromServer, String ID) {
        for (Document doc : docListFromServer) {
            if (doc.getId().equals(ID))
                return true;
        }
        return false;
    }

    public static int spaceListUpdateContentProvider(Context context, List<Space> spaceListFromServer) {
        int changed_count = 0;

        if (spaceListFromServer == null || spaceListFromServer.size() < 1)
            return changed_count;

        List<Space> localSpaces = new ArrayList<Space>();

        localSpaces = ContentProviderHelpers.spaceGetList(context);

        ArrayList<String> localSpaceIDList = new ArrayList<String>();

        for (Space space : localSpaces) {
            String id = space.getId();

            if (!isSpaceListContainsId(spaceListFromServer, id)) { // space is no more existed on server 

                // remove this space if not done before    				
                ContentProviderHelpers.spaceDeleteById(context, id);
            } else {
                // Log.v(TAG, Utils.getMethodName() + space_id);    
                localSpaceIDList.add(id);
            }
        }

        for (Space spaceFromServer : spaceListFromServer) {
            String serverSpaceID = spaceFromServer.getId();
            spaceFromServer.setSynchronized(false); // init. value

            // Log.v(TAG, Utils.getMethodName() + spaceList.get(f).toString());

            // search for existed space

            if (!localSpaceIDList.contains(serverSpaceID)) { // we have new space on server, insert it to local list
                Log.v(TAG, Utils.getMethodName() + "inserting new space");

                spaceFromServer.setSynchronized(false);
                changed_count++;

                ContentProviderHelpers.spaceInsert(context, spaceFromServer);
            } else { // space exists

                Space localSpace = ContentProviderHelpers.spaceGetById(context, serverSpaceID);

                String localSpaceModificationID = localSpace.getModificationId();

                if (localSpaceModificationID != null || !TextUtils.isEmpty(localSpaceModificationID)) {
                    if (!spaceFromServer.getModificationId().equals(localSpaceModificationID)) { // space on server is changed, we have to sync

                        // set flag that SPACE is modified, itself or the children
                        spaceFromServer.setSynchronized(false);
                        changed_count++;

                        Log.v(TAG, Utils.getMethodName() + "space on server is modified "
                                + spaceFromServer.getName());
                        // update isModified =  1 

                        ContentProviderHelpers.spaceUpdate(context, spaceFromServer);

                    }

                }

            }

        }

        return changed_count;
    }
    
    public static ArrayList<Space>  spacesSyncContentProvider(Context context, List<Space> spaceListFromServer) throws ConnectionException, ServiceRequestException, DataException {
        int synchronized_item_count = 0;
        ArrayList<Space> changedSpaces = new ArrayList<Space>();

        if (spaceListFromServer == null || spaceListFromServer.size() < 1)
            return changedSpaces;

        List<Space> localSpaces = new ArrayList<Space>();

        localSpaces = ContentProviderHelpers.spaceGetList(context);

        ArrayList<String> localSpaceIDList = new ArrayList<String>();

        for (Space space : localSpaces) {
            String id = space.getId();

            if (!isSpaceListContainsId(spaceListFromServer, id)) { // space is no more existed on server 

                changedSpaces.add(space);
                
                // delete doc and files
                ArrayList<Document> localDocList = ContentProviderHelpers.documentGetListBySpaceId(context,id);
                for (Document doc : localDocList) {
                    documentDeleteFiles(doc);
                    ContentProviderHelpers.documentDeleteById(context, doc.getId());
                }

                ContentProviderHelpers.folderDeleteBySpaceID(context, id);

                // remove this space     				
                ContentProviderHelpers.spaceDeleteById(context, id);

            } else {
                // Log.v(TAG, Utils.getMethodName() + space_id);    
                localSpaceIDList.add(id);
            }
        }

        for (Space spaceFromServer : spaceListFromServer) {
            String serverSpaceID = spaceFromServer.getId();
            spaceFromServer.setSynchronized(false); // init. value

            // 1. Step insert or update space
            if (!localSpaceIDList.contains(serverSpaceID)) { // we have new space on server, insert it to local list
                Log.v(TAG, Utils.getMethodName() + "inserting new space");
                changedSpaces.add(spaceFromServer);
                
                ContentProviderHelpers.spaceInsert(context, spaceFromServer);

            } else { // space exists

                Space tmpSpace = ContentProviderHelpers.spaceGetById(context, serverSpaceID);

                String currSpaceModificationID = tmpSpace.getModificationId();

                if (currSpaceModificationID != null || !TextUtils.isEmpty(currSpaceModificationID)) {
                    if (!spaceFromServer.getModificationId().equals(currSpaceModificationID)) { 
                        Log.v(TAG, Utils.getMethodName() + "space on server is modified "
                                + spaceFromServer.getName());
                        // update isModified =  1 
                        changedSpaces.add(spaceFromServer);
                        ContentProviderHelpers.spaceUpdate(context, spaceFromServer);
                    }

                }
            }

            // 2. Sync all contents of SYNCHRONIZE_APP space
            if (spaceFromServer.getStates().toString().contains("SYNCHRONIZE_APP")){
                Log.v(TAG, Utils.getMethodName() + "synchronize all children contents of new space with local data base");
               
                // sync documents
                documentsSyncByParentId(context, serverSpaceID, serverSpaceID, true);
                // sync folders
                foldersSyncByParentId(context, serverSpaceID, serverSpaceID, true);
            }           
        }

        return changedSpaces;
    }

    
    public static void foldersSyncByParentId(Context context, String parentId,
                                             String parentIdChain, boolean isSpace)
            throws ServiceRequestException, ConnectionException, DataException {

        List<Folder> folderListFromServer = null;

        //get folder list of current parent 
        ConnectionResult result =
                OperationHelpers.folderGetSubfoldersByParentId(context, parentId, isSpace);

        if (result.responseCode != ResponseConstants.RESPONSE_CODE_OK)
            return;

        if (!result.body.equals("[]"))
            folderListFromServer = FolderJsonFactory.parseResult(result.body);

        if (folderListFromServer == null || folderListFromServer.size() < 1)
            return;

        // search for existed folder in this space/folder
        Cursor folderCursor =
                context.getContentResolver().query(FolderDB.CONTENT_URI, null,
                        FolderDB.Columns.PARENT_ID.getName() + "='" + parentId + "'", null, null);

        List<String> currentFolderIDList = new ArrayList<String>();
        List<String> folderIDListToDelete = new ArrayList<String>();

        // delete local folder which not exists on server and create ID list of existing folders in this parent
        if (folderCursor != null && folderCursor.moveToFirst()) {
            do {
                String folder_id = folderCursor.getString(FolderDB.Columns.ID.getIndex());
                String folder_name = folderCursor.getString(FolderDB.Columns.NAME.getIndex());

                if (!isFolderListContainsID(folderListFromServer, folder_id)) { // folder is NOT exist on server 
                    Log.d(TAG, Utils.getMethodName() + "to remove" + folder_name);
                    folderIDListToDelete.add(folder_id);
                } else {
                    Log.d(TAG, Utils.getMethodName() + folder_name + "exists");
                    currentFolderIDList.add(folder_id);
                }

            } while (folderCursor.moveToNext());
        }
        if (folderCursor != null) {
            folderCursor.close();
        }

        // remove it createInstance ContentProvider
        for (String folderId : folderIDListToDelete) {
            // ... and the children  TODO
            Log.v(TAG, Utils.getMethodName() + "deleting folder" + folderId);

            // delete it self 

            ContentProviderHelpers.folderDeleteById(context, folderId);

            ContentProviderHelpers.documentDeleteByParentID(context, folderId);

            // TODO delete the children too !
        }

        // insert folders or updates
        for (Folder folderFromServer : folderListFromServer) {
            String folderIDFromServer = folderFromServer.getId();

            // make our own id chain
            folderFromServer.setIdChain(parentIdChain + ";" + folderIDFromServer);
            // search for existed folder
            if (!currentFolderIDList.contains(folderIDFromServer)) { // this folder is not found, insert it

                Log.v(TAG, Utils.getMethodName() + "inserting... ");

                ContentProviderHelpers.folderInsert(context, folderFromServer);
            } else { // folder exists
                Folder local_folder =
                        ContentProviderHelpers.folderGetById(context, folderIDFromServer);
                String local_folder_mod_id = local_folder.getModificationId();

                if (local_folder_mod_id != null
                        && !folderFromServer.getModificationId().equals(local_folder_mod_id)) { // folder on server is modified, update it

                    Log.v(TAG, Utils.getMethodName() + local_folder_mod_id + " != "
                            + folderFromServer.getModificationId() + " updating... ");

                    ContentProviderHelpers.folderUpdate(context, local_folder, folderFromServer);
                }

            }
            // synchronize documents
            documentsSyncByParentId(context, folderIDFromServer,
                    folderFromServer.getIdChain(), false);
            
            foldersSyncByParentId(context, folderIDFromServer, folderFromServer.getIdChain(),
                    false);

        }

    }

    public static Bundle documentGetListByParentId(Context context, String parentID,
                                                   String parent_id_chain, boolean isSpace) throws
            ServiceRequestException, ConnectionException, DataException {

        Bundle bundle = new Bundle();
        ArrayList<Document> docListFromServer = new ArrayList<Document>();
        ArrayList<Document> localDocList = new ArrayList<Document>();

        localDocList = ContentProviderHelpers.documentGetListByParentID(context, parentID);

        Log.v(TAG, Utils.getMethodName() + "calling getDocumentListByParentID ");
        ConnectionResult result = getDocumentListByParentId(context, parentID, isSpace);

        Log.v(TAG, Utils.getMethodName() + "parsing getDocumentListByParentID result.responseCode "
                + result.responseCode + "body " + result.body);
        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);

        if (result.responseCode != ResponseConstants.RESPONSE_CODE_OK) {
            Log.w(TAG, Utils.getMethodName() + "error responseCode:" + result.responseCode);
            bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE,
                    ResponseConstants.RESPONSE_CODE_OK);

            //return local docs
            bundle.putParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT,
                    localDocList);
            return bundle;
        }

        if (result.body.equals("[]")) {
            Log.v(TAG, Utils.getMethodName() + "no doc in this space/folder ");

            // delete local docs
            ContentProviderHelpers.documentDeleteByParentID(context, parentID);

            //return empty docs !
            bundle.putParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT,
                    docListFromServer);

            return bundle;
        }

        //waiting for response and parse it
        docListFromServer = DocumentJsonFactory.parseResult(result.body);

        if (docListFromServer == null || docListFromServer.size() < 1) {
            bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE,
                    ResponseConstants.RESPONSE_CODE_OK);

            //return local docs
            bundle.putParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT,
                    localDocList);
            return bundle;
        }

        Log.v(TAG, Utils.getMethodName() + "doc list createInstance server " + docListFromServer.toString());

        List<String> newCurrentDocIDList = new ArrayList<String>();

        for (Document doc : localDocList) {
            String docId = doc.getId();

            if (!isDocumentListContainsId(docListFromServer, docId)) { // document is NOT exist on server
                // remove it createInstance ContentProvider
                Log.v(TAG, Utils.getMethodName() + "deleting doc " + docId);

                // delete file, preview, rendition 
                documentDeleteFiles(doc);
                // delete meta data
                ContentProviderHelpers.documentDeleteById(context, docId);

            } else {
                newCurrentDocIDList.add(docId);
            }
        }

        // Log.v(TAG, Utils.getMethodName() + "we have: " + currentDocIDList.toString());

        if (docListFromServer.size() > 0) {
            // insert all documents
            for (Document tmpDocument : docListFromServer) {
                String tmpDocumentID = tmpDocument.getId();

                // make id_chain
                tmpDocument.setIdChain(parent_id_chain + ";" + tmpDocument.getId());

                if (!newCurrentDocIDList.contains(tmpDocumentID)) { // this document is not found in CP, insert it

                    Log.v(TAG, Utils.getMethodName() + "inserting doc " + tmpDocumentID);

                    ContentProviderHelpers.documentInsert(context, tmpDocument);
                } else { // document exists
                    Document local_doc =
                            ContentProviderHelpers.documentGetById(context, tmpDocumentID);
                    String local_doc_mod_id = local_doc.getModificationId();

                    if (local_doc_mod_id != null
                            && !tmpDocument.getModificationId().equals(local_doc_mod_id)) { // document on server
                        // is changed,
                        // updating it

                        Log.d(TAG,
                                Utils.getMethodName() + local_doc_mod_id + " != "
                                        + tmpDocument.getModificationId() + " replacing... ");

                        ContentProviderHelpers.documentUpdate(context, local_doc, tmpDocument);

                    }
                }

            }
        }

        bundle.putParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT,
                docListFromServer);
        return bundle;
    }

    /**
     * synchronize all documents of space/folder 
     *
     * @param context
     * @param parentId
     * @param parent_id_chain
     * @param isSpace

     * @throws com.saperion.sdb.sdk.exceptions.ServiceRequestException
     * @throws com.saperion.sdb.sdk.exceptions.ConnectionException
     * @throws com.saperion.sdb.sdk.exceptions.DataException
     */
    public static void documentsSyncByParentId(Context context, String parentId,
                                                   String parent_id_chain, boolean isSpace) throws
            ServiceRequestException, ConnectionException, DataException {

        Log.v(TAG, Utils.getMethodName() + "request doc list from server ... ");
        ConnectionResult result = getDocumentListByParentId(context, parentId, isSpace);

        Log.v(TAG, Utils.getMethodName() + "parsing getDocumentListByParentID result.responseCode "
                + result.responseCode + "body " + result.body);

        if (result.responseCode != ResponseConstants.RESPONSE_CODE_OK) {
            Log.w(TAG, Utils.getMethodName() + "we got some errors ");
            return;
        }

        if (result.body.equals("[]")) {
            Log.v(TAG, Utils.getMethodName() + "no doc in this space/folder ");

            // delete local docs
            ContentProviderHelpers.documentDeleteByParentID(context, parentId);
            // TODO delete files/rendition/previews
            return;
        }
        
        ArrayList<Document> docListFromServer = new ArrayList<Document>();
        //waiting for response and parse it
        docListFromServer = DocumentJsonFactory.parseResult(result.body);

        Log.v(TAG, Utils.getMethodName() + "doc list createInstance server " + docListFromServer.toString());

        List<String> newCurrentDocIdList = new ArrayList<String>();
        ArrayList<Document> localDocList = new ArrayList<Document>();

        localDocList = ContentProviderHelpers.documentGetListByParentID(context, parentId);
        
        for (Document localDoc : localDocList) {
            String docId = localDoc.getId();

            if (!isDocumentListContainsId(docListFromServer, docId)) { // document is NOT existed in this folder/space

                Log.v(TAG, Utils.getMethodName() + "deleting doc " + localDoc.getName());

                // delete file, preview , rendition
                documentDeleteFiles(localDoc);
                // delete meta data from db
                ContentProviderHelpers.documentDeleteById(context, docId);
                
            } else {
                newCurrentDocIdList.add(docId);
            }
        }

            // Log.v(TAG, Utils.getMethodName() + "we have: " + currentDocIDList.toString());

            // insert all documents
        for (Document docFromServer : docListFromServer) {
                String tmpDocumentID = docFromServer.getId();

                // make id_chain
                docFromServer.setIdChain(parent_id_chain + ";" + docFromServer.getId());

                if (!newCurrentDocIdList.contains(tmpDocumentID)) { // this document is not found in this space/folder, insert or update it to content provider

                    Log.v(TAG, Utils.getMethodName() + "inserting doc " + tmpDocumentID);

                    ContentProviderHelpers.documentInsert(context, docFromServer);
                } else { // document exists
                    Document local_doc =
                            ContentProviderHelpers.documentGetById(context, tmpDocumentID);
                    String local_doc_mod_id = local_doc.getModificationId();

                    if (local_doc_mod_id != null
                            && !docFromServer.getModificationId().equals(local_doc_mod_id)) { // document on server
                        // is changed,
                        // updating it

                        Log.v(TAG,
                                Utils.getMethodName() + local_doc_mod_id + " != "
                                        + docFromServer.getModificationId() + " replacing... ");

                        ContentProviderHelpers.documentUpdate(context, local_doc, docFromServer);

                    }
                }
        }
    }

    private static void documentDeleteFiles(Document localDoc) {
        String previewPath = localDoc.getPreviewAbsPath();
        if (previewPath != null ){
            File previewFiles = new File(previewPath);
            if (previewFiles.exists()) {
            try {
                previewFiles.delete();
            } catch (Exception e) {
                Log.e(TAG, Utils.getMethodName() + "error deleting  " + localDoc.getPreviewAbsPath());
            }
        }
        }

        String renditionPath = localDoc.getRenditionAbsPath();
        if (renditionPath != null ){
            
        
        File renditionFile = new File(renditionPath);
        if (renditionFile.exists()) {
            try {
                renditionFile.delete();
            } catch (Exception e) {
                Log.e(TAG, Utils.getMethodName() + "error deleting  " + localDoc.getRenditionAbsPath());
            }
        }
        }

        String filePath = localDoc.getFileAbsPath();
        if (filePath != null ){
            File docFile = new File(filePath);
            if (docFile.exists()) {
                try {
                docFile.delete();
                } catch (Exception e) {
                Log.e(TAG, Utils.getMethodName() + "error deleting  " + localDoc.getFileAbsPath());
            }
        }
    }
    }
    /**
     * refresh  children of 1.subfolder
     */
    public static void foldersUpdateByParentId(Context context, String parentId,
                                               String parent_id_chain, ArrayList<Folder> folderListFromServer) {
        ArrayList<Folder> localFolderList =
                ContentProviderHelpers.folderGetListByParentId(context, parentId);

        List<String> currentFolderIDList = new ArrayList<String>();

        // delete local folder which not exists on server and create ID list of existing folders in this parent
        for (Folder folder_tmp : localFolderList) {
            String folder_id = folder_tmp.getId();

            if (!isFolderListContainsID(folderListFromServer, folder_id)) { // folder is NOT exist on server 
                // remove it createInstance ContentProvider

                // delete itself and his children
                Log.v(TAG, Utils.getMethodName() + "deleting  folder " + folder_tmp.getName());
                ContentProviderHelpers.folderDeleteById(context, folder_id);

                // TODO delete preview , file, thumbnail too !
            } else {
                // Log.v(TAG, Utils.getMethodName() + space_id);    
                currentFolderIDList.add(folder_id);
            }
        }

        // insert folders or updates
        for (Folder folderFromServer : folderListFromServer) {
            String folderIDFromServer = folderFromServer.getId();

            // search for existed folder

            if (!currentFolderIDList.contains(folderIDFromServer)) { // this folder is not found, insert it

                Log.v(TAG, Utils.getMethodName() + "inserting folder " + folderFromServer.getName());
                // make id_chain
                folderFromServer.setIdChain(parent_id_chain + ";" + folderIDFromServer);

                ContentProviderHelpers.folderInsert(context, folderFromServer);
                // TODO was insert ok ?

            } else { // folder exists

                Folder local_folder =
                        ContentProviderHelpers.folderGetById(context, folderIDFromServer);

                if (local_folder != null
                        && !folderFromServer.getModificationId().equals(
                        local_folder.getModificationId())) {
                    // folder on server is modified, update it

                    Log.v(TAG, Utils.getMethodName() + " updating... " + folderFromServer.getName());

                    ContentProviderHelpers.folderUpdate(context, local_folder, folderFromServer);
                    // TODO was update ok ?

                }

            }

        }

    }

    /**
     * refresh contents children of 1.layer
     *
     * @param context
     * @param parentId
     * @param parent_id_chain
     * @param isSpace
     * @throws com.saperion.sdb.sdk.exceptions.SdbRemoteException
     * @throws android.os.RemoteException
     * @throws android.content.OperationApplicationException
     * @throws com.saperion.sdb.sdk.exceptions.ConnectionException
     * @throws com.saperion.sdb.sdk.exceptions.DataException
     */
    public static Bundle folderGetListAndSyncByParentId(Context context, String parentId,
                                                        String parent_id_chain, boolean isSpace) throws
            ServiceRequestException, ConnectionException, DataException {

        ArrayList<Folder> folderListFromServer = new ArrayList<Folder>();
        ArrayList<Folder> localSubFolderList = new ArrayList<Folder>();

        localSubFolderList = ContentProviderHelpers.folderGetListByParentId(context, parentId);

        Bundle bundle = new Bundle();

        //get folder list createInstance server of current parent 
        ConnectionResult result = folderGetSubfoldersByParentId(context, parentId, isSpace);

        if (result.responseCode != ResponseConstants.RESPONSE_CODE_OK) {
            Log.w(TAG, Utils.getMethodName() + " got error code createInstance network:"
                    + result.responseCode);

            // so we can only deliver local folder

            localSubFolderList = ContentProviderHelpers.folderGetListByParentId(context, parentId);

            bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE,
                    ResponseConstants.RESPONSE_CODE_OK);
            bundle.putParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT,
                    localSubFolderList);

            return bundle;
        }

        if (result.body.equals("[]")) {
            // empty local folder
            ContentProviderHelpers.folderDeleteByIdChain(context, parent_id_chain);

            bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE,
                    ResponseConstants.RESPONSE_CODE_OK);
            // return empty list
            bundle.putParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT,
                    folderListFromServer);
            return bundle;
        }

        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);
        folderListFromServer = FolderJsonFactory.parseResult(result.body);

        if (folderListFromServer == null || folderListFromServer.size() < 1) {

            bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE,
                    ResponseConstants.RESPONSE_CODE_ERROR);
            return bundle;
        }

        List<String> currentFolderIDList = new ArrayList<String>();

        // delete local folder which not exists on server and create ID list of existing folders in this parent
        for (Folder folder_tmp : localSubFolderList) {
            String folder_id = folder_tmp.getId();

            if (!isFolderListContainsID(folderListFromServer, folder_id)) { // folder is NOT exist on server 
                // remove it createInstance ContentProvider

                // delete itself and his children
                Log.v(TAG, Utils.getMethodName() + "deleting  folder " + folder_tmp.getName());
                ContentProviderHelpers.folderDeleteById(context, folder_id);

                // TODO delete preview , file, thumbnail too !
            } else {
                // Log.v(TAG, Utils.getMethodName() + space_id);    
                currentFolderIDList.add(folder_id);
            }
        }

        // insert folders or updates
        for (Folder folderFromServer : folderListFromServer) {
            String folderIDFromServer = folderFromServer.getId();
            // make id_chain
            folderFromServer.setIdChain(parent_id_chain + ";" + folderIDFromServer);
            // search for existed folder

            if (!currentFolderIDList.contains(folderIDFromServer)) { // this folder is not found, insert it

                Log.v(TAG, Utils.getMethodName() + "inserting folder " + folderFromServer.getName());

                ContentProviderHelpers.folderInsert(context, folderFromServer);
                // TODO was insert ok ?

            } else { // folder exists

                Folder local_folder =
                        ContentProviderHelpers.folderGetById(context, folderIDFromServer);

                if (local_folder != null
                        && !folderFromServer.getModificationId().equals(
                        local_folder.getModificationId())) {
                    // folder on server is modified, update it

                    Log.v(TAG, Utils.getMethodName() + " updating... " + folderFromServer.getName());

                    ContentProviderHelpers.folderUpdate(context, local_folder, folderFromServer);
                    // TODO was update ok ?

                }

            }

        }

        bundle.putParcelableArrayList(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT,
                folderListFromServer);
        return bundle;

    }

    public static void documentsUpdateByParentId(Context context, String parentID,
                                                 String parent_id_chain, ArrayList<Document> docListFromServer)
            throws ConnectionException, DataException {

        ArrayList<Document> localDocList =
                ContentProviderHelpers.documentGetListByParentID(context, parentID);

        Log.v(TAG, Utils.getMethodName() + "doc list createInstance server " + docListFromServer.toString());

        List<String> newCurrentDocIDList = new ArrayList<String>();

        for (Document doc : localDocList) {
            String docId = doc.getId();

            if (!isDocumentListContainsId(docListFromServer, docId)) { // document  NOT exists on server
                // remove it createInstance ContentProvider
                Log.v(TAG, Utils.getMethodName() + "deleting doc " + docId);

                // TODO delete ok ?
                ContentProviderHelpers.documentDeleteById(context, docId);

                // TODO delete file, preview, thumbnails ,...

                File file = new File(doc.getPreviewAbsPath());
                try {
                    file.delete();
                } catch (Exception e) {
                    Log.e(TAG, Utils.getMethodName() + "error deleting preview  " + doc.getName());
                }
            } else {
                newCurrentDocIDList.add(docId);
            }
        }

        for (Document tmpDocument : docListFromServer) {
            String tmpDocumentID = tmpDocument.getId();

            if (!newCurrentDocIDList.contains(tmpDocumentID)) { // this document is not found in our Content Provider, so insert it
                String doc_abs_path = documentGetFilePathWithoutExtension(context, tmpDocumentID);
                // add our own attributes
                tmpDocument.setIdChain(parent_id_chain + ";" + tmpDocument.getId());
                //tmpDocument.setPreviewAbsPath("");
                tmpDocument.setFileAbsPath(doc_abs_path);

                Log.v(TAG, Utils.getMethodName() + "inserting doc " + tmpDocument.getName());

                Uri doc_uri = ContentProviderHelpers.documentInsert(context, tmpDocument);
                /*
                // and get preview for this document
				ConnectionResult result = documentGetPreview(context, WSConfig.documentCreatePreviewUrl(tmpDocumentID),
				        OperationHelpers.documentCreatePreviewPathWithoutExtension(context, tmpDocumentID));

				if (result.responseCode == ResponseConstants.RESPONSE_CODE_OK)
				{
				    tmpDocument.setPreviewAbsPath(result.body);
				    ContentProviderHelpers.documentUpdate(context,tmpDocument);
				}
				*/

            } else { // document exists
                Document local_doc = ContentProviderHelpers.documentGetById(context, tmpDocumentID);

                String localDocModificationID = local_doc.getModificationId();

                if (localDocModificationID != null
                        && !tmpDocument.getModificationId().equals(localDocModificationID)) { // document on server  is changed, updating it
                    // TODO checking modification is not clean !
					/*
					// file of document is changed, so we need to get preview for this document
					ConnectionResult result = documentGetPreview(context, WSConfig.documentCreatePreviewUrl(tmpDocumentID),
					        OperationHelpers.documentCreatePreviewPathWithoutExtension(context, tmpDocumentID));

					if (result.responseCode == ResponseConstants.RESPONSE_CODE_OK)
					{

					    tmpDocument.setPreviewAbsPath(result.body);
					}
					*/

                    ContentProviderHelpers.documentUpdate(context, local_doc, tmpDocument);
                }

            }

        }

    }

    public static ConnectionResult documentGetPreview(Context context, String url,
                                                      String abs_preview_path) throws ServiceRequestException, ConnectionException, DataException {

        NetworkConnection networkConnection = new NetworkConnection(context, url);

        HashMap<String, String> accept_header = new HashMap<String, String>();
        accept_header.put("ACCEPT", "application/octet-stream");
        networkConnection.setHeaderList(accept_header);

        HashMap<String, String> requestParams = new HashMap<String, String>();
        requestParams.put(NetworkConnection.REQUEST_TYPE,
                NetworkConnection.REEQUEST_TYPE_DOWNLOAD);
        requestParams.put(NetworkConnection.FILE_PATH, abs_preview_path);
        networkConnection.setRequestParams(requestParams);

        networkConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager
                .getInstance().getUserName(), CredentialManager.getInstance().getmAccessToken()));

        return networkConnection.execute();

    }

    public static Bundle itemUpdate(Context context, String itemId, String payload, String itemType)
            throws ServiceRequestException, ConnectionException {

        Log.v(TAG, Utils.getMethodName() + " itemId: " + itemId + " payload: " + payload);

        String url = buildRequestUrl(itemType, itemId);

        NetworkConnection networkConnection = new NetworkConnection(context, url);
        networkConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager
                .getInstance().getUserName(), CredentialManager.getInstance().getmAccessToken()));

        networkConnection.setMethod(NetworkConnection.Method.PUT);

        networkConnection.setPayload(payload);

        ConnectionResult result = null;
        Bundle bundle = new Bundle();
        try {
            result = networkConnection.execute();

        } catch (ConnectionException e) {
            e.printStackTrace();
        }

        if (result == null) {

            Log.e(TAG, Utils.getMethodName() + " result == null ");

            return bundle;
        }

        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);

        if (result.responseCode != ResponseConstants.RESPONSE_CODE_OK) {
            Log.w(TAG, Utils.getMethodName() + " responseCode:" + result.responseCode);
            bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);
            bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
            return bundle;
        }

        // write to local database
        if (itemType.equals(Constants.SPACE_TYPE)) {
            Space tmpSpace = SpaceJsonFactory.jsonToSpace(result.body);

            Log.v(TAG, Utils.getMethodName() + " updating space ");

            bundle.putParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, tmpSpace);

            ContentProviderHelpers.spaceUpdate(context, tmpSpace);

        } else if (itemType.equals(Constants.FOLDER_TYPE)) {
            Folder folderFromServer = FolderJsonFactory.jsonToFolder(result.body);

            Folder localFolder = ContentProviderHelpers.folderGetById(context, itemId);
            Log.v(TAG, Utils.getMethodName() + " updating local folder " + localFolder.getName());
            ContentProviderHelpers.folderUpdate(context, localFolder, folderFromServer);
            bundle.putParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, folderFromServer);
        }
        // document
        else if (itemType.equals(Constants.DOCUMENT_TYPE)) {
            Log.v(TAG, Utils.getMethodName() + " updating document done");
            Document tmpDocument = DocumentJsonFactory.jsonToDocument(result.body);

            Document local_doc = ContentProviderHelpers.documentGetById(context, itemId);

            ContentProviderHelpers.documentUpdate(context, local_doc, tmpDocument);
            bundle.putParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, tmpDocument);
        }

        return bundle;
    }

    public static Bundle itemCreate(Context context, String payload, String itemType)
            throws ServiceRequestException, ConnectionException, DataException {

        String url = buildRequestUrl(itemType, null);

        NetworkConnection networkConnection = new NetworkConnection(context, url);
        networkConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager
                .getInstance().getUserName(), CredentialManager.getInstance().getmAccessToken()));

        networkConnection.setMethod(NetworkConnection.Method.POST);

        networkConnection.setPayload(payload);

        ConnectionResult result = null;
        Bundle bundle = new Bundle();

        result = networkConnection.execute();

        if (result == null) {
            return bundle;
        }
        bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);

        /*
        try {
			result = networkConnection.execute();
			if (result == null) {
				return bundle;
			}
			bundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);

		} catch (ConnectionException e) {
			Log.e("TAG" , Utils.getMethodName() +  e.getMessage());

            bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE,e.getMessage());
		}
        */

        if (result.responseCode != ResponseConstants.RESPONSE_CODE_CREATED) {
            bundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
            return bundle;
        }
        // write to content provider
        if (itemType.equals(Constants.SPACE_TYPE)) {
            Space tmpSpace = SpaceJsonFactory.jsonToSpace(result.body);

            ContentProviderHelpers.spaceInsert(context, tmpSpace);

            bundle.putParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, tmpSpace);
        } else if (itemType.equals(Constants.FOLDER_TYPE)) {
            
            Folder tmpFolder = FolderJsonFactory.jsonToFolder(result.body);
            tmpFolder.setIdChain(ContentProviderHelpers.getIdChainByItemId(context, tmpFolder.getParentId()) 
                    + ";" + tmpFolder.getId());
            
            Uri uri = ContentProviderHelpers.folderInsert(context, tmpFolder);
            if (uri == null)
            {
                Log.e(TAG, Utils.getMethodName() + " Uri == null " );
                throw new DataException();
            }
            bundle.putParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, tmpFolder);
            
        } else if (itemType.equals(Constants.DOCUMENT_TYPE)) {
            Document tmpDocument = DocumentJsonFactory.jsonToDocument(result.body);
            tmpDocument.setIdChain(ContentProviderHelpers.getIdChainByItemId(context, tmpDocument.getParentId())  
                    + ";" + tmpDocument.getId());
            bundle.putParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, tmpDocument);

            //ContentProviderHelpers.documentInsert(context, tmpDocument);
        }

        return bundle;
    }

    public static Bundle itemDelete(Context context, String itemId, String itemType)
            throws ServiceRequestException, ConnectionException {
        Bundle resultBundle = new Bundle();

        String url = buildRequestUrl(itemType, itemId);

        Log.v(TAG, Utils.getMethodName() + " URL: " + url);

        NetworkConnection networkConnection = new NetworkConnection(context, url);
        networkConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager
                .getInstance().getUserName(), CredentialManager.getInstance().getmAccessToken()));

        networkConnection.setMethod(NetworkConnection.Method.DELETE);
        ConnectionResult result = null;

        result = networkConnection.execute();

        resultBundle.putInt(ResponseConstants.BUNDLE_EXTRA_RESULT_CODE, result.responseCode);

        Log.v(TAG, Utils.getMethodName() + " responseCode: " + result.responseCode);

        if (result.responseCode != ResponseConstants.RESPONSE_CODE_OK) {

            resultBundle.putString(ResponseConstants.BUNDLE_EXTRA_ERROR_MESSAGE, result.body);
            return resultBundle;
        }


        if (itemType.equals(Constants.SPACE_TYPE)) { // remove space and children createInstance database

            Space space = ContentProviderHelpers.spaceGetById(context, itemId);
            resultBundle.putParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, space);
            ContentProviderHelpers.spaceDeleteById(context, itemId);

        } else if (itemType.equals(Constants.FOLDER_TYPE)) {
            Folder folder = ContentProviderHelpers.folderGetById(context, itemId);
            resultBundle.putParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, folder);
            ContentProviderHelpers.folderDeleteById(context, itemId);
        }
        // document
        else if (itemType.equals(Constants.DOCUMENT_TYPE)) {
            Document document = ContentProviderHelpers.documentGetById(context, itemId);
            resultBundle.putParcelable(ResponseConstants.BUNDLE_EXTRA_RESULT_OBJECT, document);

            // remove it locally
            ContentProviderHelpers.documentDeleteById(context, itemId);

            // TODO DELETE createInstance file system
        }

        return resultBundle;

    }

    private static String buildRequestUrl(String type, String id) {

        String url = null;

        if (id == null) {
            if (type.equals(ModelType.SPACE.toString().toLowerCase())) {
                url = WSConfig.getSpaceURL();
            } else if (type.equals(ModelType.FOLDER.toString().toLowerCase())) {
                url = WSConfig.getFolderURL();
            } else if (type.equals(ModelType.DOCUMENT.toString().toLowerCase())) {
                url = WSConfig.getDocumentURL();
            } else if (type.equals(ModelType.USER.toString().toLowerCase())) {
                url = WSConfig.getUserUrl();
            }

            return url;
        }

        if (type.equals(ModelType.SPACE.toString().toLowerCase())) {
            url = WSConfig.getSpaceURL() + "/" + id;
        } else if (type.equals(ModelType.FOLDER.toString().toLowerCase())) {
            url = WSConfig.getFolderURL() + "/" + id;
        } else if (type.equals(ModelType.DOCUMENT.toString().toLowerCase())) {
            url = WSConfig.getDocumentURL() + "/" + id;
        } else if (type.equals(ModelType.USER.toString().toLowerCase())) {
            url = WSConfig.getUserUrl() + "/" + id;
        }

        return url;

    }

    public static String userCreateAvatarPathWithExtension(Context context, String Id)
            throws DataException {
        // Path without extension
        String filePath;

        try {
            filePath = context.getFilesDir().getPath();
        } catch (NullPointerException e) {
            throw new DataException();
        }

        return filePath + "/" + WSConfig.WS_AVATAR + "/" + Id
                + "." + Constants.AVATAR_EXTENSION;
    }
    

    public static String documentCreatePreviewPathWithExtension(Context context, String docId)
            throws DataException {
        // Path without extension
        String filePath;

        try {
            filePath = context.getFilesDir().getPath();
        } catch (NullPointerException e) {
            throw new DataException();
        }

        return filePath + "/" + WSConfig.WS_PREVIEW + "/" + docId
                + "." + Constants.PREVIEW_EXTENSION;
    }

    public static String documentGetFilePathWithoutExtension(Context context, String file_name)
            throws DataException {
        String filePath;
        try {
            filePath = context.getFilesDir().getPath();
        } catch (NullPointerException e) {
            throw new DataException();
        }

        // Path without extension
        return filePath + "/" + WSConfig.WS_DOCUMENT + "/" + file_name;
    }



    public static ConnectionResult userGetList(Context context) throws ConnectionException,
            DataException, ServiceRequestException {

        NetworkConnection networkConnection =
                new NetworkConnection(context, WSConfig.getUserUrl() + "?include=recycled,modificationId");
        networkConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager
                .getInstance().getUserName(), CredentialManager.getInstance().getmAccessToken()));
        
        String if_none_match_param = SdbApplication.getInstance().getUserListEtag();
        
        if ((if_none_match_param !=null) && (if_none_match_param.length() > 0))            
            networkConnection.setIfNoneMatch(if_none_match_param);
        
        return networkConnection.execute();

    }


    public static ConnectionResult userGetCurrent(Context context) throws ConnectionException,
            DataException, ServiceRequestException {

        NetworkConnection networkConnection =
                new NetworkConnection(context, WSConfig.getUserUrl() + "/current");
        networkConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager
                .getInstance().getUserName(), CredentialManager.getInstance().getmAccessToken()));

        return networkConnection.execute();

    }

    public static ConnectionResult userCreate(Context context, String payload)  throws ConnectionException,
            DataException, ServiceRequestException {

        String url = WSConfig.getUserUrl();
        
        NetworkConnection networkConnection =
                new NetworkConnection(context, url);
        networkConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager
                .getInstance().getUserName(), CredentialManager.getInstance().getmAccessToken()));

        Log.v(TAG, Utils.getMethodName() + "payload: " + payload);

        networkConnection.setPayload(payload);
       networkConnection.setMethod(Method.POST);
  

        return networkConnection.execute();

    }
    

    public static ConnectionResult userUpdate(Context context,String Id, String payload) throws ConnectionException,
            DataException, ServiceRequestException {

        String url = WSConfig.getUserUrl() + "/" + Id;
        
        NetworkConnection networkConnection =   
                new NetworkConnection(context, url);
        networkConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager
                .getInstance().getUserName(), CredentialManager.getInstance().getmAccessToken()));

        networkConnection.setPayload(payload);

        networkConnection.setMethod(Method.PUT);

        return networkConnection.execute();

    }

    public static ConnectionResult userDelete(Context context,String Id) throws ConnectionException,
            DataException, ServiceRequestException {

        String url = WSConfig.getUserUrl() + "/" + Id;

        NetworkConnection networkConnection =
                new NetworkConnection(context, url);
        networkConnection.setCredentials(new UsernamePasswordCredentials(CredentialManager
                .getInstance().getUserName(), CredentialManager.getInstance().getmAccessToken()));

        networkConnection.setMethod(Method.DELETE);

        return networkConnection.execute();

    }

    

/*
    public static int usersUpdateContentProvider(Context context, List<User> userListFromServer) {
        int changed_count = 0;

        if (userListFromServer == null || userListFromServer.size() < 1)
            return changed_count;

        // get local existed space
        List<User> localUsers = new ArrayList<User>();

        localUsers = ContentProviderHelpers.userGetList(context);

        ArrayList<String> localUserIdList = new ArrayList<String>();

        for (User user : localUsers) {
            String id = user.getId();

            if (!isUserListContainsId(userListFromServer, id)) { // space is no more exist on server 

                // remove this space and his children createInstance ContentProvider local      				
                ContentProviderHelpers.userDeleteById(context, id);
            } else {
                localUserIdList.add(id);
            }
        }

        for (User userFromServer : userListFromServer) {
            String serverUserId = userFromServer.getId();

            // search for existed user

            if (!localUserIdList.contains(serverUserId)) { // our local space is not found, insert it
                Log.v(TAG, Utils.getMethodName() + "inserting new space");

                changed_count++;

                ContentProviderHelpers.userInsert(context, userFromServer);
            } else { // user exists

                User tmpUser = ContentProviderHelpers.spaceGetById(context, serverUserId);

                String currSpaceModificationID = tmpSpace.getModificationId();

                if (currSpaceModificationID != null || !TextUtils.isEmpty(currSpaceModificationID)) {
                    if (!userFromServer.getModificationId().equals(currSpaceModificationID)) { // space on server is changed, have to sync, if SYNC APP is set

                        changed_count++;

                        Log.v(TAG, Utils.getMethodName() + "space on server is modified "
                                + userFromServer.getName());
                        // update isModified =  1 

                        ContentProviderHelpers.spaceUpdate(context, userFromServer);

                    }

                }

            }

        }

        return changed_count;
    }
*/
    public static boolean isUserListContainsId(List<User> userList, String Id) {
        for (User user : userList) {
            if (user.getId().equals(Id))
                return true;
        }
        return false;
    }


}
