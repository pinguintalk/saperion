package com.saperion.sdb.sdk.providers;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

import com.saperion.sdb.sdk.providers.util.ColumnMetadata;

/**
 * This class was generated by the ContentProviderCodeGenerator project made by Foxykeep
 * <p/>
 * (More information available https://github.com/foxykeep/ContentProviderCodeGenerator)
 */
public abstract class DocumentContent {

    public static final Uri CONTENT_URI = Uri.parse("content://" + SdbContentProvider.AUTHORITY);

    private DocumentContent() {
    }

    /**
     * Created in version 1
     */
    public static final class DocumentDB extends DocumentContent {

        private static final String LOG_TAG = DocumentDB.class.getSimpleName();

        public static final String TABLE_NAME = "document";
        public static final String TABLE_DOC_FILES = "document_files";
        public static final String TYPE_ELEM_TYPE = "vnd.android.cursor.item/document-document";
        public static final String TYPE_DIR_TYPE = "vnd.android.cursor.dir/document";

        public static final Uri CONTENT_URI = Uri.parse(DocumentContent.CONTENT_URI + "/"
                + TABLE_NAME);

        public static enum Columns implements ColumnMetadata {
            _ID(BaseColumns._ID, "integer"),
            ID("id", "TEXT UNIQUE"),
            NAME("name", "text"),
            MODIFICATION_ID("modificationId", "text"),
            LAST_MODIFIED("lastModified", "text"),
            TYPE("type", "text"),
            DESCRIPTION("description", "text"),
            CREATION_DATE("creationDate", "text"),
            STATES("states", "text"),
            TAGS("tags", "text"),
            PARENT_ID("parentId", "text"),
            SPACE_ID("spaceId", "text"),
            ID_CHAIN("idChain", "text"),
            OWNER_ID("ownerId", "text"),
            OWNER_NAME("ownerName", "text"),
            MIME_TYPE("mimeType", "text"),
            HASH("hash", "text"),
            SIZE("size", "integer"),
            PAGES("pages", "integer"),
            PREVIEW_PATH("previewUri", "text"),
            RENDITION_PATH("renditionAbsPath", "text"),
            FILE_PATH("fileAbsPath", "text"),
            VERSION_NUMBER("versionNumber", "integer"),
            BINARY_DATA("_data", "text"),
            DISPLAY_NAME("_display_name", "text"); // display name of document for content uri


            private final String mName;
            private final String mType;

            private Columns(String name, String type) {
                mName = name;
                mType = type;
            }

            @Override
            public int getIndex() {
                return ordinal();
            }

            @Override
            public String getName() {
                return mName;
            }

            @Override
            public String getType() {
                return mType;
            }
        }

        public static final String[] PROJECTION = new String[]{Columns._ID.getName(),
                Columns.ID.getName(), Columns.NAME.getName(), Columns.TYPE.getName(),
                Columns.LAST_MODIFIED.getName(), Columns.DESCRIPTION.getName(),
                Columns.CREATION_DATE.getName(), Columns.MODIFICATION_ID.getName(),
                Columns.STATES.getName(), Columns.TAGS.getName(), Columns.PARENT_ID.getName(),
                Columns.SPACE_ID.getName(), Columns.ID_CHAIN.getName(), Columns.OWNER_ID.getName(),
                Columns.OWNER_NAME.getName(), Columns.MIME_TYPE.getName(), Columns.HASH.getName(),
                Columns.SIZE.getName(), Columns.PAGES.getName(), Columns.PREVIEW_PATH.getName(),
                Columns.RENDITION_PATH.getName(), Columns.FILE_PATH.getName(),
                Columns.BINARY_DATA.getName(), Columns.DISPLAY_NAME.getName(), Columns.VERSION_NUMBER.getName()

        };

        private DocumentDB() {
            // No private constructor
        }

        public static void createTable(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + TABLE_NAME + " ("
                    + Columns._ID.getName() + " " + Columns._ID.getType() + ", "
                    + Columns.ID.getName() + " " + Columns.ID.getType() + ", "
                    + Columns.NAME.getName() + " " + Columns.NAME.getType() + ", "
                    + Columns.MODIFICATION_ID.getName() + " " + Columns.MODIFICATION_ID.getType()
                    + ", " + Columns.LAST_MODIFIED.getName() + " " + Columns.LAST_MODIFIED.getType()
                    + ", " + Columns.TYPE.getName() + " " + Columns.TYPE.getType() + ", "
                    + Columns.DESCRIPTION.getName() + " " + Columns.DESCRIPTION.getType() + ", "
                    + Columns.CREATION_DATE.getName() + " " + Columns.CREATION_DATE.getType() + ", "
                    + Columns.STATES.getName() + " "
                    + Columns.STATES.getType() + ", " + Columns.TAGS.getName() + " "
                    + Columns.TAGS.getType() + ", " + Columns.PARENT_ID.getName() + " "
                    + Columns.PARENT_ID.getType() + ", " + Columns.SPACE_ID.getName() + " "
                    + Columns.SPACE_ID.getType() + ", " + Columns.ID_CHAIN.getName() + " "
                    + Columns.ID_CHAIN.getType() + ", " + Columns.OWNER_ID.getName() + " "
                    + Columns.OWNER_ID.getType() + ", " + Columns.OWNER_NAME.getName() + " "
                    + Columns.OWNER_NAME.getType() + ", " + Columns.MIME_TYPE.getName() + " "
                    + Columns.MIME_TYPE.getType() + ", " 
                    + Columns.HASH.getName() + " " + Columns.HASH.getType() + ", " 
                    + Columns.SIZE.getName() + " " + Columns.SIZE.getType() + ", " 
                    + Columns.PAGES.getName() + " " + Columns.PAGES.getType() + ", "
                    + Columns.RENDITION_PATH.getName() + " " + Columns.RENDITION_PATH.getType() + ", "
                    + Columns.PREVIEW_PATH.getName() + " " + Columns.PREVIEW_PATH.getType() + ", "
                    + Columns.FILE_PATH.getName() + " " + Columns.FILE_PATH.getType() + ", "
                    + Columns.VERSION_NUMBER.getName() + " "+ Columns.VERSION_NUMBER.getType() + ", "
                    + Columns.BINARY_DATA.getName() + " " + Columns.BINARY_DATA.getType() + ", "
                    + Columns.DISPLAY_NAME.getName() + ", " + Columns.DISPLAY_NAME.getType() 
                    + ", PRIMARY KEY (" + Columns._ID.getName()
                    + ")" + ");");
/*
            db.execSQL("CREATE INDEX document_id on " + TABLE_NAME + "(" + Columns.ID.getName()
                    + ");");
            db.execSQL("CREATE INDEX document_name on " + TABLE_NAME + "(" + Columns.NAME.getName()
                    + ");");
            db.execSQL("CREATE INDEX document_type on " + TABLE_NAME + "(" + Columns.TYPE.getName()
                    + ");");
            db.execSQL("CREATE INDEX document_lastModified on " + TABLE_NAME + "("
                    + Columns.LAST_MODIFIED.getName() + ");");
            db.execSQL("CREATE INDEX document_description on " + TABLE_NAME + "("
                    + Columns.DESCRIPTION.getName() + ");");
            db.execSQL("CREATE INDEX document_creationDate on " + TABLE_NAME + "("
                    + Columns.CREATION_DATE.getName() + ");");
            db.execSQL("CREATE INDEX document_states on " + TABLE_NAME + "("
                    + Columns.STATES.getName() + ");");
            db.execSQL("CREATE INDEX document_tags on " + TABLE_NAME + "(" + Columns.TAGS.getName()
                    + ");");
            db.execSQL("CREATE INDEX document_parentId on " + TABLE_NAME + "("
                    + Columns.PARENT_ID.getName() + ");");
            db.execSQL("CREATE INDEX document_spaceId on " + TABLE_NAME + "("
                    + Columns.SPACE_ID.getName() + ");");
            db.execSQL("CREATE INDEX document_idChain on " + TABLE_NAME + "("
                    + Columns.ID_CHAIN.getName() + ");");
            db.execSQL("CREATE INDEX document_ownerId on " + TABLE_NAME + "("
                    + Columns.OWNER_ID.getName() + ");");
            db.execSQL("CREATE INDEX document_ownerName on " + TABLE_NAME + "("
                    + Columns.OWNER_NAME.getName() + ");");
            db.execSQL("CREATE INDEX document_mimeType on " + TABLE_NAME + "("
                    + Columns.MIME_TYPE.getName() + ");");
            db.execSQL("CREATE INDEX document_hash on " + TABLE_NAME + "(" + Columns.HASH.getName()
                    + ");");
*/
            // create TABLE_DOC_FILES

            //  db.execSQL("CREATE TABLE " + TABLE_DOC_FILES + " ( _id INTEGER PRIMARY KEY AUTOINCREMENT, _data TEXT);");
        }

        // Version 1 : Creation of the table
        public static void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {

            if (oldVersion < 1) {
                Log.v(LOG_TAG, "Upgrading createInstance version " + oldVersion + " to " + newVersion
                        + ", data will be lost!");

                db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME + ";");
                createTable(db);
                return;
            }

            if (oldVersion != newVersion) {
                throw new IllegalStateException("Error upgrading the database to version "
                        + newVersion);
            }
        }

        static String getBulkInsertString() {
            return new StringBuilder("INSERT INTO ")
                    .append(TABLE_NAME)
                    .append(" ( ")
                    .append(Columns._ID.getName())
                    .append(", ")
                    .append(Columns.ID.getName())
                    .append(", ")
                    .append(Columns.NAME.getName())
                    .append(", ")
                    .append(Columns.MODIFICATION_ID.getName())
                    .append(", ")
                    .append(Columns.TYPE.getName())
                    .append(", ")
                    .append(Columns.LAST_MODIFIED.getName())
                    .append(", ")
                    .append(Columns.DESCRIPTION.getName())
                    .append(", ")
                    .append(Columns.CREATION_DATE.getName())
                    .append(", ")
                    .append(Columns.STATES.getName())
                    .append(", ")
                    .append(Columns.TAGS.getName())
                    .append(", ")
                    .append(Columns.PARENT_ID.getName())
                    .append(", ")
                    .append(Columns.SPACE_ID.getName())
                    .append(", ")
                    .append(Columns.ID_CHAIN.getName())
                    .append(", ")
                    .append(Columns.OWNER_ID.getName())
                    .append(", ")
                    .append(Columns.OWNER_NAME.getName())
                    .append(", ")
                    .append(Columns.MIME_TYPE.getName())
                    .append(", ")
                    .append(Columns.HASH.getName())
                    .append(", ")
                    .append(Columns.SIZE.getName())
                    .append(", ")
                    .append(Columns.PAGES.getName())
                    .append(", ")
                    .append(Columns.RENDITION_PATH.getName())
                    .append(", ")
                    .append(Columns.PREVIEW_PATH.getName())
                    .append(", ")
                    .append(Columns.FILE_PATH.getName())
                    .append(", ")
                    .append(Columns.VERSION_NUMBER.getName())
                    .append(", ")
                    .append(Columns.BINARY_DATA.getName())
                    .append(", ")
                    .append(Columns.DISPLAY_NAME.getName())
                    .append(" ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
                    .toString();
        }

        static void bindValuesInBulkInsert(SQLiteStatement stmt, ContentValues values) {
            int i = 1;
            String value;
            stmt.bindLong(i++, values.getAsLong(Columns._ID.getName()));
            value = values.getAsString(Columns.ID.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.NAME.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.LAST_MODIFIED.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.DESCRIPTION.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.MODIFICATION_ID.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.CREATION_DATE.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.STATES.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.TAGS.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.PARENT_ID.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.SPACE_ID.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.ID_CHAIN.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.OWNER_ID.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.OWNER_NAME.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.TYPE.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.MIME_TYPE.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.HASH.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.RENDITION_PATH.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.PREVIEW_PATH.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.FILE_PATH.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.BINARY_DATA.getName());
            stmt.bindString(i++, value != null ? value : "");
            value = values.getAsString(Columns.DISPLAY_NAME.getName());
            stmt.bindString(i++, value != null ? value : "");
            stmt.bindLong(i++, values.getAsLong(Columns.SIZE.getName()));
            stmt.bindLong(i++, values.getAsLong(Columns.PAGES.getName()));
            stmt.bindLong(i++, values.getAsLong(Columns.VERSION_NUMBER.getName()));

            // byte[] thumbvalue = values.getAsByte(Columns.BINARY_DATA.getName());
            // stmt.bindBlob(i++, thumbvalue );
        }
    }
}
