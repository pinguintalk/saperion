package com.saperion.sdb.sdk.operations;

import android.content.Context;
import android.os.Bundle;

import com.saperion.sdb.sdk.Constants;
import com.saperion.sdb.sdk.exceptions.ConnectionException;
import com.saperion.sdb.sdk.exceptions.DataException;
import com.saperion.sdb.sdk.exceptions.ServiceRequestException;
import com.saperion.sdb.sdk.models.Folder;
import com.saperion.sdb.sdk.providers.ContentProviderHelpers;
import com.saperion.sdb.sdk.requestmanager.Request;
import com.saperion.sdb.sdk.service.RequestService.Operation;
import com.saperion.sdb.sdk.utils.Log;
import com.saperion.sdb.sdk.utils.Utils;

public final class FolderGetSubfoldersOperation implements Operation {

    private static final String TAG = FolderGetSubfoldersOperation.class.getSimpleName();

    public static final String PARAM_FOLDER  = Constants.SDK_PACKAGE_NAME + ".param_folder";

    @Override
    public Bundle execute(Context context, Request request) throws ServiceRequestException, ConnectionException,
            DataException {

        Log.v(TAG, Utils.getMethodName() + "entry");

        Folder folder = (Folder) request.getParcelable(PARAM_FOLDER);
        String id = folder.getId();
        String id_chain = folder.getIdChain();
        if (id_chain == null)
            id_chain = ContentProviderHelpers.folderGetIdChainById(context, id);


        return OperationHelpers.folderGetListAndSyncByParentId(context, id,
                id_chain, false);

    }

}
